<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<html>
<%
session("cur_page")="IR News Clipping"
session("page_asp")="news_clipping2007"

if session("lang")="" or request("lang")="T"  then  
	session("lang")="T"
elseif request("lang")="E" then
	session("lang")="E"
end if


pagesize=15

%>
 <!--#include file="constpage2007.asp"-->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=windows-874">
<title>Investor Relations</title>
<LINK href="style2007.css" type="text/css" rel="stylesheet">	
<!--#include file = "i_constant.asp" -->
<!--#include file = "function_asp2007.asp" -->
<!--#include file = "listed_member2007.asp" -->
<!--#include file = "i_conreference.asp" -->
<!--#include file = "i_conirauthen.asp" -->
<meta name="keywords" content="<%=message_keyword%>">
<script language="javascript" src="/ir/function2007.js"></script>
<script type="text/javascript" language="JavaScript1.2" src="/ir/script2007/stm31.js"></script>
</head>

<body >

<!--#include file = "function_vbscript2007.asp" -->

<table width="100%"  border="0" cellspacing="0" cellpadding="0">
  <tr>
		<td align="center" valign="top">
			<table width="742" border="0" cellpadding="0" cellspacing="0" bgcolor="#ECECEC">
				<tr align="left" valign="top">
					<td width="1" background="images2007/dot_pink.gif"><img src="images2007/dot_pink.gif" width="1" height="1"></td>
					<td>
							<table width="100%"  border="0" cellspacing="0" cellpadding="0">
									<!--++++++++++++++++++++++++include top++++++++++++++++++++++-->
								
													<!--#include file = "i_top2007.asp" -->
										
									<!--+++++++++++++++++++++++include menu+++++++++++++++++++++-->																									  
								  
												<!--#include file = "i_menu2007.asp" -->									
								
								<!--++++++++++++++++++++++++Content+++++++++++++++++++++++++++++-->
								<FORM name="frm1"   METHOD="POST" ACTION="news_clipping2007.asp">
								<tr>
										<td height="1" align="left" valign="top" background="images2007/dot_pink.gif"><img src="images2007/dot_pink.gif" width="1" height="1"></td>
							  </tr>
							  <tr>
									<td align="center" valign="top"><img src="images2007/head_news.gif" width="740" height="80"></td>
							  </tr>
							  <tr>
									<td align="left" valign="top">
										<table width="300" border="0" cellspacing="0" cellpadding="0">
											<tr align="left" valign="top">
													<td width="14"><img src="images2007/bar_1left.gif" width="14" height="21"></td>
													<td width="130" valign="middle" background="images2007/bar_1bg1.gif"><strong>News Clipping </strong></td>
													<td width="32"><img src="images2007/bar_1creb.gif" width="32" height="21"></td>
													<td background="images2007/bar_1bg2.gif">&nbsp;</td>
													<td width="4" align="right"><img src="images2007/bar_1right.gif" width="4" height="21"></td>
											</tr>
										</table>      
								</td>		 								
							</tr>
							   <tr>
									<td align="right" valign="top" bgcolor="#FFFFFF">
									<table  border="0" cellspacing="5" cellpadding="0">
									  <tr align="right" valign="top">										
										  <td valign="right"><strong>From</strong>&nbsp;&nbsp;										
										     	<%
														if request("Start_date") = "" then
															prev_month = dateadd("m", -12, now) 
															strdate = year(prev_month) & "-" & month(prev_month) & "-01"															
														else															
															strdate = request("Start_date") 														
														end if		

												%>
												<!--#include virtual="/ir/i_calendar2007.asp" -->
										  	&nbsp;&nbsp;<strong>To</strong>	&nbsp;&nbsp;										
										  <%
												if request("End_date") = "" then												
													ptdate = year(now) & "-" & month(now) & "-" & day(now)		
												else												
													ptdate = request("End_date") 												
												end if
												%>								
												<!-- #include virtual="/ir/i_calendar2007_2.asp" -->	&nbsp;&nbsp;
										<input type="submit" name="Submit" value="GO" style="cursor:hand;" onclick="javascript:set_pageno('frm1');">&nbsp;
										</td>									
									  </tr>
									  
									</table></td>
								  </tr>
							<tr>
							<tr>
								<td align="left" valign="top" bgcolor="#FFFFFF">
								   <table width="100%" border="0" cellpadding="3" cellspacing="1" bgcolor="#CACACA">	
										  <tr align="left" bgcolor="#F07EFE">
													<td valign="top" align="center"><span class="style19">Date</span></td>
													<td valign="top"  align="center"class="style18"><span class="style19">Company Name</span></td>
													<td valign="top" align="center"><span class="style19">Title</span></td>
													<td valign="top" align="center"><span class="style19">Source</span></td>
											  </tr>
									<!--#include file="select_page2007.asp"-->
							 <%		
									if trim( session("listed_member_id") ) <>"" then
												dim totalrec
												totalrec=0
												
												strsql=""
												strsql="select count(*) as num_count from clipping as a,listed_member as b,clipping_type as c  "
												strsql=strsql & " where   a.listed_id=b.listed_id and   a.listed_id in(" &  session("listed_member_id")  & ")  and a.type_id=c.type_id  "									
												strsql=strsql & " and a.timestamp >='" & strdate & " 00:00:00'  and  a.timestamp <='" & ptdate & " 23:59:00'  and a.display='Y' and a.type_id<>1"
												
												'response.write strsql
												
												set rs = conirauthen.execute(strsql)
												if not rs.eof and not rs.bof then
												    totalrec=rs("num_count")
												end if
												rs.close
												
												session("num_count")=totalrec
												
												  %>											  
											   <!--#include file="calculate_page2007.asp"-->											 
											   <%
											   strsql=""
											  strsql="select a.*,b.share,c.type_name from clipping as a,listed_member as b,clipping_type as c  "
											  strsql=strsql & " where a.listed_id=b.listed_id and   a.listed_id in(" &  session("listed_member_id")   & ") and a.type_id=c.type_id   "
											  strsql=strsql & " and a.timestamp >='" & strdate & " 00:00:00'  and  a.timestamp <='" & ptdate & " 23:59:00'  and a.display='Y' and a.type_id<>1"
											  strsql= strsql & " order by timestamp DESC  limit " & ((session("pageno")-1)*session("pagesize")) & "," & session("pagesize")  
											  
											  'response.write strsql
											  
												set rs = conirauthen.execute(strsql)
												if not rs.eof and not  rs.bof then 
													i=1
													dim new_date
													do while not rs.eof
													strsql=""
													strsql ="select  *  from source where source_id=" & rs("source_id")
													set  rssource=conreference.execute(strsql)
													if  not rssource.eof and not rssource.bof then
														source_name=""
														source_name=rssource("source_name")
													end if	
													rssource.close
													
													if i mod 2 <> 0 then
															%>
																	<tr bgcolor="#FFFFFF">
																	<%else%>
																	<tr bgcolor="#ECECEC">
																	<%end if%>		
																			 <td width="70" align="left" valign="top">
																				 <span class="style16">
																				 <%
																				    arr=split( formatdateAm_Pm(cdate(rs("timestamp")))," ")								
																						if new_date ="" or new_date <> arr(0) then
																								new_date= arr(0)
																								response.write cdate(new_Date)
																						end if		
																				 %>																				
																				</span>
																			</td>
																			<td  width="100" align="left" valign="top" class="style18"><%=rs("share")%></td>
																			<td align="left" valign="top">
																			      <%if customer_id=0  or customer_id=98  then %>
																							   <a href="listed/<%=rs("share")%>/clipping/<%=rs("file_name")%>"  target="_bank" >
																					<%else%>
																					         <a href="http://ir.efinancethai.com/listed/<%=rs("share")%>/clipping/<%=rs("file_name")%>"  target="_bank" >
																					<%end if%>																					
																					<%=rs("title")%>
																					</a>
																			<td  width="140" align="left" valign="top"><%=rs("type_name")%></td>
																	</tr>														  
																<%					
																		rs.movenext
																		i=i+1
																		loop
																		else
																		%>
																		<tr bgcolor="#FFFFFF" >
																		   <td align="center" colspan="4"  valign="middle">
																			     <font class="no_information">
																				<%
																				if  session("lang")="T"  then 
																				     response.write Message_Info_T
																				else																				
																				      response.write Message_Info_E
																				end if
																				%></font>
																			</td>
																		</tr>
																		<%
																		end if
																		rs.close
																		conirauthen.close
																		
																		set rs=nothing
																		set conirauthen=nothing
													else%>
																<tr bgcolor="#FFFFFF" >
																		   <td align="center" colspan="4"  valign="middle">
																			     <font class="no_information">
																				<%
																				if  session("lang")="T" then 
																				     response.write Message_Info_T
																				else																				
																				      response.write Message_Info_E
																				end if
																				%></font>
																			</td>
																		</tr>
												<%	end if
																%>	
										</table></td>
						  </tr>
						   <tr>
									<td align="left" valign="top" bgcolor="#FFFFFF" >&nbsp;</td>
							</tr>
							     <input type="hidden" name="Start_date" value="<%=strdate%>">
								  <input type="hidden" name="End_date" value="<%=ptdate%>">
							<!--+++++++++++++++++++++++++ Page Button+++++++++++++++++++++++--->
									<tr>
									<td align="center" valign="top" bgcolor="#FFFFFF"><hr class="style_hr" width="100%"></td>
							  </tr>
							  <tr>
									<td align="right" valign="top" bgcolor="#FFFFFF">
								           <!--#include file = "page_button2007.asp" -->
										</td>
								</tr>	
								  
								<tr>
										<td height="1" align="left" valign="top" background="images2007/dot_gray.gif"><img src="images2007/dot_gray.gif" width="1" height="1"></td>
								  </tr>
								  <tr>
										<td align="left" valign="top">&nbsp;</td>
								</tr>											   
								</Form>		
								<!--+++++++++++++++++++++++++include footer++++++++++++++++++++++-->
									
												<!--#include file = "i_footer2007.asp" -->
												
								</table>
							</td>
							<td width="1" align="right" background="images2007/dot_pink.gif"><img src="images2007/dot_pink.gif" width="1" height="1"></td>
						</tr>	
    </table></td>
  </tr>
</table>
<!-- +++++++++++++++++ Google Analytics ++++++++++++++++ -->
          <!--#include file="i_googleAnalytics.asp"-->	
<!-- +++++++++++++++ End Google Analytics +++++++++++++++ -->
</body>
</html>
