<%
session("cur_page")="IR Calendar"

session("page_asp")="ir_calendar2007"

if session("lang")="" or request("lang")="T"  then  
	session("lang")="T"
elseif request("lang")="E" then
	session("lang")="E"
end if

pagesize=10

%>
 <!--#include file="constpage2007.asp"-->

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=windows-874">
<title>Investor Relations</title>
<LINK href="style2007.css" type="text/css" rel="stylesheet">	
<!--#include file = "i_constant.asp" -->
<!--#include file = "function_asp2007.asp" -->
<!--include file = "i_conpsims.asp" -->
<!--#include file = "i_conirauthen.asp" -->
<!--#include file = "listed_member2007.asp" -->
 <!--#include file = "i_connews.asp" -->	
<!--#include virtual = "i_conir.asp" --> 
<meta name="keywords" content="<%=message_keyword%>">
<script language="javascript" src="function2007.js"></script>
<script type="text/javascript" language="JavaScript1.2" src="script2007/stm31.js"></script>
</head>

<body >
<table width="100%"  border="0" cellspacing="0" cellpadding="0">
  <tr>
		<td align="center" valign="top">
			<table width="742" border="0" cellpadding="0" cellspacing="0" bgcolor="#ECECEC">
				<tr align="left" valign="top">
					<td width="1" background="images2007/dot_pink.gif"><img src="images2007/dot_pink.gif" width="1" height="1"></td>
					<td>
							<table width="100%"  border="0" cellspacing="0" cellpadding="0">
									<!--++++++++++++++++++++++++include top++++++++++++++++++++++-->
								
													<!--#include file = "i_top2007.asp" -->
										
									<!--+++++++++++++++++++++++include menu+++++++++++++++++++++-->																									  
								  
												<!--#include file = "i_menu2007.asp" -->									
								
								<!--++++++++++++++++++++++++Content+++++++++++++++++++++++++++++-->
											<FORM name="frm1"   METHOD="POST" ACTION="ir_calendar2007.asp">											
										<tr>
												<td height="1" align="left" valign="top" background="images2007/dot_pink.gif"><img src="images2007/dot_pink.gif" width="1" height="1"></td>
										</tr>
										<tr>
												<td align="center" valign="top"><img src="images2007/head_news.gif" width="740" height="80"></td>
										</tr>
										<tr>
												<td align="left" valign="top">
														<table width="300" border="0" cellspacing="0" cellpadding="0">
															  <tr align="left" valign="top">
																<td width="14"><img src="images2007/bar_1left.gif" width="14" height="21"></td>
																<td width="130" valign="middle" background="images2007/bar_1bg1.gif"><strong>IR Calendar</strong></td>
																<td width="32"><img src="images2007/bar_1creb.gif" width="32" height="21"></td>
																<td background="images2007/bar_1bg2.gif">&nbsp;</td>
																<td width="4" align="right"><img src="images2007/bar_1right.gif" width="4" height="21"></td>
															  </tr>
															</table>              
												</td>	
										</tr>
										<tr>
													<td align="left" valign="top" bgcolor="#FFFFFF">
														<table width="100%"  border="0" cellpadding="3" cellspacing="1" bgcolor="#CACACA">														    	
															  <tr align="left" valign="top" bgcolor="#F07EFE">
																	<td valign="middle" align="center" class="style19">Company Name </td>
																	<td valign="middle"  align="center"class="style19">Event</td>
																	<td valign="middle"  align="center"><span class="style36"><strong>Location</strong></span></td>
																	<td valign="middle"  align="center"><span class="style36"><strong>Date &amp; Time </strong></span></td>
																	<td  valign="middle"  align="center"><span class="style36"><strong>Relate Info </strong></span></td>
																	<!--td valign="middle"  align="center" class="style19">Live Audio</td-->
															  </tr>
															  	<!--#include file="select_page2007.asp"-->
																	 <%		
																			if trim( session("listed_member_id") ) <>"" then
																						dim totalrec
																						totalrec=0
																						
																						strsql=""
																						 if session("lang")="T" or session("lang")="" then
																								strsql="select count(*) as num_count  from  calendar_event  "
																								strsql=strsql & " where share in  (" & session("listed_member")  & ") and display_ir='Y'  and detail_th <> '' "		
																						else
																						    	strsql="select count(*) as num_count  from  calendar_event  "
																								strsql=strsql & " where share in  (" & session("listed_member")  & ") and display_ir='Y' and detail <> '' "	
																						end if
																						
																						set rs = conir.execute(strsql)
																						if not rs.eof and not rs.bof then
																							totalrec=rs("num_count")
																						end if
																						rs.close
																						
																						session("num_count")=totalrec																						
																	%>											  
																<!--#include file="calculate_page2007.asp"-->
															<%
															       strsql=""
																   	if session("lang")="T" or session("lang")="" then
																			strsql="select id,location_th as location,share,detail_th as detail,date_event,live_date "
																			strsql=strsql &  " ,detail_time,live_audio,time_format(start_time,'%H:%i:%s') as start_time "
																			strsql=strsql & ",time_format(stop_time,'%H:%i:%s') as stop_time  from  calendar_event  "
																			strsql=strsql & " where share in  (" & session("listed_member")  & ") and display_ir='Y'  and detail_th <> '' order by last_update desc "
																			strsql=strsql &  " limit " & ((session("pageno")-1)*session("pagesize")) & "," & session("pagesize")  
																			
																			
						
																	else
																			strsql="select id, location,share,detail,date_event,live_date,detail_time "
																			strsql=strsql & " ,live_audio,time_format(start_time,'%H:%i:%s') as start_time "
																			strsql=strsql & ",time_format(stop_time,'%H:%i:%s') as stop_time  from  calendar_event  "
																			strsql=strsql & " where share in  (" & session("listed_member")  & ") and display_ir='Y'  and detail <> '' order by last_update desc "
																			strsql=strsql &  " limit " & ((session("pageno")-1)*session("pagesize")) & "," & session("pagesize")  
																	end if
																	
																	'response.write strsql
																	set rs=conir.execute(strsql)
																	if not rs.eof and not rs.bof then
																	   	i=0
																			  do while not rs.eof 	
																						if i mod 2 =0 then
																						%>
																								<tr align="left" valign="top" bgcolor="#ECECEC">
																						<%
																						else
																							%>
																								<tr align="left" valign="top" bgcolor="#FFFFFF">
																						  <%
																						end if
																						'if sum_relate >1 then
																						%>
																								<td  valign="top" class="style18"><%=rs("share")%></td>
																								<td valign="top">
																									<%= rs("detail")	%>
																								</td>
																								<td valign="top" >
																									<%=rs("location")%>
																								</td>
																								<td valign="top" class="style18">
																									<%=rs("date_event")%><br><%
																										if session("lang")="T" or session("lang")="" then
																												response.write replace(replace(rs("detail_time"),"AM","�."),"PM","�.")
																										else
																											 response.write rs("detail_time")
																										end if
																										%>
																								</td>
																								<td valign="top" >
																									 <% 
																											strsql=""																				
																											if session("lang")="T" or session("lang")="" then
																												strsql="select relate_info_th as relate_info,file_name  from relate_info where id=" & rs("id")  & " and relate_info_th<>'' "
																											else
																											   strsql="select relate_info,file_name  from relate_info where id=" & rs("id") & " and relate_info<>'' "
																											end if
																				
																											set rs1=conir.execute(strsql)
																											if not rs1.eof and not rs1.bof then
																												n=0
																												%>
																												  <table width="100%"  border="0" cellpadding="0" cellspacing="0" >
																												<%
																												do while not rs1.eof 																					      
																																		if n >0 then
																																		%>
																																	<tr align="left" valign="middle"  > 
																																		  <td valign="middle"><hr size="1px" color="#CACACA" width="100%"></td>
																																   </tr>		
																																		<%
																																		end if
																																	%>
																																 <tr align="left" valign="top"  > 
																																		  <td valign="top" > 
																																			  +<a href="<%=pathfileserver%>/listed/<%=rs("share")%>/calendar/relate_info/<%=rs1("file_name")%>" target="_blank"><%= rs1("relate_info")%></a> 
																																		  </td>
																																   </tr>		
																																	
																													   <%
																													   n=n+1
																														rs1.movenext
																												loop
																												%>
																												 </table>
																												<%
																											else
																												 response.write"<center>-</center>"
																											end if
																											rs1.close
																									 %>
																								</td>
																								<!--td valign="top" >
																									<%
																									'	date_now=date()
																									'	time_now=time()				
																									'	date_event = rs("live_date")
																										
																									'	 if rs("live_audio")="Y" then																										 
																				                          '  response.write  cdate(date_now)  & "  " &  cdate(date_event) 
																									'		if cdate(date_now) = cdate(date_event) then
																									'		   if  time_now  >=TimeValue(rs("start_time"))  and   time_now <= TimeValue(rs("stop_time")) then
																													 %><img src="images2007/audio.gif"  width="50" height="24"  onClick="OpenLiveRadioPopup();" style="cursor:hand"><%
																									'		   end if
																									'		end if
																									'	 end if																		
																										%>
																								</td-->
																							</tr>
																						<%		
																						i=i+1
																						rs.movenext
																			  loop	
																	else%>
																  	<tr bgcolor="#FFFFFF" >
																		   <td align="center" colspan="6"  valign="middle">
																			     <font class="no_information">
																				<%
																				if  session("lang")="T" then 
																				     response.write Message_Info_T
																				else																				
																				      response.write Message_Info_E
																				end if
																				%></font>
																			</td>
																		</tr>
																<%
																	end if
															  else%>
																  	<tr bgcolor="#FFFFFF" >
																		   <td align="center" colspan="6"  valign="middle">
																			     <font class="no_information">
																				<%
																				if  session("lang")="T" then 
																				     response.write Message_Info_T
																				else																				
																				      response.write Message_Info_E
																				end if
																				%></font>
																			</td>
																		</tr>
																<%end if%>
															
														</table>
												</td>
											</tr>
						                  <tr>
													<td align="left" valign="top" bgcolor="#FFFFFF" >&nbsp;</td>
											</tr>												
											<!--+++++++++++++++++++++++++ Page Button+++++++++++++++++++++++--->
													<tr>
													<td align="center" valign="top" bgcolor="#FFFFFF"><hr class="style_hr" width="100%"></td>
											  </tr>
											  <tr>
													<td align="right" valign="top" bgcolor="#FFFFFF">
														   <!--#include file = "page_button2007.asp" -->
														</td>
												</tr>	
												  
												<tr>
														<td height="1" align="left" valign="top" background="images2007/dot_gray.gif"><img src="images2007/dot_gray.gif" width="1" height="1"></td>
												  </tr>
												  <tr>
														<td align="left" valign="top">&nbsp;</td>
												</tr>		
											</form>
								<!--+++++++++++++++++++++++++include footer++++++++++++++++++++++-->
									   
												<!--#include file = "i_footer2007.asp" -->
												
								</table>
							</td>
							<td width="1" align="right" background="images2007/dot_pink.gif"><img src="images2007/dot_pink.gif" width="1" height="1"></td>
						</tr>	
    </table></td>
  </tr>
</table>
<!-- +++++++++++++++++ Google Analytics ++++++++++++++++ -->
          <!--#include file="i_googleAnalytics.asp"-->	
<!-- +++++++++++++++ End Google Analytics +++++++++++++++ -->
</body>
</html>
