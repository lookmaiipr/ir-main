<%
session("cur_page")="main_menu"
session("open_ir")="IR"
if session("lang")="" or request("lang")="T"  then  'Language
	session("lang")="T"
elseif request("lang")="E" then
	session("lang")="E"
end if

if  Ucase(session("right_admin"))="GUEST" then
   session("current_path")=""
end if
%>
<!-- #include file = "../i_constant.asp" -->
<!-- #include file = "../includefile_i_contant.asp" -->
<!-- #include file = "../includefile_style.asp" -->

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<title><%=WEBSITE_NAME%>/Investor Relation</title>
<meta http-equiv="Content-Type" content="text/html; charset=windows-874">
<body leftmargin="0" topmargin="5" marginwidth="0" marginheight="5">
<%if session("user_id")<>"" then%>
<div align="center">  
	<table width="1000"  border="0" cellspacing="0" cellpadding="0">
		<tr>
			<td align="left" valign="top">
				<table width="100%"  border="0" cellpadding="0" cellspacing="1" bgcolor="#cfcfcf">
					<tr align="left" valign="top">         
						<td >
							<table width="100%"  border="0" cellspacing="0" cellpadding="0"  >
								<tr>
									<td align="left" valign="top">
										 <!-- #include file = "../includefile_i_top.asp" -->	
									</td>
								</tr>
								<tr  bgcolor="#FFFFFF">
									<td align="left" valign="top" >
										<table width="85%"  border="0" cellspacing="10" cellpadding="0" align="center">
											<tr align="left" valign="top">
												<td>
													<br><br>
													<table width="100%"  border="0" cellspacing="1" cellpadding="0" bgcolor="<%= session("bg_in_form")%>">
														<tr>
															<td align="left" valign="top">
																<!--<table width="100%" border="0" cellpadding="5" cellspacing="0" >
																	<tr align="left" valign="top">																	
																		<td valign="middle" colspan="2" ><img src="/ir/images/arrow_rs.gif">
																			<font class="style41_sp">IR Administration System</font>
																		</td>
																	</tr>                          
																</table>-->
																
																<table width="100%" border="0" cellpadding="5" cellspacing="0" >
																	<tr align="left" valign="top">
																		<td align="left" valign="middle" height="30" colspan="2">
																			&nbsp;&nbsp;<img src="../images/arrow_rs.gif" > <font color="#FFFFFF" size="3"><b>IR Administration System</b></font>
																		</td>																									
																	</tr>                          
																</table>																
															</td>
														</tr>
														<tr>
															<td align="left" valign="top" bgcolor="FFFFFF">
																<br><br>
															<%
															if  session("right_admin")="NEWS"  then   %>	 <!-- for system admin -->
																<table width="100%" border="0" cellpadding="10" cellspacing="0" >
																	<tr>
																		<td width="5%"></td>
																		<td align="left" valign="top" bgcolor="#FFFFFF" width="55%">	
																			  <img src="../images/arrow_r.gif" width="7" height="7">&nbsp;<b>Admin</b></font>
																				<table width="100%" border="0" cellpadding="5" cellspacing="0" >
																					<tr>
																						<td width="5%"></td>
																						<td>
																							&nbsp;&nbsp;&nbsp;&nbsp;<img src="../images/piont_dot.gif" >&nbsp;<a href="listed.asp" >Listed</a><br>
																							&nbsp;&nbsp;&nbsp;&nbsp;<img src="../images/piont_dot.gif" >&nbsp;<a href="admin_user.asp" >Admin User</a><br>
																							&nbsp;&nbsp;&nbsp;&nbsp;<img src="../images/piont_dot.gif" >&nbsp;<a href="irplus_banner.asp"  >IR Plus </a><br>
																							&nbsp;&nbsp;&nbsp;&nbsp;<img src="../images/piont_dot.gif" >&nbsp;<a href="irthai_mainmenu_general_meeting.asp"  >Main Menu </a>&nbsp;<img src="../images/icon_new.gif"><br>
																							&nbsp;&nbsp;&nbsp;&nbsp;<img src="../images/piont_dot.gif" >&nbsp;<a href="irthai_listedmenu_general_meeting.asp"  >Listed Main Menu </a>&nbsp;<img src="../images/icon_new.gif">
																							</font>
																						</td>
																					</tr>
																				</table>
																		
																			   <br><img src="../images/arrow_r.gif" width="7" height="7">&nbsp;<b>Investor</b></font>
																				<table width="100%" border="0" cellpadding="5" cellspacing="3" >
																					<tr>
																						<td width="5%"></td>
																						<td>
																							&nbsp;&nbsp;&nbsp;&nbsp;<img src="../images/piont_dot.gif" >&nbsp;<a href="ir_user.asp"  width="300">IR Member</a><br>	
																							&nbsp;&nbsp;&nbsp;&nbsp;<img src="../images/piont_dot.gif" >&nbsp;<a href="profile.asp"  >IR Member's Profile</a>
																							</font>
																						</td>
																					</tr>
																				</table>
																			
																			   <br><img src="../images/arrow_r.gif" width="7" height="7">&nbsp;<b>Menu</b></font>
																				<table width="100%" border="0" cellpadding="5" cellspacing="3" >
																						<tr>
																							<td width="5%"></td>
																							<td>
																								&nbsp;&nbsp;&nbsp;&nbsp;<img src="../images/piont_dot.gif" >&nbsp;<a href="public_eventAdmin_reporter.asp"  width="300">Public Relations</a><br>
																								&nbsp;&nbsp;&nbsp;&nbsp;<img src="../images/piont_dot.gif" >&nbsp;<a href="calendar_eventadmin_reporter.asp"  width="300">IR Calendar</a><br>
																								&nbsp;&nbsp;&nbsp;&nbsp;<img src="../images/piont_dot.gif" >&nbsp;<a href="annual_reporter.asp"  width="300">Annual Report</a><br>
																								&nbsp;&nbsp;&nbsp;&nbsp;<img src="../images/piont_dot.gif" >&nbsp;<a href="research_admin_reporter.asp"  width="300">Analyst Report</a><br>
																								&nbsp;&nbsp;&nbsp;&nbsp;<img src="../images/piont_dot.gif" >&nbsp;<a href="faq_reporter.asp"  width="300">Investor FAQs</a><br>	
																								&nbsp;&nbsp;&nbsp;&nbsp;<img src="../images/piont_dot.gif" >&nbsp;<a href="inquiry_form_reporter.asp">Inquiry Form</a><br>
																								&nbsp;&nbsp;&nbsp;&nbsp;<img src="../images/piont_dot.gif" >&nbsp;<a href="irthai_press_release.asp">Press Release</a><br>
																								&nbsp;&nbsp;&nbsp;&nbsp;<img src="../images/piont_dot.gif" >&nbsp;<a href="irthai_general_meeting.asp">General Meeting of Shareholders</a><br>
																								&nbsp;&nbsp;&nbsp;&nbsp;<img src="../images/piont_dot.gif" >&nbsp;<a href="complaints.asp">Channel Complaints</a><br>
																								</font>
																							</td>
																					</tr>
																				</table>
																			<!--<p> <img src="../images/arrow_r.gif" width="7" height="7">&nbsp;Activity </font></p>-->
																		</td>
																		<td align="left" valign="top" bgcolor="#FFFFFF" width="50%">			
																			   <img src="../images/arrow_r.gif" width="7" height="7">&nbsp;<b>Clipping</b></font>
																				<table width="100%" border="0" cellpadding="5" cellspacing="3" >
																					<tr>
																						<td width="5%"></td>
																						<td>
																							&nbsp;&nbsp;&nbsp;&nbsp;<img src="../images/piont_dot.gif" >&nbsp;<a href="IRAdmin/ListedEmail/ViewListedEMail.asp" width="300">Listed Email</a><br>	
																							&nbsp;&nbsp;&nbsp;&nbsp;<img src="../images/piont_dot.gif" >&nbsp;<a href="clipping_type.asp">News Clipping Type </a><br>
																							&nbsp;&nbsp;&nbsp;&nbsp;<img src="../images/piont_dot.gif" >&nbsp;<a href="clipping_source.asp">News Clipping Source</a><br>
																							&nbsp;&nbsp;&nbsp;&nbsp;<img src="../images/piont_dot.gif" >&nbsp;<a href="clipping.asp">News Clipping Center</a>
																							</font>
																						</td>
																					</tr>
																				</table>
																			
																			   <br><img src="../images/arrow_r.gif" width="7" height="7">&nbsp;<b>Column</b></font>
																				<table width="100%" border="0" cellpadding="5" cellspacing="3" >
																					<tr>
																						<td width="5%"></td>
																						<td>
																							&nbsp;&nbsp;&nbsp;&nbsp;<img src="../images/piont_dot.gif" >&nbsp;<a href="IRAdmin/Company/ViewCompany.asp"  width="300">Company</a><br>
																							&nbsp;&nbsp;&nbsp;&nbsp;<img src="../images/piont_dot.gif" >&nbsp;<a href="IRAdmin/Interviewee/ViewInterviewee.asp" width="300">Interviewee</a><br>	
																							&nbsp;&nbsp;&nbsp;&nbsp;<img src="../images/piont_dot.gif" >&nbsp;<a href="IRAdmin/IRSociety/ViewIRSociety.asp"width="300">IR Society</a><br>
																							&nbsp;&nbsp;&nbsp;&nbsp;<img src="../images/piont_dot.gif" >&nbsp;<a href="IRAdmin/InsightIPO/IPOShowOff/ViewIPOShowoff.asp">Insight IPO</a><br>
																							&nbsp;&nbsp;&nbsp;&nbsp;<img src="../images/piont_dot.gif" >&nbsp;<a href="IRAdmin/IPOMovement/ViewIPOMovement.asp" width="300">IPO Movement</a><br>		
																							&nbsp;&nbsp;&nbsp;&nbsp;<img src="../images/piont_dot.gif" >&nbsp;<a href="IRAdmin/Source/ViewSource.asp" width="300">IPO Research Source</a><br>
																							&nbsp;&nbsp;&nbsp;&nbsp;<img src="../images/piont_dot.gif" >&nbsp;<a href="IRAdmin/IRKnowledge/ViewIRKnowledge.asp" width="300">IR Knowledge</a>
																							</font>
																						</td>
																					</tr>
																				</table>
																			
																		      <br><img src="../images/arrow_r.gif" width="7" height="7">&nbsp;<b><a href="InvestorRelationsMetrics_reporter.asp" >Board Report</a></b></font>
																				<table width="100%" border="0" cellpadding="5" cellspacing="3" >
																					<tr>
																						<td width="5%"></td>
																						<td>
																								&nbsp;&nbsp;&nbsp;&nbsp;<img src="../images/piont_dot.gif" >&nbsp;<a href="InvestorRelationsMetrics_reporter.asp" >Investor Relations Metrics</a><br>
																								&nbsp;&nbsp;&nbsp;&nbsp;<img src="../images/piont_dot.gif" >&nbsp;<a href="stockprice_performance_reporter.asp" >Stock Price Performance</a><br>
																								&nbsp;&nbsp;&nbsp;&nbsp;<img src="../images/piont_dot.gif" >&nbsp;<a href="stockholder_composition_reporter.asp" >Stockholder Composition</a><br>
																								&nbsp;&nbsp;&nbsp;&nbsp;<img src="../images/piont_dot.gif" >&nbsp;<a href="http://www.settrade.com/AnalystConsensus/C04_10_stock_saa_p1.jsp?selectPage=10&txtSymbol=<%=replace(listed_share,"&","%26")%>" target="_blank">Analyst Coverage</a>	
																								</font>
																						 </td>
																					</tr>
																				</table>
																				
																			
																			<br><img src="../images/arrow_r.gif" width="7" height="7">&nbsp;<b>FTP File</b><br></font>
																			<table width="100%" border="0" cellpadding="5" cellspacing="3" >
																				<tr>
																					<td width="5%"></td>
																					<td>
																						&nbsp;&nbsp;&nbsp;&nbsp;<img src="../images/piont_dot.gif">&nbsp;<a href="IRAdmin/IRFTP/ViewListedFTP.asp" target="_blank">FTP File Manual</a><br>
																						</font>
																					 </td>
																				</tr>
																			</table>
																			
																				<!--<p> <img src="../images/arrow_r.gif" width="7" height="7">&nbsp;Webboard</font></p>-->
																			<!--p> <img src="../images/arrow_r.gif" width="7" height="7">&nbsp;<a href="clipping.asp"  >Feature Articles</a></font></p-->
																		</td>
																		<!--<td align="left" valign="top" bgcolor="#FFFFFF" >					
																				<!--p> <img src="../images/arrow_r.gif" width="7" height="7">&nbsp;<a href="contact_admin.asp"  width="300">Q & A</a></font></p-->
																				<!--p><a href = "/ir/admin_listed/logout.asp"><img src = "/ir/contactlisted/images/logout.gif" border="0"></a></p>
																				<p> <a href="f246_2_reporter.asp"  width="300">F246-2</a></font></p>-->														
																		<!--</td>-->
																	</tr>
																	<tr>
																		<td width="5%"></td>
																		<td valign="middle">
																			<input type="button"  name="logout"  value="Logout" onclick="Javasript: document.location.href='/ir/admin_listed/logout.asp';">
																			<br><br>
																		</td>
																	</tr>
																</table>
															<%					
																elseif  session("right_admin")="IT"  then %>
																<table width="100%" border="0" cellpadding="10" cellspacing="10" >
																	<tr>
																		<td>
																			<table width="100%" border="0" cellpadding="5" cellspacing="0" >
																				<tr>
																					<td width="5%"></td>
																					<td align="left" valign="top" bgcolor="#FFFFFF" width="50%">	
																							 <img src="../images/arrow_r.gif" width="7" height="7">&nbsp;<b>Admin</b></font>
																								<table width="100%" border="0" cellpadding="5" cellspacing="3" >
																									<tr>
																										<td width="5%"></td>
																										<td>
																											&nbsp;&nbsp;&nbsp;&nbsp;<img src="../images/piont_dot.gif" >&nbsp;<a href="listed.asp" >Listed</a><br>
																										   &nbsp;&nbsp;&nbsp;&nbsp;<img src="../images/piont_dot.gif" >&nbsp;<a href="admin_user.asp" >Admin User</a>
																										   </font>
																										</td>
																									</tr>
																								</table>
																						
																							   <br><img src="../images/arrow_r.gif" width="7" height="7">&nbsp;<b>Investor</b></font>
																								<table width="100%" border="0" cellpadding="5" cellspacing="3" >
																									<tr>
																										<td width="5%"></td>
																										<td>
																											&nbsp;&nbsp;&nbsp;&nbsp;<img src="../images/piont_dot.gif" >&nbsp;<a href="ir_user.asp"  width="300">IR Member</a>
																											</font>
																										</td>
																									</tr>
																								</table>
																							<!--p> <img src="../images/arrow_r.gif" width="7" height="7">&nbsp;<a href="research_admin_reporter.asp"  width="300">Research</a></font></p-->	
																							<!--<p> <img src="../images/arrow_r.gif" width="7" height="7">&nbsp;<a href="person.asp"  width="300">Person</a></font></p>
																							<p><a href = "/ir/admin_listed/logout.asp"><img src = "/ir/contactlisted/images/logout.gif" border="0"></a></p>
																							<p><input type="button"  name="logout"  value="Logout" style="FONT-FAMILY: Ms sans serif;FONT-SIZE:10px; font-weight: bold;cursor:hand;"  onclick="Javasript: document.location.href='/ir/admin_listed/logout.asp';"></p>-->
																						
																					</td>
																							<td align="left" valign="top" bgcolor="#FFFFFF" width="50%">	
																							<img src="../images/arrow_r.gif" width="7" height="7">&nbsp;<b>Log</b></font>
																							<table width="100%" border="0" cellpadding="5" cellspacing="3" >
																								<tr>
																									<td width="5%"></td>
																									<td>
																										&nbsp;&nbsp;&nbsp;&nbsp;<img src="../images/piont_dot.gif" >&nbsp;<a href="log_info.asp">Log Activities</a><br>
																									   &nbsp;&nbsp;&nbsp;&nbsp;<img src="../images/piont_dot.gif" >&nbsp;<a href="log_user_info.asp">Log User</a>
																									   </font>
																									</td>
																								</tr>
																							</table>
																					</td>
																				</tr>
																				<tr>
																					<td width="5%"></td>
																					<td>
																						<p><input type="button"  name="logout"  value="Logout" onclick="Javasript: document.location.href='/ir/admin_listed/logout.asp';"></p>
																					</td>
																				</tr>
																			</table>
																		</td>
																	</tr>
																</table>
																<%elseif session("right_admin")="LISTED" then %>
																<table width="100%" border="0" cellpadding="10" cellspacing="10" >
																	<tr>
																		<td>
																			<table width="100%" border="0" cellpadding="5" cellspacing="0">
																				<tr>														 
																						<td align="left" valign="top" bgcolor="#FFFFFF" width="50%">	
																					
																							<img src="../images/arrow_r.gif" width="7" height="7">&nbsp;<b>Menu</b></font>
																							<table width="100%" border="0" cellpadding="5" cellspacing="3" >
																								<tr>
																									<td>
																										
																										&nbsp;&nbsp;&nbsp;&nbsp;<img src="../images/piont_dot.gif" >&nbsp;<a href="faq.asp"  width="300">Investor FAQs</a><br>
																										&nbsp;&nbsp;&nbsp;&nbsp;<img src="../images/piont_dot.gif" >&nbsp;<a href="inquiry_form.asp" >Inquiry Form</a><br>
																										&nbsp;&nbsp;&nbsp;&nbsp;<img src="../images/piont_dot.gif" >&nbsp;<a href="calendar_eventAdmin.asp"  width="300">IR Calendar </a>
																										</font>
																									</td>
																								</tr>
																							</table>
																							
																							<br><img src="../images/arrow_r.gif" width="7" height="7">&nbsp;<b>Clipping</b></font>
																							<table width="100%" border="0" cellpadding="5" cellspacing="3" >
																								<tr>
																									<td>
																										
																										&nbsp;&nbsp;&nbsp;&nbsp;<img src="../images/piont_dot.gif" >&nbsp;<a href="clipping_listed.asp" >News Clipping Center</a>
																										</font>
																									</td>
																								</tr>
																							</table>
																							
																							<!--p> <img src="../images/arrow_r.gif" width="7" height="7">&nbsp;<a href="inquiry_form.asp" >IR Member</a></font></p-->
																							<!--p> <img src="../images/arrow_r.gif" width="7" height="7">&nbsp;<a href="contact_admin.asp"  width="300">IR Contact / Inquiry Form</a></font></p-->
																							<!--p ><a href = "/ir/admin_listed/logout.asp"> <img src = "/ir/contactlisted/images/logout.gif" border="0"></a></p-->
																							<!--<p><input type="button"  name="logout"  value="Logout"  style="FONT-FAMILY: Ms sans serif;FONT-SIZE:10px; font-weight: bold;cursor:hand;"  onclick="Javasript: document.location.href='/ir/admin_listed/logout.asp';"></p>-->

																					</td>
																					<td valign="top">
																						  <img src="../images/arrow_r.gif" width="7" height="7">&nbsp;<b><a href="InvestorRelationsMetrics.asp" >Board Report</a></b></font>
																							<table width="100%" border="0" cellpadding="5" cellspacing="3" >
																								<tr>
																									<td>
																									  
																										&nbsp;&nbsp;&nbsp;&nbsp;<img src="../images/piont_dot.gif" >&nbsp;<a href="InvestorRelationsMetrics.asp" >Investor Relations Metrics</a><br>
																										&nbsp;&nbsp;&nbsp;&nbsp;<img src="../images/piont_dot.gif" >&nbsp;<a href="stockprice_performance.asp" >Stock Price Performance</a><br>
																										&nbsp;&nbsp;&nbsp;&nbsp;<img src="../images/piont_dot.gif" >&nbsp;<a href="stockholder_composition.asp" >Stockholder Composition</a><br>
																										&nbsp;&nbsp;&nbsp;&nbsp;<img src="../images/piont_dot.gif" >&nbsp;<a href="http://www.settrade.com/AnalystConsensus/C04_10_stock_saa_p1.jsp?selectPage=10&txtSymbol=<%=replace(listed_share,"&","%26")%>" target="_blank">Analyst Coverage</a>	
																										</font>
																									</td>
																								</tr>
																							</table>
																					</td>
																				</tr>
																				<tr><td colspan="2"><p><input type="button"  name="logout"  value="Logout" onclick="Javasript: document.location.href='/ir/admin_listed/logout.asp';"></p></td></tr>
																			</table>
																		</td>
																	</tr>
																</table>
																<%elseif session("right_admin")="GUEST" then %>
																<table width="100%" border="0" cellpadding="10" cellspacing="10" >
																	<tr>
																		<td>
																			<table width="100%" border="0" cellpadding="5" cellspacing="0" >
																				<tr>														 
																					<td  valign="top" bgcolor="#FFFFFF">
																						 <img src="../images/arrow_r.gif" width="7" height="7">&nbsp;<b>Menu</b></font>
																						<table width="100%" border="0" cellpadding="5" cellspacing="3" >
																							<tr>
																								<td>
																										
																										&nbsp;&nbsp;&nbsp;&nbsp;<img src="../images/piont_dot.gif" >&nbsp;<a href="GuestListedAdmin/PressRelease/ViewPressRelease.asp"  width="300">Press Release</a><br>
																										&nbsp;&nbsp;&nbsp;&nbsp;<img src="../images/piont_dot.gif" >&nbsp;<a href="GuestListedAdmin/PublicRelations/ViewPublicGuestListed.asp" >Public Relations</font>
																										</font>
																								</td>
																							</tr>
																						</table>
																						<p><input type="button"  name="logout"  value="Logout" onclick="Javasript: document.location.href='/ir/admin_listed/logout.asp';"></p>
																							
																					</td>
																				</tr>
																			</table>
																		</td>
																	</tr>
																</table>
																
																<%end if%>	
															</td>
														</tr>
													</table>		
													<br>
												</td>
											</tr>
										</table>
										<br><br>
									</td>
								</tr>
							</table>
						</td>
					</tr>
					<!--#include file = "../i_footer.asp" -->
				</table>
			</td>
		</tr>
	</table>
	<p>&nbsp;</p>
</div>
<%else%>
<br><br><br>
<center><font style="font-size:18pt;color:red"><%response.redirect "login.asp"%></font></center>
<%end if%>
</body>
</html>
