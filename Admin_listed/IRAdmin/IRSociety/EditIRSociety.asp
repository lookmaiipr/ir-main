<!-- #include file = "../../../i_constant.asp" -->
<!-- #include file = "../../../includefile_i_contant.asp" -->
<!-- #include file = "../../../includefile_style.asp" -->
<!--#include file = "../../../i_conir.asp" -->
<!--#include file = "../../../i_conirauthen_irdb.asp" -->
<!--#include file = "../../../i_conirauthen.asp" -->
<!--#include file = "../../global_function.asp" -->

<%
if  request("Action") =  "1"  then

         if request("video_clip") = "" then
            video_clip=0
         else
            video_clip=request("video_clip")
         end if
         
			sql="update tbl_trans_society set title='"&request("title_require")&"',file_name='"&request("filename_require")&"',video='"&video_clip&"'"
			sql=sql&",event_timestamp='"&request("event_timestamp_require")&"',activated='"&request("activate")&"',updated_timestamp=now()"
			sql=sql&",user_id='"&session("user_id")&"',interviewee_id="&request("interviewee_require")&" where sc_id=" & request("sc_id")
			
			conirauthen_irdb.execute(sql)
		
			call Insert_LogAdminActivity(conirauthen,session("user_id"),"ESC",request("sc_id"))
			
			conirauthen_irdb.close
			conirauthen.close	
			
			
%>
			<script language="javascript">
				alert( "Updated ir society completely.");
				window.location.href="ViewIRSociety.asp";										
			</script>
<%
else
	if request("Action")="0" then
		conirauthen.close
		response.redirect "ViewIRSociety.asp"
	end if
end if
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<title><%=Ucase(WEBSITE_NAME)%>/Edit IR Society</title>
<meta http-equiv="Content-Type" content="text/html; charset=windows-874">
<script language="JavaScript" src = "../../irthai_function.js"></script>
<script language="JavaScript" src = "../../../validationCheck.js"></script>
<script language="JavaScript">
	function OnSelectedCompany(comp_id)
	{
		document.frm.IsPostBack.value="true";
		window.frm.submit();
	}
	
	function OnSubmitFrm(formname)
	{	
		if (validateFormOnSubmit(formname)==false)	{return false;	}
		if(!CheckDates(1,document.frm.event_timestamp_require.value)){	 alert("Date & time is invalid!.");	 return false;}		
		
		var filename = (document.frm.filename_require.value).toLowerCase();
		if(filename.indexOf("sc_")==-1)  { alert('File name is invalid!.');	 return false;}		
		
		var filename_date = filename.replace("sc_","");

		if(!CheckDates(2,filename_date)){ alert('File name is invalid!.');	 return false;}
		
      if (!(document.frm.video_clip.value=='')){
         if(!IsNumeric(document.frm.video_clip.value)){ alert('Video clip is invalid!.');	 return false;}
         if(!((document.frm.video_clip.value>=0)&&(document.frm.video_clip.value<=5))){ alert('Video clip is invalid!.');	 return false;}		      
      }

		
		// The Last Condition : All data is valid. //
		return true;
	}
	
	function checkvalue()
	{
		if (document.frm.old_company.value!=document.frm.company_require.value){return true;	}
		else if (document.frm.old_interviewee.value!=document.frm.interviewee_require.value)	{return true;	}
		else if (document.frm.old_event_timestamp.value!=document.frm.event_timestamp_require.value)	{return true;	}
		else if (document.frm.old_title.value!=document.frm.title_require.value)	{return true;	}
		else if (document.frm.old_filename.value!=document.frm.filename_require.value)	{return true;	}
		else if (document.frm.old_video.value!=document.frm.video_clip.value)	{return true;	}
		else if (document.frm.old_activate.value!=document.frm.activate.value)	{return true;	}
		else{return false;}
	}
	
</script>

</head>
<body leftmargin="0" topmargin="5" marginwidth="0" marginheight="5">
<%if session("user_id")<>"" then%>
<div align="center">  	
	<table width="1000" valign="top" border="0" cellspacing="0" cellpadding="0" >
	<tr>
	  <td align="left" valign="top">
			<table width="100%"  border="0" cellpadding="0" cellspacing="0" bgcolor="cfcfcf">
				<tr align="left" valign="top">         
						<td bgcolor="#FFFFFF">
								<table width="100%"  border="0" cellspacing="0" cellpadding="0">
										<tr>
												<td align="left" valign="top" >						
														   <!-- #include file = "../../../includefile_i_top.asp" -->
												</td>
											</tr>			
											<tr>
											  <td align="left" valign="top" bgcolor="#cfcfcf" >
														<form id="frm" name="frm" method="post" action="EditIRSociety.asp?sc_id=<%=request("sc_id")%>" onsubmit="javascript:if(checkvalue()){return OnSubmitFrm('frm');}else{return false;};">
														<table width = "100%" align="center" cellpadding=0 cellspacing="10" >
															<tr align="left" valign="top">
																<td ><br>
																	<table width="100%" border="0" cellpadding="0" cellspacing="1">
																		<tr align="left" valign="top">
																			<td width="350" valign="middle" bgcolor="#FFFFFF"><font class="stylefont"><strong><img src="../../../images/arrow.gif" width="12" height="9">Edit IR Society : <%=session("listed_name")%></strong></font></td>
																			<td bgcolor="#cfcfcf">&nbsp;</td>
																		</tr>
																	</table><br>
																</td>
															</tr>
															<tr>
																<td  align="center" valign="top"  width="100%" bgcolor="#FFFFFF">
																	<table width="90%" border="0" cellpadding=0 cellspacing=0  bordercolor="#000066"  align="center">																			   	
																		<tr>
																			<td><br>
																				<table border="0" cellpadding=5 cellspacing=0  align="center" width="100%" bgcolor="#FFFFFF">	
																				<!-----------------------------------------------input data for insert----------------------------------------------------------------->
																				<%
																			if request("IsPostBack") <> "true" then
																					sql="SELECT sc.sc_id, sc.title,sc.file_name,sc.video,sc.event_timestamp,sc.activated,c.comp_id,c.name_th,i.interviewee_id,i.f_name,i.l_name "
																					sql=sql& "FROM (tbl_trans_society as sc join tbl_master_company as c,tbl_master_interviewee as i) "
																					sql=sql& "where (sc.interviewee_id=i.interviewee_id and i.comp_id=c.comp_id) and i.activated='Y' and c.activated='Y'  and sc_id=" & request("sc_id")
																					
																					set rs = conir.execute(sql)
																								
																					if not rs.bof and not rs.eof   then																							
																						company=rs("comp_id")
																						interviewee=rs("interviewee_id")
																						event_timestamp=DateToString(rs("event_timestamp"),"YYYY-MM-DD HH:mm:ss")
																						title=rs("title")
																						filename=rs("file_name")
																						video=rs("video")
																						activate=rs("activated")
																						
																						old_company=rs("comp_id")
																						old_interviewee=rs("interviewee_id")
																						old_event_timestamp=DateToString(rs("event_timestamp"),"YYYY-MM-DD HH:mm:ss")
																						old_title=rs("title")
																						old_filename=rs("file_name")
																						old_video=rs("video")
																						old_activate=rs("activated")
																					end if
																			else
																						company=request("company_require")
																						interviewee=request("interviewee_require")
																						event_timestamp=DateToString(request("event_timestamp_require"),"YYYY-MM-DD HH:mm:ss")
																						title=request("title_require")
																						filename=request("filename_require")
																						video=request("video_clip")
																						activate=request("activate")		

																						old_company=request("old_company")
																						old_interviewee=request("old_interviewee")
																						old_event_timestamp=DateToString(request("old_event_timestamp"),"YYYY-MM-DD HH:mm:ss")
																						old_title=request("old_title")
																						old_filename=request("old_filename")
																						old_video=request("old_video")
																						old_activate=request("old_activate")																						
																			end if
																					%>				
																					<tr>
																						<td align="right" valign="top"><b><font color="#330000">Company  :</font></b></td>
																						<td align="left">
																							<select name="company_require"  msgwarning="Please selected company." size="1"   class="selection_style" onchange="javascript:OnSelectedCompany(this.id);">
																							   <option value="" selected>--- Selected Company ---</option>
																							   <%
																							   sql="select comp_id, name_th from tbl_master_company where activated='Y' order by name_th asc"
																							   
																							   set rs = conir.execute(sql)
																							   
																								if not rs.eof and not  rs.bof then 
																									do while not rs.eof
																								%>
																									<%if cstr(company)=cstr(rs("comp_id")) then%>
																									<option value="<%=rs("comp_id")%>" selected><%=rs("name_th")%></option>
																									<%else%>
																									<option value="<%=rs("comp_id")%>" ><%=rs("name_th")%></option>
																									<%end if%>																								 
																								<%
																										rs.movenext
																									loop																								
																								end if																							   																			   
																							   %>																							   
																							</select><font color="#FF0000"><strong>&nbsp;*</strong></font>
																						</td>
																					</tr>			
																					<tr>
																						<td align="right" valign="top"><b><font color="#330000">Interviewee  :</font></b></td>
																						<td align="left">			
																							<%if company <>"" then%>
																							<select name="interviewee_require"  msgwarning="Please selected interviewee." size="1"   class="selection_style">
																							   <option value="" selected>--- Selected Interviewee ---</option>
																								<%
																							   sql="select * from tbl_master_interviewee where activated = 'Y' and comp_id ="&company&"  order by f_name asc,l_name asc "
																							   
																							   set rs = conir.execute(sql)
																							   
																								if not rs.eof and not  rs.bof then 
																									do while not rs.eof
																								%>
																									<%if cstr(interviewee)=cstr(rs("interviewee_id")) then%>
																									<option value="<%=rs("interviewee_id")%>" selected><%=rs("f_name") & "&nbsp;&nbsp;" & rs("l_name")%></option>
																									<%else%>
																									<option value="<%=rs("interviewee_id")%>"><%=rs("f_name") & "&nbsp;&nbsp;" & rs("l_name")%></option>
																									<%end if%>																								 
																								<%
																										rs.movenext
																									loop																								
																								end if																							   																			   
																							   %>															   
																							</select>
																							<%else%>
																							<select name="interviewee_require"  msgwarning="Please selected interviewee." size="1"   class="selection_style" disabled>
																							   <option value="" selected>--- Selected Interviewee ---</option>
																							</select>
																							<%end if%>
																							<font color="#FF0000"><strong>&nbsp;*</strong></font>
																						</td>
																					</tr>			
																					
																					<tr>
																						<td align="right" valign="top"><b><font color="#330000">Date & Time  :</font></b>
																						</td>
																						<td align="left">	
																							<input type="text" name="event_timestamp_require" msgwarning="Please fill date & time." size="30" class="textfield_style"  value="<%=event_timestamp%>" maxlength = "19"><font color="#FF0000"><strong>&nbsp;*</strong></font><br>
																							<font class="txtnote" color="#330000">[ �ٻẺ�ѹ��� YYYY-MM-DD HH:mm:ss �� 2008-07-24 12:30:40 ]</font>
																						</td>
																					</tr>
																					<tr>
																						<td align="right" valign="top"><b><font color="#330000">Title  :</font></b></td>
																						<td align="left">	
																							<textarea rows="2" cols="61" name="title_require" maxlength ="250" msgwarning="Please fill title."  class="textfield_style"><%=title%></textarea>
																							<font color="#FF0000"><strong>&nbsp;*</strong></font>
																						</td>
																					</tr>
																					<tr>
																						<td align="right" valign="top"><b><font color="#330000">File Name  :</font></b></td>
																						<td align="left">	
																							<input type="text" name="filename_require" msgwarning="Please fill file name." size="30" class="textfield_style"  value="<%=filename%>" maxlength ="100"><font color="#FF0000"><strong>&nbsp;*</strong></font>&nbsp;<br>
																							<font class="txtnote" color="#330000">[ �ٻẺ��õ�駪������ sc_YYYYMMDDHHmmss �� sc_20080724123040 ]</font>
																						</td>
																					</tr>
																					<tr>
																						<td align="right" valign="top"><b><font color="#330000">Video Clip  :</font></b></td>
																						<td align="left">	
																							<input type="text" name="video_clip" size="10" class="textfield_style"  value="<%=video%>" maxlength = "10">
                                                                     <br><font class="txtnote" color="#330000">[ �����  ������ӹǹ File Video Clip �ҡ���� 0 �������¡��� 5  ]</font>
                                                                     <br><font class="txtnote" color="#330000">[ ��ж�������  ������ӹǹ File Video Clip ��ҡѺ 0 ���ͤ����ҧ  ]</font>
																						</td>
																					</tr>																				
																					<tr>
																						<td align="right" valign="top"><b><font color="#330000">Activate  :</font></b></td>
																						<td align="left">	
																							<select name="activate" size="1"   class="selection_style">
																							   <option value="Y" <%if activate<>"N" then%>selected<%end if%> >Yes</option>
																							   <option value="N" <%if activate="N" then%>selected<%end if%>>No</option>
																							</select><font color="#FF0000"><strong>&nbsp;*</strong></font>
																						</td>
																					</tr>		
																			<!-----------------------------------------------End input data for insert----------------------------------------------------------------->
																					<tr>
																						<td height="5" colspan="2"><BR>
																						</td>
																					</tr>
																					<tr>
																						<td height="5" ><BR></td>
																						<td align="left"  valign="top">																											
																							<!--<input type="submit"  name= "Action" value="UPDATE !!" style="cursor:hand;" >
																							<input type="button"  name="Action" value="CANCEL" onclick="javascript: window.location='ViewIRSociety.asp'">-->
																							
																							<button type="submit" name = "Action" value="1" style="cursor:pointer;">UPDATE !!</button>
																							<button type="button" onclick="javascript: history.back()" name = "Action" value="0" style="cursor:pointer;">CANCEL</button>
																						</td></tr>
																					<tr><td height="5" colspan="2"></td>
																					</tr>
																				</table>
																			</td>
																		</tr>
																			<tr>
																				<td bgcolor="#FFFFFF" colspan="2">
																						<table width="100%"  border="0"   align="center">
																							<td>
																									<font class="stylefont_a"><font color="#0000cc">
																									--->��ͧ���������ͧ���� <font color="#FF0000"><strong>&nbsp;*&nbsp;</strong></font> ���繵�ͧ�բ����� <br><br>
																									
																									</font></font>
																							</td>
																						</table>
																				</td>
																		</tr>																		
																		<tr>
																			<td  align="right" colspan="2">
																					<hr color="#7BC742" size=2>
																			</td>
																		</tr>
																		<tr>
																			<td>
																				<br><input type = "button" value = "Main Menu"  onclick="location='../../main_menu.asp'"><br><br>
																			</td>
																		</tr>
																						
																				<input type="hidden" name="sc_id" value="<%=request("sc_id")%>">																				
																				<input type="hidden" name="old_company" value="<%=old_company%>">
																				<input type="hidden" name="old_interviewee" value="<%=old_interviewee%>">
																				<input type="hidden" name="old_event_timestamp" value="<%=old_event_timestamp%>">
																				<input type="hidden" name="old_title" value="<%=old_title%>">
																				<input type="hidden" name="old_filename" value="<%=old_filename%>">
																				<input type="hidden" name="old_video" value="<%=old_video%>">
																				<input type="hidden" name="old_activate" value="<%=old_activate%>">	
																				<input type="hidden" name="IsPostBack" value="<%=request("IsPostBack")%>">																					
																	</table>
																</td>
															</tr>																		
														</table>
													</form>	
												<td>
											</tr>
										<!--#include file = "../../../i_footer.asp" -->
								</table>
							</td>
						</tr>
				</table>
	<p>&nbsp;</p>
</div>

<%else%>
<br><br><br>
<center><font style="font-size:18pt;color:red"><%response.redirect "../../login.asp"%></font></center>
<%end if%>
</body>
</html>
