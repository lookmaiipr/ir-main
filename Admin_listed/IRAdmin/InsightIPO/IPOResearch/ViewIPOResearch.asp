<!-- #include file = "../../../../i_constant.asp" -->
<!-- #include file = "../../../../includefile_i_contant.asp" -->
<!-- #include file = "../../../../includefile_style.asp" -->
<!--#include file = "../../../../i_conir.asp" -->
<!--#include file = "../../../../formatdate.asp"-->
<!--#include file = "../../../../i_conirauthen_irdb.asp" -->
<!--#include file = "../../../../i_conirauthen.asp" -->
<!--#include file = "../../../global_function.asp" -->

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<title><%=Ucase(WEBSITE_NAME)%>/IPO Research</title>
<meta http-equiv="Content-Type" content="text/html; charset=windows-874">
</head>

<body leftmargin="0" topmargin="5" marginwidth="0" marginheight="5">
<%if session("user_id")<>"" then%>

<%
session("search_company")=trim(request("search_company"))

if request.form("deltype")<>"" and request("del")="Delete"  then
     	arr_type=split(request.form("deltype"),",")
		for i=0 to ubound(arr_type)
				
			    sql ="delete from tbl_trans_ipo_research where research_id=" & arr_type(i)
				conirauthen_irdb.execute(sql)
				
				call Insert_LogAdminActivity(conirauthen,session("user_id"),"DIR",arr_type(i))
		next 	
		conirauthen.close
        conirauthen_irdb.close	   
		response.redirect "ViewIPOResearch.asp"
end if
'##############################################################################
	constpage = 20
	constpageno = 1

	if cint(request("pageno")) = "" or cint(request("pageno")) <= 0 then
		session("pageno") = 1
	else
		session("pageno") = cint(request("pageno"))
	end if

	if request("selectbutton") <> "" then
		session("selectbutton") = trim(replace(request("selectbutton"),",",""))
	else
		session("selectbutton") = ""
	end if

	session("pagesize") = constpage
	
%>

<FORM  name="frm" METHOD=POST ACTION="ViewIPOResearch.asp">
<div align="center" valign="top" > 
	<table width="715" valign="top" >
		<tr>
			<td align="left" valign="top">
				<table width="100%"  border="0" cellpadding="0" cellspacing="1" bgcolor="cfcfcf">
					<tr align="left" valign="top">         
						<td bgcolor="#FFFFFF">
							<table width="100%"  border="0" cellspacing="0" cellpadding="0">
								<tr>
									<td align="left" valign="top">
										 <!-- #include file = "../../../../includefile_i_top.asp" -->		
									</td>
								</tr>
								<tr>
									<td align="left" valign="top">
										<table width="100%"  border="0" cellspacing="10" cellpadding="0">
											<tr align="left" valign="top">
												<td>
													<table width="100%" border="0" cellpadding="0" cellspacing="1" bgcolor="#CC99CC">
														<tr align="left" valign="top">
															<td width="350" valign="middle" bgcolor="#FFFFFF">
																<font class="stylefont"><strong><img src="../../../../images/arrow.gif" width="12" height="9">Insight IPO : <%=session("listed_name")%></strong></font>
															</td>
															<td bgcolor="#Cfcfcf">&nbsp;</td>
														</tr>
													</table>
												</td>
											</tr>	
											<tr align="left" valign="top">
												<td>
													<!--#include file="../InsightIPO_menu.asp"-->
												</td>
											</tr>
											<tr align="left" valign="top">
												<td>
													<!------------------------------- Search Module ------------------------------------->
													<TABLE  cellSpacing="3" cellPadding="3" border="0">
														<TR>
															<td width="50">Company</td>
															<TD>									
																<select name="search_company" size="1" class="selection_style">			
																	<option value="" <%if session("search_company")="" then%>selected<%end if%>>-- Selected Company --</option>				
																		<%
																	   sql="select comp_id, name_th from tbl_master_company where activated='Y' order by name_th asc"
																	   
																	   set rs = conir.execute(sql)
																	   
																		if not rs.eof and not  rs.bof then 
																			do while not rs.eof
																		%>
																			<%if cstr(session("search_company"))=cstr(rs("comp_id")) then%>
																			<option value="<%=rs("comp_id")%>" selected><%=rs("name_th")%></option>
																			<%else%>
																			<option value="<%=rs("comp_id")%>" ><%=rs("name_th")%></option>
																			<%end if%>																								 
																		<%
																				rs.movenext
																			loop																								
																		end if																							   																			   
																	   %>				
																	</select>	
																	<input type="submit" name="action" value="Go" onclick="javascript:return assignpage();" style="FONT-FAMILY: Ms sans serif;FONT-SIZE:10px;cursor:hand;">																   
															</TD>
														</TR>
													</TABLE>
													<!------------------------------- Search Module ------------------------------------->
												</td>																				
											</tr>
												<%												
												select case session("selectbutton")
													case "<< First"
														session("pageno") = 1
													case "< Previous"
														if session("pageno") = 1 then
															session("pageno") = 1
														elseif session("pageno") > 0 then
															session("pageno") = session("pageno") -1
														end if
													case "Next >"
													
														if session("pageno") < session("totalpage") then
															session("pageno") = session("pageno") + 1
														elseif session("pageno") = session("totalpage") then
															session("pageno") = session("totalpage") 
														end if
													case "Last >>"
														session("pageno") = session("totalpage") 
												end select
												'-----------------------------------------------------------------------------------------
												
												sql="select count(*) as num_count from tbl_trans_ipo_research r,tbl_master_company c,tbl_master_source s "
												sql=sql&"where (r.comp_id=c.comp_id and r.source_id=s.source_id) and c.activated='Y' and s.activated='Y' " 												
												if session("search_company") <> "" then
													session("pageno") = 1
													sql = sql & " and r.comp_id = "&session("search_company")&" "
												end if
												
												set rs = conir.execute(sql)
												
												session("num_count") = rs("num_count")
							
												rs.close
												set rs = nothing

												if cint(session("num_count")) mod cint(session("pagesize")) > 0 then
													session("totalpage") = int((cint(session("num_count")) / cint(session("pagesize")))) + 1
												elseif cint(session("num_count")) > 0 then
													session("totalpage") = round(session("num_count") / session("pagesize"))
												else
													session("totalpage") = 1
												end if
												%>			
											<tr>
												<td align="right" valign="top">												
													<table width="100%" border="0" cellpadding="5" cellspacing="1" bgcolor="#FFFFFF">
														<tr>
															<td align="right" valign="top">
																<font class="stylefont">Page :&nbsp;<%=session("pageno")%>/<%=session("totalpage")%></font>
																<Hr color="#4a91d9" size=2>
															</td>
														</tr>
														<!------------------------------------------------End Title------------------------------------------------------------>
														<!------------------------------------------------Data------------------------------------------------------------>
														<tr>
															<td align="left" valign="top" >
																<table width="100%"  border="0" cellspacing="1" cellpadding="2" bgcolor="#F0B0EF" align="center">
																	<tr align="center" valign="bottom" bgcolor="<%= session("bg_in_form")%>">
																		<td width="5%"  valign="middle" ><font class="stylefont_a">
																			<img  src="../../../../images/1588.gif" height="15" width="18"   	onclick="javascript: return select_checkbox();"  style=				      
																			"cursor:hand;" ></font>
																		</td>
																		<td  width="10%"  valign="middle" >
																				<font class="stylefont_a"><font color="#FFFFFF"><b>Date & Time</b></font></font>
																		</td>
																		<td width="20%" valign="middle">
																				<font class="stylefont_a" ><font color="#FFFFFF"><b>Company</b></font></font>
																		</td>
																		<td width="15%" valign="middle">
																				<font class="stylefont_a" ><font color="#FFFFFF"><b>Research By</b></font></font>
																		</td>
																		<td width="20%" valign="middle">
																				<font class="stylefont_a" ><font color="#FFFFFF"><b>Title</b></font></font>
																		</td>
																		<td width="10%" valign="middle">
																				<font class="stylefont_a" ><font color="#FFFFFF"><b>File Name</b></font></font>
																		</td>
																		<td width="10%" valign="middle">
																				<font class="stylefont_a" ><font color="#FFFFFF"><b>Activated</b></font></font>
																		</td>
																	</tr>
														<%
														sql = "select research_id,r.event_timestamp,c.name_th,s.name,r.title,r.file_name,r.activated "
														sql = sql & "from tbl_trans_ipo_research r,tbl_master_company c,tbl_master_source s "
														sql = sql & "where (r.comp_id=c.comp_id and r.source_id=s.source_id) and c.activated='Y' and s.activated='Y' " 														
														if session("search_company") <> "" then
															sql = sql & " and r.comp_id = "&session("search_company")&" "
														end if			
														sql = sql & "order by r.activated desc,r.event_timestamp desc "
														sql=sql&" limit " & ((session("pageno")-1)*session("pagesize")) & "," & session("pagesize")  
																										
														set rs = conir.execute(sql)
														check_all=0													
														if not rs.eof and not  rs.bof then 
															do while not rs.eof
														%>
																	<tr bgcolor="#FFFFFF" valign="top">
																		<td width="5%" valign="top">
																			<center>	
																			  <% if rs("activated")="N" then 
																				 check_all=check_all+1
																			  %>																																										  
																				<input type="checkbox" name="deltype" value="<%=rs("research_id")%>"  style="cursor:hand;">
																				<%end if%>
																			</center>
																		</td>
																		<td  valign="top">
																			<font class="stylefont_a">
																			<%=DateToString(rs("event_timestamp"),"YYYY-MM-DD")%><br>
																			<%=DateToString(rs("event_timestamp"),"HH:mm:ss")%>																			
																			</font>
																		</td>
																		<td  valign="top">
																			<font class="stylefont_a"><%=rs("name_th")%></font>
																		</td>																		
																		<td  valign="top">
																			<font class="stylefont_a"><%=rs("name")%></font>
																		</td>	
																		<td  valign="top">
																			<font class="stylefont_a"><a href="EditIPOResearch.asp?research_id=<%=rs("research_id")%>" class="L">
                                                         <%if rs("title") <> ""  then%>
                                                               <%=replace(rs("title"),vbcrlf,"<br>")%>
                                                         <%else%>
                                                               <%=rs("title")%>
                                                         <%end if%>  
                                                         </a></font>
																		</td>																			
																		<td  valign="top">
																			<font class="stylefont_a"><%=rs("file_name")%></font>
																		</td>			
																		<td  valign="top" align="center">
																			<font class="stylefont_a"><%=rs("activated")%></font>
																		</td>
																	</tr>
														<%
																rs.movenext
															loop
														else%>
															
																	<tr bgcolor="#FFFFFF">
																		<td   colspan="8" align="center"> 
																				<font class="stylefont_a"><font size="3" color="#FF0000">
																			   No Information</font></font>
																		</td>
																	</tr>
														<%																														
														end if	
														conir.close
														%>	
																</table>
															</td>
														</tr>	

														<tr>
															<td bgcolor="#FFFFFF">
																<table width="100%"  border="0"   align="left">
																   <tr>
																		<td>
																			<INPUT TYPE="submit" name="del" value="Delete" onclick="return ask()"  style="FONT-FAMILY: Ms sans        
																			serif;FONT-SIZE:10px; font-weight: bold;cursor:hand;">
																			<INPUT TYPE="button" name="Add" value="Add New" onclick = "location ='AddNewIPOResearch.asp'" style="FONT-FAMILY: Ms sans serif;FONT-SIZE:10px; font-weight: bold;cursor:hand;">
																		</td>
																	</tr>
																</table>
															</td>
														<tr>
															<td bgcolor="#FFFFFF">
																	<table width="100%"  border="0"   align="center">
																		<td>
																				<font class="stylefont_a"><font color="#0000cc">
																				 --->��ԡ������ Add New ����Ѻ������� <br>
																				 --->�����͡ IPO Research ���Ǥ�ԡ������ delete ����Ѻ���ź   <br>
																				 --->��ԡ��� Title ����Ѻ������
																				</font></font>
																		</td>
																	</table>
															</td>
														</tr>
														
														<tr>
															<td  align="right">
																	<hr color="#4a91d9" size=2>
															</td>
														</tr>
														<tr>
															<td>
																	<!-------------------------------------------------Button------------------------------------------------------------>
																	<font size="1" face="MS Sans Serif">
																		<input type = "hidden" name = "totalpage" value="<%=session("totalpage")%>">	
																		<input type = "hidden" name = "pageno" value="<%=session("pageno")%>">
																		<input type = "hidden" name = "check_all" value="<%=check_all%>">
																		<input type = "button" value = "Main Menu"  onclick="location='../../../main_menu.asp'" style="FONT-FAMILY: Ms sans 
																		serif;FONT-SIZE:10px; font-weight: bold;cursor:hand;">
																		<%if session("pageno") <> 1 then %>
																			<input type = "submit" name = "selectbutton" value = "<< First" style="FONT-FAMILY: Ms sans serif;FONT-SIZE:10px; 
																			font-weight: bold;cursor:hand;">
																			<input type = "submit" name = "selectbutton" value = "< Previous" style="FONT-FAMILY: Ms sans serif;FONT-SIZE:10px; 
																			font-weight: bold;cursor:hand;">
																		<%else%>
																			<input type = "submit" name = "selectbutton" value = "<< First" disabled style="FONT-FAMILY: Ms sans 
																			serif;FONT-SIZE:10px; 
																			font-weight: bold;cursor:hand;">
																			<input type = "submit" name = "selectbutton" value = "< Previous" disabled style="FONT-FAMILY: Ms sans 
																			serif;FONT-SIZE:10px; font-weight: bold;cursor:hand;">
																		<%end if%>
																		<% if session("pageno") <> session("totalpage") then%>
																			<input type = "submit" name = "selectbutton" value = "Next >" style="FONT-FAMILY: Ms sans serif;FONT-SIZE:10px; 
																			font-weight: bold;cursor:hand;">
																			<input type = "submit" name = "selectbutton" value = "Last >>" style="FONT-FAMILY: Ms sans serif;FONT-SIZE:10px; 
																			font-weight: bold;cursor:hand;">
																		<%else%>
																			<input type = "submit" name = "selectbutton" value = "Next >" disabled style="FONT-FAMILY: Ms sans serif;FONT-SIZE:10px; 
																			font-weight: bold;cursor:hand;">
																			<input type = "submit" name = "selectbutton" value = "Last >>" disabled style="FONT-FAMILY: Ms sans 
																			serif;FONT-SIZE:10px; 
																			font-weight: bold;cursor:hand;">
																		<%end if%>
																	</font>
															<!-------------------------------------------------End Button------------------------------------------------------------>
														   </td>
														</tr>	
													</table>
												</td>
											</tr>
										</table>
									</td>
								</tr>
								<!--#include file = "../../../../i_footer.asp" -->
							</table>
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>
</div>
</form>
<DIV id="calendar1" style="Z-INDEX: 101; VISIBILITY: hidden; POSITION: absolute; BACKGROUND-COLOR: white; layer-background-color: white"></DIV>
<script language="JavaScript">
<!--
	//event when click checkbox
     function select_checkbox(){		
       //alert(count)	
	   var count=document.frm.check_all.value;
	   //alert(count);
	   if(count > 0 ){
	              if (count >1){
						for (i=0;i<count;i++){				
						   if(document.frm.deltype(i).checked==true){
								document.frm.deltype(i).checked=false;	
						   }
						   else{
									document.frm.deltype(i).checked=true;	
							}
						}
				  }else{
				          if(document.frm.deltype.checked==true){
								document.frm.deltype.checked=false;	
						   }
						   else{
									document.frm.deltype.checked=true;	
							}
					}	
				} 
		}
	
		// event when click delete button
		function ask(){
			//alert(countrec)
			var count=document.frm.check_all.value;
			// alert(count);
			var Ischecked =false;
			if(count >0 ){
				 if (count!=1){
						for (i=0;i<count;i++){
										if(frm.deltype(i).checked==true){
											 Ischecked=true;
											break;
									   }														   
							}
				 }else{
					   if(frm.deltype.checked==true){
							Ischecked=true;	
						 }
				 }
			}
	
		 if((count > 0 ) && ( Ischecked==true) ) {
					 if(!confirm("Do you want to delete them?")){
								 if(count!=1){
										   for (i=0;i<count;i++){
																if(frm.deltype(i).checked==true){
																	  frm.deltype(i).checked=false;	
															   }														   
													}
									}else{
										   if(frm.deltype.checked==true){
												 frm.deltype.checked=false;	
											 }
										}
				return false;
				}
				else {
						return true;
				}
		 }
		else{return false;
		}	
	}

	// event when click go button
	function assignpage(KeyCode){     
	    if(KeyCode==13){
					frm.totalpage.value	=1;
					frm.pageno.value	=1;
			//alert(frm.search.value);
			}
	}
//-->
</script>
<%
else
	response.redirect "../../../login.asp"
end if
%>
</body>
</html>
