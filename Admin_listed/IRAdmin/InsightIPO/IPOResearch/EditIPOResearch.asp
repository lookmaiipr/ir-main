<!-- #include file = "../../../../i_constant.asp" -->
<!-- #include file = "../../../../includefile_i_contant.asp" -->
<!-- #include file = "../../../../includefile_style.asp" -->
<!--#include file = "../../../../i_conir.asp" -->
<!--#include file = "../../../../i_conirauthen_irdb.asp" -->
<!--#include file = "../../../../i_conirauthen.asp" -->
<!--#include file = "../../../global_function.asp" -->

<%
'****************** Keep Request value *******************
if request("company_require")="" then
	session("company")=""
else
	session("company")=request("company_require")
end if

if request("source_require")="" then
	session("source")=""
else
	session("source")=request("source_require")
end if

if request("event_timestamp_require") <>"" then
	session("event_timestamp")=request("event_timestamp_require")
else
	session("event_timestamp")=DateToString(now(),"YYYY-MM-DD HH:mm:ss")
end if

if request("title_require")="" then
	session("title")=""
else
	session("title")=request("title_require")
end if

if request("filename_require") <>"" then
	session("filename")=request("filename_require")
else
	session("filename")="ir_" & DateToString(now(),"YYYYMMDDHHmmss") & ".pdf"
end if

if request("activate_require") ="" then
	session("activate")=""	
else
	session("activate")=request("activate_require")
end if
'****************** Keep Request value *******************

if  request("Action") =  "UPDATE !!"  then			
			sql="update tbl_trans_ipo_research set title='"&request("title_require")&"',file_name='"&request("filename_require")&"'"
			sql=sql&",event_timestamp='"&request("event_timestamp_require")&"',activated='"&request("activate")&"' "
			sql=sql&",updated_timestamp=now(),user_id='"&session("user_id")&"',comp_id="&request("company_require")&",source_id="&request("source_require")&""
			sql=sql&" where research_id=" & request("research_id")
				
			conirauthen_irdb.execute(sql)
		
			call Insert_LogAdminActivity(conirauthen,session("user_id"),"EIR",request("research_id"))
			
			conirauthen_irdb.close
			conirauthen.close	
%>
			<script language="javascript">
				alert( "Updated ipo research completely.");
				window.location.href="ViewIPOResearch.asp";										
			</script>
<%
else
	if request("Action")="Cancel" then
		conirauthen.close
		response.redirect "ViewIPOResearch.asp"
	end if
end if
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<title><%=Ucase(WEBSITE_NAME)%>/Edit IPO Showoff</title>
<meta http-equiv="Content-Type" content="text/html; charset=windows-874">
<script language="JavaScript" src = "../../../irthai_function.js"></script>
<script language="JavaScript" src = "../../../../validationCheck.js"></script>
<script language="JavaScript">
	function OnSubmitFrm(formname)
	{	
		if (validateFormOnSubmit(formname)==false)	{return false;	}
		if(!CheckDates(1,document.frm.event_timestamp_require.value)){	 alert("Date & time is invalid!.");	 return false;}		
		
		var filename = (document.frm.filename_require.value).toLowerCase();
		if(!IsConsistOf("ir_",filename))  { alert('File name is invalid!.');	 return false;}		
		
		var filename_date = filename.replace("ir_",""); 
		filename_date = filename_date.substring(0,filename_date.indexOf("."));
		if(!CheckDates(2,filename_date)){ alert('File name is invalid!.');	 return false;}
		if(!IsConsistOf(".doc|.xls|.pdf|.zip",filename))  { alert('File name is invalid!.');	 return false;}				
		// The Last Condition : All data is valid. //
		return true;
	}
	
	function checkvalue()
	{
		if (document.frm.old_company.value!=document.frm.company_require.value){return true;	}
		else if (document.frm.old_source.value!=document.frm.source_require.value)	{return true;	}
		else if (document.frm.old_event_timestamp.value!=document.frm.event_timestamp_require.value)	{return true;	}
		else if (document.frm.old_title.value!=document.frm.title_require.value)	{return true;	}
		else if (document.frm.old_filename.value!=document.frm.filename_require.value)	{return true;	}
		else if (document.frm.old_activate.value!=document.frm.activate.value)	{return true;	}
		else{return false;}
	}
	
	
</script>

</head>
<body leftmargin="0" topmargin="5" marginwidth="0" marginheight="5">
<%if session("user_id")<>"" then%>
<div align="center">  	
	<table width="715" valign="top" border="0" cellspacing="0" cellpadding="0" >
	<tr>
	  <td align="left" valign="top">
			<table width="100%"  border="0" cellpadding="0" cellspacing="0" bgcolor="cfcfcf">
				<tr align="left" valign="top">         
						<td bgcolor="#FFFFFF">
								<table width="100%"  border="0" cellspacing="0" cellpadding="0">
										<tr>
												<td align="left" valign="top" >						
														   <!-- #include file = "../../../../includefile_i_top.asp" -->
												</td>
											</tr>			
											<tr>
											  <td align="left" valign="top" bgcolor="#FFFFFF" >
														<form id="frm" name="frm" method="post" action="EditIPOResearch.asp?research_id=<%=request("research_id")%>" onsubmit="javascript:if(checkvalue()){return OnSubmitFrm('frm');}else{return false;};">
														<table width = "100%" align="center" cellpadding=0 cellspacing="10" >
															<tr align="left" valign="top">
																<td ><br>
																	<table width="100%" border="0" cellpadding="0" cellspacing="1" bgcolor="#CC99CC">
																		<tr align="left" valign="top">
																			<td width="350" valign="middle" bgcolor="#FFFFFF"><font class="stylefont"><strong><img src="../../../../images/arrow.gif" width="12" height="9">Insight IPO : <%=session("listed_name")%></strong></font></td>
																			<td bgcolor="#cfcfcf">&nbsp;</td>
																		</tr>
																	</table><br>
																</td>
															</tr>
															<tr align="left" valign="top">
																<td>
																	<!--#include file="../InsightIPO_menu.asp"-->
																</td>
															</tr>
														<tr>
																<td  align="center" valign="top"  width="100%" bgcolor="#FFFFFF">
																	<table width="90%" border="0" cellpadding=0 cellspacing=0  bordercolor="#000066"  align="center">			
																		<tr>
																			<td>
																				<table width="100%" border="0" cellpadding=0 cellspacing=3  style="border:5px solid #FAF8F5" bgcolor="#F0DDF0" align="center">			
																					<tr>
																						<td>&nbsp;<b><font face="MS Sans Serif"  color="#330000">Edit IPO Research</font></b></td>
																					</tr>
																				</table>
																			</td>
																		</tr>
																		<tr>
																			<td>
																				<table border="0" cellpadding=5 cellspacing=0  align="center" width="100%" bgcolor="#FAF8F5">	
																				<!-----------------------------------------------input data for insert----------------------------------------------------------------->
																				<%																			
																				if request("IsPostBack") <> "true" then					
																						sql="select * from tbl_trans_ipo_research where research_id=" & request("research_id")
																																										
																						set rs = conir.execute(sql)
																										
																						if not rs.bof and not rs.eof   then																							
																							company=rs("comp_id")
																							source=rs("source_id")
																							event_timestamp=DateToString(rs("event_timestamp"),"YYYY-MM-DD HH:mm:ss")
																							title=rs("title")
																							filename=rs("file_name")
																							activate=rs("activated")
																							
																							old_company=rs("comp_id")
																							old_source=rs("source_id")
																							old_event_timestamp=DateToString(rs("event_timestamp"),"YYYY-MM-DD HH:mm:ss")
																							old_title=rs("title")
																							old_filename=rs("file_name")
																							old_activate=rs("activated")
																						end if
																				else
																							company=request("company_require")
																							interviewee=request("source_require")
																							event_timestamp=DateToString(request("event_timestamp_require"),"YYYY-MM-DD HH:mm:ss")
																							title=request("title_require")
																							filename=request("filename_require")
																							activate=request("activate")			

																							old_company=request("old_company")
																							old_interviewee=request("old_source")
																							old_event_timestamp=DateToString(request("old_event_timestamp"),"YYYY-MM-DD HH:mm:ss")
																							old_title=request("old_title")
																							old_filename=request("old_filename")
																							old_activate=request("old_activate")																										
																				end if
																				%>			
																					<tr><td>&nbsp;</td><td></td></tr>																					
																					<tr>
																						<td align="right" valign="top"><b><font face="MS Sans Serif"  color="#330000">Company  :</font></b></td>
																						<td align="left">
																							<select name="company_require"  msgwarning="Please selected company." size="1"   class="selection_style">
																							   <option value="" selected>--- Selected Company ---</option>
																							   <%
																							   sql="select comp_id, name_th from tbl_master_company where activated='Y' order by name_th asc"
																							   
																							   set rs = conir.execute(sql)
																							   
																								if not rs.eof and not  rs.bof then 
																									do while not rs.eof
																								%>
																									<%if cstr(company)=cstr(rs("comp_id")) then%>
																									<option value="<%=rs("comp_id")%>" selected><%=rs("name_th")%></option>
																									<%else%>
																									<option value="<%=rs("comp_id")%>" ><%=rs("name_th")%></option>
																									<%end if%>																								 
																								<%
																										rs.movenext
																									loop																								
																								end if																							   																			   
																							   %>																							   
																							</select><font color="#FF0000"><strong>&nbsp;*</strong></font>
																						</td>
																					</tr>			
																					<tr>
																						<td align="right" valign="top"><b><font face="MS Sans Serif" size="2" color="#330000">Research By  :</font></b></td>
																						<td align="left">	
																							<select name="source_require"  msgwarning="Please selected research by." size="1"   class="selection_style">
																							   <option value="" selected>--- Selected Research By ---</option>
																								<%
																							   sql="select * from tbl_master_source where activated = 'Y'   order by name asc "																							
																							   set rs = conir.execute(sql)
																							   
																								if not rs.eof and not  rs.bof then 
																									do while not rs.eof
																								%>
																									<%if cstr(source)=cstr(rs("source_id")) then%>
																									<option value="<%=rs("source_id")%>" selected><%=rs("name")%></option>
																									<%else%>
																									<option value="<%=rs("source_id")%>"><%=rs("name")%></option>
																									<%end if%>																								 
																								<%
																										rs.movenext
																									loop																								
																								end if																							   																			   
																							   %>															   
																							</select>
																							<font color="#FF0000"><strong>&nbsp;*</strong></font>
																						</td>
																					</tr>			
																					
																					<tr>
																						<td align="right" valign="top"><b><font face="MS Sans Serif" size="2" color="#330000">Date & Time  :</font></b>
																						</td>
																						<td align="left">	
																							<input type="text" name="event_timestamp_require" msgwarning="Please fill date & time." size="30" class="textfield_style"  value="<%=event_timestamp%>" maxlength = "19"><font color="#FF0000"><strong>&nbsp;*</strong></font><br>
																							<font face="MS Sans Serif" size="1" color="#330000">[ �ٻẺ�ѹ��� YYYY-MM-DD HH:mm:ss �� 2008-07-24 12:30:40  ]</font>
																						</td>
																					</tr>
																					<tr>
																						<td align="right" valign="top"><b><font face="MS Sans Serif" size="2" color="#330000">Title  :</font></b></td>
																						<td align="left">	
																							<textarea rows="2" cols="61" name="title_require" maxlength ="250" msgwarning="Please fill title."  class="textfield_style"><%=title%></textarea>
																							<font color="#FF0000"><strong>&nbsp;*</strong></font>
																						</td>
																					</tr>
																					<tr>
																						<td align="right" valign="top"><b><font face="MS Sans Serif" size="2" color="#330000">File Name  :</font></b></td>
																						<td align="left">	
																							<input type="text" name="filename_require" msgwarning="Please fill file name." size="30" class="textfield_style"  value="<%=filename%>" maxlength ="100"><font color="#FF0000"><strong>&nbsp;*</strong></font>&nbsp;<br>
																							<font face="MS Sans Serif" color="#330000" size="1">[ �ٻẺ��õ�駪������ ir_YYYYMMDDHHmmss.{doc,xls,pdf,zip} �� ir_20080925173052.pdf  ]</font>
																						</td>
																					</tr>												
																					<tr>
																						<td align="right" valign="top"><b><font face="MS Sans Serif" size="2" color="#330000">Activate  :</font></b></td>
																						<td align="left">	
																							<select name="activate" size="1"   class="selection_style">
																							   <option value="Y" <%if activate<>"N" then%>selected<%end if%> >Yes</option>
																							   <option value="N" <%if activate="N" then%>selected<%end if%>>No</option>
																							</select><font color="#FF0000"><strong>&nbsp;*</strong></font>
																						</td>
																					</tr>		
																			<!-----------------------------------------------End input data for insert----------------------------------------------------------------->
																					<tr>
																						<td height="5" colspan="2"><BR>
																						</td>
																					</tr>
																					<tr>
																						<td height="5" ><BR></td>
																						<td align="left"  valign="top">																											
																							<input type="submit"  name= "Action" value="UPDATE !!" style="cursor:hand;" >
																							<input type="button"  name="Action" value="CANCEL" onclick="javascript: window.location='ViewIPOResearch.asp'"  style="cursor:hand;" >
																						</td></tr>
																					<tr><td height="5" colspan="2"></td>
																					</tr>
																				</table>
																			</td>
																		</tr>
																			<tr>
																				<td bgcolor="#FFFFFF" colspan="2">
																						<table width="100%"  border="0"   align="center">
																							<td>
																									<font class="stylefont_a"><font color="#0000cc">
																									--->��ͧ���������ͧ���� <font color="#FF0000"><strong>&nbsp;*&nbsp;</strong></font> ���繵�ͧ�բ����� <br><br>
																									
																									</font></font>
																							</td>
																						</table>
																				</td>
																		</tr>
																		<tr>
																			<td  align="right" colspan="2">
																					<hr color="#4a91d9" size=2>
																			</td>
																		</tr>
																		<tr>
																			<td >
																						<input type = "button" value = "Main Menu"  onclick="location='../../../main_menu.asp'" style="FONT-FAMILY: Ms sans 
																		serif;FONT-SIZE:10px; font-weight: bold;cursor:hand;">
																			</td>
																		</tr>		
																									
																	</table>
																</td>
															</tr>																																
																<input type="hidden" name="research_id" value="<%=request("research_id")%>">																				
																<input type="hidden" name="old_company" value="<%=old_company%>">
																<input type="hidden" name="old_source" value="<%=old_source%>">
																<input type="hidden" name="old_event_timestamp" value="<%=old_event_timestamp%>">
																<input type="hidden" name="old_title" value="<%=old_title%>">
																<input type="hidden" name="old_filename" value="<%=old_filename%>">
																<input type="hidden" name="old_activate" value="<%=old_activate%>">	
																<input type="hidden" name="IsPostBack" value="<%=request("IsPostBack")%>">															
														</table>
													</form>	
												<td>
											</tr>
										<!--#include file = "../../../../i_footer.asp" -->
								</table>
							</td>
						</tr>
				</table>	
	<p>&nbsp;</p>
</div>
<%else%>
<br><br><br>
<center><font style="font-size:18pt;color:red"><%response.redirect "../../../login.asp"%></font></center>
<%end if%>
</body>
</html>
