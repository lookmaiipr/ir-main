<!-- #include file = "../i_constant.asp" -->
<!-- #include file = "../includefile_i_contant.asp" -->
<!-- #include file = "../includefile_style.asp" -->	
<!-- #include file = "../i_conirauthen.asp" -->
<!-- #include file = "../i_conpsims.asp" -->
<!-- #include file = "../i_conprs.asp" -->
<!--#include virtual="/ir/formatdate.asp"-->
<!--#include file = "../i_listed_member.asp" -->

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<%
session("search_ID")=trim(request("Get_listedID"))
session("search_Name")=trim(request("Get_listedName"))

Function ListedName
   if listed_member_guest_id <> ""  then
      sql = "SELECT listed_id,share from listed_member where listed_id in(" & listed_member_guest_id & ") order by  share"
      set rs =conirauthen.execute(sql)
      ListedName = ""
      if session("search_ID") <> "" then
            do while not rs.eof 
               if trim(session("search_ID"))=trim(rs("listed_id")) then
                     ListedName = ListedName & " <option value=" & rs("listed_id") & ":" & rs("share") &" selected> "& rs("share") & "</option> "
               else
                     ListedName = ListedName & " <option value=" & rs("listed_id") & ":" & rs("share") & "> "& rs("share") &" </option> "
               end if
               rs.movenext
            loop                                                          
      else
            do while not rs.eof  
               ListedName = ListedName & " <option value="& rs("listed_id") & ":" & rs("share") &"> " & rs("share") &" </option> "
               rs.movenext
            loop
       end if
        rs.close      
   end if
End Function

Function StringListedName
	if strListedName <> "" then
		StringListedName = strListedName
	else 
		StringListedName = strListedName
	end if
End Function

Function GetSecID
   if session("search_ID") <> "" then
      strSql= ""
		strSql = " select sec_id from Compsec where com_id = '"& session("search_ID") &"' and sec_type ='S'  "
      strSql =strSql & " and flag<>'D' and sector_no <> '0' and sec_list_status = 'L' and (mk_type='A' or mk_type='S') "
		set rs = conpsims.execute(strSql)
		if not rs.eof and not rs.bof then
			GetSecID = rs("sec_id")
		end if
		rs.close
   end if
End Function

Function GetIndex
   if sec_id <> "" then
      strSql= ""
	  	'------------------------- Revise support new psims - by Kung :: 22/08/2012 10:56:00 ----------------------'
		'strSql = " select mk_type,sector_no from D_Stock where sec_id='"& sec_id &"' order by date desc limit 1 "
		strSql = " select mk_type,sector_no from d_stat where sec_id='"& sec_id &"' order by date desc limit 1 "
		'-------------------------------------------------------------------------------------------------------------------------
		
		set rs = conpsims.execute(strSql)
		if not rs.eof and not rs.bof then
			GetIndex = rs("mk_type") & ":" & rs("sector_no") 
		end if
		rs.close
   end if
End Function

Function GetSector
   if sector_no <> "" then
      strSql= ""
		strSql = " select  n_sector FROM Sector where i_sector='"& sector_no &"' "
		set rs = conpsims.execute(strSql)
		if not rs.eof and not rs.bof then
			GetSector = rs("n_sector") 
		end if
		rs.close   
   end if
End Function

Sub LoadStockPrice(index)
   strSql= ""
   strSql = " select year(timestamp) as cur_year, max(unix_timestamp(timestamp)+closeprice) -max(unix_timestamp(timestamp) ) as close "
   strSql = strSql & " from daily_price where share = '"&index&"' group by year(timestamp) order by cur_year desc limit 5 "
   set rs = conprs.execute(strSql) 
   if not rs.eof and not rs.bof then
      i = 0
      do while not rs.eof			
			ArrYearlyClose(i,0) = rs("cur_year")
			ArrYearlyClose(i,1) = rs("close")      
         cur_year = rs("cur_year")
         i = i + 1
         rs.movenext
      loop     
      rs.close
   end if
End sub

Function PerCentChg(c1,c2)
   dim compare1
   dim compare2
   compare1 = c1
   compare2 = c2

   pchg = "" 
   if compare1 <> "" then
      if compare2 = "" then
         compare2 = GetIPOPrice
      end if
      if compare2 <> 0 then
         pchg = ((compare1 - compare2)/compare2) * 100
      end if            
   end if
   
   if cstr(pchg) = "" then
      PerCentChg = "-"
   else
      PerCentChg = pchg
   end if
         
End Function

Function GetIPOPrice
   if sec_id <> "" then
      strSql= ""
		strSql = " select  ipo_price,trading_date from Compsec where sec_id = '"& sec_id &"' and sec_type ='S'  "
      strSql =strSql & " and flag<>'D' and sector_no <> '0' and sec_list_status = 'L' and (mk_type='A' or mk_type='S') and year(trading_date)='"&cur_year&"' order by trading_date asc "
		set rs = conpsims.execute(strSql)
		if not rs.eof and not rs.bof then
			GetIPOPrice = rs("ipo_price")
		end if
		rs.close
   end if
End Function

Function WriteText(text)
	if cstr(text) <> "" then
		if IsNumeric(text) =  true then
			WriteText = "<font class='style49'>" & FormatNumber(text,,-1) & "%</font>"
		else
			WriteText = "<font class='style49'>" & text & "</font>"
		end if		
	else
		WriteText = "<font class='style49'>-</font>"
	end if
End Function
%>
<title>Board Report :: Stock Price Performance</title>
<meta http-equiv="Content-Type" content="text/html; charset=windows-874">
<!-- #include file = "../includefile_style.asp" -->
</head>
<body leftmargin="0" topmargin="5" marginwidth="0" marginheight="5">
<%if session("user_id")<>"" then%>
<form name="frm" method="post" action="stockprice_performance_reporter.asp">
<div align="center" valign="top" > 
		<table width="715" valign="top"  border="0" cellpadding="0" cellspacing="0">
			<tr>
			  <td align="left" valign="top">
						<table width="100%"  border="0" cellpadding="0" cellspacing="1" bgcolor="#cfcfcf">
								<tr align="left" valign="top">         
										<td bgcolor="#FFFFFF">
												<table width="100%"  border="0" cellspacing="0" cellpadding="0">
														<tr>
																<td align="left" valign="top">
																     <!-- #include virtual = "/ir/includefile_i_top.asp" -->		
																</td>
														</tr>
														<tr><td>
															<table width="100%" cellpadding="0" cellspacing="0" border="0">
																<tr  bgcolor="#F5F5F5" border="0"style="border:1px solid #FFFFFF;">
																	<td>
																		<table cellpadding="5" cellspacing="0" border="0">
																			<tr valign="baseline" style="FONT-FAMILY: Ms sans serif;FONT-SIZE:1;cursor:hand;">
																				<td>			
																						<a href="InvestorRelationsMetrics_reporter.asp" class="L">Investor Relations Metrics</a>			
																				</td>
																				<td>||	</td>
																				<td>			
																						<a href="stockprice_performance_reporter.asp" class="L">Stock Price Performance</a>					
																				</td>
																				<td>||	</td>		
																				<td>					
																						<a href="stockholder_composition_reporter.asp" class="L">Stockholder Composition</a>		
																				</td>					
																				<td>||	</td>		
																				<td>
																					<a href="analyst_coverage.asp" class="L">Analyst Coverage (SAA Consensus)</a>																				
																				</td>
																			</tr>
																		</table>
																	</td>
																</tr>
															</table>
															</td>			
														</tr>														
														<tr>
																<td align="left" valign="top">
																		<table width="100%"  border="0" cellspacing="10" cellpadding="0">
																			<tr align="left" valign="top">
																				<td>
																						<table width="100%" border="0" cellpadding="0" cellspacing="1" bgcolor="<%= session("bg_in_form")%>">
																						  <tr align="left" valign="top">
																								<td width="350" valign="middle" bgcolor="#FFFFFF"><font class="stylefont"><strong><img src="../images/arrow.gif" width="12" height="9"> Stock Price Performance : <%=session("listed_name") %></strong></font></td>
																								<td bgcolor="<%= session("bg_in_form")%>">&nbsp;</td>
																						  </tr>
																						</table>
																				</td>																				
																			</tr>
                                                         <tr align="left" valign="top">
                                                            <td>
                                                               <!------------------------------- Search Module ------------------------------------->
                                                               <TABLE  cellSpacing="1" cellPadding="1" border="0">
                                                                  <TR>
                                                                     <td width="100"><font class="stylefont_a" ><font color="#000000"><b>Listed Member</b></font></font></td>
                                                                     <TD>									
                                                                           <%strListedName = ListedName%>
                                                                           <select id="search_ListedMember" name="search_ListedMember" size="1">
                                                                           <option <%if session("search_ID")="" then%>selected<%end if%>>- - Select Listed - -</option>
                                                                                 <%=StringListedName%>
                                                                           </select>&nbsp;&nbsp;                                                                
                                                                           <input type="submit" name="action" value="Go" onclick="javascript:return assignpage(frm.search_ListedMember.value);"  style="FONT-FAMILY: Ms sans serif;FONT-SIZE:10px;cursor:hand;">
                                                                           <input type="hidden" name="Get_listedID" value="<%=session("search_ID")%>">
                                                                           <input type="hidden" name="Get_listedName" value="<%=session("search_Name")%>">
                                                                     </TD>
                                                                  </TR>
                                                               </TABLE>
                                                               <!------------------------------- Search Module ------------------------------------->
                                                            </td>																				
                                                         </tr>          
																			<tr>
																					<td align="left" valign="top">
																							<table width="100%" border="0" cellpadding="1" cellspacing="1" bgcolor="#FFFFFF">
																								<tr>
																										<td align="left" valign="top" bgcolor="#FFFFFF">
																										   <table width="100%"  border="0" cellspacing="1" cellpadding="5">                                                                                 
                                                                                       <tr align="center" valign="bottom" >
                                                                                          <td>
                                                                                                <!-- ++++++++++++++++++++++++ CONTENT ++++++++++++++++++++++++ -->
                                                                                                <%if session("search_ID")  <> "" then%>
                                                                                                   <table width="100%"  border="0" cellspacing="1" cellpadding="5"   bgcolor="<%=session("bg_in_form")%>">
                                                                                                   <%
                                                                                                      sec_id = GetSecID
                                                                                                       if sec_id <> ""  then
                                                                                                         arr_Index=split(GetIndex,":")     
                                                                                                         mk_type = arr_Index(0)
                                                                                                         if mk_type = "S" then
                                                                                                            mk_type = "MAI"
                                                                                                         elseif mk_type = "A" then
                                                                                                            mk_type = "SET"
                                                                                                         end if
                                                                                                         sector_no = arr_Index(1)
                                                                                                         Dim ArrYearlyClose(5,2)
                                                                                                         Dim cur_year
                                                                                                         Call LoadStockPrice(session("search_Name"))
                                                                                                         
                                                                                                         cur_year = cur_year
                                                                                                      end if
                                                                                                   %>
                                                                                                      <tr align="center" valign="bottom"   bgcolor="<%=session("bg_in_form")%>">
                                                                                                               <td align="left" >
                                                                                                                     <font class="style_in_form" >&nbsp;</font>
                                                                                                               </td>
                                                                                                               <%
                                                                                                               i=0
                                                                                                               for i=0 to 3
                                                                                                                  %>
                                                                                                                  <td width="17%">
                                                                                                                     <font class="style_in_form" ><%=year(now)-i%></font>
                                                                                                                  </td>                                                                                                  
                                                                                                                  <%
                                                                                                               next
                                                                                                                                                                                   
                                                                                                               %>                                                                     
                                                                                                             </tr>
                                                                                                             <% if sec_id <> "" then%>
                                                                                                                <tr bgcolor="#FFFFFF">
                                                                                                                   <td align="left"><strong class="style49"><%=session("search_Name")%></strong></td>  
                                                                                                                   <td align="right"><%= WriteText(PerCentChg(ArrYearlyClose(0,1),ArrYearlyClose(1,1)))%></td>      
                                                                                                                   <td align="right"><%= WriteText(PerCentChg(ArrYearlyClose(1,1),ArrYearlyClose(2,1)))%></td>        
                                                                                                                   <td align="right"><%= WriteText(PerCentChg(ArrYearlyClose(2,1),ArrYearlyClose(3,1)))%></td>          
                                                                                                                   <td align="right"><%= WriteText(PerCentChg(ArrYearlyClose(3,1),ArrYearlyClose(4,1)))%></td>
                                                                                                               </tr>
                                                                                                               <%                                                                          
                                                                                                                Call LoadStockPrice(mk_type)
                                                                                                               %>
                                                                                                                <tr bgcolor="#FFFFFF">
                                                                                                                   <td align="left">
                                                                                                                        <%if  mk_type="MAI" then%>
                                                                                                                           <strong class="style49">MAI Index</strong>
                                                                                                                        <%elseif  mk_type = "SET" then%>
                                                                                                                           <strong class="style49">SET Index</strong>
                                                                                                                        <%end if%>
                                                                                                                   </td>  
                                                                                                                   <td align="right"><%= WriteText(PerCentChg(ArrYearlyClose(0,1),ArrYearlyClose(1,1)))%></td>      
                                                                                                                   <td align="right"><%= WriteText(PerCentChg(ArrYearlyClose(1,1),ArrYearlyClose(2,1)))%></td>        
                                                                                                                   <td align="right"><%= WriteText(PerCentChg(ArrYearlyClose(2,1),ArrYearlyClose(3,1)))%></td>          
                                                                                                                   <td align="right"><%= WriteText(PerCentChg(ArrYearlyClose(3,1),ArrYearlyClose(4,1)))%></td>                                                                                                           
                                                                                                               </tr>    
                                                                                                         <%if  mk_type="SET" then        
                                                                                                               sector_no = GetSector
                                                                                                               Call LoadStockPrice(sector_no)
                                                                                                               %>
                                                                                                                   <tr bgcolor="#FFFFFF">
                                                                                                                      <td align="left"><strong class="style49"><%=sector_no%> Sector</strong>
                                                                                                                      </td>  
                                                                                                                      <td align="right"><%= WriteText(PerCentChg(ArrYearlyClose(0,1),ArrYearlyClose(1,1)))%></td>      
                                                                                                                      <td align="right"><%= WriteText(PerCentChg(ArrYearlyClose(1,1),ArrYearlyClose(2,1)))%></td>        
                                                                                                                      <td align="right"><%= WriteText(PerCentChg(ArrYearlyClose(2,1),ArrYearlyClose(3,1)))%></td>          
                                                                                                                      <td align="right"><%= WriteText(PerCentChg(ArrYearlyClose(3,1),ArrYearlyClose(4,1)))%></td>                                                                                                           
                                                                                                                  </tr>                                                                                            
                                                                                                          <%end if%>                                                                                                         
                                                                                                     <%else%>
                                                                                                             <tr bgcolor="#FFFFFF">
                                                                                                                <td align="center" colspan="5"><strong class="style49"><font color="#FF0000">No Information</font></strong></td>
                                                                                                            </tr>                                                                                                                
                                                                                                     <%end if%>
                                                                                                 </table>         
																																<%else%>
																																<br>
																																<p>Please Select Listed Company for Load Information.</p>
                                                                                                <br>
																																<%end if%>
                                                                                              <!-- ++++++++++++++++++++++++ END CONTENT ++++++++++++++++++++++++ -->
                                                                                           </td>
                                                                                        </tr>
																											 </table>
																									</td>
																								</tr>
                                                                        <tr>
                                                                           <td  align="right">
                                                                                 <hr color="<%= session("bg_in_form")%>" size=2>
                                                                           </td>
                                                                        </tr>                                                                             
																								<tr>
                                                                           <td>
                                                                              <input type = "button" value = "Main Menu"  onclick="location='main_menu.asp'" style="FONT-FAMILY: Ms sans serif;FONT-SIZE:10px; cursor:hand;">
                                                                           </td>
                                                                         </tr>
																							</table>
																					</td>
																				</tr>
																		</table>
																</td>
														</tr>
													<!--#include virtual = "/ir/i_footer.asp" -->
												</table>
										</td>
								</tr>
						</table>
				</td>
			</tr>
		</table>

</div>
</form>
<script language="JavaScript">
<!--
	// event when click go button
	function assignpage(name){     
   
            var Arr=name.split(":");
            if (Arr.length == 2){
  				frm.Get_listedID.value	= Arr[0];
            frm.Get_listedName.value	= Arr[1];          
           }

	}
//-->
</script>
<%
else
 response.redirect "login.asp"
end if
%>
</body>
</html>