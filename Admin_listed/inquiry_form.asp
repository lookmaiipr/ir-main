<!-- #include virtual = "/ir/i_constant.asp" -->
<!-- #include virtual = "/ir/includefile_i_contant.asp" -->
<!--#include virtual="/ir/formatdate.asp"-->
<!-- #include virtual = "/ir/i_conirauthen.asp" -->
<!-- #include virtual = "/ir/i_conir.asp" -->
<!--#include virtual = "/ir/i_conirauthen_irdb.asp" -->

<%
'---------------------------------------------------------------------------------------------------------------------

	constpage = 10
	constpageno = 1

	if cint(request("pageno")) = "" or cint(request("pageno")) <= 0 then
		session("pageno") = 1
	else
		session("pageno") = cint(request("pageno"))
	end if

	if request("selectbutton") <> "" then
		session("selectbutton") = trim(replace(request("selectbutton"),",",""))
	else
		session("selectbutton") = ""
	end if


	session("pagesize") = constpage
	
	if request("search")<>"" then
			session("search") = trim(request("search"))
	else
			session("search")=""
	end if

'-----------------------------------------------------------------------------------------------------------------------
	
'##############################################################################
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<script language="javascript">
<!--
    function assignpage()     {
		window.frm1.totalpage.value=1;
		window.frm1.pageno.value=1;
	}
//-->
</script>
<title> Inquiry Form</title>
<meta http-equiv="Content-Type" content="text/html; charset=windows-874">
<!-- #include virtual = "/ir/includefile_style.asp" -->
<script language="javascript" src="/ir/function2007.js"></script>

<script language="JavaScript" src="CalendarPopup.js"></script>
<script language="JavaScript">document.write(getCalendarStyles());</script>
</head>
<body leftmargin="0" topmargin="5" marginwidth="0" marginheight="5">
<!--#include file = "../function_vbscript2007.asp" -->
<%if session("user_id")<>"" then%>
<div align="center" valign="top" > 
		<table width="715" valign="top"  border="0" cellpadding="0" cellspacing="0" >
			<tr>
			  <td align="left" valign="top">
						<table width="100%"  border="0" cellpadding="0" cellspacing="1" bgcolor="cfcfcf">
								<tr align="left" valign="top">         
										<td bgcolor="#FFFFFF">
												<table width="100%"  border="0" cellspacing="0" cellpadding="0" >
														<tr bgcolor="#cfcfcf">
																<td align="left" valign="top" bgcolor="#cfcfcf">
																     <!-- #include virtual = "/ir/includefile_i_top.asp" -->		
																</td>
														</tr>
														<tr>
																<td align="left" valign="top" bgcolor="#FFFFFF" >
																		<table width="100%"  border="0" cellspacing="0" cellpadding="5">
																			<tr align="left" valign="top">
																				<td height="40">
																						<table width="100%" border="0" cellpadding="0" cellspacing="1" bgcolor="<%= session("bg_in_form")%>" align="center">
																						  <tr align="left" valign="top">
																								<td width="350" valign="middle" bgcolor="#FFFFFF">
																								<font class="stylefont"><strong><img src="images/arrow.gif" width="12" height="9">
																								Inquiry Form  : <%=session("listed_name") %></strong></font></td>
																								<td bgcolor="<%= session("bg_in_form")%>">&nbsp;</td>
																						  </tr>
																						</table>
																			       </td>																				
																			</tr>
																			<tr >
																			    <td align="left" valign="top" >
																				     <FORM name="frm1" METHOD="POST" ACTION="inquiry_form.asp">
																			            <table width="100%" border="0" >
																							  <tr>
																									<td> 																									   																								  
																										  
																										   <font face="MS Sans Serif" size="1" color="#330000">
																										      <b>																			  
																										   <!----------------------------calendar---------------------------------------->
																													&nbsp;From :								
																												<%
																													if request("starts_date")  = "" then
																														strdate = "01/" & month(now) & "/" & year(now) -1
																														strdate = DateToString(strdate,"DD/MM/YYYY")
																													else
																													    strdate =  request("starts_date") 
																													end if
																							
																												%>
																												 <input type="text" name="starts_date" size="15" maxlength="10" value="<%=strdate%>" class="cptxtBorder">&nbsp;<A id="anchor1xx" onclick="cal1xx.select(document.forms[0].starts_date,'anchor1xx','dd/MM/yyyy'); return false;" href="#" name="anchor1xx"><img src="../images2007/calendar.gif" border="0" id="Image1"align='top' ></A>  
																											&nbsp;To :								
																										  <%
																													if request("stop_date")  = "" then
																														ptdate = DateToString(now,"DD/MM/YYYY")
																													else
																														ptdate = request("stop_date") 
																													end if
																												
																												%>								
																												 <input type="text" name="stop_date" size="15" maxlength="10" value="<%=ptdate%>" class="cptxtBorder">&nbsp;<A id="anchor2xx" onclick="cal1xx.select(document.forms[0].stop_date,'anchor2xx','dd/MM/yyyy'); return false;" href="#" name="anchor2xx"><img src="../images2007/calendar.gif" border="0" id="Image2" align='top'></A>	&nbsp;
																												 
																												 <SCRIPT language="JavaScript" id="jscal1xx">
																														var cal1xx = new CalendarPopup("testdiv1");
																														cal1xx.showNavigationDropdowns();
																												  </SCRIPT>    
																											<!--------------------------------status-------------------------------------------->
																											 Reply Status :
																											<select name="status" size="1"   style="FONT-FAMILY: Ms sans serif;FONT-SIZE:10px;cursor:hand;">
																														<option value="" <%if request("status")="" then%> selected <%end if%>>All</option>
																														<option value="Y" <%if request("status")="Y" then%> selected <%end if%>>Answered</option>		
																														<option value="N" <%if request("status")="N" then%> selected <%end if%>>No answered</option>
																														</select>	&nbsp;						
																												</b>
																										   </font>
																										   <!--------------------------------------------------------------------------->																									       
																											<input type="submit" name="Submit" value="Go" onclick="javascript:return assignpage();"  style="FONT-FAMILY: Ms sans serif;FONT-SIZE:10px; cursor:hand;">
																										 </td>
																								<td>&nbsp;</td>
																							  </tr>
																							</table>
																				</td>
																			</tr>
																			<tr>
																					<td align="left" valign="top">
																							<table width="100%" border="0" cellpadding="5" cellspacing="1" bgcolor="#FFFFFF">
																								<%																									
																									select case session("selectbutton")
																										case "<< First"
																											session("pageno") = 1
																										case "< Previous"
																											if session("pageno") = 1 then
																												session("pageno") = 1
																											elseif session("pageno") > 0 then
																												session("pageno") = session("pageno") -1
																											end if
																										case "Next >"
																											if session("pageno") < session("totalpage") then
																												session("pageno") = session("pageno") + 1
																											elseif session("pageno") = session("totalpage") then
																												session("pageno") = session("totalpage") 
																											end if
																										case "Last >>"
																											session("pageno") = session("totalpage") 
																									end select
																									'-----------------------------------------------------------------------------------------
																									strsql=""
																									strsql="select count(*) as num_count  from contact_listed "
																									strsql=strsql & " where timestamp >='" & DateToString(strdate,"YYYY-MM-DD") & " 00:00:00'  and  timestamp <='" & DateToString(ptdate,"YYYY-MM-DD")  & " 23:59:00' "																								
																							        																							
																									strsql=strsql & " and  listed_id=" &  session("listed_id") 
																									
																									'response.write strsql & "<br>"
																									
																									if trim(request("status"))<>"" then																								
																									    strsql=strsql & " and  ans_listed='" & trim(request("status")) & "' "
																									end if
																									
																									'response.write strsql
																									'response.end
																									 set rs = conir.execute(strsql)
																										
																										if rs("num_count") <>0 then		
																													session("num_count")=rs("num_count")		
																										else
																											 session("num_count")=0
																										end if
																					
																										rs.close
																										
		
																										if session("num_count") mod session("pagesize") > 0 then
																											session("totalpage") = int((session("num_count") / session("pagesize"))) + 1
																										elseif session("num_count") > 0 then
																											session("totalpage") = round(session("num_count") / session("pagesize"))
																										else
																											session("totalpage") = 1
																										end if
																										
																										
																										if  session("num_count") <>0 then
																												if session("pageno")= session("totalpage") and session("num_count") mod session("pagesize")<>0  then
																												   check_all=session("num_count") mod session("pagesize")
																												else
																												   check_all=constpage
																												end if
																										else
																											 check_all=0
																										end if
																										
																										
																										
																								 %>
																								<tr>
																										<td  align="right">
																												<font face="MS Sans Serif" size="1" color="#330000">Page :&nbsp;<%=session("pageno")%>/<%=session("totalpage")%></font>
																												<Hr color="<%= session("bg_in_form")%>"  size=2>
																										</td>
																								</tr>
																								<tr>
																										<td align="left" valign="top" >																												
																												<table width="100%"  border="0" cellspacing="1" cellpadding="5" bgcolor="<%=session("bg_in_form")%>" align="center">
																												      <tr align="center" valign="bottom"  bgcolor="<%=session("bg_in_form")%>">	
																																   <!------------------------------------------------------------------------------------------->
																																   <td  valign="middle" width="5%" >
																																			<font class="style_in_form" ><b>No</b></font>
																																	</td>
																																																														
																																	
																																	<td  valign="middle">
																																			<font class="style_in_form" ><b>Question</b></font>
																																	</td>
																																	
																																	<td width="8%"  valign="middle">
																																			<font class="style_in_form" ><b>Viewer</b></font>
																																	</td>
																																	
																																	<td width="8%"  valign="middle">
																																			<font class="style_in_form" ><b>Reply Status</b></font>
																																	</td>
																																	
																																	<td width="15%"  valign="middle">
																																			<font class="style_in_form" ><b>Posted By</b></font>
																																	</td>
																																	
																																	<td width="15%"  valign="middle">
																																			<font class="style_in_form" ><b>Date & Time</b></font>
																																	</td>
																																	
																													</tr>
																												
																												<%
																												    strsql=""
																													strsql="select id,listed_id,title,ans_listed,no_read,email,date_format(timestamp,'%Y-%m-%d %H:%i:%s') as post_date "
																													strsql=strsql & " from contact_listed where timestamp >='" & DateToString(strdate,"YYYY-MM-DD")  & " 00:00:00'  and  timestamp <='" & DateToString(ptdate,"YYYY-MM-DD")  & " 23:59:00' "		
																													strsql=strsql & " and  listed_id  =" &  session("listed_id")  & "  "																													
																													
																													if trim(request("status"))<>"" then																								
																														strsql=strsql & " and  ans_listed='" & trim(request("status")) & "' "
																													end if
																													
																													strsql=strsql & " order by timestamp DESC  limit " & ((session("pageno")-1)*session("pagesize")) & "," & session("pagesize")  
																													'response.write strsql
																													'response.end
																													 set rs = conir.execute(strsql)
																													 if not rs.eof and not rs.bof then
																													      i= ((session("pageno")-1)*session("pagesize")) 
																													      do while not rs.eof
																														    i=i+1
																														      if i mod 2=1 then																																	        
																																%>
																																			<tr bgcolor="#FFFFFF">
																																<%	else%>
																																			<tr bgcolor="#F4F4F4">
																																<%	end if%>
																																         	<td  valign="top" align="center">
																																			   	  	<font face="MS Sans Serif" size="1" color="#330000"><%=i%></font>
																																				</td>
																																				
																																				<td  valign="top">
																																			   	 	<font face="MS Sans Serif" size="1" color="#330000">
																																				    <a href="inquiry_detail.asp?id=<%=rs("id")%>"><%=rs("title")%></a>
																																				 </font>
																																				</td>
																																				<td  valign="top" align="center">
																																			   	  	<font face="MS Sans Serif" size="1" color="#330000"><%=rs("no_read")%></font>
																																				</td>
																																				<td  valign="top" align="center">
																																			   	  	<font face="MS Sans Serif" size="1" color="#330000"><%=rs("ans_listed")%></font>
																																				</td>
																																					<td  valign="top" >
																																			   	  	<font face="MS Sans Serif" size="1" color="#330000"><%=rs("email")%></font>
																																				</td>
																																					<td  valign="top" >
																																			   	  	<font face="MS Sans Serif" size="1" color="#330000"><%=rs("post_date")%></font>
																																				</td>
																																</tr>
																														 <%																													
																														 rs.movenext
																														 loop
																													 else
																													 %>
																															 <tr bgcolor="#FFFFFF">
																															  	<td   colspan="7" align="center"> 
																																		<font class="stylefont_a"><font size="3" color="#FF0000">
																																	   No Information</font></font>
																																</td>
																															</tr>
																															<%
																													 end if
																													 rs.close
																													 set rs=nothing
																													 conir.close
																													 set conir=nothing
																												%>	
																																   <!------------------------------------------------------------------------------------------->
																																
																											 </table>
																									</td>
																								</tr>																																											
																								<tr>
																										<td  align="right">
																												<Hr color="<%= session("bg_in_form")%>"  size=2>
																										</td>
																								</tr>
																								<tr>
																											<td   >
																											<!--####################################################################################-->
																												<font size="1" face="MS Sans Serif">
																												
																												<input type="hidden" name="Start_date" value="<%=strdate%>">
																												<input type="hidden" name="End_date" value="<%=ptdate%>">
					
																												<input type = "hidden" name = "totalpage" value="<%=session("totalpage")%>">	
																												<input type = "hidden" name = "pageno" value="<%=session("pageno")%>">
																												<!--<input type = "hidden" name = "delfaq" value="<'%=request("delfaq")%>">-->
											
																												<input type = "button" value = "Main Menu"  onclick="location='main_menu.asp'" style="FONT-FAMILY: Ms sans serif;FONT-SIZE:10px; cursor:hand;">
																											<%if session("pageno") <> 1 then %>
																												<input type = "submit" name = "selectbutton" value = "<< First" style="FONT-FAMILY: Ms sans serif;FONT-SIZE:10px; cursor:hand;">
																												<input type = "submit" name = "selectbutton" value = "< Previous" style="FONT-FAMILY: Ms sans serif;FONT-SIZE:10px; cursor:hand;">
																											<%else%>
																												<input type = "submit" name = "selectbutton" value = "<< First" disabled style="FONT-FAMILY: Ms sans serif;FONT-SIZE:10px; cursor:hand;">
																												<input type = "submit" name = "selectbutton" value = "< Previous" disabled style="FONT-FAMILY: Ms sans serif;FONT-SIZE:10px; cursor:hand;">
																											<%end if%>
																											<% if session("pageno") <> session("totalpage") then%>
																												<input type = "submit" name = "selectbutton" value = "Next >" style="FONT-FAMILY: Ms sans serif;FONT-SIZE:10px; cursor:hand;">
																												<input type = "submit" name = "selectbutton" value = "Last >>" style="FONT-FAMILY: Ms sans serif;FONT-SIZE:10px; cursor:hand;">
																											<%else%>
																												<input type = "submit" name = "selectbutton" value = "Next >" disabled style="FONT-FAMILY: Ms sans serif;FONT-SIZE:10px; cursor:hand;">
																												<input type = "submit" name = "selectbutton" value = "Last >>" disabled style="FONT-FAMILY: Ms sans serif;FONT-SIZE:10px; cursor:hand;">
																											<%end if%>
																											</font>
																			  </form>
																									<!--####################################################################################-->
																											</td>
																									</tr>																		
																									
																							</table>
																					</td>
																				</tr>
																		</table>
																</td>
														</tr>
													<!--#include virtual = "/ir/i_footer.asp" -->
												</table>
										</td>
								</tr>
						</table>
				</td>
			</tr>
		</table>
</div>
<DIV id="testdiv1" style="Z-INDEX: 101; VISIBILITY: hidden; POSITION: absolute; BACKGROUND-COLOR: white; layer-background-color: white"><FONT face="Tahoma"></FONT></DIV>
<%
else
 response.redirect "login.asp"
end if
%>
</body>
</html>