<!-- #include file = "../i_constant.asp" -->
<!-- #include file = "../includefile_i_contant.asp" -->
<!-- #include file = "../includefile_style.asp" -->	
<!-- #include file = "../i_conirauthen.asp" -->
<!-- #include file = "../i_conpsims.asp" -->
<!--#include virtual="/ir/formatdate.asp"-->
<!--#include file = "../i_listed_member.asp" -->

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<%
session("search_ID")=trim(request("Get_listedID"))
session("search_Name")=trim(request("Get_listedName"))


Function ListedName
   if listed_member_guest_id <> ""  then
      sql = "SELECT listed_id,share from listed_member where listed_id in(" & listed_member_guest_id & ") order by  share"
      set rs =conirauthen.execute(sql)
      ListedName = ""
      if session("search_ID") <> "" then
            do while not rs.eof 
               if trim(session("search_ID"))=trim(rs("listed_id")) then
                     ListedName = ListedName & " <option value=" & rs("listed_id") & ":" & rs("share") &" selected> "& rs("share") & "</option> "
               else
                     ListedName = ListedName & " <option value=" & rs("listed_id") & ":" & rs("share") & "> "& rs("share") &" </option> "
               end if
               rs.movenext
            loop                                                          
      else
            do while not rs.eof  
               ListedName = ListedName & " <option value="& rs("listed_id") & ":" & rs("share") &"> " & rs("share") &" </option> "
               rs.movenext
            loop
       end if
        rs.close      
   end if
End Function

Function StringListedName
	if strListedName <> "" then
		StringListedName = strListedName
	else 
		StringListedName = strListedName
	end if
End Function

Function GetSecID
   if session("search_ID") <> "" then
      strSql= ""
		strSql = " select sec_id from Compsec where com_id = '"& session("search_ID") &"' and sec_type ='S'  "
      strSql =strSql & " and flag<>'D' and sector_no <> '0' and sec_list_status = 'L' and (mk_type='A' or mk_type='S') "
		set rs = conpsims.execute(strSql)
		if not rs.eof and not rs.bof then
			GetSecID = rs("sec_id")
		end if
		rs.close
   end if
End Function


Function LoadMajorHolderInfo(sec_id)
	'ArrMajorShareholder

	if cstr(sec_id) <> "" then
			'============= [1] Load Book Closing Date =============
			 strSql = "select date_format(shareholder_as_of_date,'%Y-%m-%d')  as shareholder_as_of_date from Holder where sec_id =" & sec_id & " and flag <> 'D'  group by shareholder_as_of_date order by shareholder_as_of_date desc limit 2"
		
			 set rs = conpsims.execute(strSql) 
			if not rs.eof and not rs.bof then
				   i=0
				   do while not rs.eof
					  ArrHolderInfo(i,0) =rs("shareholder_as_of_date")
					  i = i + 1
					  rs.movenext
				   loop
			  end if
			  rs.close
			  
			'============= [2] Load Total of Share =============
			strSql = "select date_format(date,'%Y-%m-%d') as date ,listed_share  from d_stat where sec_id="& sec_id &" and date in('" & ArrHolderInfo(0,0) & "','" & ArrHolderInfo(1,0) & "')  order by date desc  "
			set rs = conpsims.execute(strSql)      
			if not rs.eof and not rs.bof then
					do while not rs.eof
                  arr_index = GetIndex(ArrHolderInfo, rs("date"),0)
						ArrHolderInfo(arr_index,1) = rs("listed_share")					   
					   rs.movenext
					loop     
			end if
			 rs.close   
	 end if
End Function


Sub LoadMajorShareHolder
   if sec_id <> "" then
      strsql = "select count(*) numrec  from Holder where sec_id = " & sec_id & "  and shareholder_as_of_date = ('" & ArrHolderInfo(0,0)  & "') and flag<>'D' order by percent_share_inhand desc"
      
      set rs = conpsims.execute(strsql)	
      numrec = 0
      if not rs.eof and not rs.bof then
         numrec = rs("numrec")
      end if
      rs.close
      
      redim ArrMajorShareHolder(numrec,8)
      
      '=============================== Load MajorShareHolder @ Last Date ===============================
      strsql = "select *  from Holder where sec_id = " & sec_id & "  and shareholder_as_of_date = ('" & ArrHolderInfo(0,0)  & "') and flag<>'D' order by percent_share_inhand desc"
      
      set rs = conpsims.execute(strsql)	
      
      v_Sum_Percent_share_inhand = 0
      
      if not rs.eof and not rs.bof then
            i = 0
            do while not rs.eof
               ArrMajorShareHolder(i,0) = trim(rs("title_name_share")) & trim(rs("first_name_share")) & trim(rs("lastname_share"))
               ArrMajorShareholder(i,1) = rs("share_type")         
               if rs("title_name_share") = "NULL" then
                  ArrMajorShareholder(i,2) =""
               else
                  ArrMajorShareholder(i,2) =rs("title_name_share")
               end if
               if rs("first_name_share") = "NULL" then
                  ArrMajorShareholder(i,3) =""
               else
                  ArrMajorShareholder(i,3) =rs("first_name_share")
               end if
               if rs("lastname_share") = "NULL" then
                  ArrMajorShareholder(i,4) =""
               else
                  ArrMajorShareholder(i,4) =rs("lastname_share")
               end if                   
               ArrMajorShareholder(i,5) = FormatNumber(rs("percent_share_inhand"),2, -1)
               ArrMajorShareholder(i,6) = FormatNumber(rs("no_share_inhand"),0)    		
               
               v_Sum_Percent_share_inhand = v_Sum_Percent_share_inhand + cdbl(rs("percent_share_inhand"))
               v_Sum_No_share_inhand = v_Sum_No_share_inhand + rs("no_share_inhand")
               
               i = i+1
               rs.movenext
            loop     
            rs.close   
      end if
      
      if cstr(ArrHolderInfo(1,0)) <> "" then
         '=============================== Load MajorShareHolder @ Old Date ===============================
         strsql = "select *  from Holder where sec_id = " & sec_id & "  and shareholder_as_of_date = ('" & ArrHolderInfo(1,0)  & "') and flag<>'D' order by percent_share_inhand desc"
         
         set rs = conpsims.execute(strsql)	
         if not rs.eof and not rs.bof then
               do while not rs.eof
                  keyword = trim(rs("title_name_share")) & trim(rs("first_name_share")) & trim(rs("lastname_share"))
                  arr_index = GetIndex(ArrMajorShareHolder,keyword ,0)
                  
                  v_Sum_Major_No_share_inhand = v_Sum_Major_No_share_inhand +  rs("no_share_inhand")			
                                
                  if cstr(arr_index) <> "" then
                     noshare_chg = ArrMajorShareHolder(arr_index,6) - rs("no_share_inhand")					
                     
                        v_Sum_Chg_share_inhand = v_Sum_Chg_share_inhand + noshare_chg
                        
                        if noshare_chg > 0 then
                           ArrMajorShareHolder(arr_index,7) = "+" & FormatNumber(noshare_chg,0)
                        elseif noshare_chg < 0 then
                           ArrMajorShareHolder(arr_index,7) = FormatNumber(noshare_chg,0)
                        elseif noshare_chg = 0 then
                           ArrMajorShareHolder(arr_index,7)  = "0"
                        end if
                  end if
                  rs.movenext
               loop     
               rs.close   
         
         end if
      end if	 
       '====================== Summary Of Total Shareholer Array =====================
       SummaryShareholder(2,0)=100
       SummaryShareholder(2,1)=ArrHolderInfo(0,1)
       'SummaryShareholder(2,2)=FormatNumber(ArrHolderInfo(0,1) - ArrHolderInfo(1,1),0)	 		   
      '====================== Summary Of Major Shareholer Array =====================
       SummaryShareholder(0,0)= v_Sum_Percent_share_inhand
       SummaryShareholder(0,1)= v_Sum_No_share_inhand
       'SummaryShareholder(0,2)=v_Sum_Chg_share_inhand	 			 
      '====================== Summary Of Minor Shareholer Array =====================
       SummaryShareholder(1,0)= SummaryShareholder(2,0) - SummaryShareholder(0,0)
       SummaryShareholder(1,1)= SummaryShareholder(2,1) - SummaryShareholder(0,1)
       SummaryShareholder(1,2) = SummaryShareholder(1,1) -( ArrHolderInfo(1,1)  -  v_Sum_Major_No_share_inhand )	 		       
       '====================== Summary Of Total Shareholer Array =====================
       'SummaryShareholder(2,2)=SummaryShareholder(0,2) + SummaryShareholder(1,2) 		    
   end if
End Sub


Function GetIndex(arrayvalue, find,index)
	GetIndex = ""

	for i = 0 to ubound(arrayvalue) -1
		'response.write "<br>" & i & " ,arrayvalue : " & arrayvalue(i,index) & " ,find : " & find & " , index : " & index 
	
		if cstr(arrayvalue(i,index)) = cstr(find) then
			'response.write " <font color='#FF0000'>Found</font> "
			GetIndex = i 
			Exit Function
		end if
	next 
End Function 
'********************************************************************************************************
Function WriteText
count0 = 0
count1 = 0
count2 = 0
count3 = 0
arrWriteText(0) = ""
arrWriteText(1) =""
arrWriteText(2) = ""
arrWriteText(3) = ""   
v_Sum_Percent_share_inhand_0 = 0
v_Sum_No_share_inhand_0 = 0
v_Sum_Chg_share_inhand_0 = 0
v_Sum_Percent_share_inhand_1 = 0
v_Sum_No_share_inhand_1 = 0
v_Sum_Chg_share_inhand_1 = 0
v_Sum_Percent_share_inhand_2 = 0
v_Sum_No_share_inhand_2 = 0
v_Sum_Chg_share_inhand_2 = 0
v_Sum_Percent_share_inhand_3 = 0
v_Sum_No_share_inhand_3 = 0
v_Sum_Chg_share_inhand_3 = 0
if sec_id <> "" then
         for i = 0 to ubound(ArrMajorShareHolder,1) - 1
               if ArrMajorShareholder(i,7) = "" then
                  ArrMajorShareholder(i,7)= ArrMajorShareholder(i,6)
               end if
		if ArrMajorShareholder(i,1) = "" then
                 arrWriteText(0) = arrWriteText(0) & "<tr bgcolor='#FFFFFF'>"
                  arrWriteText(0) = arrWriteText(0) & "<td align='left'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<font class='style49'>"&count0+1&". "&ArrMajorShareholder(i,2)&" "&ArrMajorShareholder(i,3)&" "&ArrMajorShareholder(i,4)&"</font></td>"
                  arrWriteText(0) = arrWriteText(0) & " <td align='right'><font class='style49'>"&ArrMajorShareholder(i,5)&"%</font></td> "
                  arrWriteText(0) = arrWriteText(0) & " <td align='right'><font class='style49'>"&ArrMajorShareholder(i,6)&"</font></td> "
                  arrWriteText(0) = arrWriteText(0) & " <td align='right'><font class='style49'>"&writeSign(ArrMajorShareholder(i,7))&"</font></td> "
                  arrWriteText(0) = arrWriteText(0) & "</tr>"        
                  count0 = count0 + 1
                  v_Sum_Percent_share_inhand_0 = v_Sum_Percent_share_inhand_0 + cdbl(ArrMajorShareholder(i,5))
                  v_Sum_No_share_inhand_0 = v_Sum_No_share_inhand_0 + cdbl(ArrMajorShareholder(i,6))
                  v_Sum_Chg_share_inhand_0 = v_Sum_Chg_share_inhand_0 + cdbl(ArrMajorShareholder(i,7))
                elseif ArrMajorShareholder(i,1) = 0 then
                 arrWriteText(0) = arrWriteText(0) & "<tr bgcolor='#FFFFFF'>"
                  arrWriteText(0) = arrWriteText(0) & "<td align='left'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<font class='style49'>"&count0+1&". "&ArrMajorShareholder(i,2)&" "&ArrMajorShareholder(i,3)&" "&ArrMajorShareholder(i,4)&"</font></td>"
                  arrWriteText(0) = arrWriteText(0) & " <td align='right'><font class='style49'>"&ArrMajorShareholder(i,5)&"%</font></td> "
                  arrWriteText(0) = arrWriteText(0) & " <td align='right'><font class='style49'>"&ArrMajorShareholder(i,6)&"</font></td> "
                  arrWriteText(0) = arrWriteText(0) & " <td align='right'><font class='style49'>"&writeSign(ArrMajorShareholder(i,7))&"</font></td> "
                  arrWriteText(0) = arrWriteText(0) & "</tr>"        
                  count0 = count0 + 1
                  v_Sum_Percent_share_inhand_0 = v_Sum_Percent_share_inhand_0 + cdbl(ArrMajorShareholder(i,5))
                  v_Sum_No_share_inhand_0 = v_Sum_No_share_inhand_0 + cdbl(ArrMajorShareholder(i,6))
                  v_Sum_Chg_share_inhand_0 = v_Sum_Chg_share_inhand_0 + cdbl(ArrMajorShareholder(i,7))
                elseif ArrMajorShareholder(i,1) = 1 then
                  arrWriteText(1) = arrWriteText(1) & "<tr bgcolor='#FFFFFF'>"
                  arrWriteText(1) = arrWriteText(1) & "<td align='left'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<font class='style49'>"&count1+1&". "&ArrMajorShareholder(i,2)&""&ArrMajorShareholder(i,3)&" "&ArrMajorShareholder(i,4)&"</font></td>"
                  arrWriteText(1) = arrWriteText(1) & " <td align='right'><font class='style49'>"&ArrMajorShareholder(i,5)&"%</font></td> "
                  arrWriteText(1) = arrWriteText(1) & " <td align='right'><font class='style49'>"&ArrMajorShareholder(i,6)&"</font></td> "
                  arrWriteText(1) = arrWriteText(1) & " <td align='right'><font class='style49'>"&writeSign(ArrMajorShareholder(i,7))&"</font></td> "  
                  arrWriteText(1) = arrWriteText(1) & "</tr>"         
                  count1 = count1 + 1       
                  v_Sum_Percent_share_inhand_1 = v_Sum_Percent_share_inhand_1 + cdbl(ArrMajorShareholder(i,5))
                  v_Sum_No_share_inhand_1 = v_Sum_No_share_inhand_1 + cdbl(ArrMajorShareholder(i,6))
                  v_Sum_Chg_share_inhand_1 = v_Sum_Chg_share_inhand_1 + cdbl(ArrMajorShareholder(i,7))
                elseif ArrMajorShareholder(i,1) = 2 then
                  arrWriteText(2) = arrWriteText(2) & "<tr bgcolor='#FFFFFF'>"
                  arrWriteText(2) = arrWriteText(2) & "<td align='left'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<font class='style49'>"&count2+1&". "&ArrMajorShareholder(i,2)&""&ArrMajorShareholder(i,3)&" "&ArrMajorShareholder(i,4)&"</font></td>"
                  arrWriteText(2) = arrWriteText(2) & " <td align='right'><font class='style49'>"&ArrMajorShareholder(i,5)&"%</font></td> "
                  arrWriteText(2) = arrWriteText(2) & " <td align='right'><font class='style49'>"&ArrMajorShareholder(i,6)&"</font></td> "
                  arrWriteText(2) = arrWriteText(2) & " <td align='right'><font class='style49'>"&writeSign(ArrMajorShareholder(i,7))&"</font></td> "    
                  arrWriteText(2) = arrWriteText(2) & "</tr>"                  
                  count2 = count2 + 1         
                  v_Sum_Percent_share_inhand_2 = v_Sum_Percent_share_inhand_2 + cdbl(ArrMajorShareholder(i,5))
                  v_Sum_No_share_inhand_2 = v_Sum_No_share_inhand_2 + cdbl(ArrMajorShareholder(i,6))
                  v_Sum_Chg_share_inhand_2 = v_Sum_Chg_share_inhand_2 + cdbl(ArrMajorShareholder(i,7))
                elseif ArrMajorShareholder(i,1) = 3 then
                  arrWriteText(3) = arrWriteText(3) & "<tr bgcolor='#FFFFFF'>"
                  arrWriteText(3) = arrWriteText(3) & "<td align='left'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<font class='style49'>"&count3+1&". "&ArrMajorShareholder(i,2)&""&ArrMajorShareholder(i,4)&" "&ArrMajorShareholder(i,4)&"</font></td>"
                  arrWriteText(3) = arrWriteText(3) & " <td align='right'><font class='style49'>"&ArrMajorShareholder(i,5)&"%</font></td> "
                  arrWriteText(3) = arrWriteText(3) & " <td align='right'><font class='style49'>"&ArrMajorShareholder(i,6)&"</font></td> "
                  arrWriteText(3) = arrWriteText(3) & " <td align='right'><font class='style49'>"&writeSign(ArrMajorShareholder(i,7))&"</font></td> "
                  arrWriteText(3) = arrWriteText(3) & "</tr>"   
                  count3 = count3 + 1        
                  v_Sum_Percent_share_inhand_3 = v_Sum_Percent_share_inhand_3 + cdbl(ArrMajorShareholder(i,5))
                  v_Sum_No_share_inhand_3 = v_Sum_No_share_inhand_3 + cdbl(ArrMajorShareholder(i,6))
                  v_Sum_Chg_share_inhand_3 = v_Sum_Chg_share_inhand_3 + cdbl(ArrMajorShareholder(i,7))
                end if   
         next 

         SummaryShareholder(3,0) = v_Sum_Percent_share_inhand_0
         SummaryShareholder(3,1) = v_Sum_No_share_inhand_0
         SummaryShareholder(3,2) = v_Sum_Chg_share_inhand_0
         
         SummaryShareholder(4,0) = v_Sum_Percent_share_inhand_1
         SummaryShareholder(4,1) = v_Sum_No_share_inhand_1
         SummaryShareholder(4,2) = v_Sum_Chg_share_inhand_1
         
         SummaryShareholder(5,0) = v_Sum_Percent_share_inhand_2
         SummaryShareholder(5,1) = v_Sum_No_share_inhand_2
         SummaryShareholder(5,2) = v_Sum_Chg_share_inhand_2
         
         SummaryShareholder(6,0) = v_Sum_Percent_share_inhand_3
         SummaryShareholder(6,1) = v_Sum_No_share_inhand_3
         SummaryShareholder(6,2) = v_Sum_Chg_share_inhand_3
         
        'NiTiPerson
         SummaryShareholder(7,0) = SummaryShareholder(3,0) + SummaryShareholder(5,0)
         SummaryShareholder(7,1) = SummaryShareholder(3,1) + SummaryShareholder(5,1)
         SummaryShareholder(7,2) = SummaryShareholder(3,2) + SummaryShareholder(5,2)      
         'NormalPerson
         SummaryShareholder(8,0) = SummaryShareholder(4,0) + SummaryShareholder(6,0)
         SummaryShareholder(8,1) = SummaryShareholder(4,1) + SummaryShareholder(6,1)
         SummaryShareholder(8,2) = SummaryShareholder(4,2) + SummaryShareholder(6,2)      
         
         SummaryShareholder(0,2)=SummaryShareholder(7,2)+SummaryShareholder(8,2) 	
         SummaryShareholder(2,2)=SummaryShareholder(0,2) + SummaryShareholder(1,2) 		
   end if  
End Function


function writeSign(value)
       if cdbl(value) = 0  then
         writeSign = 0
       elseif cdbl(value) >0 then
        writeSign = "+" & FormatNumber(value,0,-1)
      else
         writeSign = FormatNumber(value,0,-1)                     
      end if
end function

%>
<title>Stockholder Composition</title>
<meta http-equiv="Content-Type" content="text/html; charset=windows-874">
<!-- #include file = "../includefile_style.asp" -->
</head>
<body leftmargin="0" topmargin="5" marginwidth="0" marginheight="5">
<%if session("user_id")<>"" then%>
<form name="frm" method="post" action="stockholder_composition_reporter.asp">
<div align="center" valign="top" > 
		<table width="1000" valign="top"  border="0" cellpadding="0" cellspacing="0">
			<tr>
			  <td align="left" valign="top">
						<table width="100%"  border="0" cellpadding="0" cellspacing="1" bgcolor="#cfcfcf">
								<tr align="left" valign="top">         
										<td bgcolor="#cfcfcf">
												<table width="100%"  border="0" cellspacing="0" cellpadding="0">
														<tr>
																<td align="left" valign="top">
																     <!-- #include virtual = "/ir/includefile_i_top.asp" -->		
																</td>
														</tr>      
														<tr><td>
															<table width="100%" cellpadding="0" cellspacing="0" border="0">
																<tr  bgcolor="#FFFFFF" border="0"style="border:1px solid #FFFFFF;">
																	<td>
																		<table cellpadding="5" cellspacing="0" border="0">
																			<tr valign="baseline" style="FONT-FAMILY: Ms sans serif;FONT-SIZE:1;cursor:hand;">
																				<td>			
																						<a href="InvestorRelationsMetrics_reporter.asp" class="L">Investor Relations Metrics</a>			
																				</td>
																				<td>||	</td>
																				<td>			
																						<a href="stockprice_performance_reporter.asp" class="L">Stock Price Performance</a>					
																				</td>
																				<td>||	</td>		
																				<td>					
																						<a href="stockholder_composition_reporter.asp" class="L">Stockholder Composition</a>		
																				</td>					
																				<td>||	</td>		
																				<td>
																						<a href="analyst_coverage.asp" class="L">Analyst Coverage (SAA Consensus)</a>	
																				</td>
																			</tr>
																		</table>
																	</td>
																</tr>
															</table>
															</td>			
														</tr>																	
														<tr>
																<td align="left" valign="top">
																		<table width="100%"  border="0" cellspacing="10" cellpadding="0">
																			<tr align="left" valign="top">
																				<td>
																						<table width="100%" border="0" cellpadding="0" cellspacing="1">
																						  <tr align="left" valign="top">
																								<td width="350" valign="middle" bgcolor="#FFFFFF"><font class="stylefont"><strong><img src="../images/arrow.gif" width="12" height="9"> Stockholder Composition : <%=session("listed_name") %></strong></font></td>
																								<td>&nbsp;</td>
																						  </tr>
																						</table>
																				</td>																				
																			</tr>
                                                         <tr align="left" valign="top">
                                                            <td>
                                                               <!------------------------------- Search Module ------------------------------------->
                                                               <TABLE  cellSpacing="1" cellPadding="1" border="0">
                                                                  <TR>
                                                                     <td width="100"><font class="stylefont_a" ><font color="#000000"><b>Listed Member</b></font></font></td>
                                                                     <TD>									
                                                                           <%strListedName = ListedName%>
                                                                           <select name="search_ListedMember" size="1">
                                                                           <option <%if session("search_ID")="" then%>selected<%end if%>>- - Select Listed - -</option>
                                                                                 <%=StringListedName%>
                                                                           </select>&nbsp;&nbsp;                                                                
                                                                           <input type="submit" name="action" value="Go" onclick="javascript:return assignpage(frm.search_ListedMember.value);" >
                                                                           <input type="hidden" name="Get_listedID" value="<%=session("search_ID")%>">
                                                                           <input type="hidden" name="Get_listedName" value="<%=session("search_Name")%>">
                                                                     </TD>
                                                                  </TR>
                                                               </TABLE>
                                                               <!------------------------------- Search Module ------------------------------------->
                                                            </td>																				
                                                         </tr>          
																			<tr>
                                                            <td align="left" valign="top">
                                                              <table width="100%" border="0" cellpadding="1" cellspacing="1" bgcolor="#FFFFFF">
                                                                <tr>
                                                                   <td align="left" valign="top">
                                                                   <%if session("search_ID")  <> "" then%>
                                                                     <table width="100%" border="0" cellpadding="5" cellspacing="1" bgcolor="#FFFFFF">
																							<%
																						   sec_id = GetSecID              
																							Dim ArrHolderInfo(2,2)
																							Dim ArrMajorShareHolder()
																							Dim SummaryShareholder(9,3)
                                                                     Dim arrWriteText(3)
                                                                     Call LoadMajorHolderInfo(sec_id)  
                                                                     Call LoadMajorShareHolder
                                                                     Call WriteText              
                                                                     if sec_id <> "" then
																						   %>
																								<tr>
																										<td align="left" valign="top" bgcolor="#FFFFFF"><br>
                                                                                 <strong><font class="style49">Closing Lastdate : </strong><%=ArrHolderInfo(0,0)%></font><br>
                                                                                 <strong><font class="style49">Closing Before Lastdate : </strong><%=ArrHolderInfo(1,0)%></font>
                                                                              </td>
                                                                        </tr>
                                                                      <%end if%>
																								<tr>
																										<td align="left" valign="top" bgcolor="#FFFFFF">
																												<table width="100%"  border="0" cellspacing="1" cellpadding="5"   bgcolor="<%=session("bg_in_form")%>">
                                                                                       <tr align="center" valign="bottom"   bgcolor="<%=session("bg_in_form")%>">
                                                                                          <td  width="44%" align="left" ><font class="style_in_form" >&nbsp;</font></td>
                                                                                          <td width="18%"><font class="style_in_form" >%Of Float</font></td>
                                                                                          <td width="18%"><font class="style_in_form" >No. of Shares</font></td>
                                                                                          <td width="20%"><font class="style_in_form" >Year-to-Date Change</font></td>                                                                                                 
                                                                                        </tr>
                                                                                        <%if sec_id <> "" then%>
                                                                                          <tr bgcolor="#FFFFFF" >
                                                                                             <td  valign="top"align="left"><strong><font class="style49">������������˭�</font></strong></td>
                                                                                             <td><font class="">&nbsp;</font></td>
                                                                                             <td><font class="">&nbsp;</font></td>
                                                                                             <td><font class="">&nbsp;</font></td>                                                                                                          
                                                                                          </tr>      
                                                                                          <tr bgcolor="#FFFFFF">
                                                                                             <td  valign="top" align="left"><strong class="style49">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;�ԵԺؤ��</strong></td>
                                                                                             <td><font class="">&nbsp;</font></td>
                                                                                             <td><font class="">&nbsp;</font></td>
                                                                                             <td><font class="">&nbsp;</font></td>                                                                                                        
                                                                                          </tr>  
                                                                                          <tr bgcolor="#FFFFFF">
                                                                                             <td  valign="top" align="left"><strong class="style49">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;- 㹻����</strong></td>
                                                                                             <td><font class="">&nbsp;</font></td>
                                                                                             <td><font class="">&nbsp;</font></td>
                                                                                             <td><font class="">&nbsp;</font></td>                                                                                                        
                                                                                          </tr>   
                                                                                          <%
                                                                                          if arrWriteText(0) <> "" then
                                                                                          %>               
                                                                                              <%=arrWriteText(0)%> 
                                                                                          <%
                                                                                          else
                                                                                         %>
                                                                                             <tr bgcolor="#FFFFFF"><td align="left" colspan="4"><I><font color="#FF0000" size="1"> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;��辺�����ż������鹹ԵԺؤ��㹻����</font></I></td></tr>                                                                                                             
                                                                                           <%  
                                                                                           end if
                                                                                           %>
                                                                                          <tr bgcolor="#FFFFFF">
                                                                                             <td align="left" > &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<strong class="style49" >����ԵԺؤ��㹻���ȷ�����</strong></td>  
                                                                                                <% if arrWriteText(0) <> ""  then%>
                                                                                                <td align="right"><font class="style49"><%=FormatNumber(SummaryShareholder(3,0),2,-1)%>%</font></td>
                                                                                                <td align="right"><font class="style49" ><%=FormatNumber(SummaryShareholder(3,1),0,-1)%></font></td>
                                                                                                <td align="right"><font class="style49" ><%=writeSign(FormatNumber(SummaryShareholder(3,2),0,-1))%></font></td> 
                                                                                                  <%else%>
                                                                                                          <td align="right"><font class="style49">-</font></td>
                                                                                                         <td align="right"><font class="style49" >-</font></td>
                                                                                                         <td align="right"><font class="style49" >-</font></td>                                                                                                                                 
                                                                                                  <%end if%>
                                                                                                 </tr>      
                                                                                                <tr bgcolor="#FFFFFF">
                                                                                                   <td  valign="top" align="left"><strong class="style49">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;- ��ҧ�����</strong></td>
                                                                                                   <td><font class="style49">&nbsp;</font></td>
                                                                                                   <td><font class="style49">&nbsp;</font></td>
                                                                                                   <td><font class="style49">&nbsp;</font></td>                                                                                                        
                                                                                                </tr> 
                                                                                          <%
                                                                                           if arrWriteText(2) <> "" then
                                                                                          %>        
                                                                                              <%=arrWriteText(2)%>
                                                                                          <%
                                                                                          else
                                                                                         %>
                                                                                             <tr bgcolor="#FFFFFF"><td align="left" colspan="4"><I><font color="#FF0000" size="1"> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;��辺�����ż������鹹ԵԺؤ�ŵ�ҧ�����</font></I></td></tr>                                                                                                             
                                                                                           <%  
                                                                                           end if
                                                                                           %>
                                                                                          <tr bgcolor="#FFFFFF">
                                                                                             <td align="left" > &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<strong class="style49" >����ԵԺؤ�ŵ�ҧ����ȷ�����</strong></td>  
                                                                                             <% if arrWriteText(2) <> ""  then%>
                                                                                                <td align="right"><font class="style49"><%=FormatNumber(SummaryShareholder(5,0),2,-1)%>%</font></td>
                                                                                                <td align="right"><font class="style49" ><%=FormatNumber(SummaryShareholder(5,1),0,-1)%></font></td>
                                                                                                <td align="right"><font class="style49" ><%=writeSign(FormatNumber(SummaryShareholder(5,2),0,-1))%></font></td> 
                                                                                               <%else%>
                                                                                              <td align="right"><font class="style49">-</font></td>
                                                                                             <td align="right"><font class="style49" >-</font></td>
                                                                                             <td align="right"><font class="style49" >-</font></td>                                                                                                                                 
                                                                                               <%end if%>
                                                                                            </tr>                                                                                                
                                                                                           </tr>     
                                                                                                <tr bgcolor="#FFFFFF">
                                                                                                   <td align="left" ><strong class="style49" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;����ԵԺؤ�ŷ�����</strong></td>
                                                                                                   <td align="right"><font class="style49"><%=FormatNumber(SummaryShareholder(7,0),2,-1)%>%</font></td>
                                                                                                   <td align="right"><font class="style49" ><%=FormatNumber(SummaryShareholder(7,1),0,-1)%></font></td>
                                                                                                   <td align="right"><font class="style49" ><%=writeSign(FormatNumber(SummaryShareholder(7,2),0,-1))%></font></td>                                                                                        
                                                                                                </tr>   
                                                                                                <tr bgcolor="#FFFFFF">
                                                                                                   <td  valign="top" align="left"><strong class="style49">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;�ؤ�Ÿ�����</strong></td>
                                                                                                   <td><font class="">&nbsp;</font></td>
                                                                                                   <td><font class="">&nbsp;</font></td>
                                                                                                   <td><font class="">&nbsp;</font></td>                                                                                                        
                                                                                                </tr>  
                                                                                                <tr bgcolor="#FFFFFF">
                                                                                                   <td  valign="top" align="left"><strong class="style49">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;- 㹻����</strong></td>
                                                                                                   <td><font class="">&nbsp;</font></td>
                                                                                                   <td><font class="">&nbsp;</font></td>
                                                                                                   <td><font class="">&nbsp;</font></td>                                                                                                        
                                                                                                </tr>            
                                                                                                <%
                                                                                                if arrWriteText(1) <> "" then
                                                                                                %>               
                                                                                                    <%=arrWriteText(1)%>
                                                                                                <%
                                                                                                else
                                                                                               %>
                                                                                                   <tr bgcolor="#FFFFFF"><td align="left" colspan="4"><I><font color="#FF0000" size="1"> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;��辺�����ż������鹺ؤ�Ÿ�����㹻����</font></I></td></tr>                                                                                                             
                                                                                                 <%  
                                                                                                 end if
                                                                                                 %>
                                                                                                <tr bgcolor="#FFFFFF">
                                                                                                   <td align="left" > &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<strong class="style49" >����ؤ�Ÿ�����㹻���ȷ�����</strong></td>  
                                                                                                      <% if arrWriteText(1) <> ""  then%>
                                                                                                         <td align="right"><font class="style49"><%=FormatNumber(SummaryShareholder(4,0),2,-1)%>%</font></td>
                                                                                                         <td align="right"><font class="style49" ><%=FormatNumber(SummaryShareholder(4,1),0,-1)%></font></td>
                                                                                                         <td align="right"><font class="style49" ><%=writeSign(FormatNumber(SummaryShareholder(4,2),0,-1))%></font></td> 
                                                                                                        <%else%>
                                                                                                                <td align="right"><font class="style49">-</font></td>
                                                                                                               <td align="right"><font class="style49" >-</font></td>
                                                                                                               <td align="right"><font class="style49" >-</font></td>                                                                                                                                 
                                                                                                        <%end if%>
                                                                                                       </tr>      
                                                                                                      <tr bgcolor="#FFFFFF">
                                                                                                         <td  valign="top" align="left"><strong class="style49">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;- ��ҧ�����</strong></td>
                                                                                                         <td><font class="style49">&nbsp;</font></td>
                                                                                                         <td><font class="style49">&nbsp;</font></td>
                                                                                                         <td><font class="style49">&nbsp;</font></td>                                                                                                        
                                                                                                      </tr> 
                                                                                                <%
                                                                                                 if arrWriteText(3) <> "" then
                                                                                                %>        
                                                                                                    <%=arrWriteText(3)%>
                                                                                                <%
                                                                                                else
                                                                                               %>
                                                                                                   <tr bgcolor="#FFFFFF"><td align="left" colspan="4"><I><font color="#FF0000" size="1"> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;��辺�����ż������鹺ؤ�Ÿ����ҵ�ҧ�����</font></I></td></tr>                                                                                                             
                                                                                                 <%  
                                                                                                 end if
                                                                                                 %>
                                                                                                <tr bgcolor="#FFFFFF">
                                                                                                   <td align="left" > &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<strong class="style49" >����ؤ�Ÿ����ҵ�ҧ����ȷ�����</strong></td>  
                                                                                                   <% if arrWriteText(3) <> ""  then%>
                                                                                                   <td align="right"><font class="style49"><%=FormatNumber(SummaryShareholder(6,0),2,-1)%>%</font></td>
                                                                                                   <td align="right"><font class="style49" ><%=FormatNumber(SummaryShareholder(6,1),0,-1)%></font></td>
                                                                                                   <td align="right"><font class="style49" ><%=writeSign(FormatNumber(SummaryShareholder(6,2),0,-1))%></font></td> 
                                                                                                     <%else%>
                                                                                                    <td align="right"><font class="style49">-</font></td>
                                                                                                   <td align="right"><font class="style49" >-</font></td>
                                                                                                   <td align="right"><font class="style49" >-</font></td>                                                                                                                                 
                                                                                                     <%end if%>
                                                                                                  </tr>                                                                                                
                                                                                                 </tr>     
                                                                                                      <tr bgcolor="#FFFFFF">
                                                                                                         <td align="left" ><strong class="style49" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;����ؤ�Ÿ����ҷ�����</strong></td>
                                                                                                         <td align="right"><font class="style49"><%=FormatNumber(SummaryShareholder(8,0),2,-1)%>%</font></td>
                                                                                                         <td align="right"><font class="style49" ><%=FormatNumber(SummaryShareholder(8,1),0,-1)%></font></td>
                                                                                                         <td align="right"><font class="style49" ><%=writeSign(FormatNumber(SummaryShareholder(8,2),0,-1))%></font></td>                                                                                          
                                                                                                      </tr>                                                                                               
                                                                                                      <tr bgcolor="#FFFFFF">
                                                                                                            <td align="left" ><strong class="style49" >���������������˭������</strong></td>
                                                                                                            <td align="right"><font class="style49"><%=FormatNumber(SummaryShareholder(0,0),2,-1)%>%</font></td>
                                                                                                            <td align="right"><font class="style49" ><%=FormatNumber(SummaryShareholder(0,1),0,-1)%></font></td>
                                                                                                            <td align="right"><font class="style49" ><%=writeSign(FormatNumber(SummaryShareholder(0,2),0,-1))%></font></td>
                                                                                                         </tr>   
                                                                                                         <tr bgcolor="#FFFFFF">
                                                                                                               <td align="left" ><strong><font class="style49">���������������</font></strong></td>
                                                                                                               <td align="right"><font class="style49"><%=FormatNumber(SummaryShareholder(1,0),2,-1)%>%</font></td>
                                                                                                               <td align="right"><font class="style49" ><%=FormatNumber(SummaryShareholder(1,1),0,-1)%></font></td>
                                                                                                               <td align="right"><font class="style49" ><%=writeSign(FormatNumber(SummaryShareholder(1,2),0,-1))%></font></td>
                                                                                                         </tr> 
                                                                                                         <tr bgcolor="#FFFFFF">
                                                                                                               <td align="left" ><strong><font class="style49">����������鹷�����</font></strong></td>
                                                                                                               <td align="right"><font class="style49"><%=SummaryShareholder(2,0)%>%</font></td>
                                                                                                               <td align="right"><font class="style49" ><%=FormatNumber(SummaryShareholder(2,1),0,-1)%></font></td>
                                                                                                               <td align="right"><font class="style49" ><%=writeSign(FormatNumber(SummaryShareholder(2,2),0,-1))%></font></td>
                                                                                                            </tr>       
                                                                                              <%else%>
                                                                                               <tr bgcolor="#FFFFFF">
                                                                                                 <td align="center" colspan="4"><strong class="style49"><font color="#FF0000">No Information</font></strong></td>
                                                                                                </tr>                                                                                                   
                                                                                              <%end if%>                                                                                                       
																											 </table>
																									</td>
																								</tr>
                                                                        <%
                                                                        if sec_id <> "" then
                                                                           sqlChgPar = ""
                                                                           sqlChgPar = " select * from ChgPar where sec_id = '"& sec_id &"' "
                                                                           sqlChgPar = sqlChgPar & " and effect_date >= '"& ArrHolderInfo(1,0) &"' and effect_date <= '"& ArrHolderInfo(0,0) &"' "
                                                                           set rsChgPar = conpsims.execute(sqlChgPar)    
                                                                           if not rsChgPar.eof  then 
                                                                                %>
                                                                               <tr>
                                                                                 <td  align="left">
                                                                                       <strong><font class="style49" color="#FF0000">�����˵�</font></strong>
                                                                                 </td>
                                                                              </tr>                                                                                    
                                                                                <%
                                                                                 do while not rsChgPar.eof
                                                                                   %>
                                                                                   <tr>
                                                                                       <td  align="left" class="style49">
                                                                                            - ����ѷ�ա������¹�ŧ�ع������¹ ������ѹ��� <%=left(formatdate(rsChgPar("effect_date")),10)%> �ҡ�ҤҾ��� 
                                                                                           <%=FormatNumber(rsChgPar("old_par_val"),2,-1)%> �� <%=FormatNumber(rsChgPar("new_par_val"),2,-1)%> �ҷ������
                                                                                       </td>
                                                                                    </tr>                                                                                    
                                                                                   <%
                                                                                    n=n+1
                                                                                    rsChgPar.movenext
                                                                                 loop
                                                                           end if
                                                                           rsChgPar.close
                                                                           set rsChgPar=nothing
                                                                        end if
                                                                        %>
																		
																								<%else%>
																									<br>
																									<p><center>Please Select Listed Company for Load Information.<center></p>
																									<br>			
                                                                       <%end if%>
                                                                        <tr>
                                                                           <td  align="right">
                                                                                 <hr color="<%= session("bg_in_form")%>" size=2>
                                                                           </td>
                                                                        </tr>                                                                        
																								<tr>
                                                                           <td>
                                                                              <input type = "button" value = "Main Menu"  onclick="location='main_menu.asp'"><br><br>
                                                                           </td>
                                                                         </tr>
																							</table>
																					</td>
																				</tr>
																		</table>
																</td>
														</tr>
													<!--#include virtual = "/ir/i_footer.asp" -->
												</table>
										</td>
								</tr>
						</table>
				</td>
			</tr>
		</table>
</div>
</form>
<script language="JavaScript">
<!--
	// event when click go button
	function assignpage(name){     
   
            var Arr=name.split(":");
            if (Arr.length == 2){
  				frm.Get_listedID.value	= Arr[0];
            frm.Get_listedName.value	= Arr[1];          
           }

	}
//-->
</script>
<%
else
 response.redirect "login.asp"
end if
%>
</body>
</html>