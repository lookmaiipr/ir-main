//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	function  CheckFileName(sFormat,sHeader,sText,sLang){
	 // Check Format FileName 	
	//support format {Header}_yyyyMMddhhssmm_{T,E}.{doc,xls,pdf,zip}   
	//example  amg_20080801123540_T.pdf  ,amg_20080801123540_E.pdf
	//support format {Header}_yyyyMMdd_{T,E}.{doc,xls,pdf,zip}   
	//example  amg_20080801_T.pdf  ,amg_20080801_E.pdf
	 var Format=sFormat;
	 var Header=sHeader;	
	  var Text=sText;	  
	  var Lang=sLang;
	  var Iscorrect =true;
	   var TypeFile="doc,xls,pdf,zip";
	  
	  var Arr=Text.split(".");
	  if(Arr.length !=2){
		       Iscorrect=false;
			  return Iscorrect;
		}	
	  
	  var  ArrName=Arr[0].split("_");	  
	  if(ArrName.length !=3){
		       Iscorrect=false;
			  return Iscorrect;
		}
		
		if((ArrName[0]!=Header) || (ArrName[2]!=Lang)){
				  Iscorrect=false;				  
			     return Iscorrect;
		}
		
		if((ArrName[1].length!=8) && (ArrName[1].length!=14)){
		          Iscorrect=false;				 
			     return Iscorrect;
		}
		
	    if (!IsDates(ArrName[1]," ")){
		          Iscorrect=false;
			     return Iscorrect;
		 }
		 
		 if (Format==1){
						if(!IsTimes(ArrName[1]," ")){
					              Iscorrect=false;
								 return Iscorrect;
					   }	
		 }	
		
		if(TypeFile.indexOf(Arr[1]) ==-1 ){
	          Iscorrect=false;
			  return Iscorrect;
		}
		
		return 	Iscorrect;
	}
	//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	function CheckDates(sFormat,sText){	
	// Check Format date 	
	//sFormat=1  support format  yyyy-MM-dd hh:ss:mm  
	//example   2008-08-01 12:30:45
	//sFormat=2  support format  yyyyMMddHHssmm  
	//example   20080801123045
	//sFormat=other  support format  yyyy-MM-dd  
	//example   2008-08-01
	   var Text=sText;
	   var Iscorrect =true;
	   var Format=sFormat;
	   
	   if(Format==1){
			if(sText.length!=19)
			{
				Iscorrect=false;
				 return Iscorrect;
			}
	   			var Arr=Text.split(" ");
				if(Arr.length !=2){
					   Iscorrect=false;
					 return Iscorrect;
				}		
				 
				 if (!IsDates(Arr[0],"Date")){
						  Iscorrect=false;
						 return Iscorrect;
				 }
				 
				 if (Format==1){		 
							   if(!IsTimes(Arr[1],"Time")){
										  Iscorrect=false;
										 return Iscorrect;
							   }	
				}	
	   }
	   else if (Format==2){
				if(Text.length!=14){
					Iscorrect=false;
					return Iscorrect;
				}
				
				var vdate = Text.substring(0,8);
				 
				 if (!IsDates(vdate,"Other")){
						  Iscorrect=false;
						 return Iscorrect;
				 }
				 
			   if(!IsTimes(Text,"Other")){
						  Iscorrect=false;
						 return Iscorrect;
			   }		   
	   }else{
	   		 if (!IsDates(Text,"Date")){
						  Iscorrect=false;
						 return Iscorrect;
				 }
	   }
		return Iscorrect;	
	}	
	//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	function  IsTextBlanks(sText){
	//Check Text Blank
			var IsTextBlank=true;
			if(sText!=""){
			    IsTextBlank=false;
			}
			return IsTextBlank;
	}
	//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	function IsDates(sText,sType){
	// Check Format Date 	
	//sType=Date support format  yyyy-MM-dd
	//example   2008-08-01
	//sType=Other support format  yyyyMMdd
	//example   20080801
			var IsDate =true;
			var Text=sText;
			var Type=sType;	
			var stryear="";
			var strmonth="";
			var strdate="";
			
			if(Type=="Date"){
					var Arr=Text.split("-");
					if(Arr.length !=3){
							  Iscorrect=false;
							return Iscorrect;
					}					
					 stryear=Arr[0];
					 strmonth=Arr[1];
					 strdate=Arr[2];
			}else{
			         stryear=Text.substring(0,4);
					 strmonth=Text.substring(4,6);
					 strdate=Text.substring(6,8);
			}
			
			if ((stryear=="") || (strmonth=="") || (strdate=="")) {
			          IsDate=false;
					return IsDate;
			}else{
					if (!IsYears(stryear)){
							 IsDate=false;
							return IsDate;
					 }
					 
					 if (!IsMonths(strmonth)){
							  IsDate=false;
							 return IsDate;
					 }
					 
					 if (!IsDays(strdate,strmonth,stryear)){
							  IsDate=false;
							 return IsDate;
					 }
			}
			 
			 return IsDate;
	}
	//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	function IsTimes(sText,sType){
	// Check Format Time 	
	//sType=Time support format  hh:ss:mm
	//example   12:30:45
	//sType=Other support format  hhssmm
	//example   123045	
			var IsTime =true;
			var Text=sText;
			var Type=sType;			
			var strhour="";
			var strminute="";
			var strsec="";
								
			if(Type=="Time"){
				var Arr=Text.split(":"); 
				if(Arr.length !=3){
							IsTime=false;
							return IsTime;
					}					
				strhour=Arr[0];
				strminute=Arr[1];
				strsec=Arr[2];				
			}else{
				strhour=Text.substring(8,10);
				strminute=Text.substring(10,12);
				strsec=Text.substring(12,14);		
			}
			
			if ((strhour=="") || (strminute=="") || (strsec=="")) {
			                IsTime=false;
							return IsTime;
			}else{
						if (!IsHours(strhour)){
								 IsTime=false;
								return IsTime;
						 }
											 
						 if (!IsMinutes(strminute)){
								IsTime=false;
								return IsTime;
						 }
						 
						if (!IsSecs(strsec)){
								 IsTime=false;
								return IsTime;
						 }
				}
			 
			 return IsTime;
	}
	//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	//Check Format Year , Christian Era format yyyy  example  2008
	function IsYears(sText){
	     var  IsYear=true;		
	    if(IsNumeric(sText)){
		          if(sText >2500){				  
				    IsYear=false;
				  }
		}else{		 
		  IsYear=false;
		}
		return IsYear;
	}	
	//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	function IsMonths(sText){
	//Check Format Month ,format month is number between  1 to 12
	     var  IsMonth=true;	         
	    if(IsNumeric(sText)){
		          if((sText < 1 ) || ( sText >12)){
				    IsMonth=false;
				  }
		}else{
		  IsMonth=false;
		}
		return IsMonth;
	}	
	//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	function IsDays(sText,sMonth,sYear){
	//Check Format day 
	//Month = 4,5,6,9,11   format day is number between  1 to 30
	//Month=2   format day is number between  1 to  28  or  1 to 29
	//Month= Other  format day is number between  1 to  31  
	     var  IsDay=true;		 
	    if(IsNumeric(sText)){
		          if(((sMonth == 4 ) || (sMonth == 6 ) || (sMonth == 9 ) || (sMonth == 11 )) && ((sText <1) || (sText >30))){				   
				    IsDay=false;
				  }
				  if(sMonth==2){
						var isleap = (sYear % 4 == 0 && (sYear % 100 != 0 || sYear % 400 == 0));
						if (sText > 29 || (sText==29 && !isleap)) {						
						    IsDay=false;
						}			         
				  }
				  if((sText <1) || (sText>31)){				    
				     IsDay=false;
				  }
		}else{
		  IsDay=false;
		}
		return IsDay;
	}
	//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	//Check Format Hour ,format Hour is number between  0 to 23
	function IsHours(sText){
	     var  IsHour=true;		 
	    if(IsNumeric(sText)){
		          if((sText < 0 ) || ( sText >23)){
				    IsHour=false;
				  }
		}else{
		  IsHour=false;
		}
		return IsHour;
	}
	//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	//Check Format Minute ,format minute is number between  0 to 59
	function IsMinutes(sText){
	     var  IsMinute=true;		 
	    if(IsNumeric(sText)){
		          if((sText < 0 ) || ( sText >59)){
				    IsMinute=false;
				  }
		}else{
		  IsMinute=false;
		}
		return IsMinute;
	}
	//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	//Check Format Second ,format Second is number between  0 to 59
	function IsSecs(sText){
	     var  IsSec=true;		 
	    if(IsNumeric(sText)){
		          if((sText < 0 ) || ( sText >59)){
				    IsSec=false;
				  }
		}else{
		  IsSec=false;
		}
		return IsSec;
	}
	//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	function IsNumeric(sText)
{
   var ValidChars = "0123456789.";
   var IsNumber=true;
   var Char;

 
   for (i = 0; i < sText.length && IsNumber == true; i++) 
      { 
      Char = sText.charAt(i); 
      if (ValidChars.indexOf(Char) == -1) 
         {
         IsNumber = false;
         }
      }
   return IsNumber;   
   }
   //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	function  CheckFileNamePR(sFormat,sHeader,sText,sLang){
	 // Check Format FileName 	
	//support format {Header}_yyyyMMddhhssmm_{T,E}.{doc,xls,pdf,zip,asp}   
	//example  amg_20080801123540_T.pdf  ,amg_20080801123540_E.pdf
	//support format {Header}_yyyyMMdd_{T,E}.{doc,xls,pdf,zip,asp}   
	//example  amg_20080801_T.pdf  ,amg_20080801_E.pdf
	 var Format=sFormat;
	 var Header=sHeader;	
	  var Text=sText;	  
	  var Lang=sLang;
	  var Iscorrect =true;
	   var TypeFile="doc,xls,pdf,zip,asp";
	  
	  var Arr=Text.split(".");
	  if(Arr.length !=2){
		       Iscorrect=false;
			  return Iscorrect;
		}	
	  
	  var  ArrName=Arr[0].split("_");	  
	  if(ArrName.length !=3){
		       Iscorrect=false;
			  return Iscorrect;
		}
		
		if((ArrName[0]!=Header) || (ArrName[2]!=Lang)){
				  Iscorrect=false;				  
			     return Iscorrect;
		}
		
		if((ArrName[1].length!=8) && (ArrName[1].length!=14)){
		          Iscorrect=false;				 
			     return Iscorrect;
		}
		
	    if (!IsDates(ArrName[1]," ")){
		          Iscorrect=false;
			     return Iscorrect;
		 }
		 
		 if (Format==1){
						if(!IsTimes(ArrName[1]," ")){
					              Iscorrect=false;
								 return Iscorrect;
					   }	
		 }	
		
		if(TypeFile.indexOf(Arr[1]) ==-1 ){
	          Iscorrect=false;
			  return Iscorrect;
		}
		
		return 	Iscorrect;
	}
   
   
   /******************************************************************************************************
   By using the format parameter, which is the second parameter in the funciton isFormatDate():
  ******************************************************************************************************/
// Date Validation Javascript
function isFormatDate(str){
   if (typeof str !== "string") {
      return false;
   }

   // format: dd.mm.yyyy or dd/mm/yyyy or dd-mm-yyyy ... for Turkish
   var s = String(str).split(/[-\/., ]/);
   var dd = s[0];
   var mm = s[1];
   var yyyy = s[2];
   var dateStr = mm + '/' + dd + '/' + yyyy; 

   // mm-dd-yyyy yyyy/mm/dd mm/dd/yyyy mmm dd, yyyy mm dd, yyyy ... for Date().parse(..)
   var dt = new Date(dateStr);
   if (dt.getDate() == dd && dt.getMonth() + 1 == mm && dt.getFullYear() == yyyy) {
      return true;
   }
   else{
   return false;
   }
   
} 

   /******************************************************************************************************
   By using the Compare Date parameter ,This function return a date object after accepting 
  ******************************************************************************************************/
  
function getDateObject(dateString,dateSeperator)
{
//This function return a date object after accepting 
//a date string ans dateseparator as arguments
var curValue=dateString;
var sepChar=dateSeperator;
var curPos=0;
var cDate,cMonth,cYear;

//extract day portion
curPos=dateString.indexOf(sepChar);
cDate=dateString.substring(0,curPos);

//extract month portion 
endPos=dateString.indexOf(sepChar,curPos+1); cMonth=dateString.substring(curPos+1,endPos);

//extract year portion 
curPos=endPos;
endPos=curPos+5; 
cYear=curValue.substring(curPos+1,endPos);

//Create Date Object
dtObject=new Date(cYear,cMonth,cDate); 
return dtObject;

}


  // +++++++++++++ Support Picture By Bowl 15/05/2014 +++++++++++++
	function  CheckPicturePR(sFormat,sHeader,sText){
	 var Format=sFormat;
	 var Header=sHeader;	
	  var Text=sText;	  
	  var Iscorrect =true;
	  var TypeFile="jpg,gif,png";
	  var Arr=Text.split(".");

	  if(Arr.length !=2){
		       Iscorrect=false;
			  return Iscorrect;
		}	
	  
	  var  ArrName=Arr[0].split("_");	  	 
	  if(ArrName.length !=2){
		       Iscorrect=false;
			  return Iscorrect;
		}

		if((ArrName[0]!=Header.substring(0,2))){
				  Iscorrect=false;				  
			     return Iscorrect;
		}
		
		if((ArrName[1].length!=8) && (ArrName[1].length!=17)){
		          Iscorrect=false;				 
			     return Iscorrect;
		}
		
	  var strName = ArrName[1].replace("IMG","");
	    if (!IsDates(strName," ")){
		          Iscorrect=false;
			     return Iscorrect;
		 }
		 
		 if (Format==1){
						if(!IsTimes(strName," ")){
					              Iscorrect=false;
								 return Iscorrect;
					   }	
		 }	
		
		if(TypeFile.indexOf(Arr[1]) ==-1 ){
	          Iscorrect=false;
			  return Iscorrect;
		}

		return 	Iscorrect;
	}
	
	
	
function  CheckPictureGM(sFormat,sHeader,sText,sLang){
	 var Format=sFormat;
	 var Header=sHeader;	
	  var Text=sText;	  
	  var Lang=sLang;
	  var Iscorrect =true;
	  var TypeFile="jpg,gif,png";
	  var Arr=Text.split(".");

	  if(Arr.length !=2){
		       Iscorrect=false;
			  return Iscorrect;
		}	
	  
	 var  ArrName=Arr[0].split("_");	  	 
	  if(ArrName.length !=3){
		       Iscorrect=false;
			  return Iscorrect;
		}
		
		if((ArrName[0]!=Header.substring(0,4))){
				  Iscorrect=false;				  
			     return Iscorrect;
		}
		 
		if((ArrName[0]!=Header) || (ArrName[1].indexOf("GM") ==-1) || (ArrName[2]!=Lang)){
				  Iscorrect=false;				  
			     return Iscorrect;
		}	 
		 
		if(TypeFile.indexOf(Arr[1]) ==-1 ){
	          Iscorrect=false;
			  return Iscorrect;
		}

		return 	Iscorrect;
	}
   
 function BannerKey(e) {
      var key = (e.which) ? e.which : event.keyCode;
          
      if ((key==44) || (key==46) || (key > 47 && key < 58)) {
         return true;
      }
         return false;
   } 

  
















