<%if session("user_id")<>"" then%>
<!-- #include file = "../../../i_constant.asp" -->
<!-- #include file = "../../../includefile_i_contant.asp" -->
<!-- #include file = "../../../includefile_style.asp" -->
<!--#include file = "../../../i_conir.asp" -->
<!--#include file = "../../../i_conirauthen_irdb.asp" -->
<!--#include file = "../../../i_conirauthen.asp" -->
<!--#include file = "../../global_function.asp" -->

<%
If Request("update") <> "" Then
	Const SaveDir = "File"							' Config Filename for Upload file to Server
	
'------------------ Upload file --------------------
	sFullFilename_Th = ""
	sFullFilename_En = ""
	
	Uploaded = True
	On Error Resume Next
	
	Set objUpload = Server.CreateObject("Dundas.Upload.2")
	If Err.Number <> 0 Then
		Uploaded = False
	End If	
'populate collections and retrieve all uploaded form data (including uploaded files)
	objUpload.SaveToMemory
	If Err.Number <> 0 Then
		Uploaded = False
	End If
	
	'-------------------- Set Upload file Thai ----------------
	Set objUploadedFile_Th = objUpload.Files("uploadfile_th")
	If Err.Number <> 0 Then
		Uploaded = False
	End If
	Uploadfilename_Th = objUploadedFile_Th.OriginalPath
	
	if Uploadfilename_Th <> "" then
		sUploadedFileName = Trim(objUpload.GetFileName(objUploadedFile_Th.OriginalPath))
		sFileExt = objUpload.GetFileExt(sUploadedFileName)
		sFullFilename_Th = objUpload.Form("upload_filename_th") & "." & sFileExt
	
		objUploadedFile_Th.SaveAs server.MapPath(SaveDir) & "\" & sFullFilename_Th
		If Err.Number <> 0 Then
			Uploaded = False
		End If
	end if
	'-------------------- Set Upload file English ----------------
	Set objUploadedFile_En = objUpload.Files("uploadfile_en")
	If Err.Number <> 0 Then
		Uploaded = False
	End If
	Uploadfilename_En = objUploadedFile_En.OriginalPath
	
	if Uploadfilename_En <> "" then
		sUploadedFileName = Trim(objUpload.GetFileName(objUploadedFile_En.OriginalPath))
		sFileExt = objUpload.GetFileExt(sUploadedFileName)
		sFullFilename_En = objUpload.Form("upload_filename_en") & "." & sFileExt
	
		objUploadedFile_En.SaveAs server.MapPath(SaveDir) & "\" & sFullFilename_En
		If Err.Number <> 0 Then
			Uploaded = False
		End If
	end if	
'---------------- End Upload file ------------------
'---------------- Update Database if Upload file complete. -----------------
	if Uploaded <> False then		
			if isObject(objUpload.Form("title_th")) then 
					title_th = objUpload.Form("title_th").Value
			end if
			if isObject(objUpload.Form("title_en")) then 
					title_en = objUpload.Form("title_en").Value
			end if
			
			sql=""	
			sql="UPDATE tbl_trans_press_release set title_th='"&title_th&"',title_en='"&title_en&"'" 
			If sFullFilename_Th <> "" then
				sql=sql& ",filename_th='"& sFullFilename_Th &"'"
			end if
			
			If sFullFilename_En <> "" then
				sql=sql& ",filename_en='"& sFullFilename_En &"'"
			end if
			sql=sql & ",activated='"& objUpload.Form("activate").Value &"',updated_timestamp=now(),release_timestamp='"&objUpload.Form("event_timestamp").Value&"'"	
			sql=sql & " where id=" & objUpload.Form("id").Value
			conirauthen_irdb.execute(sql)			
			call Insert_LogAdminActivity(conirauthen,session("user_id"),"ARG",new_id)
			
			conirauthen_irdb.close
			conirauthen.close		
			
			Set objUpload = Nothing
	%>
	<script language="javascript">
		alert( "Updated press release completely.");
		window.location.href="ViewPressRelease.asp";										
	</script>
	<%	else
			Set objUpload = Nothing
	%>
	<script language="javascript">
		alert( "Can not upload file.");
		window.location.href="ViewPressRelease.asp";										
	</script>
	<%
	end if
End If
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<title><%=Ucase(WEBSITE_NAME)%>/Edit Press Release</title>
<meta http-equiv="Content-Type" content="text/html; charset=windows-874">
<script language="JavaScript" src = "../../irthai_function.js"></script>
<script language="JavaScript" src = "../../../validationCheck.js"></script>

<script language="JavaScript">	
	function OnSubmitFrm(formname)
	{	
		if (validateFormOnSubmit(formname)==false)	{return false;	}
		
		//check title_th and 	title_en
		if(( document.frm.title_th.value == "") && ( document.frm.title_en.value== "") ) {
			alert("Please fill title thai or title english one more.");
			return  false;
		}
	
		//check title_th and filename_th
		if(document.frm.title_th.value!=""){
			if((document.frm.old_filename_th.value=="")&&(document.frm.uploadfile_th.value=="")) {
					alert("Please selected file name thai.");
					return  false;
			}
		}
		
		//check title_en and filename_en
		if(document.frm.title_en.value!=""){
			if((document.frm.old_filename_en.value=="")&&(document.frm.uploadfile_en.value=="")) {
					alert("Please selected file name english.");
					return  false;
			}
		}
				
		if(!CheckDates(1,document.frm.event_timestamp.value)){	 alert("Date & time is invalid!.");	 return false;}		
	
		var filename_th = (document.frm.uploadfile_th.value).toLowerCase();
		if (filename_th!=""){
			if(!IsConsistOf(".pdf",filename_th))  { alert('File name thai  is invalid!.');	 return false;}		
		}
		
		var filename_en = (document.frm.uploadfile_en.value).toLowerCase();
		if(filename_en!="")		{
			if(!IsConsistOf(".pdf",filename_en))  { alert('File name english  is invalid!.');	 return false;}		
		}
			
		// The Last Condition : All data is valid. //
		return true;
	}
	
	function checkvalue()
	{		
			if(document.frm.uploadfile_th.value!=''){return true;}	
			else if(document.frm.uploadfile_en.value!=''){return true;	}	
			else if (document.frm.old_event_timestamp.value!=document.frm.event_timestamp.value){return true;	}
			else if (document.frm.old_title_th.value!=document.frm.title_th.value){return true;	}
			else if (document.frm.old_title_en.value!=document.frm.title_en.value)	{return true;	}
			else if (document.frm.old_activated.value!=document.frm.activate.value)	{return true;	}
			else{return false;}			
	}
</script>


</head>
<body leftmargin="0" topmargin="5" marginwidth="0" marginheight="5">
<div align="center">  	
	<table width="715" valign="top" border="0" cellspacing="0" cellpadding="0" >
	<tr>
	  <td align="left" valign="top">
			<table width="100%"  border="0" cellpadding="0" cellspacing="0" bgcolor="cfcfcf">
				<tr align="left" valign="top">         
						<td bgcolor="#FFFFFF">
								<table width="100%"  border="0" cellspacing="0" cellpadding="0">
										<tr>
												<td align="left" valign="top" >						
														   <!-- #include file = "../../../includefile_i_top.asp" -->
												</td>
											</tr>			
											<tr>
											  <td align="left" valign="top" bgcolor="#FFFFFF" >
													<form id="frm" name="frm" enctype="multipart/form-data"  method="post" action="?id=<%=request("id")%>&update=true" onsubmit="javascript:if(checkvalue()){return OnSubmitFrm('frm');}else{return false;};">
														<table width = "100%" align="center" cellpadding=0 cellspacing="10" >
															<tr align="left" valign="top">
																<td ><br>
																	<table width="100%" border="0" cellpadding="0" cellspacing="1" bgcolor="#CC99CC">
																		<tr align="left" valign="top">
																			<td width="350" valign="middle" bgcolor="#FFFFFF"><font class="stylefont"><strong><img src="../../../images/arrow.gif" width="12" height="9">Press Release : <%=session("user_id")%></strong></font></td>
																			<td bgcolor="#cfcfcf">&nbsp;</td>
																		</tr>
																	</table>
																</td>
															</tr>															
															<tr align="left" valign="top">
																<td>
																	<!------------------------------------------------------------------------------------------>
																	<TABLE  cellSpacing="10" cellPadding="0" border="0">
																		<TR>
																			<td width="50"><b>Listed :</b></td>
																			<TD><font color="#0000FF"><b><%=session("listed_name")%></b></font></TD>
																		</TR>
																	</TABLE>
																	<!----------------------------------------------------------------------------------------->
																</td>																				
															</tr>															
															<tr>
																<td  align="center" valign="top"  width="100%" bgcolor="#FFFFFF">
																	<table width="90%" border="0" cellpadding=0 cellspacing=0  bordercolor="#000066"  align="center">			
																		<tr>
																			<td>
																				<table width="100%" border="0" cellpadding=0 cellspacing=3  style="border:5px solid #FAF8F5" bgcolor="#F0DDF0" align="center">			
																					<tr>
																						<td>&nbsp;<b><font face="MS Sans Serif"  color="#330000">Edit Press Release</font></b></td>
																					</tr>
																				</table>
																			</td>
																		</tr>
																		<tr>
																			<td>
																				<table border="0" cellpadding=5 cellspacing=0  align="center" width="100%" bgcolor="#FAF8F5">	
																				<!-----------------------------------------------input data for insert----------------------------------------------------------------->		
																				<%
																					sql="SELECT * FROM tbl_trans_press_release where id=" &  request("id")
																					
																					set rs = conir.execute(sql)
																								
																					if not rs.bof and not rs.eof   then																							
																						event_timestamp=DateToString(rs("release_timestamp"),"YYYY-MM-DD HH:mm:ss")
																						title_th=rs("title_th")
																						title_en=rs("title_en")
																						filename_th=rs("filename_th")
																						filename_en=rs("filename_en")
																						activated=rs("activated")																						
																						
																						old_event_timestamp=DateToString(rs("release_timestamp"),"YYYY-MM-DD HH:mm:ss")
																						old_title_th=rs("title_th")
																						old_title_en=rs("title_en")
																						old_filename_th=rs("filename_th")
																						old_filename_en=rs("filename_en")
																						old_activated=rs("activated")													
																					
																					
																						'....................... Add for Default File name Press Release ......................
																						if filename_th <> "" then
																							default_filename =  mid(filename_th,1,instr(filename_th,".pdf")-3)
																						elseif filename_en <> "" then
																							default_filename =  mid(filename_en,1,instr(filename_en,".pdf")-3)
																						end if																					
																					end if
																					%>																						
																					<tr><td>&nbsp;</td><td&nbsp;></td></tr>																					
																					<tr>
																						<td align="right" valign="top" width="25%"><b><font face="MS Sans Serif" size="2" color="#330000">Date & Time  :</font></b>
																						</td>
																						<td align="left">	
																							<input type="text" name="event_timestamp" validate=true msgwarning="Please fill date & time." size="30" class="textfield_style"  value="<%=event_timestamp%>" maxlength = "19"><font color="#FF0000"><strong>&nbsp;*</strong></font><br>
																							<font face="MS Sans Serif" size="1" color="#330000">[ �ٻẺ�ѹ��� YYYY-MM-DD HH:mm:ss �� 2008-07-24 12:30:40 ]</font>
																						</td>
																					</tr>
																					<tr>
																						<td align="right" valign="top"><b><font face="MS Sans Serif" size="2" color="#330000">Title Thai  :</font></b></td>
																						<td align="left">	
																							<textarea rows="2" cols="61" name="title_th" maxlength ="255" msgwarning="Please fill title thai."  class="textfield_style"><%=title_th%></textarea>
																						</td>
																					</tr>
																					<tr>
																						<td align="right" valign="top"><b><font face="MS Sans Serif" size="2" color="#330000">Title English  :</font></b></td>
																						<td align="left">	
																							<textarea rows="2" cols="61" name="title_en" maxlength ="255"  class="textfield_style"><%=title_en%></textarea>
																							</font>
																						</td>
																					</tr>	
																						<!----------------------------------------------------- Upload filename ---------------------------------------------------->
																					<tr>
																						<td align="right" valign="top"><b><font face="MS Sans Serif" size="2" color="#330000">File Name  :</font></b></td>
																						<td align="left">	
																							<input type="text" name="old_filename_th" value="<%=filename_th%>" readonly class="textfield_style" size="30">																							
																							<br>																							
																							<input type="file" name="uploadfile_th"   size="30" ><br>																							
																							<font face="MS Sans Serif" color="#330000" size="1">[ �ٻẺ��õ�駪������ press_YYYYMMDDHHmmss_T.pdf  <br>�� press_20080724123040_T.pdf   ]</font>
																							<!--input type="hidden" name="upload_filename_th" value="<%'="press_"&DateToString(now(),"YYYYMMDDHHmmss") & "_T"%>"-->
                                                                     <!--<input type="hidden" name="upload_filename_th" value="<%'=mid(filename_th,1,instr(filename_th,".")-1)%>">-->
																							<input type="hidden" name="upload_filename_th" value="<%=default_filename & "_T"%>">																	 
																						</td>
																					</tr>
																						<tr>
																						<td align="right" valign="top"><b><font face="MS Sans Serif" size="2" color="#330000">File Name  :</font></b></td>
																						<td align="left">	
																							<input type="text" name="old_filename_en" value="<%=filename_en%>" readonly class="textfield_style" size="30">																							
																							<br>																							
																							<input type="file" name="uploadfile_en"   size="30" >&nbsp;&nbsp;<br>																							
																							<font face="MS Sans Serif" color="#330000" size="1">[ �ٻẺ��õ�駪������ press_YYYYMMDDHHmmss_E.pdf  <br>�� press_20080724123040_E.pdf   ]</font>
																							<!--input type="hidden" name="upload_filename_en" value="<%'="press_"&DateToString(now(),"YYYYMMDDHHmmss") & "_E"%>"-->
                                                                     <!--<input type="hidden" name="upload_filename_en" value="<%'=mid(filename_en,1,instr(filename_en,".")-1)%>">-->
																							<input type="hidden" name="upload_filename_en" value="<%=default_filename & "_E"%>">				
																						</td>
																					</tr>
																					
																					<!--------------------------------------------------- End Upload filename ---------------------------------------------------->					
																					<tr>
																						<td align="right" valign="top"><b><font face="MS Sans Serif" size="2" color="#330000">Activate  :</font></b></td>
																						<td align="left">	
																							<select name="activate" size="1"   class="selection_style">
																							   <option value="Y" <%if activated <>"N" then%>selected<%end if%> >Yes</option>
																							   <option value="N" <%if activated ="N" then%>selected<%end if%>>No</option>
																							</select><font color="#FF0000"><strong>&nbsp;*</strong></font>
																						</td>
																					</tr>			
																			<!-----------------------------------------------End input data for insert----------------------------------------------------------------->
																					<tr>
																						<td height="5" colspan="2"><BR>
																						</td>
																					</tr>
																					<tr>
																						<td height="5" ><BR></td>
																						<td align="left"  valign="top">																											
																							<input type="submit"  name= "Action" value="UPDATE !!" style="cursor:hand;" >
																							<input type="button"  name="Action" value="CANCEL" onclick="javascript: window.location='ViewPressRelease.asp'"  style="cursor:hand;" >
																						</td></tr>
																					<tr><td height="5" colspan="2"></td>
																					</tr>
																				</table>
																			</td>
																		</tr>
																			<tr>
																				<td bgcolor="#FFFFFF" colspan="2">
																						<table width="100%"  border="0"   align="center">
																							<td>
																									<input type="hidden" name="id" value="<%=request("id")%>">																				
																									<input type="hidden" name="old_event_timestamp" value="<%=old_event_timestamp%>">
																									<input type="hidden" name="old_title_th" value="<%=old_title_th%>">
																									<input type="hidden" name="old_title_en" value="<%=old_title_en%>">
																									<input type="hidden" name="old_activated" value="<%=old_activated%>">																							
																									<font class="stylefont_a"><font color="#0000cc">
																									--->��ͧ���������ͧ���� <font color="#FF0000"><strong>&nbsp;*&nbsp;</strong></font> ���繵�ͧ�բ����� <br>
																									 --->Title Thai ��� Title English ����ö�繤����ҧ���ҧ����ҧ˹���� ��е�ͧ�����ҧ����ͧ <br>
																									 --->File Name Thai ��� File Name English  ����ö�繤����ҧ���ҧ����ҧ˹���� ��е�ͧ�����ҧ����ͧ <br>
																									&nbsp;&nbsp;&nbsp;&nbsp;�¨е�ͧ����ѹ��Ѻ Title Thai ��� Title English �� ��� Title Thai �բ����� File Name Thai ��е�ͧ�բ����Ŵ����蹡ѹ
																									</font></font>
																							</td>
																						</table>
																				</td>
																		</tr>
																		<tr>
																			<td  align="right" colspan="2">
																					<hr color="#4a91d9" size=2>
																			</td>
																		</tr>
																		<tr>
																			<td >
																						<input type = "button" value = "Main Menu"  onclick="location='../../main_menu.asp'" style="FONT-FAMILY: Ms sans 
																		serif;FONT-SIZE:10px; font-weight: bold;cursor:hand;">
																			</td>
																		</tr>					
																	</table>
																</td>
															</tr>										
														</table>
													</form>	
												<td>
											</tr>
										<!--#include file = "../../../i_footer.asp" -->
								</table>
							</td>
						</tr>
				</table>	
	<p>&nbsp;</p>
</div>

<%else%>
<br><br><br>
<center><font style="font-size:18pt;color:red"><%response.redirect "../../login.asp"%></font></center>
<%end if%>
</body>
</html>
