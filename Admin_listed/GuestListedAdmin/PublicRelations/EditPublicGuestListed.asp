<%@ Language=VBScript %>

<%if session("user_id")<>"" then%>
<!-- #include file = "../../../i_constant.asp" -->
<!-- #include file = "../../../includefile_i_contant.asp" -->
<!-- #include file = "../../../includefile_style.asp" -->
<!--#include file = "../../../i_conir.asp" -->
<!--#include file = "../../../i_conirauthen_irdb.asp" -->
<!--#include file = "../../../i_conirauthen.asp" -->
<!--#include file = "../../global_function.asp" -->

<%
If Request("update") <> "" Then
	Const SaveDir = "File"							' Config Filename for Upload file to Server
'------------------ Upload file --------------------
	Uploaded = True
	On Error Resume Next
	
	Set objUpload = Server.CreateObject("Dundas.Upload.2")
	If Err.Number <> 0 Then
		Uploaded = False
	End If	
'populate collections and retrieve all uploaded form data (including uploaded files)
	objUpload.SaveToMemory
	If Err.Number <> 0 Then
		Uploaded = False
	End If
	Set objUploadedFile = objUpload.Files("uploadfile")

	If Err.Number <> 0 Then
		Uploaded = False
	End If
	Uploadfilename = objUploadedFile.OriginalPath
	
	if Uploadfilename <> "" then
		sUploadedFileName = Trim(objUpload.GetFileName(objUploadedFile.OriginalPath))
		sFileExt = objUpload.GetFileExt(sUploadedFileName)
		sFullFilename = objUpload.Form("upload_filename") & "." & sFileExt
	
		objUploadedFile.SaveAs server.MapPath(SaveDir) & "\" & sFullFilename
		If Err.Number <> 0 Then
			Uploaded = False
		End If
	end if
'---------------- End Upload file ------------------
'---------------- Update Database if Upload file complete. -----------------
	if Uploaded <> False then
			if isObject(objUpload.Form("title_english")) then 
					title_en = objUpload.Form("title_english").Value
			end if
				
			sql="update public_relation set title='"&title_en&"'"
			sql=sql&",title_th='"&objUpload.Form("title_thai_require").Value &"',last_update=now(),date_event='"&objUpload.Form("event_timestamp_require").Value&"',update_by='"&session("user_id")&"'"
			sql=sql&",display_listed='Y',display_ir='"&objUpload.Form("activate").Value&"' " 
			
			if sFullFilename <> "" then
				sql=sql&",name='"&sFullFilename&"' "		
			end if
			sql=sql&" where id=" &objUpload.Form("id").Value 			
			conirauthen_irdb.execute(sql)						

			call Insert_LogAdminActivity(conirauthen,session("user_id"),"APG",new_id)
			
			conirauthen_irdb.close
			conirauthen.close		
			
			Set objUpload = Nothing
	%>
	<script language="javascript">
		alert( "Updated public relation completely.");
		window.location.href="ViewPublicGuestListed.asp";										
	</script>
	<%	else
			Set objUpload = Nothing
	%>
	<script language="javascript">
		alert( "Can not upload file.");
		window.location.href="ViewPublicGuestListed.asp";										
	</script>
	<%
	end if
End If
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<title><%=Ucase(WEBSITE_NAME)%>/Edit Public Relation</title>
<meta http-equiv="Content-Type" content="text/html; charset=windows-874">
<script language="JavaScript" src = "../../irthai_function.js"></script>
<script language="JavaScript" src = "../../../validationCheck.js"></script>

<script language="JavaScript">	
	function OnSubmitFrm(formname)
	{	
		if (validateFormOnSubmit(formname)==false)	{return false;	}
		if(!CheckDates(1,document.frm.event_timestamp_require.value)){	 alert("Date & time is invalid!.");	 return false;}		
	
		var filename = (document.frm.uploadfile.value).toLowerCase();
		if (filename!=''){
			if(!IsConsistOf(".pdf|.doc|.zip|.xls",filename))  { alert('File name is invalid!.');	 return false;}		
		}
		// The Last Condition : All data is valid. //
		return true;
	}
	
	function checkvalue()
	{			
			if(document.frm.uploadfile.value!=""){ return true;	}			
			else if (document.frm.old_event_timestamp.value!=document.frm.event_timestamp_require.value){return true;	}
			else if (document.frm.old_title_th.value!=document.frm.title_thai_require.value){return true;	}
			else if (document.frm.old_title_en.value!=document.frm.title_english.value)	{return true;	}
			else if (document.frm.old_activated.value!=document.frm.activate.value)	{return true;	}
			else{return false;}			
	}
</script>

</head>
<body leftmargin="0" topmargin="5" marginwidth="0" marginheight="5">
<div align="center">  	
	<table width="715" valign="top" border="0" cellspacing="0" cellpadding="0" >
	<tr>
	  <td align="left" valign="top">
			<table width="100%"  border="0" cellpadding="0" cellspacing="0" bgcolor="cfcfcf">
				<tr align="left" valign="top">         
						<td bgcolor="#FFFFFF">
								<table width="100%"  border="0" cellspacing="0" cellpadding="0">
										<tr>
												<td align="left" valign="top" >						
														   <!-- #include file = "../../../includefile_i_top.asp" -->
												</td>
											</tr>			
											<tr>
											  <td align="left" valign="top" bgcolor="#FFFFFF" >
													<form id="frm" name="frm" enctype="multipart/form-data"  method="post" action="?update=true" onsubmit="javascript:if(checkvalue()){return OnSubmitFrm('frm');}else{return false;};">
													<!--<form id="frm" name="frm" enctype="multipart/form-data"  method="post" action="UploadPublicGuestListed.asp" onsubmit="javascript: return OnSubmitFrm('frm');">	-->
														
														<table width = "100%" align="center" cellpadding=0 cellspacing="10" >
															<tr align="left" valign="top">
																<td ><br>
																	<table width="100%" border="0" cellpadding="0" cellspacing="1" bgcolor="#CC99CC">
																		<tr align="left" valign="top">
																			<td width="350" valign="middle" bgcolor="#FFFFFF"><font class="stylefont"><strong><img src="../../../images/arrow.gif" width="12" height="9">Public Relations : <%=session("user_id")%></strong></font></td>
																			<td bgcolor="#cfcfcf">&nbsp;</td>
																		</tr>
																	</table>
																</td>
															</tr>															
															<tr align="left" valign="top">
																<td>
																	<!------------------------------------------------------------------------------------------>
																	<TABLE  cellSpacing="10" cellPadding="0" border="0">
																		<TR>
																			<td width="50"><b>Listed :</b></td>
																			<TD><font color="#0000FF"><b><%=session("listed_name")%></b></font></TD>
																		</TR>
																	</TABLE>
																	<!----------------------------------------------------------------------------------------->
																</td>																				
															</tr>															
															<tr>
																<td  align="center" valign="top"  width="100%" bgcolor="#FFFFFF">
																	<table width="90%" border="0" cellpadding=0 cellspacing=0  bordercolor="#000066"  align="center">			
																		<tr>
																			<td>
																				<table width="100%" border="0" cellpadding=0 cellspacing=3  style="border:5px solid #FAF8F5" bgcolor="#F0DDF0" align="center">			
																					<tr>
																						<td>&nbsp;<b><font face="MS Sans Serif"  color="#330000">Edit Public Relations</font></b></td>
																					</tr>
																				</table>
																			</td>
																		</tr>
																		<tr>
																			<td>
																				<table border="0" cellpadding=5 cellspacing=0  align="center" width="100%" bgcolor="#FAF8F5">	
																				<!-----------------------------------------------input data for insert----------------------------------------------------------------->		
																				<%
																					sql="SELECT * FROM public_relation where id=" &  request("id")
																					
																					set rs = conir.execute(sql)
																								
																					if not rs.bof and not rs.eof   then																							
																						event_timestamp=DateToString(rs("date_event"),"YYYY-MM-DD HH:mm:ss")
																						title_th=rs("title_th")
																						title_en=rs("title")
																						filename=rs("name")
																						activated=rs("display_ir")																						
																						
																						old_event_timestamp=DateToString(rs("date_event"),"YYYY-MM-DD HH:mm:ss")
																						old_title_th=rs("title_th")
																						old_title_en=rs("title")
																						old_filename=rs("name")
																						old_activated=rs("display_ir")													
																					end if
																					%>																						
																					<tr><td>&nbsp;</td><td></td></tr>																					
																					<tr>
																						<td align="right" valign="top"><b><font face="MS Sans Serif" size="2" color="#330000">Date & Time  :</font></b>
																						</td>
																						<td align="left">	
																							<input type="text" name="event_timestamp_require" msgwarning="Please fill date & time." size="30" class="textfield_style"  value="<%=event_timestamp%>" maxlength = "19"><font color="#FF0000"><strong>&nbsp;*</strong></font><br>
																							<font face="MS Sans Serif" size="1" color="#330000">[ �ٻẺ�ѹ��� YYYY-MM-DD HH:mm:ss �� 2008-07-24 12:30:40 ]</font>
																						</td>
																					</tr>
																					<tr>
																						<td align="right" valign="top"><b><font face="MS Sans Serif" size="2" color="#330000">Title Thai  :</font></b></td>
																						<td align="left">	
																							<textarea rows="2" cols="61" name="title_thai_require" maxlength ="255" msgwarning="Please fill title thai."  class="textfield_style"><%=title_th%></textarea>
																							<font color="#FF0000"><strong>&nbsp;*</strong></font>
																						</td>
																					</tr>
																					<tr>
																						<td align="right" valign="top"><b><font face="MS Sans Serif" size="2" color="#330000">Title English  :</font></b></td>
																						<td align="left">	
																							<textarea rows="2" cols="61" name="title_english" maxlength ="255"  class="textfield_style"><%=title_en%></textarea>
																							</font>
																						</td>
																					</tr>	
																					
																						<!----------------------------------------------------- Upload filename ---------------------------------------------------->
																					<tr>
																						<td align="right" valign="top"><b><font face="MS Sans Serif" size="2" color="#330000">File Name  :</font></b></td>
																						<td align="left">	
																							<input type="text" name="old_filename" value="<%=filename%>" readonly class="textfield_style" size="30">																							
																							<br>																							
																							<input type="file" name="uploadfile"   size="30" >&nbsp;&nbsp;<font color="#FF0000"><strong>&nbsp;*</strong></font>&nbsp;<br>																							
																							<font face="MS Sans Serif" color="#330000" size="1">[ �ٻẺ��õ�駪������ PR_YYYYMMDDHHmmss.{doc,zip,pdf,xls}  <br>�� PR_20080724123040.pdf  ]</font>
																							<!--input type="hidden" name="upload_filename" value="<%'="PR_"&DateToString(now(),"YYYYMMDDHHmmss")%>"-->
                                                                     <input type="hidden" name="upload_filename" value="<% if filename <> "" then%><%=mid(filename,1,instr(filename,".")-1)%><%end if%>">
																						</td>
																					</tr>
																					
																					<!--------------------------------------------------- End Upload filename ---------------------------------------------------->					
																						<td align="right" valign="top"><b><font face="MS Sans Serif" size="2" color="#330000">Activate  :</font></b></td>
																						<td align="left">	
																							<select name="activate" size="1"   class="selection_style">
																							   <option value="Y" <%if activated <>"N" then%>selected<%end if%> >Yes</option>
																							   <option value="N" <%if activated ="N" then%>selected<%end if%>>No</option>
																							</select><font color="#FF0000"><strong>&nbsp;*</strong></font>
																						</td>
																					</tr>			
																			<!-----------------------------------------------End input data for insert----------------------------------------------------------------->
																					<tr>
																						<td height="5" colspan="2"><BR>
																						</td>
																					</tr>
																					<tr>
																						<td height="5" ><BR></td>
																						<td align="left"  valign="top">																											
																							<input type="submit"  name= "Action" value="UPDATE !!" style="cursor:hand;" >
																							<input type="button"  name="Action" value="CANCEL" onclick="javascript: window.location='ViewPublicGuestListed.asp'"  style="cursor:hand;" >
																						</td></tr>
																					<tr><td height="5" colspan="2"></td>
																					</tr>
																				</table>
																			</td>
																		</tr>
																			<tr>
																				<td bgcolor="#FFFFFF" colspan="2">
																						<table width="100%"  border="0"   align="center">
																							<td>
																									<input type="hidden" name="id" value="<%=request("id")%>">																				
																									<input type="hidden" name="old_event_timestamp" value="<%=old_event_timestamp%>">
																									<input type="hidden" name="old_title_th" value="<%=old_title_th%>">
																									<input type="hidden" name="old_title_en" value="<%=old_title_en%>">
																									<input type="hidden" name="old_filename" value="<%=old_filename%>">
																									<input type="hidden" name="old_activated" value="<%=old_activated%>">																							
																									<font class="stylefont_a"><font color="#0000cc">
																									--->��ͧ���������ͧ���� <font color="#FF0000"><strong>&nbsp;*&nbsp;</strong></font> ���繵�ͧ�բ����� <br><br>
																									
																									</font></font>
																							</td>
																						</table>
																				</td>
																		</tr>
																		<tr>
																			<td  align="right" colspan="2">
																					<hr color="#4a91d9" size=2>
																			</td>
																		</tr>
																		<tr>
																			<td >
																						<input type = "button" value = "Main Menu"  onclick="location='../../main_menu.asp'" style="FONT-FAMILY: Ms sans 
																		serif;FONT-SIZE:10px; font-weight: bold;cursor:hand;">
																			</td>
																		</tr>					
																	</table>
																</td>
															</tr>										
														</table>
													</form>	
												<td>
											</tr>
										<!--#include file = "../../../i_footer.asp" -->
								</table>
							</td>
						</tr>
				</table>	
	<p>&nbsp;</p>
</div>

<%else%>
<br><br><br>
<center><font style="font-size:18pt;color:red"><%response.redirect "../../login.asp"%></font></center>
<%end if%>
</body>
</html>
