<!-- #include virtual = "/ir/i_constant.asp" -->
<!-- #include virtual = "/ir/includefile_i_contant.asp" -->
<!--#include virtual="/ir/formatdate.asp"-->
<!-- #include virtual = "/ir/i_conirauthen.asp" -->
<!-- #include virtual = "/ir/i_conpsims.asp" -->
<!--#include virtual = "/ir/i_listed_member.asp" -->

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>

<%
Function StringPeerList
	if strPeerList <> "" then
		StringPeerList = strPeerList
	else 
		StringPeerList = PeerList
	end if
End Function
%>

<%%>

<%Function PeerList%>
		<%
		'strSql = "select com_id,sec_name from Compsec where sector_no = "& sector_no &" and  sec_type =  'S' and sec_list_status = 'L' and flag <> 'D' order by sec_name"
		strSql = ""
		strSql = "select com_id,sec_id,sec_name,sector_no,mk_type from Compsec where sec_type =  'S' and mk_type in ('A','S') and sec_list_status = 'L' and flag <> 'D'  and trading_date <> '' order by sector_no,sec_name"
		set rs = conpsims.execute(strSql)
		
		if not rs.eof and not rs.bof then
			do while not rs.eof			
				'if rs("com_id") <> session("listed_id") then
					PeerList = PeerList & "<option value=" & rs("sec_id") & ":" & "sector_no=" & rs("sector_no") & "|mk_type=" & rs("mk_type") & ">" & rs("sec_name")  & "</option>"
				'end if
				rs.movenext
			loop
			rs.close		
		end if
		set rs = nothing
		
End Function		
%>

<%
Function AddString(Source,AddStr)
	
	if Source <> "" and (Cstr(AddStr) <> "" and Cstr(AddStr) <> "-1")  then
		str = "," & AddStr
	elseif Source = "" then
		str = AddStr
	end if
	
	AddString = Source & str
End Function
%>

<%

Sub LoadStockInfo()
	' ArrStockInfo(i,0|sec_id;i,1|sec_id;i,2|account_form;i,3|revenue;i,4|close;i,5|listed_share;i,6|mk_cap;i,7|pe;i,8|%EBIDA Margin;i,9|%Net profit Margin;i,10|year financial statement)
	strSecId = ""
	
	if listed <> -1 and cstr(listed) <> "" then
		ArrStockInfo(0,0) = listed
		strSecId = AddString(strSecId,listed)
	end if	
	if peera <> -1 and cstr(peera) <> "" then
		ArrStockInfo(1,0) = peera
		strSecId = AddString(strSecId,peera)
	end if	
	if peerb <> -1 and cstr(peerb) <> "" then
		ArrStockInfo(2,0) = peerb
		strSecId = AddString(strSecId,peerb)
	end if	
	if peerc <> -1 and cstr(peerc) <> "" then
		ArrStockInfo(3,0) = peerc
		strSecId = AddString(strSecId,peerc)
	end if	
	
	'========= 1 : sec_id , account_form , m_value1 ========'
	strSQL = "select C.sec_id,account_form , m_value1 from Compsec C , Short_Fin S "
	strSQL = strSQL & " where C.sec_id in (" & strSecId & ") and C.sec_id = S.sec_id and (sec_type = 'S' and flag <> 'D' " 
	strSQL = strSQL & " and sector_no <> '0' and sec_list_status = 'L' and (mk_type='A' or mk_type='S') and account_form <> 'N') and i_series = 1100 "
	strSQL = strSQL & " order by C.sec_id "	

	set rs = conpsims.execute(strSQL)
		
	strSecId = ""		
	if not rs.eof and not rs.bof then
		do while not rs.eof			
			arr_index = GetIndex(ArrStockInfo, rs("sec_id"),0)		' Find Index Array By sec_id
			
			if strSecId = "" then
				strSecId = rs("sec_id")			
			else
				strSecId = strSecId & "," & rs("sec_id")			
			end if
			
			ArrStockInfo(arr_index,1) = rs("sec_id")
			ArrStockInfo(arr_index,2) = rs("account_form")
			ArrStockInfo(arr_index,3) = rs("m_value1")

			rs.movenext
		loop
		rs.close		
	end if
	set rs = nothing	
	
	'========= 2 ========'
	strSQL = "select date_format(max(date),'%Y-%m-%d') as close_date from d_stat"	
	
	set rs = conpsims.execute(strSQL)
	if not rs.eof and not rs.bof then
		close_date = rs("close_date")
		rs.close		
	end if
	set rs = nothing	
	
	'========= 3 ========'
	strSQL = "select sec_id,close,listed_share,mk_cap,pe from d_stat where sec_id in (" & strSecId & ") and date='" & close_date & "'  order by sec_id  "
	'response.write strSQL
	set rs = conpsims.execute(strSQL)
	if not rs.eof and not rs.bof then
		do while not rs.eof		
			arr_index = GetIndex(ArrStockInfo, rs("sec_id"),1)		' Find Index Array By sec_id
			
			ArrStockInfo(arr_index,4) = rs("close")
			ArrStockInfo(arr_index,5) = FormatNumber(rs("listed_share")/1000000,2)
			ArrStockInfo(arr_index,6) = FormatNumber(rs("mk_cap")/1000000,2)
			ArrStockInfo(arr_index,7) = rs("pe")

			rs.movenext
		loop
		rs.close		
	end if
	set rs = nothing	
		
	'========= 5 = EBIDA Margin= Net Profit Margin ======'	
	for i = 0 to ubound(ArrStockInfo,1)-1
	
		if Cstr(ArrStockInfo(i,1)) <> "" then
			maxyear = ""
			strSQL = "select max(fiscal) as maxyear from Finance where quarter=9 and sec_id = " & ArrStockInfo(i,1)			
			'response.write strSQL
			'response.end
			set rs = conpsims.execute(strSQL)
			maxyear = rs("maxyear")
			ArrStockInfo(i,10)= maxyear
		
			if cstr(maxyear) <> "" then
				strSQL = "select F.accu_amount,M.acc_code,F.fiscal  from M_AcCode M , Finance F where M.acc_code in (499993,499994) and M.acc_code = F.accountid and F.sec_id=" & ArrStockInfo(i,1) & " and F.fin_state_type='C' and F.flag <> 'D' and M.acc_formid = " & ArrStockInfo(i,2) & " and F.quarter='9' and F.fiscal in ('" & maxyear & "','" & maxyear - 1 & "') order by acc_code,fiscal desc "

				Mebida = 0
				LMebida = 0
				Mnetprofit = 0
				LMnetprofit = 0
						
				set rs = conpsims.execute(strSQL)			
				if not rs.eof and not rs.bof then
					do while not rs.eof						
						if rs("fiscal") = maxyear then
							if rs("acc_code") = "499993" then
								Mebida = rs("accu_amount")
							elseif rs("acc_code") = "499994" then
								Mnetprofit = rs("accu_amount")
							end if
						else
							if rs("acc_code") = "499993" then
								LMebida = rs("accu_amount")
							elseif rs("acc_code") = "499994" then
								LMnetprofit = rs("accu_amount")
							end if	
						end if
						rs.movenext
					loop
					rs.close		
				end if
				set rs = nothing			
			end if
			
			ArrStockInfo(i,8) = PerCentChg(Mebida,LMebida)
			ArrStockInfo(i,9) = PerCentChg(Mnetprofit,LMnetprofit)
		end if		
	next		
	
End Sub

Function GetIndex(arrayvalue, find,index)
	for i = 0 to ubound(arrayvalue) -1
		'response.write "<br>" & i & " ,arrayvalue : " & arrayvalue(i,index) & " ,find : " & find & " , index : " & index 
	
		if cstr(arrayvalue(i,index)) = cstr(find) then
			'response.write " <font color='#FF0000'>Found</font> "
			GetIndex = i 
			Exit Function
		end if
	next 
End Function 

Function PerCentChg(compare1,compare2)
	pchg = "" 
	
	if isNumeric(compare1) = true and isNumeric(compare2) = true then
		if compare2 <> 0 then
			pchg = ((compare1 - compare2)/compare2) * 100
		end if
	end if
	
	if cstr(pchg) = "" then
		PerCentChg = "-"
	else
		PerCentChg = FormatNumber(pchg,2) & "%"
	end if
End Function

Function GetValue(ArrStock,sec_id,value_index)
	arr_index = GetIndex(ArrStock, sec_id,0)	
	GetValue = ArrStockInfo(arr_index,value_index)	
End Function

Function WriteText(text)
	if isnull(text) = true then
		text = ""
	end if
	if cstr(text) <> "" then
		if IsNumeric(text) =  true then
			WriteText = "<font class='style49'>" & FormatNumber(text,2,-1) & "</font>"
		else
			WriteText = "<font class='style49'>" & text & "</font>"
		end if		
	else
		WriteText = "<font class='style49'>-</font>"
	end if
End Function

Function DivideNum(num1,num2)
	if num1 <> "" and num2<> "" then
		DivideNum = num1/num2		
	else
		DivideNum = ""
	end if
End Function 
%>


<title> Board Report :: Investor Relations Metrics</title>
<meta http-equiv="Content-Type" content="text/html; charset=windows-874">
<!-- #include file = "../includefile_style.asp" -->
<script language="javascript" src="../function2007.js"></script>
</head>
<body leftmargin="0" topmargin="5" marginwidth="0" marginheight="5">
<%if session("user_id")<>"" then%>
<div align="center" valign="top" > 
		<table width="1000" valign="top"  border="0" cellpadding="0" cellspacing="0" >
			<tr>
			  <td align="left" valign="top">
						<table width="100%"  border="0" cellpadding="0" cellspacing="1" bgcolor="#cfcfcf">
								<tr align="left" valign="top">         
										<td bgcolor="#cfcfcf">
												<table width="100%"  border="0" cellspacing="0" cellpadding="0" >
														<tr bgcolor="#cfcfcf">
															<td align="left" valign="top" bgcolor="#cfcfcf">
																 <!-- #include virtual = "/ir/includefile_i_top.asp" -->		
															</td>
														</tr>
														<tr><td><br>
															<table width="100%" cellpadding="0" cellspacing="0" border="0">
																<tr  bgcolor="#FFFFF" border="0"style="border:1px solid #FFFFFF;">
																	<td>
																		<table cellpadding="5" cellspacing="0" border="0">
																			<tr valign="baseline">
																				<td>			
																					<a href="InvestorRelationsMetrics_reporter.asp" class="L">Investor Relations Metrics</a>			
																				</td>
																				<td>||	</td>
																				<td>			
																					<a href="stockprice_performance_reporter.asp" class="L">Stock Price Performance</a>					
																				</td>
																				<td>||	</td>		
																				<td>					
																					<a href="stockholder_composition_reporter.asp" class="L">Stockholder Composition</a>		
																				</td>					
																				<td>||	</td>		
																				<td>
																					<a href="analyst_coverage.asp" class="L">Analyst Coverage (SAA Consensus)</a>																				
																				</td>
																			</tr>
																		</table>
																	</td>
																</tr>
															</table>
															</td>			
														</tr>														
														<tr>
															<td align="left" valign="top">
																<table width="100%"  border="0" cellspacing="10" cellpadding="0">
																	<tr align="left" valign="top">
																		<td>
																			<table width="100%" border="0" cellpadding="0" cellspacing="1" align="center">
																				<tr align="left" valign="top">
																					<td width="350" valign="middle" bgcolor="#FFFFFF"><font class="stylefont"><strong><img src="../images/arrow.gif" width="12" height="9">Investor Relations Metrics  : <%=session("listed_name") %></strong></font></td>
																					<td>&nbsp;</td>
																				</tr>
																			</table>
																	   </td>																				
																	</tr>
																	<tr >
																		<td>
																			<table width="100%"  border="0" cellspacing="0" cellpadding="5"bgcolor="#FFFFFF">
																				<tr align="left" valign="top">
																					<td>		
																						<FORM name="frm" METHOD="POST" ACTION="InvestorRelationsMetrics_reporter.asp" onsubmit="return ValidateFrm();">
																							<table width="100%" border="0">
																								<tr valign="top">
																									<td align="left">
																									<%																						
																										strSQLSector = "select i_sector, n_sector from Sector where i_sector > 0 and i_sector <> 17 order by n_sector"	
																										
																										strObjDDLMarket = ""
																										strObjDDLSector = ""
																										
																										set rs = conpsims.execute(strSQLSector)																									
																										if not rs.eof and not rs.bof then
																											strObjDDLMarket = "<select id='DDLMarket' name='DDLMarket' size='1' style='WIDTH:80px;FONT-FAMILY: Ms sans serif;FONT-SIZE:12px;cursor:hand;' onChange='OnSelectedItem(this);'>"
																											strObjDDLSector = "<select id='DDLSector' name='DDLSector' size='1' style='WIDTH:80px;FONT-FAMILY: Ms sans serif;FONT-SIZE:12px;cursor:hand;' onChange='OnSelectedItem(this);'>"
																											do while not rs.eof		
																												Select Case rs("i_sector")
																													Case 99, 98
																														if cstr(request("DDLMarket"))=cstr(rs("i_sector"))  then
																															strObjDDLMarket = strObjDDLMarket & "<option value=" & rs("i_sector") & " Selected>" & rs("n_sector") & "</option>"
																														else
																															strObjDDLMarket = strObjDDLMarket & "<option value=" & rs("i_sector") & ">" & rs("n_sector") & "</option>"
																														end if
																													Case Else
																														if cstr(request("DDLSector"))=cstr(rs("i_sector"))  then
																															strObjDDLSector = strObjDDLSector & "<option value=" & rs("i_sector") & " Selected>" & rs("n_sector") & "</option>"
																														else
																															strObjDDLSector = strObjDDLSector & "<option value=" & rs("i_sector") & ">" & rs("n_sector") & "</option>"
																														end if
																												End Select																										
																												rs.movenext
																											loop
																											
																											strObjDDLMarket = strObjDDLMarket & "</select>"
																											strObjDDLSector = strObjDDLSector & "</select>"
																											rs.close		
																										end if
																										set rs = nothing		
																									%>																						
																										<table width="100%" cellpadding=0 cellspacing=0 border=0>
																											<tr valign="middle">
																												<td width="8%"><b>Market :</b></td>
																												<td><%=strObjDDLMarket%></td>
																												<td>&nbsp;&nbsp;&nbsp;</td>
																												<td width="8%"><b>Sector :</b></td>
																												<td><%=strObjDDLSector%></td>	
																												<td width="100%">&nbsp;</td>
																											</tr>		
																										</table>
																									</td>
																								</tr>																				
																								<tr valign="top">
																									<td><%										
																											'sector_no = GetSecNo		
																											'strPeerList = PeerList						
																											listed = request("share")
																											peera = request("peera")
																											peerb = request("peerb")
																											peerc = request("peerc")			
																												
																											dim ArrStockInfo(4,11)
																											dim close_date
																											if listed <> "" then
																												call LoadStockInfo				
																											end if
																											'===================================
																											'for x = 0 to ubound(ArrStockInfo,1) - 1
																											'	for y = 0 to ubound(ArrStockInfo,2) - 1
																											'		response.write "<br>   x,y =" & x & "," & y & " = " & ArrStockInfo(x,y)
																											'	next 
																											'next
																											'===================================																								
																										%>						
																										<br>
																										<table width="100%" border="0" cellspacing="1" cellpadding="5" bgcolor="<%=session("bg_in_form")%>">
																											<tr valign="top" bgcolor="#FFFFFF">
																												<td align="center">&nbsp;</td>
																												<td align="center">	
																													<select id="share" name="share" size="1"   style="WIDTH:80px;FONT-FAMILY: Ms sans serif;FONT-SIZE:12px;cursor:hand;" onChange="OnSelectedItem(this);">
																														<option value="-1">- Listed -</option>
																													</select></td>
																												<td align="center">
																													<select id="peera" name="peera" size="1"   style="WIDTH:80px;FONT-FAMILY: Ms sans serif;FONT-SIZE:12px;cursor:hand;" onChange="OnSelectedItem(this);">
																														<option value="-1">- Peer A -</option>
																													</select>
																												</td>
																												<td align="center">
																													<select id="peerb" name="peerb" size="1"   style="WIDTH:80px;FONT-FAMILY: Ms sans serif;FONT-SIZE:12px;cursor:hand;" onChange="OnSelectedItem(this);">
																														<option value="-1">- Peer B -</option>
																													</select>
																												</td>
																												<td align="center">
																													<select id="peerc"  name="peerc" size="1"   style="WIDTH:80px;FONT-FAMILY: Ms sans serif;FONT-SIZE:12px;cursor:hand;" onChange="OnSelectedItem(this);">
																														<option value="-1">- Peer C -</option>
																													</select>
																												</td>
																											</tr>
																											<tr valign="top">
																												<td align="center"><font color="#FFFFFF"><b>Measurement</b></font></td>	
																												<td align="right" width="15%"><font class="style_in_form" ><b><div id="secname_share"></div></b></font></td>
																												<td align="right" width="15%"><font class="style_in_form" ><b><div id="secname_peera"></div></font></td>
																												<td align="right" width="15%"><font class="style_in_form" ><b><div id="secname_peerb"></div></b></font></td>
																												<td align="right" width="15%"><font class="style_in_form" ><b><div id="secname_peerc"></div></b></font></td>
																											</tr>																				
																											<tr bgcolor="#EFEFEF">
																												<td colspan="5"><font class="stylefont_a"><b>Share Information</b> <%if close_date <> "" then%> (<%=close_date%>) <%end if%></font></td>																										
																											</tr>
																											<tr bgcolor="#FFFFFF">
																												<td><font class="stylefont_a">&nbsp;&nbsp;&nbsp;Stock Price</font></td>	
																												<td align="right"><%= WriteText(GetValue(ArrStockInfo,listed,4))%></td>
																												<td align="right"><%= WriteText(GetValue(ArrStockInfo,peera,4))%></td>
																												<td align="right"><%= WriteText(GetValue(ArrStockInfo,peerb,4))%></td>
																												<td align="right"><%= WriteText(GetValue(ArrStockInfo,peerc,4))%></td>
																											</tr>
																											<tr bgcolor="#FFFFFF">
																												<td><font class="stylefont_a">&nbsp;&nbsp;&nbsp;Daily trading volume (M Shared)</font></td>	
																												<td align="right"><%= WriteText(GetValue(ArrStockInfo,listed,5))%></td>
																												<td align="right"><%= WriteText(GetValue(ArrStockInfo,peera,5))%></td>
																												<td align="right"><%= WriteText(GetValue(ArrStockInfo,peerb,5))%></td>
																												<td align="right"><%= WriteText(GetValue(ArrStockInfo,peerc,5))%></td>
																											</tr>
																											<tr bgcolor="#FFFFFF">
																												<td><font class="stylefont_a">&nbsp;&nbsp;&nbsp;Market capitalization (M Baht)</font></td>	
																												<td align="right"><%= WriteText(GetValue(ArrStockInfo,listed,6))%></td>
																												<td align="right"><%= WriteText(GetValue(ArrStockInfo,peera,6))%></td>
																												<td align="right"><%= WriteText(GetValue(ArrStockInfo,peerb,6))%></td>
																												<td align="right"><%= WriteText(GetValue(ArrStockInfo,peerc,6))%></td>
																											</tr>
																											<tr bgcolor="#EFEFEF">
																												<td colspan="5"><font class="stylefont_a"><b>Valuation</b></font></td>																								
																											</tr>
																											<tr bgcolor="#FFFFFF">
																												<td><font class="stylefont_a">&nbsp;&nbsp;&nbsp;Price/earnings multiple</font></td>	
																												<td align="right"><%= WriteText(GetValue(ArrStockInfo,listed,7))%></td>
																												<td align="right"><%= WriteText(GetValue(ArrStockInfo,peera,7))%></td>
																												<td align="right"><%= WriteText(GetValue(ArrStockInfo,peerb,7))%></td>
																												<td align="right"><%= WriteText(GetValue(ArrStockInfo,peerc,7))%></td>
																											</tr>
																											<tr bgcolor="#FFFFFF">
																												<td><font class="stylefont_a">&nbsp;&nbsp;&nbsp;Market cap/revenue multiple</font></td>	
																												<td align="right"><%= WriteText(DivideNum(GetValue(ArrStockInfo,listed,6),GetValue(ArrStockInfo,listed,3)))%></td>
																												<td align="right"><%= WriteText(DivideNum(GetValue(ArrStockInfo,peera,6),GetValue(ArrStockInfo,peera,3)))%></td>
																												<td align="right"><%= WriteText(DivideNum(GetValue(ArrStockInfo,peerb,6),GetValue(ArrStockInfo,peerb,3)))%></td>
																												<td align="right"><%= WriteText(DivideNum(GetValue(ArrStockInfo,peerc,6),GetValue(ArrStockInfo,peerc,3)))%></td>
																											</tr>
																											<tr bgcolor="#EFEFEF">
																												<td><font class="stylefont_a"><b>Financial Results</b></font></td>	
																												<td align="center"><font class="stylefont_a"><b><%=GetValue(ArrStockInfo,listed,10)%></b></font></td>	
																												<td align="center"><font class="stylefont_a"><b><%=GetValue(ArrStockInfo,peera,10)%></b></font></td>	
																												<td align="center"><font class="stylefont_a"><b><%=GetValue(ArrStockInfo,peerb,10)%></b></font></td>	
																												<td align="center"><font class="stylefont_a"><b><%=GetValue(ArrStockInfo,peerc,10)%></b></font></td>	
																											</tr>
																											<tr bgcolor="#FFFFFF">
																												<td><font class="stylefont_a">&nbsp;&nbsp;&nbsp;Revenue (M Baht)</font> </td>	
																												<td align="right"><%= WriteText(GetValue(ArrStockInfo,listed,3))%></td>
																												<td align="right"><%= WriteText(GetValue(ArrStockInfo,peera,3))%></td>
																												<td align="right"><%= WriteText(GetValue(ArrStockInfo,peerb,3))%></td>
																												<td align="right"><%= WriteText(GetValue(ArrStockInfo,peerc,3))%></td>
																											</tr>
																											<tr bgcolor="#FFFFFF">
																												<td><font class="stylefont_a">&nbsp;&nbsp;&nbsp;EBIDA margin%</font></td>	
																												<td align="right"><%= WriteText(GetValue(ArrStockInfo,listed,8))%></td>
																												<td align="right"><%= WriteText(GetValue(ArrStockInfo,peera,8))%></td>
																												<td align="right"><%= WriteText(GetValue(ArrStockInfo,peerb,8))%></td>
																												<td align="right"><%= WriteText(GetValue(ArrStockInfo,peerc,8))%></td>
																											</tr>
																											<tr bgcolor="#FFFFFF">
																												<td><font class="stylefont_a">&nbsp;&nbsp;&nbsp;Net profit margin%</font></td>	
																												<td align="right"><%= WriteText(GetValue(ArrStockInfo,listed,9))%></td>
																												<td align="right"><%= WriteText(GetValue(ArrStockInfo,peera,9))%></td>
																												<td align="right"><%= WriteText(GetValue(ArrStockInfo,peerb,9))%></td>
																												<td align="right"><%= WriteText(GetValue(ArrStockInfo,peerc,9))%></td>
																											</tr>					
																										</table> 																							
																									</td>
																									<td width="40">
																										<table width="100%" border="0" cellpadding="3" cellspacing="3"><tr><td>																				
																										<input type="submit" name="Submit" value="Go" valign="baseline" >
																										</td></tr></table>																	
																									</td>
																								</tr>
																							</table>
																							<div style="display:none;">
																									<select name="AllMasterStock" size="1"   style="FONT-FAMILY: Ms sans serif;FONT-SIZE:12px;cursor:hand;">
																										<%= StringPeerList%>
																									</select>				
																									<select name="masterStock" size="1"   style="FONT-FAMILY: Ms sans serif;FONT-SIZE:12px;cursor:hand;">
																										<%'= StringPeerList%>
																									</select>
																									<input type="hidden" id="selected_share" name="selected_share" value="<%=request("selected_share")%>">
																									<input type="hidden" id="selected_peera" name="selected_peera" value="<%=request("selected_peera")%>">
																									<input type="hidden" id="selected_peerb" name="selected_peerb" value="<%=request("selected_peerb")%>">
																									<input type="hidden" id="selected_peerc" name="selected_peerc" value="<%=request("selected_peerc")%>">
																							</div>
																						</FORM>	
																					</td>
																				</tr>
																				<tr>
																					<td  align="right">
																							<hr color="#7BC742" size=2>
																					</td>
																				</tr>
																				<tr>
																					<td >
																					<!--####################################################################################-->
																						<input type = "button" value = "Main Menu"  onclick="location='main_menu.asp'">
																					<!--####################################################################################-->
																					</td>
																				</tr>
																				<tr><td>&nbsp;</td></tr>																			
																			</table>		
																		</td>
																	</tr>																			
																</table>
															</td>				
														</tr>
													<!--#include virtual = "/ir/i_footer.asp" -->
												</table>
										</td>
								</tr>
						</table>
				</td>
			</tr>
		</table>
</div>

<script language="JavaScript">
	var AllMasterStock = document.getElementById("AllMasterStock");
	var masterStock = document.getElementById("masterStock");
	var objshare = document.getElementById("share");
	var objPeerA = document.getElementById("peera");
	var objPeerB = document.getElementById("peerb");
	var objPeerC = document.getElementById("peerc");

	function OnSelectedItem(obj)	{			
		switch ((obj.id).toLowerCase())	{		
				case "ddlmarket":									
					if(obj.value=='98'){	//**** Market : MAI ****
						document.getElementById("DDLSector").disabled=true;					
						LoadMasterPeerList("mk_type=S");
						
						document.getElementById("DDLSector").selectedIndex=0;	
					}else if (obj.value=='99') {							//**** Market : SET ****
						document.getElementById("DDLSector").disabled=false;					
						LoadMasterPeerList("sector_no="+document.getElementById("DDLSector").value+"|mk_type=A");						
					}
					LoadPeerList(masterStock,objshare,objshare[(objshare.selectedIndex)].value);
					LoadPeerList(masterStock,objPeerA,objPeerA[(objPeerA.selectedIndex)].value);
					LoadPeerList(masterStock,objPeerB,objPeerB[(objPeerB.selectedIndex)].value);
					LoadPeerList(masterStock,objPeerC,objPeerC[(objPeerC.selectedIndex)].value);	
					break;
				case "ddlsector":								
					LoadMasterPeerList("sector_no="+document.getElementById("DDLSector").value+"|mk_type=A");	
					LoadPeerList(masterStock,objshare,objshare[(objshare.selectedIndex)].value);
					LoadPeerList(masterStock,objPeerA,objPeerA[(objPeerA.selectedIndex)].value);
					LoadPeerList(masterStock,objPeerB,objPeerB[(objPeerB.selectedIndex)].value);
					LoadPeerList(masterStock,objPeerC,objPeerC[(objPeerC.selectedIndex)].value);	
					break;
				default:				
					//******* Select Current Selected Value ********
					var TextboxID;
						
					TextboxID = "selected_"+obj.id;		
					document.getElementById(TextboxID).value = obj[(obj.selectedIndex)].value;		
					//******************************************
					LoadPeerList(masterStock,objshare,objshare[(objshare.selectedIndex)].value);
					LoadPeerList(masterStock,objPeerA,objPeerA[(objPeerA.selectedIndex)].value);
					LoadPeerList(masterStock,objPeerB,objPeerB[(objPeerB.selectedIndex)].value);
					LoadPeerList(masterStock,objPeerC,objPeerC[(objPeerC.selectedIndex)].value);			

					break;
		}				
	}
	
	function LoadMasterPeerList(keyword)	{
		if(keyword!="") {
			var optn;
			var M_value;
			clearOption(masterStock);	
				
			for (var i=0; i < AllMasterStock.length; i++)
			{					
				M_value = AllMasterStock[i].value;
				optn = document.createElement("OPTION");
				optn.text = AllMasterStock[i].text;								
				optn.value = M_value.substring(0,M_value.indexOf(":"));				
				if((M_value).indexOf(keyword) != -1) 				{			
					masterStock.options.add(optn); 	
				}				
			}			
		}				
	}	
	
	function LoadPeerList(source,destination,selectedValue)	{
		var optn;
		var con, con1, con2;
		
		clearOption(destination,1);				
			
		for (var i=0; i < source.length; i++)
		{					
			optn = document.createElement("OPTION");
			optn.text = source[i].text;
			optn.value = source[i].value;					
			switch (destination.id)			{
				case "share": 
					var listed_member = "<%=listed_member%>";
					
					if(listed_member.indexOf('\'' + optn.text + '\'')!=-1){
						destination.options.add(optn);
					}					
					break;
				case "peera": 
					con =  document.getElementById("selected_share");
					con1 = document.getElementById("selected_peerb");
					con2 = document.getElementById("selected_peerc");

					if (((con.value=="") || (con.value!=source[i].value))&& ((con1.value == "") || (con1.value != source[i].value)) && ((con2.value == "") || (con2.value != source[i].value))) { destination.options.add(optn); }
					break;
				case "peerb":
					con =  document.getElementById("selected_share");
					con1 = document.getElementById("selected_peera");
					con2 = document.getElementById("selected_peerc");
				
					if (((con.value=="") || (con.value!=source[i].value))&& ((con1.value == "") || (con1.value != source[i].value)) && ((con2.value == "") || (con2.value != source[i].value))) { 	destination.options.add(optn); }				
					break;
				case "peerc":			
					con =  document.getElementById("selected_share");
					con1 = document.getElementById("selected_peera");
					con2 = document.getElementById("selected_peerb");
					
					if (((con.value=="") || (con.value!=source[i].value))&& ((con1.value == "") || (con1.value != source[i].value)) && ((con2.value == "") || (con2.value != source[i].value))) { 	destination.options.add(optn); }			
					break;
			}			
			
			if (source[i].value==selectedValue){
				optn.selected = true;
			}else{
				optn.selected = false;
			}
		}			
	}	
	
function writetxt(text) {
	document.getElementById("space").innerHTML = document.getElementById("space").innerHTML + "<br>" + text;
}	
	
	function clearOption(Ctrl,index)	{	
		var i_loop = 0;
		if (index){
			i_loop = index;
		}
		var intTotalItems= Ctrl.options.length;
		
		for(var intCounter=intTotalItems;intCounter>=i_loop;intCounter--) { 
			Ctrl.remove(intCounter); 
		}
	}

	function SelectedPeer()	{		
		var selected_share = "<%=request("share")%>";
		var selected_peera = "<%=request("peera")%>";
		var selected_peerb = "<%=request("peerb")%>";
		var selected_peerc = "<%=request("peerc")%>";
		
		if ((selected_share != "")&&(selected_share !="-1")) { objshare.value=selected_share;document.getElementById("secname_share").innerHTML=objshare[objshare.selectedIndex].text;	}
		if ((selected_peera != "")&&(selected_peera !="-1")) { objPeerA.value=selected_peera;document.getElementById("secname_peera").innerHTML=objPeerA[objPeerA.selectedIndex].text;}
		if ((selected_peerb != "")&&(selected_peerb !="-1")) { objPeerB.value=selected_peerb;document.getElementById("secname_peerb").innerHTML=objPeerB[objPeerB.selectedIndex].text;}
		if ((selected_peerc != "")&&(selected_peerc !="-1")) { objPeerC.value=selected_peerc;document.getElementById("secname_peerc").innerHTML=objPeerC[objPeerC.selectedIndex].text;}
	}
	
	function ValidateFrm() {
		if  ((document.getElementById("share").selectedIndex== -1) ||(document.getElementById("share").value== -1)) {alert('Please select Share to load information.');return false}
		else{ return true;		}		
	}
	
	OnSelectedItem(document.getElementById("DDLMarket"));
	SelectedPeer();	
</script>
<%
else
 response.redirect "login.asp"
end if
%>
</body>
</html>