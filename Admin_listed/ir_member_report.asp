<%session("cur_page")="IR_user"
session("open_ir")="IR"
if session("lang")="" or request("lang")="T"  then  'Language
	session("lang")="T"
elseif request("lang")="E" then
	session("lang")="E"
end if

%>
<!-- #include file = "../i_constant.asp" -->
<!-- #include file = "../i_style.asp" -->
<!--#include file = "../i_conirauthen.asp" -->
<!--#include file = "../i_conir.asp" -->
<!--#include file="../formatdate.asp"-->
<!--#include file="../checkbox.asp"-->

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<title> IR Member Report</title>
<meta http-equiv="Content-Type" content="text/html; charset=windows-874">
</head>
<body leftmargin="0" topmargin="5">
<div align="center" valign="top" > 
<table width="800px"  border="0" cellspacing="1" cellpadding="3" bgcolor="#F0B0EF">
	  <tr align="center" valign="bottom" bgcolor="#F3CDF3">
		<td width="10px"><font class="stylefont_a" >No.</font></td>
		<td width="100px"><font class="stylefont_a" ><font color="#0000cc"><b>E-mail</b></font></font></td>
		<td width="100px"><font class="stylefont_a" ><font color="#0000cc"><b>Name-Surname</b></font></font></td>		
		<td ><font class="stylefont_a" ><font color="#0000cc"><b>Address</b></font></font></td>	
		<td width="30px"><font class="stylefont_a" ><font color="#0000cc"><b>Telphone</b></font></font></td>																																		
		<td width="30px"><font class="stylefont_a" ><font color="#0000cc"><b>Company</b></font></font></td>																																
		<td width="30px"><font class="stylefont_a" ><font color="#0000cc"><b>Profile </b></font></font></td>
		<td width="50px"><font class="stylefont_a" ><font color="#0000cc"><b>Last Update </b></font></font></td>
	</tr>
	<%
	sql="select a.email,a.first_name,a.last_name,a.company,date_format(a.timestamp,'%Y-%m-%d %H:%i:%s') as post_date,b.profile,a.address,a.telphone "	
	sql=sql & " from ir_member as a,member_profile as b where a.profile_id=b.profile_id "
	sql=sql & " order by a.timestamp desc "

	set rs = conirauthen.execute(sql)
	if not rs.eof and not  rs.bof then 
		i=1
		do while not rs.eof
				%>
				<tr bgcolor="#FFFFFF">
					<td valign="top" align="center"><font class="stylefont_a" ><%=i%></font></td>
					<td  valign="top" align="left"><font class="stylefont_a" ><%=rs("email")%></font></td>
					<td  valign="top" align="left"> <font class="stylefont_a" ><%=rs("first_name") & "  " & rs("last_name") %></font></td>
					<td  valign="top" align="left">
						<%if rs("address") = "" then%>
							<font class="stylefont_a" ><center>-</center></font>
						<%else%>
							<font class="stylefont_a" ><%=rs("address")%></font>
						<%end if%>
					</td>
					<td  valign="top" align="left">
						<%if rs("telphone") = "" then%>
							<font class="stylefont_a" ><center>-</center></font>
						<%else%>
							<font class="stylefont_a" ><%=rs("telphone")%></font>
						<%end if%>
					</td>
					<td  valign="top" align="left"><font class="stylefont_a" ><%=rs("company")%></font></td>
					<td  valign="top"><font class="stylefont_a" ><%=rs("profile")%></font></td>
					<td  valign="top"><font class="stylefont_a" ><%=rs("post_date")%></font></td>									
				</tr>
				<%		
				i=i+1
				rs.movenext
		loop
	end if
	%>
</table>																		
</div>
</form>
</body>
</html>