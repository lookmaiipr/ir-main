<!-- #include file = "../i_constant.asp" -->
<!-- #include file = "../includefile_i_contant.asp" -->
<!-- #include file = "../includefile_style.asp" -->	
<!-- #include file = "../i_conirauthen.asp" -->
<!--#include virtual="/ir/formatdate.asp"-->
<!--#include file = "../i_listed_member.asp" -->
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<title>Stock Price Performance</title>
<meta http-equiv="Content-Type" content="text/html; charset=windows-874">
</head>
<body leftmargin="0" topmargin="5" marginwidth="0" marginheight="5">
<%if session("user_id")<>"" then%>
<form name="form1" method="post" action="analyst_coverage.asp">
<div align="center" valign="top" > 
		<table width="1000" valign="top"  border="0" cellpadding="0" cellspacing="0">
			<tr>
			  <td align="left" valign="top">
						<table width="100%"  border="0" cellpadding="0" cellspacing="1" bgcolor="#cfcfcf">
								<tr align="left" valign="top">         
										<td bgcolor="#cfcfcf">
												<table width="100%"  border="0" cellspacing="0" cellpadding="0">
														<tr>
																<td align="left" valign="top">
																     <!-- #include virtual = "/ir/includefile_i_top.asp" -->		
																</td>
														</tr>
														<tr><td>
															<table width="100%" cellpadding="0" cellspacing="0" border="0">
																<tr  bgcolor="#F5F5F5" border="0"style="border:1px solid #FFFFFF;">
																	<td>
																		<table cellpadding="5" cellspacing="0" border="0">
																			<tr valign="baseline">
																				<td>			
																						<a href="InvestorRelationsMetrics_reporter.asp" class="L">Investor Relations Metrics</a>			
																				</td>
																				<td>||	</td>
																				<td>			
																						<a href="stockprice_performance_reporter.asp" class="L">Stock Price Performance</a>					
																				</td>
																				<td>||	</td>		
																				<td>					
																						<a href="stockholder_composition_reporter.asp" class="L">Stockholder Composition</a>		
																				</td>					
																				<td>||	</td>		
																				<td>
																						<a href="analyst_coverage.asp" class="L">Analyst Coverage (SAA Consensus)</a>	
																				</td>
																			</tr>
																		</table>
																	</td>
																</tr>
															</table>
															</td>			
														</tr>		
														<tr>
																<td align="left" valign="top">
																		<table width="100%"  border="0" cellspacing="10" cellpadding="0">
																			<tr align="left" valign="top">
																				<td>
																						<table width="100%" border="0" cellpadding="0" cellspacing="1">
																						  <tr align="left" valign="top">
																								<td width="350" valign="middle" bgcolor="#FFFFFF"><font class="stylefont"><strong><img src="../images/arrow.gif" width="12" height="9"> Analyst Coverage : <%=session("listed_name") %></strong></font></td>
																								<td>&nbsp;</td>
																						  </tr>
																						</table>
																				</td>																				
																			</tr>
																			<tr>
																					<td align="left" valign="top"><br>
																							<table width="100%" border="0" cellpadding="5" cellspacing="1" bgcolor="#FFFFFF">
																								<tr>
																										<td align="left" valign="top" bgcolor="#FFFFFF">
																										   <table width="100%"  border="0" cellspacing="1" cellpadding="1">
                                                                                       <tr>
                                                                                          <td align="right" valign="top">
                                                                                             <Hr color="#7BC742" size=2><br>
                                                                                          </td>
                                                                                       </tr>                                                               
                                                                                       <tr align="center" valign="bottom" >
                                                                                          <td>
                                                                                             <table width="660"  border="0" cellspacing="2" cellpadding="5" bgcolor="#CFEBBA" align="center">					
                                                                                             <%
                                                                                          if listed_member_id <> "" then
                                                                                             member_id_arr = split(listed_member_id,",")
                                                                                             member_share_arr = split(Replace(listed_member,"'",""),",")
                                                                                             
                                                                                             v_member = 0
                                                                                             
                                                                                             for i = 0 to ubound(member_id_arr)
                                                                                                if cstr(member_id_arr(i)) <> "" then
                                                                                                   link = "http://www.settrade.com/AnalystConsensus/C04_10_stock_saa_p1.jsp?selectPage=10&txtSymbol="&Replace(member_share_arr(i),"&","%26")
                                                                                                   if (i mod 4) = 0 then
                                                                                                      %>
                                                                                                <tr bgcolor="white"><td align="center"><a href="<%=link%>" target="_blank"><%=member_share_arr(i)%></a></td>												
                                                                                                      <%
                                                                                                   elseif (i mod 4) = 3 then
                                                                                                      %>
                                                                                                   <td align="center"><a href="<%=link%>" target="_blank"><%=member_share_arr(i)%></a></td></tr>
                                                                                                      <%
                                                                                                   else
                                                                                                      %>
                                                                                                   <td align="center"><a href="<%=link%>" target="_blank"><%=member_share_arr(i)%></a></td>			
                                                                                                      <%
                                                                                                   end if			
                                                                                                   v_member = v_member +1							
                                                                                                end if
                                                                                             next
                                                                                             '========== write empty column for complete table ==========
                                                                                             if (v_member mod 4) = 1 then
                                                                                                %>
                                                                                                   <td>&nbsp;</td>
                                                                                                   <td>&nbsp;</td>
                                                                                                   <td>&nbsp;</td>
                                                                                                </tr>																	
                                                                                                <%
                                                                                             elseif (v_member mod 4) = 2 then
                                                                                                %>
                                                                                                   <td>&nbsp;</td>
                                                                                                   <td>&nbsp;</td>
                                                                                                </tr>																	
                                                                                                <%
                                                                                             elseif (v_member mod 4) = 3 then
                                                                                                %>
                                                                                                   <td>&nbsp;</td>
                                                                                                </tr>																	
                                                                                                <%
                                                                                             end if
                                                                                          end if
                                                                                             %>
                                                                                             </table>                                                                                              
                                                                                           </td>
                                                                                        </tr>
																											 </table>
																									</td>
																								</tr>
                                                                        <tr>
                                                                           <td bgcolor="#FFFFFF">
                                                                                 <table width="100%"  border="0"   align="center">
                                                                                    <td>
                                                                                          <font class="stylefont_a"><font color="#0000cc">
                                                                                           --->��ԡ���� Listed ����Ѻ��ѧ˹�� Analyst Coverage (SAA Consensus) �ͧ Listed ����
                                                                                          </font></font>
                                                                                    </td>
                                                                                 </table>
                                                                           </td>
                                                                        </tr>                                                                      
                                                                        <tr>
                                                                           <td  align="right">
                                                                                 <hr color="#7BC742" size=2>
                                                                           </td>
                                                                        </tr>                                                                        
																								<tr>
                                                                           <td>
                                                                              <input type = "button" value = "Main Menu"  onclick="location='main_menu.asp'"><br><br>
                                                                           </td>
                                                                         </tr>
																							</table>
																					</td>
																				</tr>
																		</table>
																</td>
														</tr>
													<!--#include virtual = "/ir/i_footer.asp" -->
												</table>
										</td>
								</tr>
						</table>
				</td>
			</tr>
		</table>
</div>
</form>
<%
else
 response.redirect "login.asp"
end if
%>
</body>
</html>