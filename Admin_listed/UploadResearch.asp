<%
max_department = 2
dim allow_department(2)
allow_department(1) = "IT"
allow_department(2) = "REA"
%>
<!-- #include file = "i_authen.asp" -->

<!-- #include file = "..\i_conbfitweb.asp" -->

<HTML>
<HEAD>
<TITLE>Upload File</TITLE>
<META NAME="Generator" CONTENT="EditPlus">
<META NAME="Author" CONTENT="Juice">
<META NAME="Keywords" CONTENT="">
<META NAME="Description" CONTENT="">

<link rel="stylesheet" href="../../th/e/ieshow.css">
<style>
<!--
.topic { font-family: MS Sans Serif, Tahoma;  color: rgb(0,0,0); font-size: 10pt }
.header { font-family: MS Sans Serif; font-size: 20pt}
.text { font-family: MS Sans Serif, Tahoma; font-size: 9pt; color: rgb(0,0,0) }
.b { font-family: MS Sans Serif, Tahoma; font-size: 9pt; font-weight:bold }
.head { font-family: MS Sans Serif, Tahoma; font-size: 10pt }
.border { border: 1px solid rgb(0,0,0) }

a:link { color: rgb(20,50,0); text-decoration: none; font-family: Microsoft Sans Serif, Tahoma; font-size: 10pt }
a:visited { color: '#9933FF'; text-decoration: none; font-family: Microsoft Sans Serif, Tahoma; font-size: 10pt }
a:active { color: rgb(0,0,0); text-decoration: none; font-family: Microsoft Sans Serif, Tahoma; font-size: 10pt ;}
a:hover { color: '#FF33CC'; text-decoration:  none; font-family: Microsoft Sans Serif, Tahoma; font-size: 10pt }
h1
	{margin-bottom:.0001pt;
	page-break-after:avoid;
	font-size:18.0pt;
	font-family:"MS Sans Serif";
	color:blue;
	text-decoration:underline;
	text-underline:single; margin-left:0cm; margin-right:0cm; margin-top:0cm}
-->

</style>

</HEAD>

<%
Const cnstMaxFileSize = 1048576
Const cnstUploadDir = "Upload"
Const cnstPDFDir = "../PDF"

Function Upload(intResearchID)
	Dim strPDFFile
	Dim strExt
	Dim blnHasFile
	Dim strSQL

	On Error Resume Next

	Set objUpload = Server.CreateObject("Dundas.Upload.2")
	If Err.Number <> 0 Then
		%>Error ! Cannot create the instant of Upload control. <%=Err.Description %><br><%
		Upload = False
		Exit Function
	End If

	objUpload.MaxFileCount = 1
	objUpload.MaxFileSize = cnstMaxFileSize	
	objUpload.UseVirtualDir = True

	objUpload.Save cnstUploadDir
	If Err.Number <> 0 Then
		%>Error ! Cannot create file in Upload folder:<%=cnstUploadDir %>. <%=Err.Description %><br><%
		Upload = False
		Exit Function
	End If

	blnHasFile = False
	For Each objFile In objUpload.Files
		blnHasFile = True
		%>
		File <%=objFile.OriginalPath %>, size <%=objFile.Size %> bytes, is uploaded to server successfully.<br>
		<%

		strExt = objUpload.GetFileExt(objFile.Path)
		If UCase(strExt) <> "PDF" Then
			%>Error ! The uploaded file is <%=strExt %> file, not PDF file.<br><%

			ClearFile objFile
			Upload = False
			Exit Function
		End If

		intResearchID = objUpload.Form("researchid").Value
		strPDFFile = cnstPDFDir & "/" & intResearchID & ".pdf"
		If objUpload.FileExists(strPDFFile) Then
			%>File <%=strPDFFile %> has already existed. This is reuploading. Replace with the new file.<br><%

			objUpload.FileDelete(strPDFFile)
			If Err.Number <> 0 Then
				%>Error ! Cannot delete old PDF File. <%=Err.Description %>. Cannot replace.<br><%

				ClearFile objFile
				Upload = False
				Exit Function
			End If
			%>Delete the old file.<br><%
		End If

		objFile.Move(strPDFFile)
		If Err.Number <> 0 Then
			%>Error ! Cannot move file to PDF folder:<%=cnstPDFDir %>. <%=Err.Description %><br><%

			ClearFile objFile
			Upload = False
			Exit Function
		End If
		%>Move and change file name to <%=strPDFFile %>.<br><%

		strSQL = "update research set uploaded=1 where researchid=" & intResearchID
		conn.Execute strSQL
	Next

	If Not blnHasFile Then
		%>Error ! No File uploaded.<br><%

		Upload = False
		Exit Function
	End If

	Upload = True
End Function


Sub ClearFile(objFile)
	On Error Resume Next

	objFile.Delete
	If Err.Number <> 0 Then
		%>Error ! Cannot delete uploaded File. <%=Err.Description %><br><%
	Else
		%>The uploaded file is deleted.<br><%
	End If
End Sub

%>


<BODY BGCOLOR="#FFFFFF">
<b><font size="2" color="#000099" >
<% 
If Not Upload(intResearchID) Then
	%><br><center><font color="red" size="+2">Upload fails</font></center><br><%
Else
	%>
	<center>
	<form action="DetailResearch.asp" method="post">
	<input type="hidden" name="researchid" value="<%=intResearchID %>">
	<input type="submit" value="  OK  ">
	</form>
	</center>
	<%
End If
%>

</font></b>
</BODY>
</HTML>
