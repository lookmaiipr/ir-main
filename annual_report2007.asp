<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<html>
 <%
 
session("cur_page")="IR Annual Report"
 session("page_asp")="annual_report2007"
 
if session("lang")="" or request("lang")="T"  then  
	session("lang")="T"
elseif request("lang")="E" then
	session("lang")="E"
end if


pagesize=9
 %>
 <!--#include file="constpage2007.asp"-->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=windows-874">
<title>Investor Relations</title>
<LINK href="style2007.css" type="text/css" rel="stylesheet">	
<!--#include file = "i_constant.asp" -->
<!--#include file = "function_asp2007.asp" -->
<!--#include file = "listed_member2007.asp" -->	
<!--#include file = "i_conir.asp" -->
<!--#include file = "i_conpsims.asp" --> 
<meta name="keywords" content="<%=message_keyword%>">
<script language="javascript" src="/ir/function2007.js"></script>
<script type="text/javascript" language="JavaScript1.2" src="/ir/script2007/stm31.js"></script>
</head>

<body >
<table width="100%"  border="0" cellspacing="0" cellpadding="0">
  <tr>
		<td align="center" valign="top">
			<table width="742" border="0" cellpadding="0" cellspacing="0" bgcolor="#ECECEC">
				<tr align="left" valign="top">
					<td width="1" background="images2007/dot_pink.gif"><img src="images2007/dot_pink.gif" width="1" height="1"></td>
					<td>
							<table width="100%"  border="0" cellspacing="0" cellpadding="0">
									<!--++++++++++++++++++++++++include top++++++++++++++++++++++-->
								
													<!--#include file = "i_top2007.asp" -->
										
									<!--+++++++++++++++++++++++include menu+++++++++++++++++++++-->																									  
								  
												<!--#include file = "i_menu2007.asp" -->									
								
								<!--++++++++++++++++++++++++Content+++++++++++++++++++++++++++++-->
								<FORM name="frm1" METHOD="POST" ACTION="annual_report2007.asp">
								<tr>
										<td height="1" align="left" valign="top" background="images2007/dot_pink.gif"><img src="images2007/dot_pink.gif" width="1" height="1"></td>
								</tr>
								<tr>
										<td align="center" valign="top"><img src="images2007/head_annual.gif" width="740" height="80"></td>
								</tr>								
								<tr>
										<td align="left" valign="top">
											<table width="300" border="0" cellspacing="0" cellpadding="0">
												  <tr align="left" valign="top">
														<td width="14"><img src="images2007/bar_1left.gif" width="14" height="21"></td>
														<td width="130" valign="middle" background="images2007/bar_1bg1.gif"><strong>Annual Report </strong></td>
														<td width="32"><img src="images2007/bar_1creb.gif" width="32" height="21"></td>
														<td background="images2007/bar_1bg2.gif">&nbsp;</td>
														<td width="4" align="right"><img src="images2007/bar_1right.gif" width="4" height="21"></td>
												  </tr>
											</table>          
										</td>
								</tr>
								
								<tr>
									<td align="left" valign="top" bgcolor="#FFFFFF">
										 <table width="100%"  border="0" cellpadding="0" cellspacing="8">
										     <!--#include file="select_page2007.asp"-->
										      <%
											      if trim( session("listed_member") ) <>"" then
											        dim arrtmp()
													dim totalrec
													dim flag
														 
													totalrec=0
													
													strsql=""
													'strsql = "SELECT count(*) as sumrec  from annual  where share in (" & session("listed_member")  & ")"				
													strsql = sql_annual_main(session("listed_member"),session("lang"),0)					'=========== Revise Support TH/EN
																	 
													set rs =conir.execute(strsql)	
													if not rs.eof and not rs.bof then
														   if rs("sumrec") >=pagesize then											     
															   sumrec=pagesize-1
														   else
																sumrec=rs("sumrec")-1
														   end if
														totalrec=rs("sumrec")
													end if
													rs.close
													
													redim arrtmp(sumrec,6)													
													
													session("num_count")=totalrec
											  %>											  
											   <!--#include file="calculate_page2007.asp"-->
											   <%
											     strsql=""												    
												 'strsql="select * from annual where share in(" & session("listed_member")   & ") order by annualyear desc,receive_date desc "
												 'strsql=strsql & " limit " & ((session("pageno")-1)*session("pagesize")) & "," & session("pagesize") 
												 '=========== Revise Support TH/EN
												 strsql = sql_annual_main(session("listed_member"),session("lang"),((session("pageno")-1)*session("pagesize")) & "," & session("pagesize"))	
																									
												 set rs=conir.execute(strsql)
												if not  rs.eof and  not rs.bof then
													   i=0
														do while not rs.eof 
															    arr_id=split( session("listed_member_id"),",")
																arr_share=split(replace(session("listed_member"),"'",""),",")
																for n=0 to ubound(arr_id)
																		 if arr_share(n)=rs("share") then																	   
																			   arrtmp(i,0)=arr_id(n)	
																			  exit for
																		 end if                                                                     
																next
																 arrtmp(i,1)=rs("share")
																 arrtmp(i,2)=rs("filename_annual")
																  arrtmp(i,3)=rs("picture_annual")
																  arrtmp(i,4)=rs("annualyear")
																  arrtmp(i,5)=rs("receive_date")
																 '  response.write  arrtmp(i,0) & "   " & arrtmp(i,1) & "   "  & arrtmp(i,2) & "   " & arrtmp(i,3) &  "<br>"
																 i=i+1
															 rs.movenext
														loop
												end if
												rs.close
												set rs=nothing
												conir.close
												set conir=nothing
												
												n=0
											  
											  if sumrec >=0 then
											   for i=0 to sumrec 
														if arrtmp(i,0) <> "" then
															strsql=""
															strsql = "SELECT com_name_th,com_name_eng ,com_id FROM Company where com_id in ("& arrtmp(i,0) &")"			
															set rspsim = conpsims.execute(strsql)											
															if not rspsim.eof and not rspsim.bof  then		
																	if n mod 3 =0 then
																	    flag=0
																		%>																		
																			<tr align="center" valign="top">	
																	<%
																	elseif i mod 3=1 then
																	    flag=0
																	else
																	    flag=1
																	end if
																	n=n+1
																	%>
																	      	<td width="33%">
																					 	<table width="100%"  border="0" cellpadding="1" cellspacing="0"  align="center">		
																						   <tr  valign="top" >
																									<td  valign="top" align="center">
																											<table width="80" border="0" cellpadding="5" cellspacing="1" bgcolor="#000000">
																												<tr>
																													<td align="center" valign="top" bgcolor="#F4A4FE">
																													<img src="listed/<%=arrtmp(i,1)%>/images/<%=arrtmp(i,3)%>" width="70" height="98">
																													</td>
																												</tr>
																											</table>
																									</td>																			
																								</tr>
																								
																								  <tr align="center" valign="top">
																										<td><FONT face="MS Sans Serif, AngsanaUPC" color=#333333 size=1>
																										   <a href="<%=pathfileserver%>/listed/<%=arrtmp(i,1)%>/annual/<%=arrtmp(i,2)%>"  target="_bank">
																													<%if session("lang")="T" or session("lang")="" then
																														 response.write rspsim("com_name_th")
																														' response.write "(" & arrtmp(i,4) & ")"
																													else
																														 response.write rspsim("com_name_eng")																													
																													end if		
																													response.write "(" & arrtmp(i,4)-543 & ")"
																													%>
																													</a>
																													<%if DateDiff("d",arrtmp(i,5),now()) <=7 then
																													 %><img src="/ir/images2007/icon_new.gif"><%
																													end if%>
																										</FONT></td>																										
																								</tr>																						
																						</table>
																				</td>
																	<%if flag=1 then%>
																			</tr>
																	<%end if
															end if
														end if
												next  
												rspsim.close
												set rspsim=nothing
												conpsims.close
												set conpsims=nothing
												else
																			%>
																		<tr bgcolor="#FFFFFF" >
																		   <td align="center"   valign="middle">
																			    <font class="no_information">
																				<%
																				if  session("lang")="T"  then 
																				     response.write Message_Info_T
																				else																				
																				      response.write Message_Info_E
																				end if
																				%></font>
																			</td>
																		</tr>
																		<%
												end if
																						
										  	else%>
																<tr bgcolor="#FFFFFF" >
																		   <td align="center" colspan="4"  valign="middle">
																			     <font class="no_information">
																				<%
																				if  session("lang")="T"  then 
																				     response.write Message_Info_T
																				else																				
																				      response.write Message_Info_E
																				end if
																				%></font>
																			</td>
																		</tr>
												<%	end if
																%>	
										</table></td>
									  </tr>
									  
								<!--+++++++++++++++++++++++++ Page Button+++++++++++++++++++++++--->
								
								   	<tr>
									<td align="center" valign="top" bgcolor="#FFFFFF"><hr class="style_hr" width="100%"></td>
							  </tr>
							  <tr>
									<td align="right" valign="top" bgcolor="#FFFFFF">
								           <!--#include file = "page_button2007.asp" -->
										</td>
								</tr>	
								  
								<tr>
										<td height="1" align="left" valign="top" background="images2007/dot_gray.gif"><img src="images2007/dot_gray.gif" width="1" height="1"></td>
								  </tr>
								  <tr>
										<td align="left" valign="top">&nbsp;</td>
								</tr>											   
								</Form>									   
								<!--+++++++++++++++++++++++++include footer++++++++++++++++++++++-->			
								
												<!--#include file = "i_footer2007.asp" -->
								
												
								</table>
							</td>
							<td width="1" align="right" background="images2007/dot_pink.gif"><img src="images2007/dot_pink.gif" width="1" height="1"></td>
						</tr>	
    </table></td>
  </tr>
</table>
<!-- +++++++++++++++++ Google Analytics ++++++++++++++++ -->
          <!--#include file="i_googleAnalytics.asp"-->	
<!-- +++++++++++++++ End Google Analytics +++++++++++++++ -->
</body>
</html>
