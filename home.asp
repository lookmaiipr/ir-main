<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<!-- saved from url=(0066)http://www.dussthai.com/page_b.php?cid=4&cname=Quailty%20Assurance -->

<!--#include file = "../../i_constant.asp" -->
<!--#include file = "i_constant_listed.asp" -->
<!--#include file = "function.asp" -->
<!--#include file = "../../i_connews.asp" -->
<!--#include file = "../../function_asp2007.asp" -->
<!--#include file="../../constpage2007.asp"-->
<!--#include file = "../../i_conir.asp" -->

<%
session("cur_page")="IR " &  listed_share & " IR Home"
session("page_asp")="home.asp"
page_name="IR Home"
%>


<script>
	
bool Frame::canNavigate(const Frame& targetFrame)
{
    // Frame-busting is generally allowed, but blocked for sandboxed frames lacking the 'allow-top-navigation' flag.
    if (!securityContext()->isSandboxed(SandboxTopNavigation) && targetFrame == tree().top())
        return true;

    if (securityContext()->isSandboxed(SandboxNavigation)) {
        if (targetFrame.tree().isDescendantOf(this))
            return true;

        const char* reason = "The frame attempting navigation is sandboxed, and is therefore disallowed from navigating its ancestors.";
        if (securityContext()->isSandboxed(SandboxTopNavigation) && targetFrame == tree().top())
            reason = "The frame attempting navigation of the top-level window is sandboxed, but the 'allow-top-navigation' flag is not set.";

        printNavigationErrorMessage(targetFrame, reason);
        return false;
    }

    ASSERT(securityContext()->securityOrigin());
    SecurityOrigin& origin = *securityContext()->securityOrigin();

    // This is the normal case. A document can navigate its decendant frames,
    // or, more generally, a document can navigate a frame if the document is
    // in the same origin as any of that frame's ancestors (in the frame
    // hierarchy).
    //
    // See http://www.adambarth.com/papers/2008/barth-jackson-mitchell.pdf for
    // historical information about this security check.
    if (canAccessAncestor(origin, &targetFrame))
        return true;

    // Top-level frames are easier to navigate than other frames because they
    // display their URLs in the address bar (in most browsers). However, there
    // are still some restrictions on navigation to avoid nuisance attacks.
    // Specifically, a document can navigate a top-level frame if that frame
    // opened the document or if the document is the same-origin with any of
    // the top-level frame's opener's ancestors (in the frame hierarchy).
    //
    // In both of these cases, the document performing the navigation is in
    // some way related to the frame being navigate (e.g., by the "opener"
    // and/or "parent" relation). Requiring some sort of relation prevents a
    // document from navigating arbitrary, unrelated top-level frames.
    if (!targetFrame.tree().parent()) {
        if (targetFrame == client()->opener())
            return true;
        if (canAccessAncestor(origin, targetFrame.client()->opener()))
            return true;
    }

    printNavigationErrorMessage(targetFrame, "The frame attempting navigation is neither same-origin with the target, nor is it the target's parent or opener.");
    return false;
}
</script>

<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=windows-874">
		<title><%=message_title%> :: <%=page_name%></title>
		<meta name="keywords" content="<%=message_keyword%>">
		<meta name="description" content="<%=message_description%>">
		<meta name="author" content="0">
		<meta name="revisit-after" content="30 days">
		<meta name="robots" content="all">
		<meta name="rating" content="general">
		<link href="<%=shortcut_icon%>" rel="shortcut icon" type="image/x-icon">
		<link rel="stylesheet" href="style_listed.css" type="text/css" >
		<script language="javascript" src="../../function2007.js"></script>
		<script language="javascript" src="Scripts/function.js"></script>
		<!--CSS IR-->
		<link href="css/style_ir.css" rel="stylesheet" type="text/css">
		<link href="css/fonts.css" rel="stylesheet" type="text/css">
		<!--CSS MENU-->
		<link rel='stylesheet' type='text/css' href='css/styles_menu.css' />
		<script language="javascript" src="scripts/jquery.min.js"></script>
		<script language="javascript" src="scripts/menu1.js"></script>
		<link href="css/index.css" rel="stylesheet" type="text/css">
	</head>
	<body style="background:#FFFFFF;">
		<table width="100%" border="0" cellspacing="0" cellpadding="0">
			<tr>
				<td valign="top">
					<table width="990" border="0" align="center" cellpadding="0" cellspacing="0">
						<tr>
							<td valign="top">
								<table width="990" border="0" align="center" cellpadding="0" cellspacing="0">
									<tr>
										<td>
											<table width="994" border="0" align="center" cellpadding="0" cellspacing="0">
												<tr>
													<td valign="top">
														<!-- .............................................................. Top .............................................................. -->	
																<!--#include file = "i_top.asp" -->
														<!-- .......................................................... End Top. ........................................................ -->		
														<!-- .............................................................. Menu Top.............................................................. -->	
																<!--#include file="i_menu_top.asp"-->
														<!-- ......................................................... End Menu Left ........................................................... -->		
													</td>
												 </tr>
											</table>
										</td>
									</tr>
								</table>
							</td>
						</tr>
					</table>
					<table width="100%" border="0" cellspacing="0" cellpadding="0">
						<tr>
							  <td bgcolor="#1f2b7f">
								<table width="990" border="0" align="center" cellpadding="0" cellspacing="0">
									<tr>
										<td>
											<img src="images/home_28.jpg" width="30" height="3" alt="">
										</td>
									</tr>
								</table>
							</td>
						</tr>
					</table>
					 <table width="990" border="0" align="center" cellpadding="0" cellspacing="0">
						<tr>
							<td valign="top" bgcolor="#FFFFFF">
								<table width="100%" border="0" cellspacing="0" cellpadding="0">
									<tr>
										<td>
											<img src="images/home_30.jpg" width="219" height="7" alt="">
										</td>
									</tr>
								</table>
								<table width="100%" border="0" cellspacing="0" cellpadding="0">
									<tr>
										<td width="771" valign="top">
											<table width="990" border="0" align="center" cellpadding="0" cellspacing="0">
												<tr>
													<td>
														<table width="990" border="0" cellspacing="0" cellpadding="0">
															<tr>
																<td valign="top">
																	<table width="100%" border="0" cellspacing="0" cellpadding="0">
																		<tr>
																			<td bgcolor="#b5b6b8"><img src="images/a1.gif" width="10" height="1" alt=""></td>
																		</tr>
																	</table>
																	<table width="100%" border="0" cellspacing="0" cellpadding="0">
																		<tr>
																			<td width="1" bgcolor="#B5B6B8">
																				<img src="images/a1.gif" width="1" height="10" alt="">
																			</td>
																			<td width="988" height="187" valign="top">
																				<!-- .............................................................. Banner IR.............................................................. -->	
																						<!--#include file="i_banner_ir.asp"-->
																				<!-- ......................................................... End Banner IR ........................................................... -->		
																			</td>
																			<td width="1" bgcolor="#B5B6B8">
																				<img src="images/a1.gif" width="1" height="10" alt="">
																			</td>
																		</tr>
																	</table>
																	<table width="100%" border="0" cellspacing="0" cellpadding="0">
																		<tr>
																			<td bgcolor="#B5B6B8">
																				<img src="images/a1.gif" width="10" height="1" alt="">
																			</td>
																		</tr>
																	</table>
																</td>
															</tr>
														</table>
													</td>
												</tr>
											</table>
										</td>
									</tr>
								</table>
								<div style="width:990px;">
									<table width="100%" border="0" cellspacing="0" cellpadding="0">
										<tr>
											<td width="230" height="800" valign="top">
												<!-- .............................................................. Menu Left .............................................................. -->	
														<!--#include file="i_menu_left.asp"-->
												<!-- ......................................................... End Menu Left ........................................................... -->	
											</td>
											<td width="10" valign="top">
												<img src="images/a1.gif" width="10" height="10" alt="">
											</td>
											<td width="750" valign="top">
												<!--<div id="banner_ocean">
													<a href="http://portal.tsd.co.th/th/index.html" target="_blank"><img src="images/AW_Banner_OCEAN.jpg" width="750"></a>
												</div>-->
												<div id="profile_price_bg">
													<div id="profile">
														<%=home_company_name%>
														
														<%if session("lang")="E" then%>
														
															<p class="text_08">Since 1985, The founder started as distributing faucets and general sanitary ware company. 
															The company has been developing and gathering experience for several decades in sanitary ware business. 
															Thereafter the management has established company named Ocean Commerce Co.,Ltd. in year 2003 by using experience
															and market knowledge to distribute the products under brand of Duss, Bay, Icon, Saza and Union. 
															The target market has good respond to these products....</p>

															<div id="button_01">
																<a href="javascript:void(0);" onclick="parent.location='http://www.dussthai.com/page_a.php?cid=73&amp;cname=�����ź���ѷ&lang=eng';" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('Image1','','<%=more_detail_hover%>',1)">
																<img src="<%=more_detail%>" width="133" height="38" id="Image1"/></a>
															</div>
														
														<%else%>
														
															<p class="text_08">����������� �.�.2528 ����͵��������鹨ҡ����ѷ �Ѵ��˹��¡�͡��� 
															����ػ�ó��آ�ѳ������ ����Ѳ��������ҧ����С�ó��������� ��ʹ�������������Ժ�ը���з��������� 
															����ѷ �����¹ ������ê �ӡѴ ���㹻� 2546 ��������������»��ʺ��ó� ����ѡ��Ҿ��ҹ��õ�Ҵ 
															�¨Ѵ��˹����Թ��������ù�� (1) ��� (DUSS)  (2) �����¹ (UNION) (3) ��� (BAY) 
															(4) �ҫ�� (SAZA) (5) �մ (FEED) (6) �ͤ͹ (ICON) (7) ����� (3P) ...</p>
															
															<div id="button_01">
																<a href="javascript:void(0);" onclick="parent.location='http://www.dussthai.com/page_a.php?cid=73&amp;cname=�����ź���ѷ&lang=thai';" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('Image1','','<%=more_detail_hover%>',1)">
																<img src="<%=more_detail%>" width="133" height="38" id="Image1"/></a>
															</div>
															
															<%end if%>
														
													</div>
													<div id="price">
														<%=home_topic_price%>
														<div id="price_bg">
															<!-- ..............................................................Stock Quote .............................................................. -->	
																	<!--#include file="i_stock_quote.asp"-->
															<!-- ......................................................... End Stock Quote  ........................................................... -->	
															<!--<a href="#" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('Image2','','images/stock_price.png',1)">
															<img src="images/set_index.png" width="193" height="240" id="Image2" /></a>-->
														</div>
													</div>
												</div>
												<div id="w1_download">

													<table width="100%" cellpadding="0" cellspacing="0" border="0">
														<tr>
															<td align="left" width="295">
																<%if session("lang")="E" then%><img src="images/topic_ocean_en.jpg"><%else%><img src="images/topic_ocean.jpg"><%end if%>
															</td>
															<td>
																<table width="100%" cellpadding="0" cellspacing="0" border="0">
																	<tr>
																		<td width="10">&nbsp;</td>
																		<td align="left" valign="top" width="320">
																			&bull;&nbsp;<%if session("lang")="E" then%>Notification of the intention to exercise of OCEAN-W1<%else%>˹ѧ����駡�����Է�ԫ���������ѭ�����ʴ��Է��<%end if%>
																		</td>
																		<td align="left" valign="top">
																			<a style="text-decoration: none;" href="pdf/�������駡�˹�������Է��_OCEAN_W1_���駷��_3_TH.pdf" target="_blank">TH&nbsp;<img src="images/pdf_icon.gif"></a>
																			&nbsp;&nbsp;|&nbsp;&nbsp;
																			<a style="text-decoration: none;" href="pdf/�������駡�˹�������Է��_OCEAN_W1_���駷��_3_ENG.pdf" target="_blank">EN&nbsp;<img src="images/pdf_icon.gif"></a>
																		</td>
																		<td width="10">&nbsp;</td>
																	</tr>
																	<tr>
																		<td></td>
																		<td colspan="2" class="line_w1"></td>
																		<td></td>
																	</tr>
																	<tr>
																		<td width="10">&nbsp;</td>
																		<td align="left" valign="top" style="padding-top: 5px;">
																			&bull;&nbsp;<%if session("lang")="E" then%>TERMS OF RIGHT AND DUTIES of OCEAN-W1<%else%>��͡�˹��Է�� OCEAN-W1<%end if%>
																		</td>
																		<td align="left" valign="top" style="padding-top: 5px;">
																			<a style="text-decoration: none;" href="pdf/��͡�˹���Ҵ����Է�����˹�ҷ��_TH_20141113.pdf" target="_blank">TH&nbsp;<img src="images/pdf_icon.gif"></a>
																			&nbsp;&nbsp;|&nbsp;&nbsp;
																			<a style="text-decoration: none;" href="pdf/��͡�˹���Ҵ����Է�����˹�ҷ��_ENG_20141113.pdf" target="_blank">EN&nbsp;<img src="images/pdf_icon.gif"></a>
																		</td>
																		<td width="10">&nbsp;</td>
																	</tr>
																	<tr>
																		<td></td>
																		<td colspan="2" class="line_w1"></td>
																		<td></td>
																	</tr>
																	<tr>
																		<td width="10">&nbsp;</td>
																		<td align="left" valign="top" style="padding-top: 5px;">
																			&bull;&nbsp;<%if session("lang")="E" then%>EXERCISE NOTIFICATION FORM TO PURCHASE<br>ORDINARY SHARES OF OCEAN-W1 No.3<%else%>Ẻ�ʴ������ӹ�������Է�� OCEAN-W1 ���駷�� 3<%end if%>
																		</td>
																		<td align="left" valign="top" style="padding-top: 5px;">
																			<a style="text-decoration: none;" href="pdf/notification_form_no3_TH.pdf" target="_blank">TH&nbsp;<img src="images/pdf_icon.gif"></a>
																			&nbsp;&nbsp;|&nbsp;&nbsp;
																			<a style="text-decoration: none;" href="pdf/notification_form_no3_EN.pdf" target="_blank">EN&nbsp;<img src="images/pdf_icon.gif"></a>
																		</td>
																		<td width="10">&nbsp;</td>
																	</tr>
																</table>
															</td>
														</tr>
													</table>
												</div>
												<div id="news_annual">
													<div id="news">
														<!--<div id="topic_news">
															<%=home_topic_news%>
														</div>-->
														<!-- .............................................................. News Update .............................................................. -->	
																<!--#include file="i_news_room.asp"-->
														<!-- ......................................................... End News Update ........................................................... -->	
													</div>
													<div id="annual">
														<div id="topic_annual">
															<%=home_topic_annual%>
														</div>
														<div id="cover"> 
															<a href="annual_report.asp">
																<img src="images/annual_report.png" width="225" height="166" />
															</a>
															<p class="text_10">
																<a href="annual_report.asp">
																	��§ҹ��Шӻթ�Ѻ�������ʹͼ�����稢ͧ<br/>
																	��ô��Թ�ҹ���Ἱ��Ժѵԧҹ�ͧ<br/>
																	����ѷ �����¹ ������ê �ӡѴ ��Ҫ�
																</a>
															</p>
														</div>
													</div>
												</div>
												<div id="highlight">
													<div id="shotcut" style="margin-right: 4px;">
														<!--#include file = "../../i_conir.asp" -->
														<!--#include file="../../function_asp2007.asp"-->
															<%
																sql=sql_Listed_mainmenu(listed_com_id,session("lang"))
																set rs_menuleft = conir.execute(sql)
																if not rs_menuleft.eof and not  rs_menuleft.bof then 
																		do while not rs_menuleft.eof
																			%>
																			<a href='general_meeting.asp?menu_id=<%=rs_menuleft("menu_id")%>' onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('Image200','','images/highlight_01_hover.jpg',1)">
																			<img src="images/highlight_01.jpg" alt="" width="245" height="155" id="Image200" /></a>
																			<%
																		rs_menuleft.movenext
																		loop
																else
																%>
																	<a href="general_meeting.asp" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('Image200','','images/highlight_01_hover.jpg',1)">
																	<img src="images/highlight_01.jpg" alt="" width="245" height="155" id="Image200" /></a>
																<%
																end if	
																rs_menuleft.close
																set rs_menuleft = nothing				
															%>
													</div>
													<div id="shotcut" style="margin-right: 5px;">
														<a href="finance_highlights.asp" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('Image300','','images/highlight_02_hover.jpg',1)">
														<img src="images/highlight_02.jpg" alt="" width="245" height="155" id="Image300" /></a>
													</div>
													<div id="shotcut">
														<a href="analyst.asp" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('Image400','','images/highlight_03_hover.jpg',1)">
														<img src="images/highlight_03.jpg" alt="" width="245" height="155" id="Image400" /></a>
													</div>
												</div>
											</td>
										</tr>
										<!-- .............................................................. Go to Top .............................................................. -->	
												<!--#include file = "i_page_top.asp" -->
										<!-- .......................................................... End Go to Top .......................................................... -->
									</table>
								</div>
								<!-- .............................................................. Footer .............................................................. -->	
										<!--#include file = "i_footer.asp" -->
								<!-- .......................................................... End Footer. ......................................................... -->
							</td>
						</tr>
					</table>
				</td>
			</tr>
		</table>
		<!--#include file="i_googleAnalytics.asp"-->	
	</body>
</html>