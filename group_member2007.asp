<%
session("cur_page")="IR Members"
session("page_asp")="group_member2007"

if session("lang")="" or request("lang")="T"  then  
	session("lang")="T"
elseif request("lang")="E" then
	session("lang")="E"
end if


%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=windows-874">
<title>Investor Relations</title>
<LINK href="style2007.css" type="text/css" rel="stylesheet">	
<!--#include file = "i_constant.asp" -->
<!--#include file = "function_asp2007.asp" -->
<!--#include file = "i_conpsims.asp" -->
<!--#include file = "i_conirauthen.asp" -->
<!--#include file = "listed_member2007.asp" -->
<meta name="keywords" content="<%=message_keyword%>">
<script language="javascript" src="/ir/function2007.js"></script>
<script type="text/javascript" language="JavaScript1.2" src="/ir/script2007/stm31.js"></script>
</head>

<body >
<table width="100%"  border="0" cellspacing="0" cellpadding="0">
  <tr>
		<td align="center" valign="top">
			<table width="742" border="0" cellpadding="0" cellspacing="0" bgcolor="#ECECEC">
				<tr align="left" valign="top">
					<td width="1" background="images2007/dot_pink.gif"><img src="images2007/dot_pink.gif" width="1" height="1"></td>
					<td>
							<table width="100%"  border="0" cellspacing="0" cellpadding="0">
									<!--++++++++++++++++++++++++include top++++++++++++++++++++++-->
								
													<!--#include file = "i_top2007.asp" -->
										
									<!--+++++++++++++++++++++++include menu+++++++++++++++++++++-->																									  
								  
												<!--#include file = "i_menu2007.asp" -->									
								
								<!--++++++++++++++++++++++++Content+++++++++++++++++++++++++++++-->
								 								
									   <tr>
											<td height="1" align="left" valign="top" background="images2007/dot_pink.gif"><img src="images2007/dot_pink.gif" width="1" height="1"></td>
										</tr>
										<tr>
											<td align="center" valign="top"><img src="images2007/head_members.gif" width="740" height="80"></td>
										</tr>
										<tr>
											<td align="left" valign="top">
												<table width="300" border="0" cellspacing="0" cellpadding="0">
													  <tr align="left" valign="top">
														<td width="14"><img src="images2007/bar_1left.gif" width="14" height="21"></td>
														<td width="130" valign="middle" background="images2007/bar_1bg1.gif"><strong>IR Members </strong></td>
														<td width="32"><img src="images2007/bar_1creb.gif" width="32" height="21"></td>
														<td background="images2007/bar_1bg2.gif">&nbsp;</td>
														<td width="4" align="right"><img src="images2007/bar_1right.gif" width="4" height="21"></td>
													  </tr>
												</table>              
											</td>			
										</tr>
														 
										 <tr>
												<td align="center" valign="top" bgcolor="#FFFFFF"><p>&nbsp;</p>													
													<table width="600" border="0" cellpadding="0" cellspacing="1" bgcolor="#CC00CC">
														<tr>
														  <td align="left" valign="top" bgcolor="#FFFFFF">
																<table width="100%"  border="0" cellspacing="0" cellpadding="5">
																 <%
																 
																   if trim(session("listed_member")) <> "" then  
																					  dim i
																					  
																					   strsql=""
																					   strsql="SELECT a.com_name_eng,a.com_name_th,a.com_id,b.sec_name "
																						strsql=strsql + " FROM Company as a,Compsec as b  "
																						strsql=strsql + " where a.com_id=b.com_id and b.sec_name in(" &  session("listed_member")  & ") "
																								
																					   if request("Alpha")<>"" then	
																									 select case request("Alpha")
																									 case "A-E":																					
																										 strsql=strsql + " and (a.com_name_eng like 'A%' or a.com_name_eng like 'B%' or a.com_name_eng like 'C%'  "
																										 strsql=strsql + " or a.com_name_eng like 'D%' or a.com_name_eng like 'E%' ) "	
																										 
																									case "F-J":
																										strsql=strsql + " and (a.com_name_eng like 'F%' or a.com_name_eng like 'G%' or a.com_name_eng like 'H%'  "
																										 strsql=strsql + " or a.com_name_eng like 'I%'  or a.com_name_eng like 'J%' ) "	
																										 
																									case "K-O":
																										 strsql=strsql + " and (a.com_name_eng like 'K%' or a.com_name_eng like 'L%' or a.com_name_eng like 'M%'  "
																										 strsql=strsql + " or a.com_name_eng like 'N%'  or a.com_name_eng like 'O%' ) "	
																										 
																								   case "P-T":
																										strsql=strsql + " and (a.com_name_eng like 'P%' or a.com_name_eng like 'Q%' or a.com_name_eng like 'R%'  "
																										strsql=strsql + " or a.com_name_eng like 'S%' or a.com_name_eng like 'T%' ) "	
																										 
																									case "U-Z":
																										strsql=strsql + " and (a.com_name_eng like 'U%' or a.com_name_eng like 'V%' or a.com_name_eng like 'W%'  "
																										strsql=strsql + " or a.com_name_eng like 'X%'  or a.com_name_eng like 'Y%'  or a.com_name_eng like 'Z%' ) "	
					
																									end select		
																									
																					elseif request("selectsector")<>"" then		
																						 strsql=strsql + "  and b.sector_no ="& request("selectsector") 
																					elseif request("select_mk_type")<>"" then
																						strsql=strsql + "  and b.mk_type ='"& request("select_mk_type")  & "' "
																					end if
																					
																					strsql=strsql + "  order by  com_name_eng  asc"	
																					'response.write strsql
																					'response.end
																					
																					set rs = conpsims.execute(strsql)	
																					if not rs.EOF then
																						i=0
																						do while not rs.eof  
																							if i mod 2=0 then
																							 %>
																							   <tr bgcolor="#FFFFFF">
																							 <%
																							else
																							 %>
																							   <tr bgcolor="#ECECEC">
																							 <%
																							end if
																							%>
																							   <td align="left" valign="middle">
																									&nbsp;<img src="images2007/piont_dot.gif" >&nbsp;<a  class="group_member" href="listed/<%= rs("sec_name")%>/home.asp?listed_name=<%=replace(rs("sec_name"),"&","_")%>" target="_blank" >
																									<%
																									if  request("Alpha")="" then
																											if  session("lang")="T" or   session("lang")=""  then 
																												  response.write rs("com_name_th") & "  [" & rs("sec_name") & "]"
																											else
																												 response.write  rs("com_name_eng") & "  [" & rs("sec_name") & "]"
																											end if	
																									else
																									       response.write  rs("com_name_eng") & "  [" & rs("sec_name") & "]"
																									end if
																									
																									%></a>
																								</td>
																							</tr>
																							<%
																							i=i+1
																						rs.movenext
																						loop
																					else
																					%>
																					<tr>
																							<td >&nbsp;</td>					
																					</tr>
																							<tr bgcolor="#FFFFFF" >
																								   <td align="center"   valign="middle">
																										 <font class="no_information">
																										<%
																										if  session("lang")="T"  then 
																											 response.write Message_Info_T
																										else																				
																											  response.write Message_Info_E
																										end if
																										%></font>
																									</td>
																								</tr>
																							<%
																					end if
																	else%>
																				<tr>
																							<td >&nbsp;</td>					
																					</tr>
																	     	<tr bgcolor="#FFFFFF" >
																		   <td align="center"  valign="middle">
																			     <font class="no_information">
																				<%
																				if  session("lang")="T"  then 
																				     response.write Message_Info_T
																				else																				
																				      response.write Message_Info_E
																				end if
																				%></font>
																			</td>
																		</tr>
																<%	end if
																 %>	
																 <tr>
																		<td >&nbsp;</td>					
																</tr>
																
																<tr>
																	<td align="left">
																	   <font class="less_than_singn"><<&nbsp;</font><a  class="txt_back" href="ir_members2007.asp" >Back </a>
																	</td>
																</tr>
																<tr>
																		<td >&nbsp;</td>					
																</tr>																
																			
												</table>
												</td>
											</tr>
									</table>
						<br><br><br>
			</br>			
			<!--+++++++++++++++++++++++++include footer++++++++++++++++++++++-->
									
												<!--#include file = "i_footer2007.asp" -->
												
								</table>
							</td>
							<td width="1" align="right" background="images2007/dot_pink.gif"><img src="images2007/dot_pink.gif" width="1" height="1"></td>
						</tr>	
    </table></td>
  </tr>
</table>
<!-- +++++++++++++++++ Google Analytics ++++++++++++++++ -->
          <!--#include file="i_googleAnalytics.asp"-->	
<!-- +++++++++++++++ End Google Analytics +++++++++++++++ -->
</body>
</html>
