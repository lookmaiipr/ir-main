<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<title>eFinanceThai.com/Investor Relation</title>
<meta http-equiv="Content-Type" content="text/html; charset=windows-874">
<style type="text/css">
<!--
body {  margin: 0px  0px; padding: 0px  0px}
a:link { color: #333333; text-decoration: none}
a:visited { color: #333333; text-decoration: none}
a:active { color: #cc99cc; text-decoration: underline}
a:hover { color: #cc99cc; text-decoration: underline}
.style3 {
	font-size: 1px;
	font-family: "MS Sans Serif", AngsanaDSE;
}
.style7 {color: #666666}
.style8 {color: #000066}
.style9 {color: #333333}
.style10 {
	color: #CC66CC;
	font-weight: bold;
}
.style11 {color: #CC66CC}
-->
</style>
</head>

<body leftmargin="0" topmargin="5" marginwidth="0" marginheight="5">
<div align="center">  <table width="715"  border="0" cellspacing="0" cellpadding="0">
    <tr>
      <td align="left" valign="top"><table width="100%"  border="0" cellpadding="0" cellspacing="1" bgcolor="cfcfcf">
        <tr align="left" valign="top">
          <td width="140"><table width="100%" border="0" cellpadding="0" cellspacing="0">
            <tr>
              <td height="48" align="left" valign="top"><img src="images/efinance_logo.gif" width="139" height="88"></td>
            </tr>
            <tr>
              <form name="form1" method="post" action="">
                <td height="30" align="left" valign="top" bgcolor="#FFFFFF"><table width="100%"  border="0" cellspacing="5" cellpadding="0">
                    <tr align="left" valign="top">
                      <td class="style3">Language : <a href="#"><img src="images/thai.gif" width="20" height="14" border="0"></a> <a href="#"><img src="images/eng.gif" width="20" height="14" border="0"></a></td>
                    </tr>
                </table></td>
              </form>
            </tr>
            <tr>
              <td align="left" valign="top" bgcolor="#FFFFFF"><img src="images/gateway_lsidebar.gif" width="139" height="32"></td>
            </tr>
            <tr>
              <td align="left" valign="top" bgcolor="#FFFFFF"><table width="100%" border="0" cellpadding="5" cellspacing="0" bgcolor="#Cfcfcf">
                <tr>
                  <td align="left" valign="top" bgcolor="#FAF8F5"><font size="1" face="MS Sans Serif, AngsanaUPC"><a href="investor_listed.htm">Listed Company </a></font></td>
                </tr>
                <tr>
                  <td align="left" valign="top" bgcolor="#FAF8F5"><font size="1" face="MS Sans Serif, AngsanaUPC"><a href="investor_news.htm">News</a></font></td>
                </tr>
                <tr>
                  <td align="left" valign="top" bgcolor="#FAF8F5"><font size="1" face="MS Sans Serif, AngsanaUPC"><a href="investor_annual.htm">Annual Reports</a></font></td>
                </tr>
                <tr>
                  <td align="left" valign="top" bgcolor="#FAF8F5"><font size="1" face="MS Sans Serif, AngsanaUPC"><a href="investor_announ.htm">Announcement</a></font></td>
                </tr>
                <tr>
                  <td align="left" valign="top" bgcolor="#FAF8F5"><font size="1" face="MS Sans Serif, AngsanaUPC"><a href="investor_public.htm">Public Relation</a></font></td>
                </tr>
                <tr>
                  <td align="left" valign="top" bgcolor="#FAF8F5"><font size="1" face="MS Sans Serif, AngsanaUPC"><a href="investor_upcom.htm">Upcoming Event</a></font></td>
                </tr>
                <tr>
                  <td align="left" valign="top" bgcolor="#FAF8F5"><font size="1" face="MS Sans Serif, AngsanaUPC"><a href="investor_FAQ.htm">FAQs</a></font></td>
                </tr>
              </table></td>
            </tr>
            <tr>
              <td align="left" valign="top" background="images/line_w.gif" bgcolor="#FFFFFF"><img src="images/line_w.gif" width="4" height="4"></td>
            </tr>
            <tr>
              <td align="left" valign="top" bgcolor="#FFFFFF"><img src="images/right_top.gif" width="139" height="46"></td>
            </tr>
            <tr>
              <td align="left" valign="top" bgcolor="#FFFFFF"><table width="100%"  border="0" cellpadding="0" cellspacing="5" bgcolor="#cfcfcf">
                <tr>
                  <td><table width="100%" border="0" cellpadding="0" cellspacing="1" bgcolor="#CC99CC">
                    <tr>
                      <td align="center" valign="top" bgcolor="#FFFFFF"><strong><em>
                        <object classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" codebase="http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=6,0,29,0" width="123" height="100">
                          <param name="movie" value="images/banner.swf">
                          <param name="quality" value="high">
                          <embed src="images/banner.swf" quality="high" pluginspage="http://www.macromedia.com/go/getflashplayer" type="application/x-shockwave-flash" width="123" height="100"></embed>
                        </object>
                      </em></strong></td>
                    </tr>
                  </table></td>
                </tr>
                <tr>
                  <td><table width="100%" border="0" cellpadding="0" cellspacing="1" bgcolor="#CC99CC">
                    <tr>
                      <td align="left" valign="top" bgcolor="#FFFFFF"><font size="1" face="MS Sans Serif, AngsanaUPC"><strong><em></em></strong></font></td>
                    </tr>
                  </table></td>
                </tr>
                <tr>
                  <td>&nbsp;</td>
                </tr>
              </table></td>
            </tr>
            <tr>
              <td align="left" valign="top">&nbsp;</td>
            </tr>
          </table></td>
          <td bgcolor="#FFFFFF"><table width="100%"  border="0" cellspacing="0" cellpadding="0">
            <tr>
              <td align="left" valign="top"><table width="100%"  border="0" cellpadding="0" cellspacing="1" bgcolor="#000066">
                  <tr>
                    <td align="left" valign="top"><img src="images/investor.gif" width="572" height="48"></td>
                  </tr>
              </table></td>
            </tr>
            <tr>
              <td align="left" valign="top"><table width="100%"  border="0" cellspacing="8" cellpadding="0">
                  <tr align="left" valign="top">
                    <td width="320"><table width="100%"  border="0" cellspacing="0" cellpadding="0">
                        <tr>
                          <td align="left" valign="top"><font size="2" face="MS Sans Serif, AngsanaUPC"><strong><span class="style8"><u>Why ? Investors Relation</u></span></strong></font><font color="#333333" size="2" face="MS Sans Serif, AngsanaUPC"><strong><br>
                              </strong></font><span class="style7"><font size="2" face="MS Sans Serif, AngsanaUPC"><font size="1"><span class="style9"><strong>eFinanceThai.com</strong> Nononon nononononon nonon nonon nnono nonon nonon nonon nono</span></font> <span class="style9"><font size="1">nonon nononononon nonon nonon nnono nonon nonon nonon nono</font> <font size="1">nonon nononononon nonon nonon nnono nonon nonon nonon nono</font></span></font></span></td>
                        </tr>
                        <tr>
                          <td align="left" valign="top">&nbsp;</td>
                        </tr>
                        <tr>
                          <td align="left" valign="top"><table width="100%" border="0" cellpadding="0" cellspacing="1" bgcolor="#CC99CC">
                            <tr align="left" valign="top">
                              <td width="140" valign="middle" bgcolor="#FFFFFF"><font color="#333333" size="2" face="MS Sans Serif, AngsanaUPC"><strong><img src="images/arrow.gif" width="12" height="9"> News</strong></font></td>
                              <td bgcolor="#Cfcfcf">&nbsp;</td>
                            </tr>
                          </table></td>
                        </tr>
                        <tr>
                          <td align="left" valign="top"><table width="100%" border="0" cellpadding="5" cellspacing="1" bgcolor="#CFCFCF">
                            <tr>
                              <td align="left" valign="top" bgcolor="#FAF8F5"><p><font size="1" face="MS Sans Serif, AngsanaUPC"><span class="style10">BBC Company Limited</span><br>
                                      <a href="investor_announ.htm"><font color="#333333">Announcement Regarding a Public Release of Unaudited Financial Results of Bank</font></a></font></p>
                                <p><font size="1" face="MS Sans Serif, AngsanaUPC"><strong><span class="style11">HSCB Company Limited</span><font color="#CC99CC"><br>
                                                                </font></strong><font color="#333333" size="1" face="MS Sans Serif, AngsanaUPC"><a href="investor_announ.htm">Announcement Regarding a Public Release of Nonononon Nonono Nononono nonononono nonono</a></font></font></p></td>
                            </tr>
                          </table></td>
                        </tr>
                        <tr>
                          <td align="left" valign="top">&nbsp;</td>
                        </tr>
                        <tr>
                          <td align="left" valign="top"><table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr align="left" valign="top">
                              <td colspan="2"><table width="100%" border="0" cellpadding="0" cellspacing="1" bgcolor="#CC99CC">
                                <tr align="left" valign="top">
                                  <td width="140" valign="middle" bgcolor="#FFFFFF"><font color="#333333" size="2" face="MS Sans Serif, AngsanaUPC"><strong><img src="images/arrow.gif" width="12" height="9"> Announcement</strong></font> </td>
                                  <td bgcolor="#Cfcfcf">&nbsp;</td>
                                </tr>
                              </table></td>
                              </tr>
                            <tr align="left" valign="top">
                              <td width="70"><font color="#333333" size="1" face="MS Sans Serif, AngsanaUPC"><strong><br>
                                22 JAN</strong></font></td>
                              <td><p><font size="1" face="MS Sans Serif, AngsanaUPC"><strong><font color="#CC66CC"><br>
                                </font>        <span class="style8">BBC Company Limited</span></strong><br>
                                        <a href="investor_announ.htm"><font color="#333333">Announcement Regarding a Public Release of Unaudited Financial Results of Bank</font></a></font></p>
                                  <p><font size="1" face="MS Sans Serif, AngsanaUPC"><strong><span class="style8">HSCB Company Limited</span><font color="#CC99CC"><br>
                                                                  </font></strong><font color="#333333" size="1" face="MS Sans Serif, AngsanaUPC"><a href="investor_announ.htm">Announcement Regarding a Public Release of Nonononon Nonono Nononono nonononono nonono</a></font></font></p></td>
                            </tr>
                            <tr align="left" valign="top">
                              <td colspan="2"><table width="100%" border="0" cellpadding="0" cellspacing="1" bgcolor="#CC99CC">
                                  <tr align="left" valign="top">
                                    <td width="140" valign="middle" bgcolor="#FFFFFF"><font color="#333333" size="2" face="MS Sans Serif, AngsanaUPC"><strong><img src="images/arrow.gif" width="12" height="9"> Public Relation </strong></font></td>
                                    <td bgcolor="#Cfcfcf">&nbsp;</td>
                                  </tr>
                              </table></td>
                            </tr>
                            <tr align="left" valign="top">
                              <td><font color="#333333" size="1" face="MS Sans Serif, AngsanaUPC"><strong><br>
      22 JAN</strong></font></td>
                              <td><p><font size="1" face="MS Sans Serif, AngsanaUPC"><strong><font color="#CC99CC"><br>
                                            </font><span class="style8">SONY Company Limited</span></strong><br>
                                            <a href="investor_announ.htm"><font color="#333333">Announcement Regarding a Public Release of Unaudited Financial Results of Bank</font></a></font></p>
                                  <p><font size="1" face="MS Sans Serif, AngsanaUPC"><strong><span class="style8">HSCB Company Limited</span><font color="#CC99CC"><br>
                                                                    </font></strong><font color="#333333" size="1" face="MS Sans Serif, AngsanaUPC"><a href="investor_announ.htm">Announcement Regarding a Public Release of Nonononon Nonono Nononono nonononono nonono</a></font></font></p>
                                  <p><font size="1" face="MS Sans Serif, AngsanaUPC"><strong><span class="style8">TTI Company Limited</span><font color="#CC99CC"><br>
                                                                    </font></strong><font color="#333333" size="1" face="MS Sans Serif, AngsanaUPC"><a href="investor_announ.htm">Announcement Regarding a Public Release of Nonononon Nonono Nononono nonononono nonono</a></font></font></p>                                  </td>
                            </tr>
                          </table></td>
                        </tr>
                        <tr>
                          <td align="left" valign="top">&nbsp;</td>
                        </tr>
                    </table></td>
                    <td><table width="100%"  border="0" cellspacing="0" cellpadding="0">
                        <tr>
                          <td align="left" valign="top"><table width="100%" border="0" cellpadding="0" cellspacing="1" bgcolor="#CC99CC">
                            <tr align="left" valign="top">
                              <td width="140" valign="middle" bgcolor="#FFFFFF"><font color="#333333" size="2" face="MS Sans Serif, AngsanaUPC"><strong><img src="images/arrow.gif" width="12" height="9"> Annual Reports</strong></font></td>
                              <td bgcolor="#Cfcfcf">&nbsp;</td>
                            </tr>
                          </table></td>
                        </tr>
                        <tr>
                          <td align="left" valign="top"><table width="100%"  border="0" cellspacing="8" cellpadding="0">
                            <tr align="center" valign="top">
                              <td width="50%"><table width="80" border="0" cellpadding="5" cellspacing="1" bgcolor="#000066">
                                  <tr>
                                    <td align="center" valign="top" bgcolor="#CFCFCF"><img src="images/vanachai46.gif" width="70" height="98"></td>
                                  </tr>
                              </table></td>
                              <td><table width="80" border="0" cellpadding="5" cellspacing="1" bgcolor="#000066">
                                  <tr>
                                    <td align="center" valign="top" bgcolor="#CFCFCF"><img src="images/siam2003.gif" width="70" height="98"></td>
                                  </tr>
                              </table></td>
                            </tr>
                            <tr align="center" valign="top">
                              <td><font color="#333333" size="1" face="MS Sans Serif, AngsanaUPC"><a href="vanachai/annual.htm">Vanachai Group Public Company Limited</a></font></td>
                              <td><font color="#333333" size="1" face="MS Sans Serif, AngsanaUPC"><a href="#">The Siam Cement Public Company Limited</a></font></td>
                            </tr>
                            <tr align="center" valign="top">
                              <td><table width="80" border="0" cellpadding="5" cellspacing="1" bgcolor="#000066">
                                  <tr>
                                    <td align="center" valign="top" bgcolor="#CFCFCF"><img src="images/ais46.gif" width="70" height="98"></td>
                                  </tr>
                              </table></td>
                              <td><table width="80" border="0" cellpadding="5" cellspacing="1" bgcolor="#000066">
                                  <tr>
                                    <td align="center" valign="top" bgcolor="#CFCFCF"><img src="images/shin46.gif" width="70" height="98"></td>
                                  </tr>
                              </table></td>
                            </tr>
                            <tr align="center" valign="top">
                              <td><font color="#333333" size="1" face="MS Sans Serif, AngsanaUPC"><a href="#">AIS Company Limited</a></font></td>
                              <td><font color="#333333" size="1" face="MS Sans Serif, AngsanaUPC"><a href="#">Shin Corporation Company Limited</a></font></td>
                            </tr>
                            <tr align="center" valign="top">
                              <td><table width="80" border="0" cellpadding="5" cellspacing="1" bgcolor="#000066">
                                <tr>
                                  <td align="center" valign="top" bgcolor="#CFCFCF"><img src="images/gmm.gif" width="70" height="98"></td>
                                </tr>
                              </table></td>
                              <td><table width="80" border="0" cellpadding="5" cellspacing="1" bgcolor="#000066">
                                <tr>
                                  <td align="center" valign="top" bgcolor="#CFCFCF"><img src="images/thantawan.gif" width="70" height="98"></td>
                                </tr>
                              </table></td>
                            </tr>
                            <tr align="center" valign="top">
                              <td><font color="#333333" size="1" face="MS Sans Serif, AngsanaUPC"><a href="#">GMM Grammy Public  Company Limited</a></font></td>
                              <td><font color="#333333" size="1" face="MS Sans Serif, AngsanaUPC"><a href="#">Thantawan Industry Public  Company Limited</a></font></td>
                            </tr>
                          </table></td>
                        </tr>
                        <tr>
                          <td align="left" valign="top"><table width="100%" border="0" cellpadding="0" cellspacing="1" bgcolor="#CC99CC">
                            <tr align="left" valign="top">
                              <td width="150" valign="middle" bgcolor="#FFFFFF"><font color="#333333" size="2" face="MS Sans Serif, AngsanaUPC"><strong><img src="images/arrow.gif" width="12" height="9"> Upcoming Event </strong></font></td>
                              <td bgcolor="#Cfcfcf">&nbsp;</td>
                            </tr>
                          </table></td>
                        </tr>
                        <tr>
                          <td align="left" valign="top"><table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr align="left" valign="top">
                              <td width="70"><font color="#333333" size="1" face="MS Sans Serif, AngsanaUPC"><strong><br>
                                22 JAN</strong></font></td>
                              <td><font size="1" face="MS Sans Serif, AngsanaUPC"><a href="investor_announ.htm"><font color="#333333"><br>
                                Announcement Regarding a Public Release of Unaudited Financial Results of Bank</font></a></font></td>
                            </tr>
                            <tr align="left" valign="top">
                              <td><font color="#333333" size="1" face="MS Sans Serif, AngsanaUPC"><br>
                                23 JAN</font></td>
                              <td><font size="1" face="MS Sans Serif, AngsanaUPC"><font color="#333333" size="1" face="MS Sans Serif, AngsanaUPC"><a href="investor_announ.htm"><br>
                                Announcement Regarding a Public Release of Nonononon Nonono Nononono nonononono nonono</a></font></font></td>
                            </tr>
                          </table></td>
                        </tr>
                        <tr>
                          <td align="left" valign="top">&nbsp;</td>
                        </tr>
                    </table></td>
                  </tr>
              </table></td>
            </tr>
          </table></td>
        </tr>
        <tr align="right" valign="top">
          <td colspan="2"><span class="style7"><font size="1" face="Verdana, Arial, Helvetica, sans-serif"><u>Copyright &reg; 2004 eFinanceThai.com</u></font></span></td>
        </tr>
      </table></td>
    </tr>
  </table>
  <p>&nbsp;</p>
</div>
</body>
</html>
