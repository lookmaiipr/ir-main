<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">

<%
PageType="Home"
Session("redirect_page")=""
Session("PageName")="Investor"
Session("PageASP")="index.asp"

%>
 <!-- #include file="../i_constant.asp" -->
 <!--#include file = "../i_connews.asp" -->
 <!--#include file = "../i_conir.asp" -->
<!--#include file = "../i_coninv_station.asp" -->
<!--#include file = "../i_conradio.asp" -->
<!--#include file = "../formatdate.asp" -->
<!--#include file = "../function_asp2007.asp" -->
 <!--#include file = "IC_MemberType.asp" -->
 <!--#include file = "Scripts/Scripts.asp" -->
 
 <%
 strsql = "select * FROM life_style where display = 'Y'order by timestamp desc limit 1"
  set rs = connews.Execute(strsql)
  if not rs.EOF then
      lifestyle_id=rs("id")     
      lifestyle_name=rs("name")          
      'lifestyle_img=lifestyle_name & "_irplus.gif"
      lifestyle_img=lifestyle_name & "_irplus.jpg"
      lifestyle_by=rs("f_name") & "&nbsp;" & rs("l_name")
      lifestyle_slogan=Chr(34) & "&nbsp;" & rs("slogan") & "&nbsp;" & Chr(34)
      lifestyleFiles_all=rs("files_all")
      If  isnull(rs("multi"))=false   and  rs("multi") <> "" and  rs("multi") <> "-" Then
          lifestyle_multi=rs("multi") 
      else
         lifestyle_multi=""
      end if
      
      if isnull(rs("company"))=false and rs("company")<>"" then
            lifestyle_company=rs("company")
       else
             lifestyle_company=""
       end if  
    
  end if
  rs.close
 %> 
     

<html>
<head>
<meta name="google-site-verification" content="hlHm-vmtFKCUMiLOqF398ZP-5EWWwU_jnGsbkeljZmc" /> 
<meta http-equiv="Content-Type" content="text/html; charset=windows-874">
<meta name="keywords" content="<%=message_keyword%>">
<title>IR Plus : Investor Relations , Thailand Listed Company Information</title>
<LINK href="Themes/Themes.css" type="text/css" rel="stylesheet">	
<script type="text/javascript" language="JavaScript1.2" src="../function2007.js"></script>
<script language="JavaScript" type="text/JavaScript" src="Scripts/Scripts.js?id=20130311" ></script>
<script language="JavaScript" type="text/JavaScript" src="../js/jquery-1.11.3.min.js" ></script>
<script type="text/javascript" language="JavaScript1.2" src="stmenu.js"></script>
<link rel="icon" href="favicon.ico" type="image/x-icon">
<link rel="shortcut icon" href="favicon.ico" type="image/x-icon">
<style>
@media (min-width: 992px)
.col-md-4 {
    width: 33.33333333%;
}

.weshare {
    background: #004A87;
    width: 100%;
    padding: 15px 7px;
    overflow: hidden;
}
</style>
</head>
<body >
<table width="100%"  border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td align="center" valign="top">
        <table width="1000" border="0" cellpadding="0" cellspacing="0">
               <tr>
                    <td align="left" valign="top" bgcolor="#FFFFFF">
                          <table width="100%"  border="0" cellspacing="0" cellpadding="0">
				<tr align="left" valign="top">
                                    <td colspan="4">
                                          <!--#include file = "Banner/IC_TopBanner775X80.asp" -->
                                    </td>
				 </tr>			 
                                <tr align="left" valign="top">
                                    <td colspan="2">
                                            <!--#include file = "TopSection.asp" -->
                                    </td>
                                    <td width="272" rowspan="2"><img src="<%=patheFinanceThai & "lifestyle/images/" & lifestyle_img%>" width="272" alt="Investor Relations : Thai Listed Company Informations,MANAGEMENT'S LIFESTYLE"></td>
                               </tr>
                               
                               <tr align="left" valign="top">
                                    <td width="155" rowspan="7" background="images/bg_menu.jpg">
                                             <!--#include file = "LeftSection.asp" -->
                                    </td>
                             
                                    <td>
                                            <table width="100%"  border="0" cellspacing="0" cellpadding="0">
                                                      <tr align="left" valign="top">
                                                                <td colspan="4">&nbsp;</td>
                                                      </tr>
                                                      <tr align="left" valign="top">
                                                            <td width="20">&nbsp;</td>
                                                            <!--<td width="193">                                                         
                                          <!--include file = "IC_Login.asp" -->    
                                                            <!--</td>-->
                                                            <td width="20">&nbsp;</td>
                                                            <td width="340">
                                                                    <!--#include file = "ManagementLifeStyle/IC_LifeStyle.asp" -->
                                                            </td>
                                                        </tr>
                                                      <tr align="left" valign="top">
                                                                <td colspan="4">&nbsp;</td>
                                                      </tr>
                                        </table></td>
                            </tr>
                        
                            <tr align="left" valign="top">
                                    <td colspan="2" align="right"><img src="images/management_bar.jpg" width="845" height="6"></td>
                             </tr>      
							 
							 
									   <tr align="left" valign="top">
                                <td colspan="2">
                                        <table width="100%"  border="0" cellspacing="0" cellpadding="0">
                                                  <tr align="left" valign="top">
                                                        <td width="315">
                                                               <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                                  <tr align="left" valign="top">
                                                                     <td width="24" align="left" valign="top"><img src="images/movement_head00.jpg" width="24" height="47"></td>
                                                                     <td width="159" Height="47" id="MenuIPOMovement"  onclick="reset();ShowIPO(this,'IPOMovement','Images/movement_head01.jpg');" onMouseOut="M_swapImageRestor(this)" onMouseOver="M_swapImage(this,'Images/movement_head01.jpg')" alt="Investor Relations : Thai Listed Company Informations, IPO MOVEMENT">&nbsp;</td>
                                                                     <td width="132" Height="47" id="MenuInsightIPO"  onclick="reset();ShowIPO(this,'InsightIPO','Images/insight_head01.jpg');" onMouseOut="M_swapImageRestor(this)" onMouseOver="M_swapImage(this,'Images/insight_head01.jpg')" alt="Investor Relations : Thai Listed Company Informations, INSIGHT IPO">&nbsp;</td>                                                                         
                                                                    </div>
                                                               </table>
                                                        </td>
                                                        <td width="1" rowspan="2" bgcolor="#01118D"><img src="images/space.gif" width="1" height="8"></td>
                                                        <td width="315"><img src="images/society_head.jpg" width="315" height="47" alt="Investor Relations : Thai Listed Company Informations, IR SOCIETY"></td>
                                                        <td width="1" rowspan="2" bgcolor="#01118D"><img src="images/space.gif" width="1" height="8"></td>
                                                        <td background="images/bg_cg.jpg"><img src="images/knowledge_head.jpg" width="213" height="47" alt="Investor Relations : Thai Listed Company Informations, IR KNOWLEDGE"></td>
                                                </tr>
                                                
                                                  <tr align="left" valign="top">
                                                            <td width="315" background="images/cg_head02.jpg">
                                                                   <DIV id="InsightIPO" style="DISPLAY: block">
                                                                        <!--#include file = "InsightIPO/IC_InsightIPO.asp" -->
                                                                  </DIV>
                                                                  <DIV id="IPOMovement" style="DISPLAY: none">
                                                                        <!--#include file = "IPOMovement/IC_IPOMovement.asp" -->
                                                                  </DIV>                                                           
                                                            </td>
                                                            <td background="images/cg_head02.jpg">
                                                                      <!--#include file = "IRSociety/IC_IRSociety.asp" -->
                                                            </td>
                                                            <td background="images/cg_head02.jpg">
                                                                     <!--#include file = "IRKnowledge/IC_IRKnowledge.asp" -->
                                                           </td>
                                                  </tr>
                                    </table></td>
										</tr>
							 
								<!--
                          <tr align="left" valign="top">
                                <td colspan="2">
                                        <table width="100%"  border="0" cellspacing="0" cellpadding="0">
                                                  <tr align="left" valign="top">
                                                        <td width="315"><img src="images/insight_head.jpg" width="315" height="47"></td>
                                                        <td width="1" rowspan="2" bgcolor="#01118D"><img src="images/space.gif" width="1" height="8"></td>
                                                        <td width="315"><img src="images/cg_head.jpg" width="315" height="47"></td>
                                                        <td width="1" rowspan="2" bgcolor="#01118D"><img src="images/space.gif" width="1" height="8"></td>
                                                        <td background="images/bg_cg.jpg"><img src="images/movement_head.jpg" width="213" height="47"></td>
                                                </tr>
                                                
                                                  <tr align="left" valign="top">
                                                            <td background="images/cg_head02.jpg"> 
								-->
                                                                    <!--include file = "InsightIPO/IC_InsightIPO.asp" -->
                        <!--
                                    </td>
                                                            <td background="images/cg_head02.jpg">
								-->
                                                                     <!--include file = "CGonStage/IC_CGonStage.asp" -->
								<!--
                                                            </td>
                                                            <td background="images/cg_head02.jpg">
								-->
                                                                     <!--include file = "IPOMovement/IC_IPOMovement.asp" -->
								<!--									 
                                                           </td>
                                                  </tr>
                                    </table></td>
                     </tr>
							-->
                     
                      <tr align="left" valign="top">
                            <td colspan="2"><img src="images/space.gif" width="20" height="20"></td>
                      </tr>
                      
                      <tr align="left" valign="top">
                            <td colspan="2">
                                    <table width="100%"  border="0" cellspacing="0" cellpadding="0">
                                            <tr align="left" valign="top">
                                                    <td width="17"><img src="images/space.gif" width="17" height="20"></td>
                                                    <td width="479">
                                                            <table width="100%"  border="0" cellspacing="0" cellpadding="0">
                                                                  <tr>
                                                                        <td align="left" valign="top">
                                                                                  <!--#include file = "InfoRequest/EmailAlert/IC_EmailAlert.asp" -->
                                                                        </td>
                                                                </tr>
                                                                 <tr>
                                                                        <td align="left" valign="top">&nbsp;</td>
                                                                 </tr>
                                                                 <tr>
                                                                        <td align="left" valign="top">
                                                                                <!--#include file = "WebcastAndPresentation/IC_WebCast.asp" -->
                                                                        </td>
                                                                 </tr>
                                                              <tr>
                                                                        <td align="left" valign="top">&nbsp;</td>
                                                              </tr>
                                                              <tr>
                                                                    <td align="center" valign="top">
                                                                            <!--#include file = "Banner/IC_Banner468X60.asp" -->
                                                                    </td>
                                                              </tr>
                                                       </table></td>
                                                       
                                            <td width="20">
                                                    <img src="images/space.gif" width="20" height="20">
                                             </td>
                                             
                                            <td>
                                                    <table width="100%"  border="0" cellspacing="0" cellpadding="0">
                                                            <tr>
                                                                    <td align="center" valign="top">
                                                                            <!--#'include file = "LiveAudio/IC_LiveAudio.asp" -->
																			<br><img src="images/efin.png" width="250">
                                                                    </td>
                                                            </tr>
                                                          <tr>
                                                                 <td align="left" valign="top"><br><br></td>
                                                          </tr>                                                         
                                                          <tr>
                                                                <td align="center" valign="top">
                                                                          <!--#'include file = "InvestorStation/IC_InvestorStation.asp" -->
																	<!--<table width="100%"  border="0" cellspacing="1" cellpadding="2">
																		<tr>
																			<td align="center" >
																				<br>
																				<a href="http://onlineasset.co.th"  target="blank"><img src="Images/logo_onlineasset.png" border="0" width="200" height="60"></a>
																				<br><br>
																			</td>
																		</tr>
																		<tr>
																			<td align="center" >
																				<a href="http://www.efinancethai.com"  target="blank"><img src="Images/logo_efinancethai.png" border="0" width="290" height="43"></a>
																				<br><br>
																			</td>
																		</tr>
																		<tr>
																			<td align="center" >
																				<img src="Images/logo_efinancethai_tv.png" border="0" width="290" height="43">
																			</td>
																		</tr>
																		<tr>
																			<td align="center" >
																				<table width="209" border="0" cellspacing="0" cellpadding="0">
																						<tr><td align="center" valign="top" colspan="3"><img src="Images/eic_top.jpg" width="209" height="83" /></td></tr>
																						<tr>
																							<td><img src="Images/eic_left.jpg"></td>
																							<td align="center" valign="top">
																								<a href="http://www.efinancethai.com/eInvestorChannel/index.aspx" target="blank" title="����¡��ʴ">
																									 <img src="Images/eInvestorChannel_Banner_New.gif" width="170" height="92" border="0" /></a></td>
																							<td><img src="Images/eic_right.jpg"></td></tr>
																						<tr>
																							 <td align="center" valign="top" colspan="3"><img src="Images/eic_buttom.jpg" width="209" height="34" /></td>
																						</tr>
																					</table>
																			</td>
																		</tr>
																	</table>-->
															
																	<div class="weshare" >
																	<IFRAME  id="ifrWeShare" src="https://www.efinancethai.com/weshare/weshare.aspx"  frameBorder="0"  width="315"    height="500"  scrolling ="no"></IFRAME>
																	</div>
															
																	
                                                                </td>
                                                         </tr>
                                                    </table>
                                                 </td>
                                          </tr>
                                
                                    <tr>
                                        <td align="left" valign="top">&nbsp;</td>
                                    </tr>
                     <!--</table>-->
                     </td>
                     
                    <td width="17" align="right"><img src="images/space.gif" width="17" height="20"></td>
                   </tr>
              </table></td>
          </tr>
          
          <tr align="left" valign="top">
                <td colspan="2">
                        <table width="100%"  border="0" cellspacing="0" cellpadding="0">
                              <tr align="left" valign="top">
                                    <td width="17" rowspan="6"><img src="images/space.gif" width="17" height="20"></td>
                                    <td colspan="5">&nbsp;</td>
                                    <td width="17" rowspan="6" align="right"><img src="images/space.gif" width="17" height="20"></td>
                              </tr>
                              
                              <tr align="left" valign="top">
                                    <td colspan="5"><img src="images/calendar_head.jpg" width="181" height="25" alt="Investor Relations : Thai Listed Company Informations, IR CALENDAR"></td>
                              </tr>
                              <tr align="left" valign="top">
                                    <td colspan="5">
                                              <!--#include file = "IRCalendar/IC_CaLendar.asp" -->
                                    </td>
                               </tr>
                              
                              <tr align="left" valign="top">
                                    <td colspan="5">&nbsp;</td>
                              </tr>
                  </table></td>
                </tr>                           
          <tr align="left" valign="top">
            <td colspan="2">
            <table width="100%"  border="0" cellspacing="0" cellpadding="0">
                 <tr align="left" valign="top">
                   <td width="17" rowspan="6" ><img src="images/space.gif" width="17" height="20"></td>
                   <td colspan="5">
                        <table width="100%"  border="0" cellspacing="0" cellpadding="0">
                              <tr>
                                  <td width="51%" valign="top">
                                             <table width="100%"  border="0" cellspacing="0" cellpadding="0">
                                                   <tr>
                                                            <td background="images/bg_head.jpg">
                                                               <img src="images/announcement_head.jpg" width="217" height="30" alt="Investor Relations : Thai Listed Company Informations, SET ANNOUNCEMENT">
                                                            </td>
                                                    </tr>
                                                    <tr>
                                                            <td>
                                                               <!--#include file = "SetAnnouncement/IC_SetAnnouncement.asp" -->
                                                            </td>
                                                    </tr>
                                                </table>
                                       <br>
                                             <table width="100%"  border="0" cellspacing="0" cellpadding="0">
                                                   <tr>
                                                            <td background="images/bg_head.jpg">
                                                               <img src="images/press_head.jpg" width="217" height="30" alt="Investor Relations : Thai Listed Company Informations, PRESS RELEASE">
                                                            </td>
                                                    </tr>
                                                    <tr>
                                                            <td>
                                                               <!--#include file = "PressRelease/IC_PressRelease.asp" -->
                                                            </td>
                                                    </tr>
                                                </table>  
                                        <br>
                                  </td>
                                 <td width="20"><img src="images/space.gif" width="20" height="20"></td>
                                 <td width="1" bgcolor="#01118D"><img src="images/space.gif" width="1" height="20"></td>
                                 <td width="17"><img src="images/space.gif" width="17" height="20"></td>
                                  <td valign="top">
                                       <table width="100%"  border="0" cellspacing="0" cellpadding="0">
                                             <tr>
                                                      <td background="images/bg_head.jpg">
                                                         <img src="images/news_head.jpg" width="222" height="30" alt="Investor Relations : Thai Listed Company Informations, NEWS UPDATE">
                                                      </td>
                                              </tr>
                                              <tr>
                                                      <td>
                                                         <!--#include file = "NewsUpdate/IC_NewsUpdate.asp" -->
                                                      </td>
                                              </tr>
                                          </table>                     
                                       <br>
                                        <table width="100%"  border="0" cellspacing="0" cellpadding="0">
                                             <tr>
                                                      <td background="images/bg_head.jpg">
                                                         <img src="images/public_head.jpg" width="222" height="30" alt="Investor Relations : Thai Listed Company Informations, PUBLIC RELATIONs">
                                                      </td>
                                              </tr>
                                              <tr>
                                                      <td>
                                                         <!--#include file = "PublicRelations/IC_PublicRelations.asp" -->
                                                      </td>
                                              </tr>
                                          </table>     
                                       <br>                                       
                                  </td>                               
                              </tr>
                        </table>       
                   </td>
                   <td width="17" rowspan="6" align="right"><img src="images/space.gif" width="17" height="20"></td>
                 </tr>
                 <tr align="left" valign="top">
                     <td colspan="2">&nbsp;</td>
                   </tr>
               </table></td>
             </tr>         
        </table></td>
      </tr>
      
        <!--#include file = "BottomSection.asp" -->
    
    </table></td>
  </tr>
</table>
<map name="Map">
  <area shape="circle" coords="282,27,14" href="#">
</map>
 <script language="javascript">
 function reset(){

	SetDefault('MenuInsightIPO','Images/insight_head02.jpg','InsightIPO');
	SetDefault('MenuIPOMovement','Images/movement_head02.jpg','IPOMovement');
 }
 
 reset();
 ShowIPO(document.getElementById('MenuIPOMovement'),'IPOMovement','Images/movement_head01.jpg');

 </script>
<!-- .................................................. Google Analytics .................................................. -->
                                 <!--#include file="../i_googleAnalytics.asp"-->	
<!-- ........................................... End Google Analytics .................................................. -->
</body>
</html>
