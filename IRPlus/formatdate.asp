<%
function formatdate(strdate)
				if len(year(strdate)) >=4 then
								if year(strdate) >2500 then
											years=year(strdate)-543
								else
											years=year(strdate)
								end if
				else
							years="20" & year(strdate)
				end if
				if len(month(strdate)) <>2 then 
							months="0" & month(strdate)
				else
							months=month(strdate)
				end if
				if len(day(strdate)) <>2 then 
							days="0" & day(strdate)
				else
							days=day(strdate)
				end if
				if len(hour(strdate)) <>2 then 
							hours="0" & hour(strdate)
				else
							hours=hour(strdate)
				end if
				if len(minute(strdate)) <>2 then 
							minutes="0" & minute(strdate)
				else
							minutes=minute(strdate)
				end if
					if len(second(strdate)) <>2 then 
							seconds="0" & second(strdate)
				else
							seconds=second(strdate)
				end if
				lastupdate=years & "-" & months & "-" & days & " " & hours & ":" & minutes & ":" & seconds
				formatdate=lastupdate
	end function
	
	function formatdateAm_Pm(strdateAmPm)
	
				if len(year(strdateAmPm)) >=4 then
							if year(strdateAmPm) >2500 then
										years=year(strdateAmPm)-543
							else
										years=year(strdateAmPm)
							end if
				else
						years="20" & trim(year(strdateAmPm))
				end if
				
				if len(month(strdateAmPm)) <>2 then 
							months="0" & month(strdateAmPm)
				else
							months=month(strdateAmPm)
				end if
				
				if len(day(strdateAmPm)) <>2 then 
							days="0" & day(strdateAmPm)
				else
							days=day(strdateAmPm)
				end if
			
				if len(hour(strdateAmPm)) <>2 then 
							hours="0" & hour(strdateAmPm)
				else
							hours=hour(strdateAmPm)
				end if
				
				if len(minute(strdateAmPm)) <>2 then 
							minutes="0" & minute(strdateAmPm)
				else
							minutes=minute(strdateAmPm)
				end if
				
				if len(second(strdateAmPm)) <>2 then 
							seconds="0" & second(strdateAmPm)
				else
							seconds=second(strdateAmPm)
				end if
				
				strtime1= hours & ":" & minutes & ":" & seconds
				'response.write "strtime=" & strtime1 & "<br>"
				 'response.end
				if timevalue(strtime1) >timevalue("12:00:00") then
				        if hours=12 then
						   strtime=strtime1 & " PM"
						else
								if len(hours-12)=2 then  
										strtime=(hours-12) & ":" & minutes & ":" & seconds & " PM"
								else
										strtime= "0" & (hours-12) & ":" & minutes & ":" & seconds & " PM"
								end if
						end if
				   'response.write strtime &  strtime1
				   'response.end
				 else
						strtime=strtime1 & " AM"
				 end if
				lastupdate=years & "-" & months & "-" & days & "  " & strtime
				formatdateAm_Pm=lastupdate
	end function
	
	function DateToString(vdate,format)
	'----- format date is DD/MM/YYYY HH:mm:ss tt
		if isdate(vdate) then
			vday = day(vdate)
			vmonth = month(vdate)
			vyear = year(vdate)
			vhour = hour(vdate)
			vmin = minute(vdate)
			vsec = second(vdate)
			
			'----- Find Sec ----
			if len(vsec) < 2 then
				vsec = "0" & vsec
			end if
			
			if len(vmin) < 2 then
				vmin = "0" & vmin
			end if			
			
			if len(vhour) < 2 then
				vhour = "0" & vhour
			end if
			
			tmpstring = Replace(format,"ss",vsec)
			tmpstring = Replace(tmpstring,"mm",vmin)
			tmpstring = Replace(tmpstring,"HH",vhour)
			
			if (instr(UCASE(format),"YYYY") > 0)  then
				if len(vyear) = 2 then
					vyear = "20" & vyear
				end if
				tmpstring = Replace(tmpstring,"YYYY",vyear)	
			elseif  (instr(UCASE(format),"YY") > 0)  then
				if len(vyear) > 2 then
					vyear = right(vyear,2)
				end if
				tmpstring = Replace(tmpstring,"YY",vyear)	
			end if

         if (instr(UCASE(format),"MMMM") > 0) then
				tmpstring = Replace(tmpstring,"MMMM",fullMonth(vmonth))			
			elseif (instr(UCASE(format),"MMM") > 0) then
				tmpstring = Replace(tmpstring,"MMM",shortMonth(vmonth))
			elseif (instr(UCASE(format),"MM") > 0) then
				if len(vmonth) = 1 then
					vmonth = "0" & vmonth
				end if
				tmpstring = Replace(tmpstring,"MM",vmonth)	
			end if		
			

			if len(vday) = 1 then
				vday = "0" & vday
			end if
			
			tmpstring = Replace(tmpstring,"DD",vday)	
			
			'------- Find for AM : PM Format --------
			if instr(format,"tt") > 0 then
				tmpstring = Replace(tmpstring,"tt",AmPm(vhour))	
			end if
			

			DateToString = tmpstring
		else
			DateToString = "Type mismatch."
		end if
	end function
	
	 function DateToStringTH(vdate,format)
	'----- format date is DD/MM/YYYY HH:mm:ss tt
		if isdate(vdate) then
			vday = day(vdate)
			vmonth = month(vdate)
			vyear = year(vdate)
			vhour = hour(vdate)
			vmin = minute(vdate)
			vsec = second(vdate)
			
			'----- Find Sec ----
			if len(vsec) < 2 then
				vsec = "0" & vsec
			end if
			
			if len(vmin) < 2 then
				vmin = "0" & vmin
			end if			
			
			if len(vhour) < 2 then
				vhour = "0" & vhour
			end if
			
			tmpstring = Replace(format,"ss",vsec)
			tmpstring = Replace(tmpstring,"mm",vmin)
			tmpstring = Replace(tmpstring,"HH",vhour)
			
			if (instr(UCASE(format),"YYYY") > 0)  then
				if len(vyear) = 2 then
					vyear = "20" & vyear
				end if
                                 vyear = cint(vyear) + 543                                
				tmpstring = Replace(tmpstring,"YYYY",vyear)	
			elseif  (instr(UCASE(format),"YY") > 0)  then
                              if len(vyear) = 2 then
                                 vyear = "20" & vyear
                              end if
                              vyear = cint(vyear) + 543         
				if len(vyear) > 2 then
					vyear = right(vyear,2)
                                 else
                                       if len(vyear) = 2 then
                                             vyear = "20" & vyear
                                       end if
				end if
				tmpstring = Replace(tmpstring,"YY",vyear)	
			end if
			
         if (instr(UCASE(format),"MMMM") > 0) then
				tmpstring = Replace(tmpstring,"MMMM",fullMonthTH(vmonth))
			elseif (instr(UCASE(format),"MMM") > 0) then
				tmpstring = Replace(tmpstring,"MMM",shortMonthTH(vmonth))
			elseif (instr(UCASE(format),"MM") > 0) then
				if len(vmonth) = 1 then
					vmonth = "0" & vmonth
				end if
				tmpstring = Replace(tmpstring,"MM",vmonth)	
			end if	

			if len(vday) = 1 then
				vday = "0" & vday
			end if
			
			tmpstring = Replace(tmpstring,"DD",vday)	

			DateToStringTH = tmpstring
		else
			DateToStringTH = "Type mismatch."
		end if
	end function
	
	function shortMonth(intmonth)
		select case intmonth
			case 1 : strRet = "Jan"
			case 2 : strRet = "Feb"
			case 3 : strRet = "Mar"
			case 4 : strRet = "Apr"
			case 5 : strRet = "May"
			case 6 : strRet = "Jun"
			case 7 : strRet = "Jul"
			case 8 : strRet = "Aug"
			case 9 : strRet = "Sep"
			case 10 : strRet = "Oct"
			case 11 : strRet = "Nov"
			case 12 : strRet = "Dec"
		end select

		shortMonth=strRet
	end function
	
	function shortMonthTH(intmonth)
		select case intmonth
			case 1 : strRet = "�.�."
			case 2 : strRet = "�.�."
			case 3 : strRet = "��.�."
			case 4 : strRet = "��.�."
			case 5 : strRet = "�.�."
			case 6 : strRet = "��.�."
			case 7 : strRet = "�.�."
			case 8 : strRet = "�.�."
			case 9 : strRet = "�.�."
			case 10 : strRet = "�.�."
			case 11 : strRet = "�.�."
			case 12 : strRet = "�.�."
		end select

		shortMonthTH=strRet
	end function

	function fullMonth(intmonth)
		select case intmonth
			case 1 : strRet = "January"
			case 2 : strRet = "February"
			case 3 : strRet = "March"
			case 4 : strRet = "April"
			case 5 : strRet = "May"
			case 6 : strRet = "June"
			case 7 : strRet = "July"
			case 8 : strRet = "August"
			case 9 : strRet = "September"
			case 10 : strRet = "October"
			case 11 : strRet = "November"
			case 12 : strRet = "December"
		end select
		fullMonth=strRet
	end function
	
	function fullMonthTH(intmonth)
		select case intmonth
			case 1 : strRet = "���Ҥ�"
			case 2 : strRet = "����Ҿѹ��"
			case 3 : strRet = "�չҤ�"
			case 4 : strRet = "����¹"
			case 5 : strRet = "����Ҥ�"
			case 6 : strRet = "�Զع�¹"
			case 7 : strRet = "�á�Ҥ�"
			case 8 : strRet = "�ԧ�Ҥ�"
			case 9 : strRet = "�ѹ��¹"
			case 10 : strRet = "���Ҥ�"
			case 11 : strRet = "��Ȩԡ�¹"
			case 12 : strRet = "�ѹ�Ҥ�"
		end select
		fullMonthTH=strRet
	end function
	
	function AmPm(vhour)
		if vhour = 12 then
			AmPm = "PM"
		elseif vhour > 12 then
			AmPm = "PM"
		else
			AmPm = "AM"
		end if
	end function 
%>