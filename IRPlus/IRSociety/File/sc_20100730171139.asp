<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<title>eFinanceThai.com</title>
     <meta http-equiv="Content-Type" content="text/html; charset=windows-874">		
		<script language="javascript" src="../../IRTHAI/IRTHAI%202.0/Template/function.js"></script>
<style type="text/css">
<!--
body,td,th {
	font-family: Tahoma, sans-serif;
	font-size: small;
	color: #000000;
}
body {
	margin-left: 0px;
	margin-top: 0px;
	margin-right: 0px;
	margin-bottom: 0px;
	background-image: url();
}
a:link {
	color: #000066;
	text-decoration: none;
}
a:visited {
	text-decoration: none;
	color: #000066;
}
a:hover {
	text-decoration: none;
	color: #FF6600;
}
a:active {
	text-decoration: none;
	color: #FF6600;
}
A.M:link {
	COLOR: #000066; TEXT-DECORATION: none;
}
A.M:visited {
	COLOR: #000066; TEXT-DECORATION: none;
}
A.M:active {
	COLOR: #FF6600; TEXT-DECORATION: underline;
}
A.M:hover {
	COLOR: #FF6600; TEXT-DECORATION: underline;
}

A.T:link {
	COLOR: #F4F4F4; TEXT-DECORATION: none;
}
A.T:visited {
	COLOR: #F4F4F4; TEXT-DECORATION: none;
}
A.T:active {
	COLOR: #F4F4F4; TEXT-DECORATION: none;
}
A.T:hover {
	COLOR: #99FF00; TEXT-DECORATION: none;<br>
}
.style10 {font-size: x-small}
.style7 {font-size: small;
	font-weight: bold;
	color: #010F8C;
}
.style11 {font-size: 18px;
	color: #010F8C;
	font-weight: bold;
	font-family: Tahoma, sans-serif;
}
h1,h2,h3,h4,h5,h6 {
	font-family: Tahoma, sans-serif;
	font-weight: bold;
}
h1 {
	font-size: 18px;
	color: #000066;
}
h2 {
	font-size: 18px;
	color: #FF9900;
}
.style12 {
	color: #FF8c00;
	font-size: 18px;
	font-weight: bold;
}
.style13 {
	font-size: 18px;
	color: #000066;
	font-weight: bold;
}
-->
</style>
</head>
	<body>
			<table width="100%" border="0" cellspacing="0" cellpadding="0" ID="Table1" bgcolor="#FFFFFF">
				<tr>				
					<td width="100%" valign="top">						
						
						<font size="3">						
						
						<!--****************************************************************************************************-->
				
						</font>
				
						<table width="100%" border="0" cellpadding="0" cellspacing="5">
                            <tr>
                                <td align="left" valign="top">
                                  <p class="MsoNormal" align="center">
									&nbsp;</p>
									<p class="MsoNormal" align="center"><b>
									<font color="#0000FF"><font size="4">&quot;</font><span lang="TH" style="line-height: 115%"><font size="4">������ 
									�ͧ�š���ǡ �ͧ�ԡĵ�������͡��&quot; 
									����ѡ��Ҿ����ʴ���ǵ� </font></span></font>
									</b></p>
									<p class="MsoNormal" align="center">
									<span lang="TH" style="line-height: 115%; font-weight: 700">
									<font size="4" color="#0000FF">�ͧ �ѷ�Ŵ� 
									ʧ���ʧ ���Ѵ��ý��¡���ҧἹ���ط�� ����ѷ 
									������� �ӡѴ (��Ҫ�)&nbsp; </font></span></p>
									<p class="MsoNormal" align="center">
									<font size="2">&nbsp;<img border="0" src="../Images/sc_20100730171139_img1.gif" width="400" height="300"></font></p>
									<p class="MsoNormal"><font size="2">
									&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </font>
									<font color="#010F8C"><b>
									<span lang="TH" style="line-height: 115%">
									<font size="3">
									�ҡ�ѹ��ͧ�Ӥѭ�ͧ��áԨ�ص��ˡ���ö¹�� 
									������������鹷ҧ��¡����������ҧ����Թ�ͧ��áԨ�ç���蹹���ѹ 
									�����ҡ��� </font></span><font size="3">19</font><span lang="TH" style="line-height: 115%"><font size="3"> 
									�� 
									���¤��������蹻�Сͺ�Ѻ��������繤��ͧ�š���ǡ 
									&quot;��ԡ�ԡĵ������͡��&quot; 
									�觼�����ͼ��������繡��ѧ�Ӥѭ㹡�����ҧ��çҹ���¡���ҧἹ���ط�� 
									����ѷ ������� �ӡѴ (��Ҫ�) 
									���繷�����ѡ�ѹ㹹�� &quot;˭ԧ���������� &quot;</font></span></b></font></p>
									<p class="MsoNormal"><font size="2">&nbsp;</font><span lang="TH" style="line-height: 115%"><font size="3"><img border="0" src="../Images/sc_20100730171139_img2.gif" width="400" height="300" align="left"></font></span></p>
									<p class="MsoNormal"><font size="2">
									&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </font>
									<span lang="TH" style="line-height: 115%">
									<font size="3">�س�ѷ�Ŵ� ʧ���ʧ ���� 
									�س��� 
									���١��Ǥ�ⵢͧ��ͺ����㹺�ôҾ���ͧ������&nbsp;
									</font></span><font size="3">6</font><span lang="TH" style="line-height: 115%"><font size="3"> 
									�� ���չ�ͧ��� </font></span>
									<font size="3">4</font><span lang="TH" style="line-height: 115%"><font size="3"> 
									������չ�ͧ�ت�ش��ͧ���������蹡ѹ�Ѻ�� 
									�س��Ǻ͡�Ѻ������ 
									�Ҩ�����������չ�ͧ������¤� 
									�֧������ͤ�͹��ҧ�����º㹡�÷ӧҹ 
									�����ҹ�Ѻ����� 
									��д��¤�����������١��� 
									�֧����������ѡɳй���·���繤��ͺ�ҧἹ�Ѵ�������ͧ��ҧ�
									</font></span></p>
									<p class="MsoNormal"><font size="3">
									&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </font>
									<font color="#000066">
									<span lang="TH" style="line-height: 115%">
									<font size="3">�س��� 
									����稡���֡�Ҫ���Ѹ���ҡ�ç���¹��Թ���പ�� 
									(�ҧ�л�) �ҡ��鹨֧�ҡ������١����� 
									�������������ë����� 
									��оҳԪ����ʵ����С�úѭ�� �͡�ҢҺѭ�� 
									��ѧ�ҡ����稡���֡���繺ѳ�Ե�������Է����¸�����ʵ�� 
									���������Ҫ�ǧ������觡�÷ӧҹ��������鹡�÷ӧҹ��� 
									����ѷ �ʹ����� ����� ��� ����ѷ </font>
									</span><font size="3">IFCT</font><font size="3">
									</font>
									<span lang="TH" style="line-height: 115%">
									<font size="3">
									��͹������������繿ѹ��ͧ�Ӥѭ������ѷ 
									������µ�� 
									����繺���ѷ����Դ��鹨ҡ����������͡ѹ 
									�����ҧ��µ�Ҥ������ê�� (�����) �Ѻ 
									����������� 㹰ҹйѡ�ѭ����蹺ء�ԡ 
									���������� </font></span><font size="3">3</font><span lang="TH" style="line-height: 115%"><font size="3"> 
									�� </font></span></font></p>
									<p class="MsoNormal"><font size="3">
									&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &quot;</font><span lang="TH" style="line-height: 115%"><font size="3">��͹˹�ҷ������ҷӧҹ������ѷ����ͻٹ� 
									���������ӧҹ��� </font></span>
									<font size="3">Auditor</font><font size="3">&nbsp; 
									firm </font>
									<span lang="TH" style="line-height: 115%">
									<font size="3">��� </font></span>
									<font size="3">IFCT</font><font size="3">
									</font>
									<span lang="TH" style="line-height: 115%">
									<font size="3">��觷ӧҹ�ҧ����Թ�����
									</font></span><font size="3">2-3</font><span lang="TH" style="line-height: 115%"><font size="3"> 
									�ըҡ��鹨֧价ӧҹ����ͻٹ </font></span>
									<font size="3">3</font><span lang="TH" style="line-height: 115%"><font size="3">�� 
									��� �繡������ŧ�ع�����ҧ��µ�Ҥ������ê�蹢ͧ����蹡Ѻ����������� 
									��觾����繹ѡ�ѭ�դ��á� 
									��������ѹ���ҧ�ҹ�Ѻ���Ѵ��âͧ����� 
									����������ӵ���訴����¹�Ѵ��駺���ѷ 
									ŧ�ѭ�� �Դ�ѭ���ͧ 
									��������з���繼��仢͡���Թ�ҡ��Ҥ�� 
									��觡���繵����������� 
									����з�觡�����ҧ�ç�ҹ���� 
									�������ö����ç�ҹ�� 
									����繻�ж����ͧ��á�����ҧ�ԪҪվ�ҧ��ҹ����Թ 
									��觾���ͧ����繻��ʺ��ó�����ҡ&quot;</font></span></p>
									<p class="MsoNormal"><font size="3">&nbsp;</font></p>
									<p class="MsoNormal"><font size="3">
									&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </font>
									<font color="#000066">
									<span lang="TH" style="line-height: 115%">
									<font size="3">㹻� ��. </font></span>
									<font size="3">2534</font><span lang="TH" style="line-height: 115%"><font size="3"> 
									������������ҹ�Ѻ����ѷ����ѹ������ç���蹹���ѹ�˭����ش㹻������ 
									���� ����ѷ ������� �ӡѴ (��Ҫ�) ����
									</font></span><font size="3">TOP </font>
									<span lang="TH" style="line-height: 115%">
									<font size="3">
									��觢�й�鹺���ѷ�ѧ������������Ҵ��ѡ��Ѿ��� 
									㹰ҹйѡ��������ҧ����Թ </font></span>
									</font></p>
									<p class="MsoNormal"><font size="3">
									&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </font>
									<span lang="TH" style="line-height: 115%">
									<font size="3">��ǧ�������� </font></span>
									<font size="3">5</font><span lang="TH" style="line-height: 115%"><font size="3"> 
									�� 
									��觡��������鹷ӧҹ������ѷ�ѡ���˭�ͧ��áԨ����ѹ 
									�繡�÷ӧҹ���ǹ�ͧ��ü�ѡ�ѹ����ѷ�������Ҵ��ѡ��Ѿ��� 
									����繡�â��§ҹ�����áԨ��������ͧ 
									���¤�������ͼ�����繤���蹪ͺ����ѡ㹡�âǹ�����Ҥ������ 
									�֧������͵Ѵ�Թ��֡�ҵ�ͷҧ��ҹ </font>
									</span><font size="3">MBA </font>
									<span lang="TH" style="line-height: 115%">
									<font size="3">������������ 
									������稡���֡������Һѳ�Ե�������Է����¸�����ʵ�� 
									㹻� �.�. </font></span><font size="3">2537&nbsp;
									</font></p>
									<p class="MsoNormal"><font size="3">&nbsp;<img border="0" src="../Images/sc_20100730171139_img3.gif" width="400" height="300" align="right"></font></p>
									<p class="MsoNormal"><font size="3">
									&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </font>
									<font color="#000066">
									<span lang="TH" style="line-height: 115%">
									<font size="3">��ǧ�������� </font></span>
									<font size="3">5</font><span lang="TH" style="line-height: 115%"><font size="3"> 
									�Ѵ�� �繡�÷ӧҹ���ǹ�ͧ��� </font>
									</span><font size="3">Bidding &nbsp;IPP </font>
									<span lang="TH" style="line-height: 115%">
									<font size="3">
									����з�觺���ѷ��ͧ༪ԭ�Ѻʶҹ��ó�ҧ���ɰ�Ԩ 
									���ͷ�����¡�ѹ��� �ԡĵ����ӡ�� 㹻�
									</font></span><font size="3">2540</font><span lang="TH" style="line-height: 115%"><font size="3"> 
									�觼���� </font></span><font size="3">Demand
									</font>
									<span lang="TH" style="line-height: 115%">
									<font size="3">��������ѹŴŧ 
									�ѵ�ҡ��ⵢͧ����ѷ��Ŵŧ仴��� ������ǡѹ 
									�ç���蹹���ѹ���ըӹǹ�ҡ��� 
									������ա��ѧ��ü�Ե�ҡ���Ҥ�����ͧ��� 
									��觹͡�ҡ������Ŵŧ���Ǣ�����ǡѹ�ѵ�ҡ�ê���˹���������鹵���ѵ�ҡ���š����¹�Թ�ͧầ��ҵ� 
									�觼���������Ѻ�ͺ����˹�ҷ��ҧ��ҹ����èһ�Ѻ�ç���ҧ˹�������е�ҧ�����
									</font></span></font></p>
									<p class="MsoNormal">
									<font size="3" color="#000066">&nbsp;</font></p>
									<p class="MsoNormal"><font size="3">
									&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </font>
									<span lang="TH" style="line-height: 115%">
									<font size="3">
									&nbsp;�ҡʶҹ��ó����ͧ����繨ش�Ӥѭ�������������âͧ����ѷ����ѡ��Ҿ㹵�ǵ��ͧ�� 
									������դ�����������ͧ��ҧ� �ͧ��áԨ 
									��Сͺ�Ѻ�դ��������㹪�ǧ������ѷ�������Ҵ� 
									�����ҧ�� 
									�������������դ�������ҧ�㹤�������������ö�ͧ�� 
									�֧���ͺ���§ҹ��ҹ </font></span>
									<font size="3">Investor Relations (IR)
									</font>
									<span lang="TH" style="line-height: 115%">
									<font size="3">���������á���ŧҹ��ҹ
									</font></span><font size="3">IR </font>
									<span lang="TH" style="line-height: 115%">
									<font size="3">�����Ѻ��ô��� </font></span>
									<font size="3">portfolio management </font>
									<span lang="TH" style="line-height: 115%">
									<font size="3">�Ǻ���Ѻ����ҧἹ���ط�� 
									��й��ʹ͢����Ţ�����âͧ����ѷ���ѡŧ�ع��Һ 
									����з���ѹ��� �س�ѷ�Ŵ� ʧ���ʧ 
									��ç���˹� ���Ѵ��ý��¡���ҧἹ���ط�� 
									�ͧ����ѷ ������� �ӡѴ ��Ҫ�</font></span></p>
									<p class="MsoNormal"><font size="3">&nbsp;</font></p>
									<p class="MsoNormal"><font size="3">
									&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </font>
									<font color="#000066">
									<span lang="TH" style="line-height: 115%">
									<font size="3">����ͧ�����Ӥѭ�ͧ�ҹ </font>
									</span><font size="3">IR </font>
									<span lang="TH" style="line-height: 115%">
									<font size="3">����յ�ͻ������㹻Ѩ�غѹ 
									�͡������� 
									�Ѩ�غѹ���������������դ�������㹧ҹ
									</font></span><font size="3">IR </font>
									<span lang="TH" style="line-height: 115%">
									<font size="3">�ҡ��觢�� 
									��੾�м��������дѺ�٧ 
									�ա����ѧ�ա���š����¹�����������к���ѷ�����ѹ�ҡ��鹼�ҹ�ҧ��Ҥ�
									</font></span><font size="3">Thai Investor 
									Relations Club (TIRC) </font>
									<span lang="TH" style="line-height: 115%">
									<font size="3">������ٹ���ҧ 
									�������Ҫԡ������ҡ��鹵���ӴѺ 
									�ҡ�������ҳ </font></span><font size="3">
									45%</font><span lang="TH" style="line-height: 115%"><font size="3"> 
									����з�觻Ѩ�غѹ�Ҵ��һ���ҳ </font></span>
									<font size="3">75-80%</font><span lang="TH" style="line-height: 115%"><font size="3"> 
									�ͧ����ѷ������¹������ 
									�����ѧ����ǵ����ա��� 
									�͹Ҥ��ҡ��ͧ��þѲ�ҧҹ��ҹ </font></span>
									<font size="3">IR </font>
									<span lang="TH" style="line-height: 115%">
									<font size="3">��������Է���Ҿ��� 
									��èк�è���ѡ�ٵ�ŧ�㹡���֡�Ҵ��� 
									�֧������ö�Ѳ�ҧҹ </font></span>
									<font size="3">IR </font>
									<span lang="TH" style="line-height: 115%">
									<font size="3">�����ҧ�������Է���Ҿ</font></span></font></p>
									<p class="MsoNormal"><font size="3">&nbsp;</font></p>
									<p class="MsoNormal"><font size="3">
									&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </font>
									<span lang="TH" style="line-height: 115%">
									<font size="3">
									����Ӥѭ������Ѻ�ҡ��û�Ժѵ�˹�ҷ��ѡŧ�ع����ѹ�� 
									���͡�÷�����龺�������觵�ҧ� 
									㹷�ȹз����ҧ�Ţ�鹡������ �դ�����
									</font></span><font size="3">Globalization
									</font>
									<span lang="TH" style="line-height: 115%">
									<font size="3">
									��觷���������͡���龺�СѺ�ѡŧ�ع��йѡ���������ҡ˹�����µ� 
									����֧���͡��㹡�÷ӧҹ���բ���͹ҵ</font></span></p>
									<p class="MsoNormal"><font size="3">&nbsp;</font></p>
									<p class="MsoNormal"><font size="3">
									&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<font color="#000066"> </font>
									</font><font color="#000066">
									<span lang="TH" style="line-height: 115%">
									<font size="3">������¢ͧ��ô��Թ�ҹ��ҹ
									</font></span><font size="3">IR</font><font size="3">
									</font>
									<span lang="TH" style="line-height: 115%">
									<font size="3">�ͧ����ѷ㹪�ǧ�չ�� 
									�ͺ͡������ͧ�ҡ㹻� </font></span>
									<font size="3">2008</font><span lang="TH" style="line-height: 115%"><font size="3"> 
									����ѷ ������� �ӡѴ (��Ҫ�</font></span><font size="3">)</font><span lang="TH" style="line-height: 115%"><font size="3"> 
									���Ѻ�Ѵ���͡����� </font></span>
									<font size="3">Final</font><font size="3"> 
									List IR </font>
									<span lang="TH" style="line-height: 115%">
									<font size="3">��Ҵ�˭� �ѹ�Ѻ��� </font>
									</span><font size="3">3</font><span lang="TH" style="line-height: 115%"><font size="3"> 
									�ѧ��� 㹻չ������鹻��繵��� 
									�֧ͨ�ҧ������¡�þѲ�ҧҹ��ҹ </font>
									</span><font size="3">IR</font><font size="3">
									</font>
									<span lang="TH" style="line-height: 115%">
									<font size="3">�����Ǣ����������ѹ�Ѻ
									</font></span><font size="3">1</font><span lang="TH" style="line-height: 115%"><font size="3"> 
									���� </font></span><font size="3">2</font><span lang="TH" style="line-height: 115%"><font size="3"> 
									�բ�ҧ˹�� </font></span></font></p>
									<p class="MsoNormal"><font size="3">&nbsp;<img border="0" src="../Images/sc_20100730171139_img4.gif" width="400" height="300" align="left"></font></p>
									<p class="MsoNormal"><font size="3">
									&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </font>
									<span lang="TH" style="line-height: 115%">
									<font size="3">��ǹἹ��ô��Թ�ҹ㹪�ǧ
									</font></span><font size="3">6</font><span lang="TH" style="line-height: 115%"><font size="3"> 
									��͹�ҡ��� �س��� �͡��� </font></span>
									<font size="3">6</font><span lang="TH" style="line-height: 115%"><font size="3"> 
									��͹��ѧ ���ҧἹ��ô��Թ�ҹ��� </font>
									</span><font size="3">cycle</font><font size="3">
									</font>
									<span lang="TH" style="line-height: 115%">
									<font size="3">�ͧ��áԨ 
									��觤Ҵ��ó���ҼŻ�Сͺ���㹪�ǧ �����
									</font></span><font size="3">4/2553</font><span lang="TH" style="line-height: 115%"><font size="3"> 
									�����㹷�ȷҧ���բ�� 
									�¨��ա���ҧἹ��е�鹡��ŧ�ع������ǧ��͹�ѹ��¹ 
									��觨��繡���ҧἹ������ҧ����ᵡ��ҧ 
									�������ҧ�������������ѡŧ�ع㹵�Ҵ�
									</font></span></p>
									<p class="MsoNormal"><font size="3">&nbsp;</font></p>
									<p class="MsoNormal"><font size="3">
									&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </font>
									<span lang="TH" style="line-height: 115%">
									<font size="3" color="#000066">
									�Դ���¡ѹ����෤�Ԥ����Ѻ��͡Ѻ���Ǥ��Ǣͧ����ѷ㹴�ҹ��ҧ�&nbsp; 
									�͡���Ǵ�����˹�ҷ���������ѧ����� 
									��͹��蹺ؤ��ҡ�㹺���ѷ��ͧ�����ѹ��֡������������Ҷ֧����稨�ԧ���ç�ѹ��͹ 
									�����ѧ�ҡ��鹽��¹ѡŧ�ع����ѹ������繼���Ъ�����ѹ��֧������÷���鵡ŧ�ѹ����ͧ������ѡŧ�ع��Һ 
									���繡�û�Ъ�����ѹ������ż�ҹ�á� ��ҧ� 
									���ͼ�ҹ��ѧ������١��Ңͧ����ѷ </font>
									</span></p>
									<p class="MsoNormal"><font size="2">&nbsp;&nbsp;&nbsp;</font><font size="3">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
									</font>
									<span lang="TH" style="line-height: 115%">
									<font size="3">�� �س��� 
									���Ըա�����ҧ����������������ѡŧ�ع�¡�ú����üŧҹ�����仵���������� 
									��ͧ�� </font></span><font size="3">cash</font><font size="3"> 
									flow </font>
									<span lang="TH" style="line-height: 115%">
									<font size="3">�����������С����� 
									��觷���ҹ�Һ���ѷ ������� �ӡѴ 
									(��Ҫ�)�ѧ����·����ѡŧ�ع�Դ��ѧ</font></span></p>
									<p class="MsoNormal"><font size="2">&nbsp;</font></p>
									<p class="MsoNormal"><font size="3">&nbsp;</font></p>
									<p class="MsoNormal"><font size="3">By :
									</font>
									<span lang="TH" style="line-height: 115%">
									<font size="3">������ó� ���Ф�����ó</font></span></p>
									<p class="MsoNormal"><font size="3">IR Plus</font></p>
                                  <p ALIGN="left" STYLE="margin-bottom: 0cm">
											&nbsp;</p>
                                            
                                            
                                  </td>
                              </tr>
                   </table>
                </td>
               </tr>							
			</table>		
	</body>
</html>
