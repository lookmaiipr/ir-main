<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">

<%

PageType="Other"
Session("redirect_page")=""
Session("PageName")="File Not Found!!"
Session("PageASP")="FileNotFound.asp"
%>
 <!--#include file = "Scripts/Scripts.asp" -->
 <!-- #include file="../i_constant.asp" -->
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=windows-874">
<meta name="keywords" content="<%=message_keyword%>">
<meta name="description" content="<%=message_description%>">
<title>IR Plus</title>
<LINK href="Themes/Themes.css" type="text/css" rel="stylesheet">
<script language="JavaScript" type="text/JavaScript" src="Scripts/Scripts.js" ></script>
<script type="text/javascript" language="JavaScript1.2" src="../function2007.js"></script>
<script type="text/javascript" language="JavaScript1.2" src="stmenu.js"></script>
<style>
._button {
	display: inline-block;
	padding: 8px 25px;
	border: 2px solid #000;
	color: #FFF;
	background-color: transparent;
	border-radius: 4px;
	font-size: 18px;
}
._button:hover{
	color:#404347;
	background-color:#FFF;
	text-decoration:none
}
</style>
</head>
<body  >
<table width="100%"  border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td align="center" valign="top">
        <table width="1000" border="0" cellpadding="0" cellspacing="0">
                <tr>
                    <td align="left" valign="top" bgcolor="#FFFFFF">
                            <table width="100%"  border="0" cellspacing="0" cellpadding="0">
                                       <tr align="left" valign="top">
                                            <td colspan="2">
                                                <table width="100%"  border="0" cellspacing="0" cellpadding="0" style="min-height:500px;padding-top:100px;">
                                                    <tr align="left" valign="top">
                                                        <td>                                                                
                                                                <table width="100%"  border="0" cellspacing="0" cellpadding="0">
                                                                        <tr height="20px">
                                                                            <td  width="20px"> </td>
                                                                             <td> </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td  width="20px"> </td>
                                                                            <td align="left" valign="top"> 
                                                                          </td>
                                                                    </tr>
                                                                    <tr height="20px">
                                                                            <td  width="20px"> </td>
                                                                             <td> </td>
                                                                      </tr>
                                                                       <tr>
                                                                            <td  width="20px"> </td>
                                                                            <td align="center" valign="top">                   
										<span class="style12"><font style="color:red !important;font-size:120px;">404</font></span>      <br><br>
										<span class="style12"><font color="black" !important">Oops! We can't seem to find the page you're looking for. </font></span>    
										<br><br>
                                                                            </td>
                                                                      </tr>
                                                                      <tr height="15px">
                                                                            <td  width="20px"> </td>
                                                                             <td> </td>
                                                                      </tr>                                                                     
                                                                             
                                                                    <tr >
                                                                            <td  width="20px"> &nbsp;</td>
                                                                             <td> &nbsp;</td>
                                                                     </tr>
                                                                      <tr >
                                                                            <td  width="20px"> &nbsp;</td>
                                                                             <td>&nbsp; </td>
                                                                     </tr>
                                                                     <tr >
                                                                            <td  width="20px"> &nbsp;</td>
                                                                             <td>&nbsp; </td>
                                                                     </tr>
                                                            </table></td>
                                                    </tr>
                                           </table></td>
                                </tr>
                    </table></td>
            </tr> 
    </table></td>
  </tr>
</table>
<map name="Map">
  <area shape="circle" coords="282,27,14" href="#">
</map>
<!-- .................................................. Google Analytics .................................................. -->
                                 <!--#include file="../i_googleAnalytics.asp"-->	
<!-- ........................................... End Google Analytics .................................................. -->
</body>
</html>
