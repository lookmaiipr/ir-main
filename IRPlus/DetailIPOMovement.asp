<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<%
PageType="Other"
Session("redirect_page")=""
Session("PageName")="Detail IPO Movement"
Session("PageASP")="DetailIPOMovement.asp"
%>

 <!--#include file = "Scripts/Scripts.asp" -->
 <!-- #include file="../i_constant.asp" -->
 <!--#include file = "../i_conir.asp" -->
 
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=windows-874">
<meta name="keywords" content="<%=message_keyword%>">
<meta name="description" content="<%=message_description%>">
<title>IR Plus</title>
<LINK href="Themes/Themes.css" type="text/css" rel="stylesheet">
<script language="JavaScript" type="text/JavaScript" src="Scripts/jquery.js" ></script>
<script language="JavaScript" type="text/JavaScript" src="Scripts/Scripts.js" ></script>
<script type="text/javascript" language="JavaScript1.2" src="stmenu.js"></script>
</head>
<body style="" >
<table width="100%"  border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td align="center" valign="top">
        <table width="1000" border="0" cellpadding="0" cellspacing="0">
                <tr>
                    <td align="left" valign="top" bgcolor="#FFFFFF">
                            <table width="100%"  border="0" cellspacing="0" cellpadding="0">
                                      <tr align="left" valign="top">
                                                <td width="155" colspan="2"> 
                                                <!--#include file = "TopSection.asp" -->
                                                </td>
                                      </tr>
                                       <tr align="left" valign="top">
                                            <td colspan="2">
                                                <table width="100%"  border="0" cellspacing="0" cellpadding="0">
                                                    <tr align="left" valign="top">
                                                            <td width="155" background="images/bg_menu.jpg">
                                                                 <table width="100%"  border="0" cellspacing="0" cellpadding="0">                                                           
                                                                       <tr align="left" valign="top">
                                                                            <td background="images/menu_left.jpg">                                                                    
                                                                                <!--#include file = "LeftSection.asp" --></td>
                                                                       </tr>  
                                                                </table></td>
                                                        <td>
                                                                <table width="100%"  border="0" cellspacing="0" cellpadding="0">
                                                                   <% if request("file_name") <> "" then
                                                                                                          file_name = request("file_name") 
                                                                                               else
                                                                                                     strsql=""
                                                                                                     strsql = "SELECT movement_id,title,file_name FROM  tbl_trans_ipo_movement "
                                                                                                     strsql=strsql & "   where activated ='Y'  order by event_timestamp DESC  limit 1 "
                          
                                                                                                     set rs = conir.execute(strsql)
                                                                                                     if not  rs.eof and not  rs.bof then    
                                                                                                           file_name = rs("file_name")
                                                                                                    end if
                                                                                                    rs.close
                                                                                               end if
                                                                                           %>
                                                                  <tr height="20px">
                                                                                <td  width="20px"> </td>
                                                                                 <td> </td>
                                                                            </tr>
                                                                        <tr>
                                                                         <td  width="20px"> </td>
                                                                            <td align="left" valign="top"> 
                                                                                    <table width="100%"  border="0" cellspacing="0" cellpadding="0">
                                                                                          <tr align="right" valign="top">
                                                                                                <td align="left" valign="bottom">
                                                                                                     <%                                                                                                                                                
                                                                                                         sub_title1=mid(ArrMenu(3,2),1,instr(ArrMenu(3,2),"Movement")-1)
                                                                                                         sub_title2=mid(ArrMenu(3,2),instr(ArrMenu(3,2),"Movement"),len(ArrMenu(3,2)))
                                                                                                     %>
                                                                                                     <span class="style13"><%=Ucase(sub_title1)%></span> 
                                                                                                     <span class="style12"><%=Ucase(sub_title2)%></span>
                                                                                                </td>
                                                                                                <%if trim(file_name)<>"" then%>
                                                                                                <td width="88" valign="bottom"><a href="AllIPOMovement.asp" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('Image151','','images/alldetail_b.gif',1)"><img src="images/alldetail_a.gif" name="Image151" width="88" height="17" border="0"></a></td>
                                                                                                <%end if%>
                                                                                          </tr>
                                                                                          <tr align="right" valign="top" bgcolor="#01118D">
                                                                                                <td colspan="3" align="left"><img src="images/space.gif" width="1" height="1"></td>
                                                                                          </tr>
                                                                                    </table>
                                                                          </td>
                                                                    </tr>
                                                                    
                                                                     <tr height="15px">
                                                                                <td  width="20px"> </td>
                                                                                 <td> </td>
                                                                            </tr>
                                                                        
                                                                    <tr>
                                                                     <td  width="20px"> </td>
                                                                            <td align="left" valign="top">   
                                                                                <%if file_name="" then%>
                                                                                     <Div align="Center">
											<!--#include file = "IC_Noinformation.asp" -->     
                                                                                      </Div>
                                                                               <%else%>
																												<IFRAME id="Iframedetail"  allowtransparency="true" src="IPOMovement/File/<%=file_name%>.asp" frameBorder="0"  width="100%" height="800px" scrolling ="no"></IFRAME>
                                                                                <%end if%>
                                                                            </td>
                                                                    </tr>
                                                                        <tr height="15px">
                                                                                <td  width="20px"> </td>
                                                                                 <td> </td>
                                                                            </tr>
                                                            </table></td>
                                                    </tr>
                                            </table></td>
                                </tr>
                    </table></td>
            </tr> 
             <!--#include file = "BottomSection.asp" -->
    </table></td>
  </tr>
</table>
<script type="text/javascript"> 
		$(function(){			
				sizeFrame(); 
				$("#Iframedetail").load(sizeFrame); 
		});
</script>
<map name="Map">
  <area shape="circle" coords="282,27,14" href="#">
</map>
<!-- .................................................. Google Analytics .................................................. -->
                                 <!--#include file="../i_googleAnalytics.asp"-->	
<!-- ........................................... End Google Analytics .................................................. -->
</body>
</html>
