<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">

<%

PageType="Other"
Session("redirect_page")=""
Session("PageName")="Site Map"
Session("PageASP")="SiteMap.asp"
%>
 <!--#include file = "Scripts/Scripts.asp" -->
 <!-- #include file="../i_constant.asp" -->
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=windows-874">
<meta name="keywords" content="<%=message_keyword%>">
<meta name="description" content="<%=message_description%>">
<title>IR Plus</title>
<link rel="shortcut icon" href="Images/favicon.ico">
<LINK href="Themes/Themes.css" type="text/css" rel="stylesheet">
<script language="JavaScript" type="text/JavaScript" src="Scripts/Scripts.js" ></script>
<script type="text/javascript" language="JavaScript1.2" src="../function2007.js"></script>
<script type="text/javascript" language="JavaScript1.2" src="stmenu.js"></script>
</head>
<body  >
<table width="100%"  border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td align="center" valign="top">
        <table width="1000" border="0" cellpadding="0" cellspacing="0">
                <tr>
                    <td align="left" valign="top" bgcolor="#FFFFFF">
                            <table width="100%"  border="0" cellspacing="0" cellpadding="0">
                                      <tr align="left" valign="top">
                                                <td width="155" colspan="2"> 
                                                <!--#include file = "TopSection.asp" -->
                                                </td>
                                      </tr>
                                       <tr align="left" valign="top">
                                            <td colspan="2">
                                                <table width="100%"  border="0" cellspacing="0" cellpadding="0">
                                                    <tr align="left" valign="top">
                                                            <td width="155" background="images/bg_menu.jpg">
                                                                 <table width="100%"  border="0" cellspacing="0" cellpadding="0">                                                           
                                                                       <tr align="left" valign="top">
                                                                            <td background="images/menu_left.jpg">                                                                    
                                                                                <!--#include file = "LeftSection.asp" --></td>
                                                                       </tr>  
                                                                </table></td>
                                                        <td>                                                                
                                                                <table width="100%"  border="0" cellspacing="0" cellpadding="0">
                                                                        <tr height="20px">
                                                                            <td  width="20px"> </td>
                                                                             <td> </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td  width="20px"> </td>
                                                                            <td align="left" valign="top"> 
                                                                                    <table width="100%"  border="0" cellspacing="0" cellpadding="0">
                                                                                          <tr align="right" valign="top">
                                                                                                <td align="left" valign="bottom">
                                                                                                        <%                                                                                                                                                
                                                                                                                                            sub_title1=Mid(ArrMenu(15,0), 1, InStr(1, ArrMenu(15,0), "Map", vbTextCompare) - 1)
                                                                                                                                            sub_title2=Mid(ArrMenu(15,0), InStr(1, ArrMenu(15,0), "Map", vbTextCompare), Len(ArrMenu(15,0)))
                                                                                                                                        %>
                                                                                                        <font  class="style13"><%=Ucase(sub_title1)%></font><font class="style12"><%=Ucase(sub_title2)%></font></td>
                                                                                            </tr>
                                                                                          <tr align="right" valign="top" bgcolor="#01118D">
                                                                                                <td align="left"><img src="images/space.gif" width="1" height="1"></td>
                                                                                          </tr>
                                                                                    </table>
                                                                          </td>
                                                                    </tr> 
                                                                <tr height="20px">
                                                                            <td  width="20px"> </td>
                                                                             <td> </td>
                                                                      </tr>                                                                       
                                                                       <tr>
                                                                            <td  width="20px"> </td>
                                                                            <td align="center" valign="top"><br>
                                                                               <table width="85%"  border="0" cellspacing="3" cellpadding="0">
                                                                            <tr align="left" valign="top">
                                                                              <td width="28"><img src="images/icon_research.gif" width="25" height="21"></td>
                                                                              <td><a  class="detail" href="FindMember.asp">IR PLUS MEMBERS </a></td>
                                                                              <td width="28"><img src="images/icon_research.gif" width="25" height="21"></td>
                                                                              <td><a class="detail" href="FindCorporateInfo.asp">LISTED COMPANIES</a></td>
                                                                              <td width="28"><img src="images/icon_research.gif" width="25" height="21"></td>
                                                                              <td>IPO</td>
                                                                            </tr>
                                                                            <tr align="left" valign="top">
                                                                              <td>&nbsp;</td>
                                                                              <td>&nbsp;</td>
                                                                              <td>&nbsp;</td>
                                                                              <td>&nbsp;</td>
                                                                              <td>&nbsp;</td>
                                                                              <td><img src="images/page.gif" width="18" height="18" align="absmiddle"><a  class="detail" href="DetailIPOShowOff.asp">INSIGHT IPO</a> </td>
                                                                            </tr>
                                                                            <tr align="left" valign="top">
                                                                              <td><img src="images/icon_research.gif" width="25" height="21"></td>
                                                                              <td>NEWS ROOM </td>
                                                                              <td><img src="images/icon_research.gif" width="25" height="21"></td>
                                                                              <td><a class="detail"  href="Research.asp">RESEARCH</a></td>
                                                                              <td>&nbsp;</td>
                                                                              <td><img src="images/page.gif" width="18" height="18" align="absmiddle"><a  class="detail" href="DetailIPOMovement.asp">IPO MOVEMENT </a></td>
                                                                            </tr>
                                                                            <tr align="left" valign="top">
                                                                              <td>&nbsp;</td>
                                                                              <td><img src="images/page.gif" width="18" height="18" align="absmiddle"><a  class="detail" href="AllCalendar.asp">IR CALENDAR</a></td>
                                                                              <td>&nbsp;</td>
                                                                              <td>&nbsp;</td>
                                                                              <td>&nbsp;</td>
                                                                              <td>&nbsp;</td>
                                                                            </tr>
                                                                            <tr align="left" valign="top">
                                                                              <td>&nbsp;</td>
                                                                              <td><img src="images/page.gif" width="18" height="18" align="absmiddle"><a class="detail" href="AllSetAnnouncement.asp">SET ANNOUNCEMENT</a> </td>
                                                                              <td><img src="images/icon_research.gif" width="25" height="21"></td>
                                                                              <td><a  class="detail"  href="RelateIRInfo.asp">RELATE IR INFO</a></td>
                                                                              <td><img src="images/icon_research.gif" width="25" height="21"></td>
                                                                              <td><a  class="detail" href="OurService.asp">OUR SERVICE</a></td>
                                                                            </tr>
                                                                            <tr align="left" valign="top">
                                                                              <td>&nbsp;</td>
                                                                              <td><img src="images/page.gif" width="18" height="18" align="absmiddle"><a  class="detail" href="AllPublicRelations.asp">PUBLIC RELATIONS</a></td>
                                                                              <td>&nbsp;</td>
                                                                              <td>&nbsp;</td>
                                                                              <td>&nbsp;</td>
                                                                              <td>&nbsp;</td>
                                                                            </tr>
                                                                            <tr align="left" valign="top">
                                                                              <td>&nbsp;</td>
                                                                              <td><img src="images/page.gif" width="18" height="18" align="absmiddle"><a class="detail" href="AllPressRelease.asp">PRESS RELEASE</a></td>
                                                                              <td><img src="images/icon_research.gif" width="25" height="21"></td>
                                                                              <td><a  class="detail" href="Reference.asp">OUR REFERENCE</a></td>
                                                                              <td><img src="images/icon_research.gif" width="25" height="21"></td>
                                                                              <td><a  class="detail"  href="InvestorFAQs.asp">INVESTOR FAQs</a></td>
                                                                            </tr>
                                                                            <tr align="left" valign="top">
                                                                              <td>&nbsp;</td>
                                                                              <td>&nbsp;</td>
                                                                              <td>&nbsp;</td>
                                                                              <td>&nbsp;</td>
                                                                              <td>&nbsp;</td>
                                                                              <td>&nbsp;</td>
                                                                            </tr>
                                                                            <tr align="left" valign="top">
                                                                              <td><img src="images/icon_research.gif" width="25" height="21"></td>
                                                                              <td>INFO REQUEST </td>
                                                                               <td><img src="images/icon_research.gif" width="25" height="21"></td>
                                                                              <td><a class="detail"   href="Disclaimer.asp">DISCLAIMER</a></td>
                                                                               <td><img src="images/icon_research.gif" width="25" height="21"></td>
                                                                              <td><a class="detail"   href="IRWebSiteManual.asp">IR WEBSITE MANUAL</a></td>                                                                            
                                                                            </tr>
                                                                            <tr align="left" valign="top">
                                                                              <td>&nbsp;</td>
                                                                              <td><img src="images/page.gif" width="18" height="18" align="absmiddle"><a  class="detail"  href="EmailAlert.asp">E-MAIL ALERT</a> </td>
                                                                              <td>&nbsp;</td>
                                                                              <td>&nbsp;</td>
                                                                              <td>&nbsp;</td>
                                                                              <td>&nbsp;</td>
                                                                            </tr>
                                                                            <tr align="left" valign="top">
                                                                              <td>&nbsp;</td>
                                                                              <td><img src="images/page.gif" width="18" height="18" align="absmiddle"><a   class="detail"  href="InquiryForm.asp">INQUIRY FORM</a> </td>
                                                                                <td><img src="images/icon_research.gif" width="25" height="21"></td>
                                                                              <td><a class="detail"   href="SiteMap.asp">SITE MAP</a></td>
                                                                              <td><img src="images/icon_research.gif" width="25" height="21"></td>
                                                                              <td><a  class="detail"   href="ContactUs.asp">CONTACT US</a></td>                                                                          
                                                                            </tr>
                                                                            <tr align="left" valign="top">
                                                                              <td>&nbsp;</td>
                                                                              <td>&nbsp;</td>
                                                                              <td>&nbsp;</td>
                                                                              <td>&nbsp;</td>
                                                                              <td>&nbsp;</td>
                                                                              <td>&nbsp;</td>
                                                                            </tr>																			
                                                                            <tr align="left" valign="top">
                                                                              <td><img src="images/icon_research.gif" width="25" height="21"></td>
                                                                              <td><a class="detail"   href="OurPartners.asp">OUR PARTNERS</a></td>
                                                                              <td><img src="images/icon_research.gif" width="25" height="21"></td>
                                                                              <td><a class="detail"   href="PlusYourCompany.asp">PLUS YOUR COMPANY</a></td>
                                                                              <td><img src="images/icon_research.gif" width="25" height="21"></td>
                                                                              <td><a  class="detail"   href="IntroduceCompany.asp">INTRODUCE A COMPANY</a></td>                                                      
                                                                            </tr>																			
                                                                            <tr align="left" valign="top">
                                                                              <td>&nbsp;</td>
                                                                              <td>&nbsp;</td>
                                                                              <td>&nbsp;</td>
                                                                              <td>&nbsp;</td>
                                                                              <td>&nbsp;</td>
                                                                              <td>&nbsp;</td>
                                                                            </tr>
                                                                          </table>
                                                                          <p>&nbsp;</p> </td>                                                                            
                                                                      </tr>

																	  
                                                                      <tr height="15px">
                                                                            <td  width="20px"> </td>
                                                                             <td> </td>
                                                                      </tr>                                                                     
                                                                             
                                                                    <tr >
                                                                            <td  width="20px"> &nbsp;</td>
                                                                             <td> &nbsp;</td>
                                                                     </tr>
                                                                    
                                                            </table></td>
                                                    </tr>
                                           </table></td>
                                </tr>
                    </table></td>
            </tr> 
             <!--#include file = "BottomSection.asp" -->
    </table></td>
  </tr>
</table>
<map name="Map">
  <area shape="circle" coords="282,27,14" href="#">
</map>
<!-- .................................................. Google Analytics .................................................. -->
                                 <!--#include file="../i_googleAnalytics.asp"-->	
<!-- ........................................... End Google Analytics .................................................. -->
</body>
</html>
