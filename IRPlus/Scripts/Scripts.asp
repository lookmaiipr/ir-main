<%
DIM ArrMenu(30,4)
ArrMenu(0,0)="Home"
ArrMenu(1,0)="IR Plus Members"
ArrMenu(2,0)="Corporate Info"
ArrMenu(3,0)="IPO"
ArrMenu(3,1)="Insight IPO"
ArrMenu(3,2)="IPO Movement"
ArrMenu(3,3)="IPO Show off"
ArrMenu(3,4)="IPO Research"
ArrMenu(4,0)="News Room"
ArrMenu(4,1)="IR Calendar"
ArrMenu(4,2)="SET Announcement"  
ArrMenu(4,3)="Public Relations" 
ArrMenu(4,4)="Press Release" 
ArrMenu(5,0)="Blog"
ArrMenu(6,0)="Webboard"
ArrMenu(7,0)="Research"
ArrMenu(8,0)="Info Request"
ArrMenu(8,1)="E-mail Alerts"
ArrMenu(8,2)="Inquiry Form"
ArrMenu(9,0)="Relate IR Info"
ArrMenu(10,0)="Our Service"
ArrMenu(11,0)="Our Reference"
ArrMenu(12,0)="Investor FAQs"
ArrMenu(13,0)="Disclaimer"
ArrMenu(14,0)="IR Website Manual"
ArrMenu(15,0)="Site Map"
ArrMenu(16,0)="Contact Us"
ArrMenu(17,0)="Management 's Lifestyle"
ArrMenu(18,0)="CG on Stage"
ArrMenu(19,0)="Live Audio"
ArrMenu(20,0)="News Update"
ArrMenu(21,0)="Webcasts & Presentation"
ArrMenu(22,0)="Register"
ArrMenu(23,0)="IR Society"
ArrMenu(24,0)="IR Knowledge"
ArrMenu(25,0)="Listed Companies"
ArrMenu(26,0)="Our Partners"
ArrMenu(27,0)="Plus Your <br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Company"
ArrMenu(28,0)="Introduce <br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;A Company"
ArrMenu(29,0)="Plus Your Company"
ArrMenu(30,0)="Introduce A Company"

'''''''''''''''''''''' ************************** Create By Bowl on 09/08/10 *******************************
if session("lang")="" or request("lang")="T"  then  'Language
	session("lang")="T"
elseif request("lang")="E" then
	session("lang")="E"
end if

'''''''''''''''''''''' ************************** Create By Kung on 11/09/09 *******************************

Function IsListedMember(listed)
	if listed<>"" then
		if isnumeric(listed) = true then
		'+++++++++ Check Listed ID ++++++++
			if Instr(session("ListedMembers_id") ,listed) > 0 then
				IsListedMember = True
			else
				IsListedMember=False		
			end if
	
		else
		'+++++++ Check Listed Name +++++++
			if Instr(session("ListedMembers_share") ,listed) > 0 then
				IsListedMember = True
			else
				IsListedMember=False		
			end if
		end if
	else
		IsListedMember=False	
	end if
End Function 


Function ListedName(listed_id)	
	if listed_id<>"" then
		if isnumeric(listed_id) = true then    		
			 str_Listed_id = replace(trim(session("ListedMembers_id")) & "," & trim(session("GuestListed_id")),"'","")
			 str_Listed = replace(trim(session("ListedMembers_share")) & "," & trim(session("GuestListed_share")),"'","")
			
			 arrListed_id = split(trim(str_Listed_id) ,",")
			 arrListed = split(trim(str_Listed) ,",")
                     
			 for i=0 to ubound(arrListed_id)
				if trim(arrListed_id(i)) <> ""  then		
					listedID = arrListed_id(i)
					
					if cstr(listedID) = cstr(listed_id) then
						ListedName = arrListed(i)
						exit Function
					end if
				 end if
			 next
		 end if
	 end if
		
End Function 
'''''''''''''''''''''' *********************** End Create By Kung on 11/09/09 *******************************

'//Get Link of Listed
Function GetLink(share ,flag)    
     strLink=""
     if flag=1 then
             strsql=""
             strsql="Select  link  from listed_member where share='" & share & "'"
             set rs1 = conirauthen.execute(strsql)
             if not rs1.EOF then
                 strLink=rs1("link")
             else
                strLink=""
             end if
             rs1.close 
      else
              set rs1=nothing
              conirauthen.close
              set conirauthen=nothing
      end if 
     GetLink=strLink
End Function 

'// Check value short financial
Function NumberShortFinancial(N) 
        If IsNumeric(N) Then
            If N = 0 Then
                NumberShortFinancial = "-"
            Else
                NumberShortFinancial = Hnumber(N)
            End If
        Else
            NumberShortFinancial = N
        End If
    End Function
    
Function Hnumber(val) 
        If IsNumeric(val) Then
            Hnumber = FormatNumber(val, 2, -1)
        Else
            Hnumber = "-"
        End If
  End Function

  Function GetLink_Listed(listed_share)	
	if listed_share<>"" then

       str_Listed = replace(trim(session("ListedMembers_share")) & "," & trim(session("GuestListed_share"))& "," & trim(session("General_share")),"'","")   ' Job Request 10-07-0010 By Bowl
       str_Listed_link = replace(trim(session("ListedMembers_link")) & "," & trim(session("GuestListed_link"))& "," & trim(session("General_link")),"'","")       ' Job Request 10-07-0010 By Bowl
      
       arrListed = split(trim(str_Listed) ,",")
       arrListed_link = split(trim(str_Listed_link) ,",")
                  
       for k=0 to ubound(arrListed)
         if trim(arrListed(k)) <> ""  then		
            listed = arrListed(k)
            
            if cstr(listed) = cstr(listed_share) then
               GetLink_Listed = arrListed_link(k)
               exit Function
            end if
          end if
       next
    end if

		
End Function 

 Function GetFolder_Listed(listed_share)	    ' Add by Kung fix for COM7 10/08/2015
	if listed_share<>"" then	
       str_Listed2 = replace(trim(session("ListedMembers_share")),"'","")
       str_Listed_folder =replace(trim(session("ListedMembers_folder")),"'","")
      
       arrListed2 = split(trim(str_Listed2) ,",")
       arrListed_folder = split(trim(str_Listed_folder) ,",")
                  
       for k=0 to ubound(arrListed2)
         if trim(arrListed2(k)) <> ""  then		
            listed = arrListed2(k)
            if cstr(listed) = cstr(listed_share) then
               GetFolder_Listed = arrListed_folder(k)		
               exit Function
            end if
          end if
       next
    end if
End Function 

 Function GetFolder_ListedbyID(listed_id)	    ' Add by Kung fix for COM7 10/08/2015
	if listed_id<>"" then	
		str_ListedID = replace(trim(session("ListedMembers_id")),"'","")
       str_Listed3 = replace(trim(session("ListedMembers_share")),"'","")
       str_Listed_folder =replace(trim(session("ListedMembers_folder")),"'","")	   
      arrListedID = split(trim(str_ListedID) ,",")
       arrListed3 = split(trim(str_Listed3) ,",")
       arrListed_folder = split(trim(str_Listed_folder) ,",")            
       for k=0 to ubound(arrListedID)
         if trim(arrListedID(k)) <> ""  then		
            listed = arrListedID(k)
            if cstr(listed) = cstr(listed_id) then
               GetFolder_ListedbyID = arrListed_folder(k)					 
               exit Function
            end if
          end if
       next
    end if
End Function 


 ' Job Request 10-07-0010 By Bowl
Function IsListedGeneral(listed)
	if listed<>"" then
		if isnumeric(listed) = true then
		'+++++++++ Check Listed ID ++++++++
			if Instr(session("General_id") ,listed) > 0 then
				IsListedGeneral = True
			else
				IsListedGeneral=False		
			end if
	
		else
		'+++++++ Check Listed Name +++++++
			if Instr(session("General_share") ,listed) > 0 then
				IsListedGeneral = True
			else
				IsListedGeneral=False		
			end if
		end if
	else
		IsListedGeneral=False	
	end if
End Function 

Function GetMemberListedIntroBanner(share)

	strsql = "SELECT * FROM listed_member where share='" & listed_share & "'  "
	  set rs = conirauthen.Execute(strsql)
	  if not rs.EOF then
		 intro_banner = rs("intro_banner")	
		 intro_banner_name = rs("intro_banner_name")	
	  end if
	  rs.close	
	  
	 if intro_banner <> "Y" then
	 
		if listed_share="PYLON" then
			Response.redirect("home.asp")
		elseif listed_share="SUPER" then
			Response.redirect("homebanner.asp")
		elseif listed_share="TKS" then
			Response.redirect("index.asp")
		elseif listed_share="SST" then
			Response.redirect("index.asp")
		else
			Response.redirect("homepage.asp")
		end if

	else
	%>
	
		<%if listed_share="PYLON" then%>
		
		<script type="text/javascript">
			GetIntroBannerPYLON('<%=intro_banner%>','<%=intro_banner_name%>');
		</script>	
		
		<%elseif listed_share="SUPER" then%>
		
		<script type="text/javascript">
			GetIntroBannerSUPER('<%=intro_banner%>','<%=intro_banner_name%>');
		</script>
		
		<%elseif listed_share="TKS" then%>
		<script type="text/javascript">
			GetIntroBannerTKS('<%=intro_banner%>','<%=intro_banner_name%>');
		</script>	
		
		<%elseif listed_share="SST" then%>
		<script type="text/javascript">
			GetIntroBannerSST('<%=intro_banner%>','<%=intro_banner_name%>');
		</script>	
				
		<%else%>
		
		<script type="text/javascript">
			GetIntroBannerMemberListed('<%=intro_banner%>','<%=intro_banner_name%>');
		</script>	

		<%end if%>
	<%
	 end if
End Function 





Function GetMemberListedIntroBannerFullPage(listed_share)

	strsql = "SELECT * FROM listed_member where share='" & listed_share & "'  "

	  set rs = conirauthen.Execute(strsql)
	  if not rs.EOF then
		 intro_banner = rs("intro_banner")	
		 intro_banner_name = rs("intro_banner_name")	
	  end if
	  rs.close	

	  GetMemberListedIntroBannerFullPage = intro_banner & "|" & intro_banner_name
	  

End Function 


%>