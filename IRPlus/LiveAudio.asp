<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">

<%

PageType="Other"
Session("redirect_page")=""
Session("PageName")="Live Audio"
Session("PageASP")="LiveAudio.asp"	

%>

 <!-- #include file="../i_constant.asp" -->
 
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=windows-874">
<meta name="keywords" content="<%=message_keyword%>">
<title>IR Plus</title>
<LINK href="Themes/Themes.css" type="text/css" rel="stylesheet">	
<script language="JavaScript" type="text/JavaScript" src="Scripts/Scripts.js" ></script>
</head>

<body>
<div align="center">
  <table width="312" border="0" cellpadding="0" cellspacing="1" bgcolor="#EAEDED">
		<tr>
			<td align="center" valign="top" bgcolor="#FFFFFF">
				<table width="100%"  border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td align="left" valign="middle">
							<table width="310"  border="0" cellspacing="0" cellpadding="0">
								<tr align="left" valign="top">
								  <td colspan="3"><img src="images/live_head.jpg" width="310" height="86" border="0" usemap="#Map3"></td>
								</tr>
								<tr align="left" valign="top">
								  <td width="1" rowspan="2" bgcolor="#050E8D"><p><img src="images/space.gif" width="1" height="1"></p></td>
								  <td align="center"><img src="images/live_service.gif" width="300" height="21" border="0" usemap="#Map"></td>
								  <td width="1" rowspan="2" align="right" bgcolor="#050E8D"><img src="images/space.gif" width="1" height="1"></td>
								</tr>
								<tr align="left" valign="top">
								  <td align="center">
												<%
												if request("filename") <> "" then
													'++++ Play Retrospec Live Radio ++++
													liveradio = server_path_radioRetrospec & "/"&request("filename")&".asf"
												else
													liveradio = server_path_radio
												end if
												%>
								  
											<script language="JavaScript">
												  if ( navigator.appName == "Netscape" ) {Radio("SITE",'<%=liveradio%>',2); }
												  else if (navigator.appName == "Opera"){Radio("SITE",'<%=liveradio%>',1);}                              
												  else {Radio("SITE",'<%=liveradio%>',1);}
												  
											   //-->
											  </script>
								  </td>
								</tr>
								<tr align="left" valign="top">
								  <td colspan="3"><img src="images/live_bottom.jpg" width="310" height="9"></td>
								</tr>
							</table>
						</td>
					</tr>
				</table>
			</td>
    </tr>
  </table>
</div>
<map name="Map3">
  <area shape="circle" coords="276,24,14" href="AllLiveAudio.asp" target="_blank">
</map>
<map name="Map">
  <area shape="rect" coords="158,0,298,22"  href="#" style="cursor:hand" onclick="window.open('FeedBackLiveAudio.asp','_blank','toolbar=0,scrollbars=0,location=0,statusbar=0,menubar=0,resizable=0,width=720,height=690,left=0,top=0');">
</map>
<!-- .................................................. Google Analytics .................................................. -->
                                 <!--#include file="../i_googleAnalytics.asp"-->	
<!-- ........................................... End Google Analytics .................................................. -->
</body>
</html>
