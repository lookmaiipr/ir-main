<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">

<%

PageType="Other"
Session("redirect_page")=""
Session("PageName")="Contact Us"
Session("PageASP")="ContactUs.asp"
%>
 <!--#include file = "Scripts/Scripts.asp" -->
 <!-- #include file="../i_constant.asp" -->
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=windows-874">
<meta name="keywords" content="<%=message_keyword%>">
<meta name="description" content="<%=message_description%>">
<title>IR Plus</title>
<LINK href="Themes/Themes.css" type="text/css" rel="stylesheet">
<script language="JavaScript" type="text/JavaScript" src="Scripts/Scripts.js" ></script>
<script type="text/javascript" language="JavaScript1.2" src="../function2007.js"></script>
<script type="text/javascript" language="JavaScript1.2" src="stmenu.js"></script>
</head>
<body  >
<table width="100%"  border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td align="center" valign="top">
        <table width="1000" border="0" cellpadding="0" cellspacing="0">
                <tr>
                    <td align="left" valign="top" bgcolor="#FFFFFF">
                            <table width="100%"  border="0" cellspacing="0" cellpadding="0">
                                      <tr align="left" valign="top">
                                                <td width="155" colspan="2"> 
                                                <!--#include file = "TopSection.asp" -->
                                                </td>
                                      </tr>
                                       <tr align="left" valign="top">
                                            <td colspan="2">
                                                <table width="100%"  border="0" cellspacing="0" cellpadding="0">
                                                    <tr align="left" valign="top">
                                                            <td width="155" background="images/bg_menu.jpg">
                                                                 <table width="100%"  border="0" cellspacing="0" cellpadding="0">                                                           
                                                                       <tr align="left" valign="top">
                                                                            <td background="images/menu_left.jpg">                                                                    
                                                                                <!--#include file = "LeftSection.asp" --></td>
                                                                       </tr>  
                                                                </table></td>
                                                        <td>                                                                
                                                                <table width="100%"  border="0" cellspacing="0" cellpadding="0">
                                                                        <tr height="20px">
                                                                            <td  width="20px"> </td>
                                                                             <td> </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td  width="20px"> </td>
                                                                            <td align="left" valign="top"> 
                                                                                    <table width="100%"  border="0" cellspacing="0" cellpadding="0">
                                                                                          <tr align="right" valign="top">
                                                                                                <td align="left" valign="bottom">
                                                                                                        <%                                                                                                                                                
                                                                                                                                            sub_title1=Mid(ArrMenu(16,0), 1, InStr(1, ArrMenu(16,0), "Us", vbTextCompare) - 1)
                                                                                                                                            sub_title2=Mid(ArrMenu(16,0), InStr(1, ArrMenu(16,0), "Us", vbTextCompare), Len(ArrMenu(16,0)))
                                                                                                                                        %>
                                                                                                        <font  class="style13"><%=Ucase(sub_title1)%></font><font class="style12"><%=Ucase(sub_title2)%></font></td>
                                                                                            </tr>
                                                                                          <tr align="right" valign="top" bgcolor="#01118D">
                                                                                                <td align="left"><img src="images/space.gif" width="1" height="1"></td>
                                                                                          </tr>
                                                                                    </table>
                                                                          </td>
                                                                    </tr>                                                              
									<%if session("lang")="E" then%>
                                                                       <tr>
                                                                            <td  width="20px"> </td>
                                                                            <td align="center" valign="top"><br>										
                                                                                 <table width="90%"  border="0" cellspacing="10" cellpadding="0">
                                                                                      <tr align="left" valign="top">
                                                                                        <td colspan="3"><span class="style17">Online Asset Company Limited</span></td>
                                                                                      </tr>
                                                                                      <tr align="left" valign="top">
                                                                                        <td width="15%">Contact Us</td>
                                                                                        <td>:</td>
                                                                                        <td><strong>Ladapa Rojthanong</strong></td>
                                                                                      </tr>
                                                                                      <tr align="left" valign="top">
                                                                                        <td>Address </td>
                                                                                        <td>:</td>
                                                                                        <td>466 Ratchadapisek Road, Samsaen-nok, Huay Kwang, Bangkok 10310, Thailand</td>
                                                                                      </tr>
                                                                                      <tr align="left" valign="top">
                                                                                        <td>Telephone </td>
                                                                                        <td>:</td>
                                                                                        <td><strong><%=office_tel%></strong></td>
                                                                                      </tr>
                                                                                      <tr align="left" valign="top">
                                                                                        <td>Fax</td>
                                                                                        <td>:</td>
                                                                                        <td><strong>02 022 6255</strong></td>
                                                                                      </tr>
                                                                                      <tr align="left" valign="top">
                                                                                        <td>E-Mail</td>
                                                                                        <td>:</td>
                                                                                        <td><a class="detail"  href="mailto:<%=IRCONTACT_EMAIL%>"><%=IRCONTACT_EMAIL%></a></td>
                                                                                      </tr>
                                                                                      <tr align="left" valign="top">
                                                                                        <td> Website </td>
                                                                                        <td>:</td>
                                                                                        <td><a  class="detail"  href="<%="http://www." & lcase(WEBSITE_NAME )%>"><%="www." & lcase(WEBSITE_NAME )%></a></td>
                                                                                      </tr>
                                                                                </table>		
									    </td>                                                                            
                                                                      </tr>
                                                                      <tr>
                                                                            <td  width="20px"> </td>
                                                                            <td align="center" valign="top"><p><img src="images/map_online_2015_en.jpg" width="688" height="752"></p></td>
                                                                       </tr>																				
									<%else%>
                                                                       <tr>
                                                                            <td  width="20px"> </td>
                                                                            <td align="center" valign="top"><br>										
                                                                                 <table width="90%"  border="0" cellspacing="10" cellpadding="0">
                                                                                      <tr align="left" valign="top">
                                                                                        <td colspan="3"><span class="style17">����ѷ �͹�Ź� ����� �ӡѴ </span></td>
                                                                                      </tr>
                                                                                      <tr align="left" valign="top">
                                                                                        <td width="15%">�Դ���</td>
                                                                                        <td>:</td>
                                                                                        <td><strong>�سŮ��� �è�췹�</strong></td>
                                                                                      </tr>
                                                                                      <tr align="left" valign="top">
                                                                                        <td>������� </td>
                                                                                        <td>:</td>
                                                                                        <td>466 ����Ѫ�����ɡ �ǧ����ʹ�͡ ࢵ���¢�ҧ ��ا෾��ҹ�� 10310</td>
                                                                                      </tr>
                                                                                      <tr align="left" valign="top">
                                                                                        <td>�������Ѿ�� </td>
                                                                                        <td>:</td>
                                                                                        <td><strong><%=office_tel%></strong></td>
                                                                                      </tr>
                                                                                      <tr align="left" valign="top">
                                                                                        <td>���������</td>
                                                                                        <td>:</td>
                                                                                        <td><strong>02 022 6255</strong></td>
                                                                                      </tr>
                                                                                      <tr align="left" valign="top">
                                                                                        <td>�����</td>
                                                                                        <td>:</td>
                                                                                        <td><a class="detail"  href="mailto:<%=IRCONTACT_EMAIL%>"><%=IRCONTACT_EMAIL%></a></td>
                                                                                      </tr>
                                                                                      <tr align="left" valign="top">
                                                                                        <td> ���䫴� </td>
                                                                                        <td>:</td>
                                                                                        <td><a  class="detail"  href="<%="http://www." & lcase(WEBSITE_NAME )%>"><%="www." & lcase(WEBSITE_NAME )%></a></td>
                                                                                      </tr>
                                                                                </table>
									    </td>                                                                            
                                                                      </tr>
                                                                      <tr>
                                                                            <td  width="20px"> </td>
                                                                            <td align="center" valign="top"><p><img src="images/map_online_2015_th.jpg" width="688" height="752"></p></td>
                                                                       </tr>																				
									<%end if%>										
                                                                      <tr height="15px">
                                                                            <td  width="20px"> </td>
                                                                             <td> </td>
                                                                      </tr>                                                                     
                                                                             
                                                                    <tr >
                                                                            <td  width="20px"> &nbsp;</td>
                                                                             <td> &nbsp;</td>
                                                                     </tr>
                                                                    
                                                            </table></td>
                                                    </tr>
                                           </table></td>
                                </tr>
                    </table></td>
            </tr> 
             <!--#include file = "BottomSection.asp" -->
    </table></td>
  </tr>
</table>
<map name="Map">
  <area shape="circle" coords="282,27,14" href="#">
</map>
<!-- .................................................. Google Analytics .................................................. -->
                                 <!--#include file="../i_googleAnalytics.asp"-->	
<!-- ........................................... End Google Analytics .................................................. -->
</body>
</html>
