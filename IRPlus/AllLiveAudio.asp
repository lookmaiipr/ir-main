<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">

<%
PageType=""
Session("redirect_page")=""
Session("PageName")="All  Live Audio"
Session("PageASP")="AllLiveAudio.asp"

%>

 <!--#include file="../constpage2007.asp"-->
<!--#include file = "../i_constant.asp" -->
<!--#include file = "../function_asp2007.asp" -->
<!--#include file = "../i_conradio.asp" -->
 <!--#include file = "Scripts/Scripts.asp" -->
 
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=windows-874">
<meta name="keywords" content="<%=message_keyword%>">
<title>IR Plus</title>
<LINK href="Themes/Themes.css" type="text/css" rel="stylesheet">	
<script language="JavaScript" type="text/JavaScript" src="Scripts/Scripts.js" ></script>
<script type="text/javascript" language="JavaScript1.2" src="stmenu.js"></script>
</head>
<body style="" >
<table width="100%"  border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td align="center" valign="top"><table width="1000" border="0" cellpadding="0" cellspacing="0">
      <tr>
        <td align="left" valign="top" bgcolor="#FFFFFF">
            <table width="100%"  border="0" cellspacing="0" cellpadding="0">
             <tr align="left" valign="top">
               <td width="155" colspan="2">
                      <!-- .................................................. Top .................................................. -->       
                           <!--#include file = "TopSection.asp" -->                      
                     <!-- .............................................. End Top ............................................... -->         
               </td>
             </tr>
             <tr align="left" valign="top">
               <td colspan="2"><table width="100%"  border="0" cellspacing="0" cellpadding="0">
                 <tr align="left" valign="top">
                   <td><table width="100%"  border="0" cellspacing="0" cellpadding="0">
                        <tr height="20px">                               
                                 <td> </td>
                          </tr>  
                     <tr>                     
                       <td align="left" valign="top"><table width="100%"  border="0" cellspacing="0" cellpadding="0">
                         <tr align="right" valign="top">
                              <td align="left" valign="bottom">
                                   <%                                                                                                                                                
                                       sub_title1=mid(ArrMenu(19,0),1,instr(ArrMenu(19,0),"Audio")-1)
                                       sub_title2=mid(ArrMenu(19,0),instr(ArrMenu(19,0),"Audio"),len(ArrMenu(19,0)))
                                   %>
                                   <span class="style13"><%=Ucase(sub_title1)%></span> 
                                   <span class="style12"><%=Ucase(sub_title2)%></span>
                              </td>
                        </tr>
                         <tr align="right" valign="top" bgcolor="#01118D">
                           <td align="left"><img src="images/space.gif" width="1" height="1"></td>
                         </tr>
                       </table></td>
                     </tr>
                      <tr height="20px">                               
                                 <td> </td>
                          </tr>  
                     <tr>
                       <td align="center" valign="top">
                        <!-- ++++++++++++++++++++++++++++++++++++++++++++++++++ Content ++++++++++++++++++++++++++++++++++++++++++++++++++-->	     
									<table width="100%"  border="0" cellpadding="5" cellspacing="1" bgcolor="#EAEDED">
										<tr align="center" valign="top" bgcolor="#082489">
										  <td width="10%" bgcolor="#082489"><span class="styleTitleTable">Date</span></td>                                                                                       
										  <td width="20%" bgcolor="#082489"><span class="styleTitleTable">Time</span></td>
										  <td><span class="styleTitleTable">Detail</span></td>
										  <td width="10%"><span class="styleTitleTable">Sound</span></td>
										</tr>
										<%
										
										  limit_retro = 7
										  SeriesDate=""
										  strsql=""
										  strsql = "SELECT date_format(live_date,'%Y-%m-%d') as live_date FROM daily_radio "
										  strsql=strsql & " where radio_type='LI' and live_date < curdate() group by live_date desc  limit " & limit_retro 

										  set rs = conradio.execute(strsql)
										  if not rs.eof and not  rs.bof then 
											 do while not rs.eof	
												   SeriesDate = SeriesDate & " '"& rs("live_date") & "',"
											 rs.movenext
											 loop
											rs.movefirst
											rs.close()
										  end if                                                                                           
										 SeriesDate = Left(SeriesDate,len(SeriesDate) - 1)

										  strsql=""
										  strsql = "SELECT date_format(d.live_date,'%Y-%m-%d') as live_date,d.seq,filename,company,interviewee,time_format(start_time,'%H:%i') as start_time "
										  strsql=strsql & " ,time_format(stop_time,'%H:%i')as stop_time,detail from daily_radio d join live_schedule s on d.radio_type='LI' and d.seq=s.seq  "
										  strsql=strsql & " where d.live_date=s.live_date and d.live_date in (" & SeriesDate & ") order by live_date desc,d.seq  "
										  set rs = conradio.execute(strsql)
										if not rs.eof and not  rs.bof then 
											  if i=1 then
												post_date="05/07/01 10:11:32 "
											 end if                                                                                      
											 do while not rs.eof	
											 %>
											  <tr align="left" valign="top">
											 <td bgcolor="#FFFFFF" align="center"><strong>
													 <%
													   old_date= formatdateAm_Pm(post_date)
													  new_date=formatdateAm_Pm(rs("live_date"))
														if left(trim(old_date),10) <>left(trim(new_date),10) then			
															  response.write left(trim(new_date),10)
															  post_date=rs("live_date") 
													   end if%>	
											</strong></td>                                                                                             
												<td align="left" bgcolor="#FFFFFF" ><div align="center"><strong><%=rs("start_time")%> - <%=rs("stop_time")%></strong> </div></td>
												<td bgcolor="#FFFFFF"><%=rs("detail")%> </td>
												<td align="center" valign="middle" bgcolor="#FFFFFF">
												<% if rs("filename") <> "" then%>
												<a href="javascript:void(0);"  style="cursor:hand" onClick="window.open('LiveAudio.asp?filename=<%=rs("filename")%>','video','width=340,height=170,top=0,left=0');"><img src="images/icon_sound.gif" width="25" height="21" border="0"></a>
												<%else%>
												   -
												<%end if%>
												</td>
											  </tr>                                                                                           
											 <%
											 rs.movenext
											 loop
										else
											  %>
												<tr class="bgcolor_no_information">
													  <td align="center" colspan="3"  valign="middle">
														   <font class="no_information">
														 <%
														 if  session("lang")="T" or session("lang")="" then 
															  response.write Message_Info_T
														 else																				
															   response.write Message_Info_E
														 end if
														 %></font>
													  </td>
												 </tr>                                                                                   
											 <%                                                                                       
										end if
										%>
									  </table>
						
                        <!-- ++++++++++++++++++++++++++++++++++++++++++++++++++ End Content +++++++++++++++++++++++++++++++++++++++++++++++-->	                                   
                    </td>
                  </tr>
                    <tr height="25px">                               
                                 <td> </td>
                          </tr>  
                     <tr>
                </table></td>
              </tr>
            </table></td>
          </tr>
        </table></td>
      </tr>
      <!-- ..................................................  footer .................................................. -->         
                     <!--#include file = "BottomSection.asp" -->
      <!-- .............................................. End footer ................................................ -->                     
    </table></td>
  </tr>
</table>
<!-- .................................................. Google Analytics .................................................. -->
                                 <!--#include file="../i_googleAnalytics.asp"-->	
<!-- ........................................... End Google Analytics .................................................. -->
</body>
</html>
