<%
session("cur_page")="IR  " & request("title")
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">

<%
PageType=""
Session("redirect_page")=""
Session("PageName")="Detail Public Relations"
Session("PageASP")="DetailPublicRelaions.asp"	
%>

<!--#include file = "Scripts/Scripts.asp" -->
<!-- #include file="../i_constant.asp" -->
 <!--#include file = "../i_conir.asp" -->

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=windows-874">
<meta name="keywords" content="<%=message_keyword%>">
<title>IR Plus</title>
<LINK href="Themes/Themes.css" type="text/css" rel="stylesheet">	
<script language="JavaScript" type="text/JavaScript" src="Scripts/Scripts.js" ></script>
<script type="text/javascript" language="JavaScript1.2" src="stmenu.js"></script>
<script language="javascript" src="../function2007.js"></script>
<script language="javascript" src="Scripts/FrameManager.js"></script>
</head>
<body >
<table width="100%"  border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td align="center" valign="top"><table width="1000" border="0" cellpadding="0" cellspacing="0">
      <tr>
        <td align="left" valign="top" bgcolor="#FFFFFF">
            <table width="100%"  border="0" cellspacing="0" cellpadding="0">
             <tr align="left" valign="top">
               <td width="155" colspan="2">
                      <!-- .................................................. Top .................................................. -->       
                           <!--#include file = "TopSection.asp" -->                      
                     <!-- .............................................. End Top ............................................... -->         
               </td>
             </tr>
             <tr align="left" valign="top">
               <td colspan="2"><table width="100%"  border="0" cellspacing="0" cellpadding="0">
                 <tr align="left" valign="top">
                   <td><table width="100%"  border="0" cellspacing="0" cellpadding="0">
                    <tr height="20px">                        
                         <td> </td>
                    </tr>
                     <tr>
                       <td align="left" valign="top"><table width="100%"  border="0" cellspacing="0" cellpadding="0">
                         <tr align="right" valign="top">
                              <td align="left" valign="bottom">
                                	<%                                                                                                                                                
						sub_title1=mid(ArrMenu(4,3),1,instr(ArrMenu(4,3),"Relations")-1)
						sub_title2=mid(ArrMenu(4,3),instr(ArrMenu(4,3),"Relations"),len(ArrMenu(4,3)))
					%>
					<span class="style13"><%=Ucase(sub_title1)%></span> 
					<span class="style12"><%=Ucase(sub_title2)%></span>
                              </td>
                        </tr>
                         <tr align="right" valign="top" bgcolor="#01118D">
                           <td align="left"><img src="images/space.gif" width="1" height="1"></td>
                         </tr>
                       </table></td>
                     </tr>
                     <tr height="10px">
                        <td  > </td>                        
                    </tr>
                     <tr>
                       <td align="center" valign="top">
                        <!-- ++++++++++++++++++++++++++++++++++++++++++++++++++ Content ++++++++++++++++++++++++++++++++++++++++++++++++++-->	                    
                          <table width="100%"  border="0" cellspacing="0" cellpadding="0">
                              <tr>
                                 <td align="left" valign="top">
				<%
				if  session("lang")="E"  then
					strsql="SELECT *,name,title"
				else
					strsql="SELECT *,name_th as name , title_th as title"
				end if			
				strsql =strsql&  " FROM public_relation where id='"&request("id") &"' and display_ir='Y'  "
				if  session("lang")="E"  then
					strsql=strsql & " and title<>'' and  name <> '' and  name <> '-' "
				else
					strsql=strsql & " and title_th<>'' and name_th <> '' and name_th <> '-' "
				end if
				 set rs = conir.execute(strsql)
				 if not  rs.eof and not  rs.bof then   
					file_name=rs("name")
				 end if
				 
				 rs.close
				%>		
				<%if file_name="" then%>
					<Div align="Center">
						<!--#include file = "IC_Noinformation.asp" -->     
					</Div>
				<%else%>		
					<!--<IFRAME id="detail" allowtransparency="true" src="<%=pathfileserver & "/Listed/"&replace(request("share"),"_","&")&"/Public_Relation/File/" &file_name&""%>" width="100%"<%if IsLocalpath then %>onload="if (window.parent && window.parent.autoIframe) {window.parent.autoIframe('detail');}" <%else%>height="800px" scrolling="Yes" <%end if%> frameborder="0" scrolling="Yes"></IFRAME>-->
					<IFRAME id="detail" allowtransparency="true" src="<%=pathfileserver & "/Listed/"&replace(request("share"),"_","&")&"/Public_Relation/File/" &file_name&""%>" width="100%" height="800px" frameborder="0" scrolling="Yes"></IFRAME>
				
				<%end if%>
                               </td>
                            </tr>
                            <tr>
                              <td align="left" valign="top">&nbsp;</td>
                            </tr>
                          </table>
                        <!-- ++++++++++++++++++++++++++++++++++++++++++++++++++ End Content +++++++++++++++++++++++++++++++++++++++++++++++-->	                                   
                    </td>
                  </tr>
                </table></td>
              </tr>
            </table></td>
          </tr>
        </table></td>
      </tr>
      <!-- ..................................................  footer .................................................. -->         
                     <!--#include file = "BottomSection.asp" -->
      <!-- .............................................. End footer ................................................ -->                     
    </table></td>
  </tr>
</table>
<map name="Map">
  <area shape="circle" coords="282,27,14" href="#">
</map>
<!-- .................................................. Google Analytics .................................................. -->
                              <!--#include file="../i_googleAnalytics.asp"-->	
<!-- ............................................End Google Analytics .................................................. -->
</body>
</html>
