<%
session("cur_page")="IR Site Map"
session("page_asp")="site_map2007"

%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=windows-874">
<title>Investor Relations</title>
<LINK href="style2007.css" type="text/css" rel="stylesheet">	
<!--#include file = "i_constant.asp" -->
<!--#include file = "function_asp2007.asp" -->
<!--include file = "i_conpsims.asp" -->
<!--#include virtual = "i_conirauthen.asp" -->
<!--#include file = "listed_member2007.asp" -->
 <!--#include file = "i_connews.asp" -->	
<!--#include virtual = "i_conir.asp" --> 
<meta name="keywords" content="<%=message_keyword%>">
<script language="javascript" src="function2007.js"></script>
<script type="text/javascript" language="JavaScript1.2" src="script2007/stm31.js"></script>
</head>

<body >
<table width="100%"  border="0" cellspacing="0" cellpadding="0">
  <tr>
		<td align="center" valign="top">
			<table width="742" border="0" cellpadding="0" cellspacing="0" bgcolor="#ECECEC">
				<tr align="left" valign="top">
					<td width="1" background="images2007/dot_pink.gif"><img src="images2007/dot_pink.gif" width="1" height="1"></td>
					<td>
							<table width="100%"  border="0" cellspacing="0" cellpadding="0">
									<!--++++++++++++++++++++++++include top++++++++++++++++++++++-->
								
													<!--#include file = "i_top2007.asp" -->
										
									<!--+++++++++++++++++++++++include menu+++++++++++++++++++++-->																									  
								  
												<!--#include file = "i_menu2007.asp" -->									
								
								<!--++++++++++++++++++++++++Content+++++++++++++++++++++++++++++-->
								 <tr>
            <td height="1" align="left" valign="top" background="images2007/dot_pink.gif"><img src="images2007/dot_pink.gif" width="1" height="1"></td>
          </tr>
          <tr>
            <td align="center" valign="top"><img src="images2007/head_sitemap.gif" width="740" height="80"></td>
          </tr>
          <tr>
            <td align="left" valign="top"><table width="300" border="0" cellspacing="0" cellpadding="0">
              <tr align="left" valign="top">
                <td width="14"><img src="images2007/bar_1left.gif" width="14" height="21"></td>
                <td width="130" valign="middle" background="images2007/bar_1bg1.gif"><strong>Site Map </strong></td>
                <td width="32"><img src="images2007/bar_1creb.gif" width="32" height="21"></td>
                <td background="images2007/bar_1bg2.gif">&nbsp;</td>
                <td width="4" align="right"><img src="images2007/bar_1right.gif" width="4" height="21"></td>
              </tr>
			   </table>              </td>
          </tr>

			<tr>
            <td align="center" valign="top" bgcolor="#FFFFFF">
				 <!-------------------------------------------------->	
			<br>
			<br>
			<table width="90%" border="0"cellspacing="0" cellpadding="0" >
			
			<!--  1 -->
		
			
				<tr align="left">
					<td align="left" valign="top" width="30%">
						<table width="100%" border="0"cellspacing="0" cellpadding="0" >
						<td align="left" valign="top" colspan="2">
						<b><a href="investor.asp">IR Home</a></b>
			</td>
		</tr>
				<tr><td width="10%" valign="top"  align="center" >    <img src="images2007/page.gif" width="18" height="18"></td>
					<td valign="top">What is Investor Relations ?</td>	
						<tr><td width="10%" valign="top"  align="center" >    <img src="images2007/page.gif" width="18" height="18"></td>
					<td valign="top">E-mail Alerts</td>	
						<tr><td width="10%" valign="top"  align="center" >    <img src="images2007/page.gif" width="18" height="18"></td>
					<td valign="top">News Update</td>	
						<tr><td width="10%" valign="top"  align="center" >    <img src="images2007/page.gif" width="18" height="18"></td>
					<td valign="top">IR Calendar</td>	
						<tr><td width="10%" valign="top"  align="center" >    <img src="images2007/page.gif" width="18" height="18"></td>
					<td valign="top">SET Annoucement</td>	
						<tr><td width="10%" valign="top"  align="center" >    <img src="images2007/page.gif" width="18" height="18"></td>
					<td valign="top">Public Relations</td>	
						<tr><td width="10%" valign="top"  align="center" >    <img src="images2007/page.gif" width="18" height="18"></td>
					<td valign="top">Stock Information</td>	
						<tr><td width="10%" valign="top"  align="center" >    <img src="images2007/page.gif" width="18" height="18"></td>
					<td valign="top">Annual Report</td>	
					</tr>
			</table>
</td>

		
			<td align="left" valign="top" width="35%">
				<table width="100%" border="0"cellspacing="0" cellpadding="0" >
				<tr align="left">
						<td><b><a href="ir_members2007.asp">IR Members</a></b></td>
					</tr>
			</td>
	</table>
	
	
					<td align="left" valign="top" width="35%">
						<table width="100%" border="0"cellspacing="0" cellpadding="0" >
							<tr align="left">
								<td align="left" valign="top" colspan="2">
								<b>News Room</b>
							</td>
						</tr>
			
				<tr><td  width="10%"valign="top"  align="center" ><img src="images2007/page.gif"width="18" height="18"></td>
					<td valign="top" ><a href="news_update2007.asp">News Update</a></td>
				<tr><td width="10%"valign="top"  align="center" ><img src="images2007/page.gif"width="18" height="18"></td>
					<td valign="top" ><a href="news_clipping2007.asp">News Clippling</a></td>
				<tr><td  width="10%"valign="top"  align="center" ><img src="images2007/page.gif"width="18" height="18"></td>
					<td valign="top" ><a href="set_announcement2007.asp">SET Annoucement</a></td>
				<tr><td width="10%"valign="top"  align="center" ><img src="images2007/page.gif"width="18" height="18"></td>
					<td valign="top" ><a href="ir_calendar2007.asp">IR Calendar</a></td>
				<tr><td width="10%"valign="top"  align="center" ><img src="images2007/page.gif"width="18" height="18"></td>
					<td valign="top" ><a href="public_relations2007.asp">Public Relations</a></td>
						</tr>
			</table>
</td>																		
			
			</tr>
			<!--2-->
			<td align="left" valign="top" >
			<br>
			<br>
				<table width="100%" border="0"cellspacing="0" cellpadding="0" >
					<tr align="left">
						<td ><b><a href="annual_report2007.asp">Annual Report</a></b></td>
					</td>
				</tr>
		</table>	
		
			<td align="left" valign="top" >
			<br>
			<br>
				<table width="100%" border="0"cellspacing="0" cellpadding="0" >
					<tr align="left">
			<td><b><a href="investor_faqs2007.asp">Investor FAQs</a></b></td>
		</td>
	</tr>
</table>	
			
					<td align="left" valign="top" >
					<br>
					<br>
						<table width="100%" border="0"cellspacing="0" cellpadding="0" >
							<tr align="left">
						<td align="left" valign="top" colspan="2">
							<b>Info Request</b>
			
				<tr><td width="10%" valign="top"  align="center" >    <img src="images2007/page.gif"width="18" height="18"></td>
					<td valign="top">
					    <%  if customer_id=0 or customer_id=98 then%>
						     <a href="sign_in_alert2007.asp">
						<%else%>
						     <a href="<%=pathfileserver%>/sign_in_alert2007.asp"  target="_blank">
						<%end if%>
						E-mail Alerts</a></td>
				<tr><td width="10%" valign="top"  align="center" >   <img src="images2007/page.gif"width="18" height="18"></td>
					<td valign="top">
								<%  if customer_id=0 or customer_id=98 then%>
					           <a href="inquiry_form2007.asp">
							   <%else%>
							      <a href="<%=pathfileserver%>/inquiry_form2007.asp"  target="_blank">
							   <%end if%>
							   Inquiry Form</a></td>
							</tr>
			</table>
</td>
			
		
			<!--3-->
<tr align="left">
			<td align="left" valign="top" >
			<br>
			<br>
				<table width="100%" border="0"cellspacing="0" cellpadding="0" >
					<tr align="left">
					<td ><b><a href="disclaimer2007.asp">Disclaimer</a></b></td>
				</td>
			</tr>
</table>	
			<td align="left" valign="top" >
			<br>
			<br>
				<table width="100%" border="0"cellspacing="0" cellpadding="0" >
					<tr align="left">
						<td><b><a href="site_map2007.asp">Site Map</a></B></td>
						</td>
					</tr>
		</table>
		
			<td align="left" valign="top" >
			<br>
			<br>
				<table width="100%" border="0"cellspacing="0" cellpadding="0" >
					<tr align="left">
			<td><b><a href="ir_website_manual2007.asp">IR Website Manual</a></B></td>
					</td>
			</tr>
</table>	
			
			</tr>
</table>
	<br>
<br>
<br>			
			</td>
			</tr>
												
										
								<!--+++++++++++++++++++++++++include footer++++++++++++++++++++++-->
									
												<!--#include file = "i_footer2007.asp" -->
												
								</table>
							</td>
							<td width="1" align="right" background="images2007/dot_pink.gif"><img src="images2007/dot_pink.gif" width="1" height="1"></td>
						</tr>	
    </table></td>
  </tr>
</table>
<!-- +++++++++++++++++ Google Analytics ++++++++++++++++ -->
          <!--#include file="i_googleAnalytics.asp"-->	
<!-- +++++++++++++++ End Google Analytics +++++++++++++++ -->
</body>
</html>
