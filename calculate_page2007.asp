<%

if session("num_count") mod session("pagesize") > 0 then
	session("totalpage") = int((session("num_count") / session("pagesize"))) + 1
elseif session("num_count") > 0 then
	session("totalpage") = round(session("num_count") / session("pagesize"))
else
	session("totalpage") = 1
end if

%>