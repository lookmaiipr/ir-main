Imports System.IO
Imports System.Diagnostics


Public Class Form1

    Inherits System.Windows.Forms.Form
    Public watchfolder As FileSystemWatcher


    Dim SearchString As String
    Dim filename As String
    Dim line1 As String
    Dim line2 As String
    Dim line3 As String
    Dim line4 As String
    Dim line5 As String
    Dim line6 As String
    Dim line7 As String
    Dim line8 As String

    Dim hostname As String




#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents txt_watchpath As System.Windows.Forms.TextBox
    Friend WithEvents txt_folderactivity As System.Windows.Forms.TextBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents btn_startwatch As System.Windows.Forms.Button
    Friend WithEvents btn_stop As System.Windows.Forms.Button
    Friend WithEvents Ftp1 As nsoftware.IPWorks.Ftp
    Friend WithEvents Ipport1 As nsoftware.IPWorks.Ipport
    Friend WithEvents logfile As System.Windows.Forms.CheckBox
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Me.txt_watchpath = New System.Windows.Forms.TextBox
        Me.txt_folderactivity = New System.Windows.Forms.TextBox
        Me.Label1 = New System.Windows.Forms.Label
        Me.btn_startwatch = New System.Windows.Forms.Button
        Me.btn_stop = New System.Windows.Forms.Button
        Me.Ftp1 = New nsoftware.IPWorks.Ftp(Me.components)
        Me.Ipport1 = New nsoftware.IPWorks.Ipport(Me.components)
        Me.logfile = New System.Windows.Forms.CheckBox
        Me.SuspendLayout()
        '
        'txt_watchpath
        '
        Me.txt_watchpath.Location = New System.Drawing.Point(80, 24)
        Me.txt_watchpath.Name = "txt_watchpath"
        Me.txt_watchpath.Size = New System.Drawing.Size(224, 20)
        Me.txt_watchpath.TabIndex = 0
        Me.txt_watchpath.Text = "e:/ir/file"
        '
        'txt_folderactivity
        '
        Me.txt_folderactivity.Location = New System.Drawing.Point(24, 80)
        Me.txt_folderactivity.Multiline = True
        Me.txt_folderactivity.Name = "txt_folderactivity"
        Me.txt_folderactivity.Size = New System.Drawing.Size(392, 256)
        Me.txt_folderactivity.TabIndex = 1
        Me.txt_folderactivity.Text = ""
        '
        'Label1
        '
        Me.Label1.Location = New System.Drawing.Point(32, 24)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(40, 16)
        Me.Label1.TabIndex = 2
        Me.Label1.Text = "path"
        '
        'btn_startwatch
        '
        Me.btn_startwatch.Location = New System.Drawing.Point(344, 8)
        Me.btn_startwatch.Name = "btn_startwatch"
        Me.btn_startwatch.Size = New System.Drawing.Size(64, 23)
        Me.btn_startwatch.TabIndex = 3
        Me.btn_startwatch.Text = "start"
        '
        'btn_stop
        '
        Me.btn_stop.Location = New System.Drawing.Point(344, 40)
        Me.btn_stop.Name = "btn_stop"
        Me.btn_stop.Size = New System.Drawing.Size(64, 23)
        Me.btn_stop.TabIndex = 4
        Me.btn_stop.Text = "stop"
        '
        'Ftp1
        '
        Me.Ftp1.About = ""
        Me.Ftp1.FirewallPort = 1080
        Me.Ftp1.FirewallType = nsoftware.IPWorks.FtpFirewallTypes.fwNone
        Me.Ftp1.TransferMode = nsoftware.IPWorks.FtpTransferModes.tmDefault
        '
        'Ipport1
        '
        Me.Ipport1.About = ""
        Me.Ipport1.EOL = "" & Microsoft.VisualBasic.ChrW(0)
        Me.Ipport1.EOLB = New Byte() {CType(0, Byte)}
        Me.Ipport1.FirewallPort = 1080
        Me.Ipport1.FirewallType = nsoftware.IPWorks.IpportFirewallTypes.fwNone
        '
        'logfile
        '
        Me.logfile.Location = New System.Drawing.Point(32, 344)
        Me.logfile.Name = "logfile"
        Me.logfile.TabIndex = 5
        Me.logfile.Text = "Log File"
        '
        'Form1
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
        Me.ClientSize = New System.Drawing.Size(440, 373)
        Me.Controls.Add(Me.logfile)
        Me.Controls.Add(Me.btn_stop)
        Me.Controls.Add(Me.btn_startwatch)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.txt_folderactivity)
        Me.Controls.Add(Me.txt_watchpath)
        Me.Name = "Form1"
        Me.Text = "FTP_PUBLIC"
        Me.ResumeLayout(False)

    End Sub

#End Region

    Private Sub btn_startwatch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_startwatch.Click
        watchfolder = New System.IO.FileSystemWatcher

        watchfolder = New System.IO.FileSystemWatcher

        'this is the path we want to monitor 
        watchfolder.Path = txt_watchpath.Text

        'Add a list of Filter we want to specify 
        'make sure you use OR for each Filter as we need to 
        'all of those

        watchfolder.NotifyFilter = IO.NotifyFilters.DirectoryName
        watchfolder.NotifyFilter = watchfolder.NotifyFilter Or _
        IO.NotifyFilters.FileName
        watchfolder.NotifyFilter = watchfolder.NotifyFilter Or _
        IO.NotifyFilters.Attributes

        ' add the handler to each event 
        ' AddHandler watchfolder.Changed, AddressOf logchange
        AddHandler watchfolder.Created, AddressOf logchange
        'AddHandler watchfolder.Deleted, AddressOf logchange

        ' add the rename handler as the signature is different 
        'AddHandler watchfolder.Renamed, AddressOf logrename

        'Set this property to true to start watching 
        watchfolder.EnableRaisingEvents = True
        btn_startwatch.Enabled = False
        btn_stop.Enabled = True
        'End of code for btn_start_click 

    End Sub
    Private Sub logchange(ByVal source As Object, ByVal e As _
System.IO.FileSystemEventArgs)
        If e.ChangeType = IO.WatcherChangeTypes.Changed Then
            txt_folderactivity.Text &= "File " & e.FullPath & _
            " has been modified" & vbCrLf




        End If
        If e.ChangeType = IO.WatcherChangeTypes.Created Then
            txt_folderactivity.Text &= "File " & e.FullPath & _
            " has been created" & vbCrLf

            filename = e.FullPath


         




            '********** coppy file to company ****************


            SearchString = e.Name
            Dim firstfont As Integer = InStr(SearchString, "PR_")
            firstfont = firstfont + 4

            Dim lastfont As Integer = InStr(4, SearchString, "_")

            lastfont = lastfont
            Dim foldername As String = Mid(SearchString, firstfont, (lastfont) - (firstfont))


            Dim hasfolder As String = Directory.Exists(line8 & "/ir/Listed/" & foldername & "/public_relation")
            If hasfolder = False Then
                Try
                    Directory.CreateDirectory(line8 & "/ir/Listed/" & foldername & "/public_relation")
                Catch
                    MsgBox("Error: " & Err.Number & " " & Err.Description)
                End Try

            End If
            Dim path As String = line8 & "/ir/Listed/" & foldername & "/public_relation/" & SearchString



            File.Copy(filename, path)

            txt_folderactivity.Text &= "coppy file to" & path & "sucessfull" & _
            vbCrLf
            'Directory.CreateDirectory()

            '*************************************************


            Dim lfile As String = line1 '"D:/ir/ftp"

            Dim user As String = line3 '"ftp_user"
            Dim pass As String = line4 '"juice_ksc"




            Try


                Ftp1.User = user
                Ftp1.Password = pass

                Ftp1.LocalFile = filename
                Ftp1.RemoteFile = "/ir/file/" & SearchString 'line5 & SearchString"
                Ftp1.Upload()


            Catch
                MsgBox("Error: " & Err.Number & " " & Err.Description)
            End Try
            ' MsgBox(filename)



          


        End If

        If logfile.Checked Then





            Dim MyFileStream2 As FileStream = New FileStream("logfile.txt", IO.FileMode.Append, IO.FileAccess.Write, IO.FileShare.Read)
            Dim myStreamWriter As New StreamWriter(MyFileStream2)
            Dim title_log As String
            Dim clock2 As String = Format(Now(), "yyy-MM-dd HH:m:s")
            Dim file_name As String = e.Name




            ' Do
            Try
                '��¹�����Ũҡ�������� 1 ��÷Ѵ
                myStreamWriter.WriteLine(" " & file_name & "        " & clock2 & "                  ")

            Catch
                MessageBox.Show("can`t write log file")
            Finally


                myStreamWriter.Close()
                MyFileStream2.Close()
            End Try
        End If



    End Sub


    Public Sub logrename(ByVal source As Object, ByVal e As _
System.IO.RenamedEventArgs)
        txt_folderactivity.Text &= "File" & e.OldName & _
        " has been renamed to " & e.Name & vbCrLf
    End Sub

    Private Sub btn_stop_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn_stop.Click
        ' Stop watching the folder 
        watchfolder.EnableRaisingEvents = False
        btn_startwatch.Enabled = True
        btn_stop.Enabled = False
    End Sub

    Private Sub txt_folderactivity_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txt_folderactivity.TextChanged

    End Sub

    Private Sub Ipport1_OnConnected(ByVal sender As Object, ByVal e As nsoftware.IPWorks.IpportConnectedEventArgs) Handles Ipport1.OnConnected






    End Sub

    Private Sub logfile_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles logfile.CheckedChanged

    End Sub

    Private Sub txt_watchpath_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txt_watchpath.TextChanged

    End Sub

    Private Sub Form1_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        '*********************��ҹ�͹�ԡ�����������ҧ����õ�ҧ�
        Dim MyFileStream As New FileStream("config.txt", IO.FileMode.OpenOrCreate, FileAccess.Read, FileShare.Read)
        Dim MyStreamReader As New StreamReader(MyFileStream)



        line1 = MyStreamReader.ReadLine() 'path of localfile
        line2 = MyStreamReader.ReadLine() 'hostname
        line3 = MyStreamReader.ReadLine() 'user
        line4 = MyStreamReader.ReadLine() 'password
        line5 = MyStreamReader.ReadLine() 'remotefile
        line6 = MyStreamReader.ReadLine() 'ipport
        line7 = MyStreamReader.ReadLine() 'ipporthost
        line8 = MyStreamReader.ReadLine() 'drive disk
        hostname = line2  '"192.168.168.101"
        Ftp1.RemoteHost = hostname
        Ipport1.RemoteHost = line7 '192.168.168.108
        Ipport1.RemotePort = line6 '2023

        Ipport1.Connected = True
    End Sub

    Private Sub Ftp1_OnConnectionStatus(ByVal sender As Object, ByVal e As nsoftware.IPWorks.FtpConnectionStatusEventArgs) Handles Ftp1.OnConnectionStatus

    End Sub

    Private Sub Ftp1_OnEndTransfer(ByVal sender As Object, ByVal e As nsoftware.IPWorks.FtpEndTransferEventArgs) Handles Ftp1.OnEndTransfer
        Ipport1.DataToSend = "PR;" & SearchString & vbCrLf

        txt_folderactivity.Text &= "File " & filename & " FTP TO 192.168.168.101 " & "successfull" & _
                  vbCrLf


        txt_folderactivity.Text &= "Message IP Port Data To Send FTP_MANAGER" & _
      vbCrLf & "--------------------------------" & vbCrLf

        Ftp1.Logoff()


    End Sub
End Class
