<%
session("cur_page")="IR Inquiry Form"
session("page_asp")="inquiry_detail2007"

%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=windows-874">
<title>Investor Relations</title>
<LINK href="style2007.css" type="text/css" rel="stylesheet">	
<!--#include file = "i_constant.asp" -->
<!--#include file = "function_asp2007.asp" -->
<!--#include virtual = "i_conir.asp" --> 
<meta name="keywords" content="<%=message_keyword%>">
<script language="javascript" src="/ir/function2007.js"></script>
<script type="text/javascript" language="JavaScript1.2" src="/ir/script2007/stm31.js"></script>
</head>

<body >
<table width="100%"  border="0" cellspacing="0" cellpadding="0">
  <tr>
		<td align="center" valign="top">
			<table width="742" border="0" cellpadding="0" cellspacing="0" bgcolor="#ECECEC">
				<tr align="left" valign="top">
					<td width="1" background="images2007/dot_pink.gif"><img src="images2007/dot_pink.gif" width="1" height="1"></td>
					<td>
							<table width="100%"  border="0" cellspacing="0" cellpadding="0">
									<!--++++++++++++++++++++++++include top++++++++++++++++++++++-->
								
													<!--#include file = "i_top2007.asp" -->
										
									<!--+++++++++++++++++++++++include menu+++++++++++++++++++++-->																									  
								  
												<!--#include file = "i_menu2007.asp" -->									
								
								<!--++++++++++++++++++++++++Content+++++++++++++++++++++++++++++-->
						         <tr>
										<td height="1" align="left" valign="top" background="images2007/dot_pink.gif">
										<img src="images2007/dot_pink.gif" width="1" height="1"></td>
							  </tr>
							  <tr>
										<td align="center" valign="top"><img src="images2007/head_request.gif" width="740" height="80"></td>
							  </tr>
								  
								   <tr>
										<td align="left" valign="top">
										   <table width="300" border="0" cellspacing="0" cellpadding="0">
												  <tr align="left" valign="top">
													<td width="14"><img src="images2007/bar_1left.gif" width="14" height="21"></td>
													<td width="130" valign="middle" background="images2007/bar_1bg1.gif"><strong>Inquiry Form </strong></td>
													<td width="32"><img src="images2007/bar_1creb.gif" width="32" height="21"></td>
													<td background="images2007/bar_1bg2.gif">&nbsp;</td>
													<td width="4" align="right"><img src="images2007/bar_1right.gif" width="4" height="21"></td>
												  </tr>
										</table>              
										</td>
									  </tr>
										
								<tr>
										<td align="left" valign="top" bgcolor="#FFFFFF">
										    <table width="100%"  border="0" cellspacing="8" cellpadding="0">
											      <tr>
															<td align="left" valign="top">
															    <table width="100%"  border="0" cellspacing="0" cellpadding="0">
																			<tr align="right" valign="top">
																			  <td><img src="images2007/piont_dot.gif" width="5" height="5">
																			  <a href="inquiry_form2007.asp">Other Questions</a></td>
																			  <td width="200"><img src="images2007/piont_dot.gif" width="5" height="5"> 
																			  <a href="add_inquiry_form2007.asp" >Questions to Management</a></td>
																			</tr>
																</table></td>
														  </tr>
											           
													   <!------------------------------------------------------------------------------------->
													   <%
													   strsql=""
													   strsql="select  title,date_format(timestamp,'%d/%m/%y %H.%i %p') as timestamp "
													   strsql=strsql & " from contact_listed  where id=" & request("id")
													   set rs=conir.execute(strsql)
													   if not rs.eof and not rs.bof then
													   %>
													      <tr>
															   <td align="left" valign="top"><strong>Question</strong></td>
														  </tr>
														  
														  <tr>
																<td align="center" valign="top">
																	<table width="80%"  border="0" cellpadding="5" cellspacing="1" bgcolor="#CC00CC">
																		<tr>
																				<td align="left" valign="top" bgcolor="#EFEFEF">
																				<p align="left"><strong><%=rs("title")%></strong></p></td>
																		</tr>
																		<tr>
																			<td align="left" valign="top" bgcolor="#EFEFEF"><strong><%=rs("timestamp")%></strong></td>
																		</tr>
																</table></td>
														  </tr>
														  <%
														        strsql=""
																strsql="select  answer,date_format(timestamp,'%d/%m/%y %H.%i %p') as ans_date "
																strsql=strsql & "  from ans_listed where contactlisted_id=" & request("id") & " order by timestamp "
																set rs1=conir.execute(strsql)
																if not rs1.eof and not rs1.bof then
																    i=1
																	do while not rs1.eof 
														  %>
														  <tr>
																<td align="left" valign="top"><strong>Answer <%=i%> </strong></td>
														  </tr>
														  
														  <tr>
																<td align="center" valign="top">
																     <table width="80%"  border="0" cellpadding="5" cellspacing="1" bgcolor="#DBDBDB">
																			<tr>
																				<td align="left" valign="top" bgcolor="#FFFFFF">
																				<p align="left"><%=replace(replace(rs1("answer"),vbcrlf,"<br>"),space(1)," ")%></p></td>
																		</tr>
																		<tr>
																			<td align="left" valign="top" bgcolor="#FFFFFF"><strong><%=rs1("ans_date")%></strong></td>
																		</tr>
																	</table>
															</td>
														</tr>
														<%
														       i=i+1
															   rs1.movenext
															   loop
														end if
														rs1.close
														%>
														<tr>
															<td align="left" valign="top"><hr class="style_hr" width="100%"></td>
														  </tr>
														<%end if%>
													  <!------------------------------------------------------------------------------------->
											 </table>
										</td>
								</tr>
										
								<!--+++++++++++++++++++++++++include footer++++++++++++++++++++++-->
									
												<!--#include file = "i_footer2007.asp" -->
												
								</table>
							</td>
							<td width="1" align="right" background="images2007/dot_pink.gif"><img src="images2007/dot_pink.gif" width="1" height="1"></td>
						</tr>	
    </table></td>
  </tr>
</table>
<!-- +++++++++++++++++ Google Analytics ++++++++++++++++ -->
          <!--#include file="i_googleAnalytics.asp"-->	
<!-- +++++++++++++++ End Google Analytics +++++++++++++++ -->
</body>
</html>
