function trim(s)
{
  return s.replace(/^\s+|\s+$/, '');
}

function Isrequire(elm)
{
	var config_require="require";
	
	if(elm)
	{
		if (elm.validate=='true')
		{
			return true;
		}
		else if((elm.name).indexOf(config_require)==-1){
			return false;
		}
		else{
			return true;
		}
	
	}
}

function validateFormOnSubmit(formIdent)
{
	var Isvalid = true;
	var illegalChars  =/^[^'"\s]+[^'"]*$/;

	var form, elements, i, elm; 
	var objectValue;
	form = document.getElementById 
    ? document.getElementById(formIdent) 
    : document.forms[formIdent]; 
	
	if (document.getElementsByTagName)
	{
		elements = form.elements;
		for( i=0, elm; elm=elements.item(i++); )
		{
			if ((elm.getAttribute('type') == "text")||(elm.getAttribute('type') == "textarea"))
			{	
						if(Isrequire(elm)){
								if (elm.value.length  == 0 ){
									if(elm.msgwarning){
										alert(elm.msgwarning);
									}else{
										alert('Please enter require field.');
									}
									
									elm.className = 'textfield_style_notice';
									elm.focus();
									Isvalid = false;
									break;
								}else{
									elm.className = 'textfield_style';
								}
						}
		
						objectValue=trim(elm.value);
						if (objectValue!="")
						{
								if(illegalChars.test(objectValue)==false)
								{									
									alert('Invalid Character , please try again.');					
									
									elm.className = 'textfield_style_notice';
									elm.focus();
									Isvalid = false;
									break;
								}else{
									elm.className = 'textfield_style';
								}
								
						}				
			}
			else if(elm.getAttribute('type') == "password")		
			{
						if(Isrequire(elm)){
								if (elm.value.length  == 0 ){
									if(elm.msgwarning){
										alert(elm.msgwarning);
									}else{
										alert('Please enter require field.');
									}	
									
									elm.className = 'textfield_style_notice';
									elm.focus();
									Isvalid = false;
									break;
								}else{
									elm.className = 'textfield_style';
								}
						}
			}
			else if (elm.getAttribute('type') == "select-one")												//Combo type
			{
						if(Isrequire(elm)){
								if (elm.value.length  == 0 ){
										if(elm.msgwarning){
											alert(elm.msgwarning);
										}else{
											alert('Please select require data.');
										}	
										
										elm.className = 'selection_style_notice';
										elm.focus();
										Isvalid = false;
										break;
								}else{
									elm.className = 'selection_style';
								}
						}
			}
			else if (elm.getAttribute('type') == "file")												//Upload File Control type
			{
						if(Isrequire(elm)){
								if (elm.value.length  == 0 ){
								if(elm.msgwarning){
										alert(elm.msgwarning);
									}else{
										alert('Please select require data.');
									}	
								
									elm.className = 'file_style_notice';
									elm.focus();
									Isvalid = false;
									break;
								}else{
									elm.className = 'file_style';
								}
						}
			}
		}
	}
	
	return Isvalid;	
	
}

/*----------------------Add function check length and allow only letters and numbers  17-10-2008 -------------------*/

function checkLength(name,expression,lengths)
{
		//expression ={>,<,=}
		var chk;
		var elm = document.getElementById(name);
		
		switch(expression)
		{
			case ">":
				if(elm.value.length>lengths){chk= true;}else{chk= false;}
				break;    
			case "<":
				if(elm.value.length<lengths){chk= true;}else{chk= false;}
			  break;
			case "=":
				if(elm.value.length==lengths){chk= true;}else{chk= false;}
				break;
			case "<=":
				if(elm.value.length<=lengths){chk= true;}else{chk= false;}
				break;
			default:
				chk= false;
		}
		
		if(chk==false)
		{
			alert('The input is the wrong length.');	
			//elm.value='';
			elm.className = 'textfield_style_notice';
			elm.focus();
		}else{
			elm.className = 'textfield_style';
		}
		return chk;
}

function bannedKey(evt,EngRight,ThaiRight,NumRight)
{
	var allowedEng = EngRight; 
	var allowedThai = ThaiRight; 
	var allowedNum = NumRight; 
	
	var Isbanned=false;

	var k = evt.keyCode;

		if(allowedEng==true)
		{
			if(((k>=65)&&(k<=90))||((k>=97)&&(k<=122))){Isbanned=true;}
		}
	
		if(allowedThai==true)
		{
			if(((k>=161)&&(k<=255))||((k>=3585)&&(k<=3675))){Isbanned=true;}
		}

		if(allowedNum==true)
		{
			if((k>=48)&&(k<=57)){Isbanned=true;}
		}

	return Isbanned;
}

function IsConsistOf(needstr,teststr)
{	
	if((needstr=="")||(teststr=="")){	return false;	}
	else{
		if(needstr.indexOf("|")>-1)		{
				var Arr=needstr.split("|");			
				var i;
				for(i=0;i<=Arr.length; i++)		{
					if(Arr[i]!=""){
						if(teststr.indexOf(Arr[i]) !=-1) {	return true;		}
					}
				}
		}
		else{
				if(teststr.indexOf(needstr) !=-1) {		return true;		}
		}
		
		return false;
	}
}


//function to check valid email address
function isValidEmail(ObjTextEmail){
  validRegExp = /^[^@]+@[^@]+.[a-z]{2,}$/i;
  strEmail = ObjTextEmail.value;

   // search email text for regular exp matches
    if (strEmail.search(validRegExp) == -1) 
   {
			if(ObjTextEmail.msgwarning){
				alert(ObjTextEmail.msgwarning);
			}else{
				alert('A valid e-mail address is required.\nPlease amend and retry');
			}	
			return false;
    } 
    
	return true; 
}


