<%
session("cur_page")="IR Website Manual"
session("page_asp")="ir_website_manual2007"
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=windows-874">
<title>Investor Relations</title>
<LINK href="style2007.css" type="text/css" rel="stylesheet">	
<!--#include file = "i_constant.asp" -->
<!--#include file = "function_asp2007.asp" -->
<!--include file = "i_conpsims.asp" -->
<!--#include file = "i_conirauthen.asp" -->
<!--#include file = "listed_member2007.asp" -->
 <!--#include file = "i_connews.asp" -->	
<!--#include virtual = "i_conir.asp" --> 
<meta name="keywords" content="<%=message_keyword%>">
<script language="javascript" src="/ir/function2007.js"></script>
<script type="text/javascript" language="JavaScript1.2" src="/ir/script2007/stm31.js"></script>
</head>

<body >
<table width="100%"  border="0" cellspacing="0" cellpadding="0">
  <tr>
		<td align="center" valign="top">
			<table width="742" border="0" cellpadding="0" cellspacing="0" bgcolor="#ECECEC">
				<tr align="left" valign="top">
					<td width="1" background="images2007/dot_pink.gif"><img src="images2007/dot_pink.gif" width="1" height="1"></td>
					<td>
							<table width="100%"  border="0" cellspacing="0" cellpadding="0">
									<!--++++++++++++++++++++++++include top++++++++++++++++++++++-->
								
													<!--#include file = "i_top2007.asp" -->
										
									<!--+++++++++++++++++++++++include menu+++++++++++++++++++++-->																									  
								  
												<!--#include file = "i_menu2007.asp" -->									
								
								<!--++++++++++++++++++++++++Content+++++++++++++++++++++++++++++-->
					<tr>
            <td height="1" align="left" valign="top" background="images2007/dot_pink.gif"><img src="images2007/dot_pink.gif" width="1" height="1"></td>
          </tr>
          <tr>
            <td align="center" valign="top"><img src="images2007/head_manual.gif" width="740" height="80"></td>
          </tr>
          <tr>
            <td align="left" valign="top"><table width="300" border="0" cellspacing="0" cellpadding="0">
              <tr align="left" valign="top">
                <td width="14"><img src="images2007/bar_1left.gif" width="14" height="21"></td>
                <td width="130" valign="middle" background="images2007/bar_1bg1.gif"><strong>IR Website Manual </strong></td>
                <td width="32"><img src="images2007/bar_1creb.gif" width="32" height="21"></td>
                <td background="images2007/bar_1bg2.gif">&nbsp;</td>
                <td width="4" align="right"><img src="images2007/bar_1right.gif" width="4" height="21"></td>
              </tr>
            </table>              </td>
          </tr>
          <tr>
            <td align="left" valign="top" bgcolor="#FFFFFF">&nbsp;</td>
          </tr>		

        <tr bgcolor="#FFFFFF">			      
						<td align="center">
						    <font class="no_information">Under  Construction</font>
						  </td>					      
			</tr>	

           <tr>
            <td align="left" valign="top" bgcolor="#FFFFFF">&nbsp;</td>
          </tr>				
												
										
								<!--+++++++++++++++++++++++++include footer++++++++++++++++++++++-->
									
												<!--#include file = "i_footer2007.asp" -->
												
								</table>
							</td>
							<td width="1" align="right" background="images2007/dot_pink.gif"><img src="images2007/dot_pink.gif" width="1" height="1"></td>
						</tr>	
    </table></td>
  </tr>
</table>
<!-- +++++++++++++++++ Google Analytics ++++++++++++++++ -->
          <!--#include file="i_googleAnalytics.asp"-->	
<!-- +++++++++++++++ End Google Analytics +++++++++++++++ -->
</body>
</html>
