<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<%
session("cur_page")="IR Public Relations"
session("page_asp")="public_relations2007"

if session("lang")="" or request("lang")="T"  then  
	session("lang")="T"
elseif request("lang")="E" then
	session("lang")="E"
end if


pagesize=15

%>

 <!--#include file="constpage2007.asp"-->
 
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=windows-874">
<title>Investor Relations</title>
<LINK href="style2007.css" type="text/css" rel="stylesheet">	
<!--#include file = "i_constant.asp" -->
<!--#include file = "function_asp2007.asp" -->
<!--#include file = "listed_member2007.asp" -->
<!--#include virtual = "i_conir.asp" --> 
<meta name="keywords" content="<%=message_keyword%>">
<script language="javascript" src="/ir/function2007.js"></script>
<script type="text/javascript" language="JavaScript1.2" src="/ir/script2007/stm31.js"></script>
</head>

<body >
<table width="100%"  border="0" cellspacing="0" cellpadding="0">
  <tr>
		<td align="center" valign="top">
			<table width="742" border="0" cellpadding="0" cellspacing="0" bgcolor="#ECECEC">
				<tr align="left" valign="top">
					<td width="1" background="images2007/dot_pink.gif"><img src="images2007/dot_pink.gif" width="1" height="1"></td>
					<td>
							<table width="100%"  border="0" cellspacing="0" cellpadding="0">
									<!--++++++++++++++++++++++++include top++++++++++++++++++++++-->
								
													<!--#include file = "i_top2007.asp" -->
										
									<!--+++++++++++++++++++++++include menu+++++++++++++++++++++-->																									  
								  
												<!--#include file = "i_menu2007.asp" -->									
								
								<!--++++++++++++++++++++++++Content+++++++++++++++++++++++++++++-->
								   <FORM name="frm1" METHOD="POST" ACTION="public_relations2007.asp">
									 <tr>
										<td height="1" align="left" valign="top" background="images2007/dot_pink.gif"><img src="images2007/dot_pink.gif" width="1" height="1"></td>
									  </tr>
									  <tr>
										<td align="center" valign="top"><img src="images2007/head_news.gif" width="740" height="80"></td>
									  </tr>
									  <tr>
										<td align="left" valign="top"><table width="300" border="0" cellspacing="0" cellpadding="0">
										  <tr align="left" valign="top">
											<td width="14"><img src="images2007/bar_1left.gif" width="14" height="21"></td>
											<td width="130" valign="middle" background="images2007/bar_1bg1.gif"><strong>Public Relations </strong></td>
											<td width="32"><img src="images2007/bar_1creb.gif" width="32" height="21"></td>
											<td background="images2007/bar_1bg2.gif">&nbsp;</td>
											<td width="4" align="right"><img src="images2007/bar_1right.gif" width="4" height="21"></td>
										  </tr>
										</table>              </td>	
										</tr>
									  <tr>
										<td align="left" valign="top" bgcolor="#FFFFFF">
											<table width="100%" border="0" cellpadding="3" cellspacing="1" bgcolor="#CACACA">	
														<!--#include file="select_page2007.asp"-->
												<%
												if trim( session("listed_member") ) <>"" then
														dim totalrec
														dim strtmpsql
												        totalrec=0
														strtmpsql=""
														if trim( session("lang"))="T" or trim(session("lang"))="" then
														    strtmpsql= "   and title_th<>'' "
														else
														     strtmpsql= "  and title<>'' "
														end if
														
													 
														strsql="SELECT count(*) as rec_count FROM public_relation where display_ir='Y' and share in ("& session("listed_member")  &") " & strtmpsql
														
														
														set rs = conir.execute(strsql)
														if not rs.eof and not rs.bof then
															totalrec=rs("rec_count")
														end if
														rs.close
														
														session("num_count")=totalrec
														
												%>											  
											   <!--#include file="calculate_page2007.asp"-->
											   <%
								
												       strsql = "SELECT * FROM public_relation where display_ir='Y' and share in ("&  session("listed_member")  &") " & strtmpsql
													   strsql=strsql & " order by last_update desc  limit " & ((session("pageno")-1)*session("pagesize")) & "," & session("pagesize") 				
														set rs = conir.execute(strsql)
														if not rs.eof and not  rs.bof then 
														i=1														
														dim new_date
														
														do while not rs.eof
															
															if i mod 2 <> 0 then
														%>
															<tr bgcolor="#FFFFFF">
															<%else%>
															<tr bgcolor="#ECECEC">
															<%end if%>
															
															<td width="70" align="left" valign="top"><span class="style16">
															<%														
															
															arr=split( formatdateAm_Pm(cdate(rs("date_event")))," ")								
															if new_date ="" or new_date <> arr(0) then
																	new_date= arr(0)
																	response.write cdate(new_Date)
															end if		
															%>
															</span>
															</td>								
															<td align="left" valign="top" class="style18"><%=rs("share")%> </td>
															<td align="left" valign="top">
																	<a href="/ir/read_detail/read_detail.asp?title=Public Relations&id=<%=rs("id")%>&share=<%=replace(rs("share"),"&","_")%>" target="_blank">
																	<%
																	  if trim( session("lang"))="T" or trim(session("lang"))="" then
																			 response.write rs("title_th")
																		else																		 
																			response.write rs("title")																		
																		end if
																	%>
																	</a>
																</td>								
															</tr>		
															<%																			
																	rs.movenext
																	i=i+1
																	loop
																	else
																			%>
																		<tr bgcolor="#FFFFFF" >
																		   <td align="center" colspan="4"  valign="middle">
																			    <font class="no_information">
																				<%
																				if  session("lang")="T"  then 
																				     response.write Message_Info_T
																				else																				
																				      response.write Message_Info_E
																				end if
																				%></font>
																			</td>
																		</tr>
																		<%
																	end if
																	rs.close
																	conir.close
																	
																	set rs=nothing
																	set conir=nothing
															else%>
																<tr bgcolor="#FFFFFF" >
																		   <td align="center" colspan="4"  valign="middle">
																			     <font class="no_information">
																				<%
																				if  session("lang")="T" then 
																				     response.write Message_Info_T
																				else																				
																				      response.write Message_Info_E
																				end if
																				%></font>
																			</td>
																		</tr>
												<%	end if
																%>	
															</table>
															</td>
															</tr>
															<tr>
																	<td align="left" valign="top" bgcolor="#FFFFFF" >&nbsp;</td>
															</tr>
															<!--+++++++++++++++++++++++++ Page Button+++++++++++++++++++++++--->
																
																		<tr>
																			<td align="center" valign="top" bgcolor="#FFFFFF"><hr class="style_hr" width="100%"></td>
																	  </tr>
																	  <tr>
																			<td align="right" valign="top" bgcolor="#FFFFFF">
																				   <!--#include file = "page_button2007.asp" -->
																				</td>
																		</tr>	
																		  
																		<tr>
																				<td height="1" align="left" valign="top" background="images2007/dot_gray.gif"><img src="images2007/dot_gray.gif" width="1" height="1"></td>
																		  </tr>
																		  <tr>
																				<td align="left" valign="top">&nbsp;</td>
																		</tr>											   
																		</Form>		
																		
																<!--+++++++++++++++++++++++++include footer++++++++++++++++++++++-->
																	
																				<!--#include file = "i_footer2007.asp" -->
																				
												</table>
							</td>
							<td width="1" align="right" background="images2007/dot_pink.gif"><img src="images2007/dot_pink.gif" width="1" height="1"></td>
						</tr>	
    </table></td>
  </tr>
</table>
<!-- +++++++++++++++++ Google Analytics ++++++++++++++++ -->
          <!--#include file="i_googleAnalytics.asp"-->	
<!-- +++++++++++++++ End Google Analytics +++++++++++++++ -->
</body>
</html>
